-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Generation Time: Sep 16, 2021 at 11:29 AM
-- Server version: 10.5.10-MariaDB-1:10.5.10+maria~focal
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tool`
--

-- --------------------------------------------------------

--
-- Table structure for table `a1crud_class`
--

CREATE TABLE `a1crud_class` (
  `id` int(11) NOT NULL,
  `idproject` int(11) NOT NULL,
  `classname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `properties` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `lengths` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `a1crud_class`
--

INSERT INTO `a1crud_class` (`id`, `idproject`, `classname`, `properties`, `attributes`, `lengths`) VALUES
(1, 1, 'Banner', 'id,Image,Position', 'INT,VARCHAR,INT', '0,200,0'),
(2, 1, 'AboutMealPlan', 'id,Title,SubTitle,Content', 'INT,VARCHAR,VARCHAR,TEXT', '0,100,150,0'),
(3, 1, 'Footer', 'id,Content1,Content2,Content3,AndroidLink,IosLink', 'INT,TEXT,TEXT,TEXT,VARCHAR,VARCHAR', '0,0,0,0,100,100'),
(4, 1, 'HeaderInfo', 'id,Phone,Mail', 'INT,VARCHAR,VARCHAR', '0,15,30'),
(5, 1, 'MenuHeader', 'id,Name,Position,Link,IdParent', 'INT,VARCHAR,INT,VARCHAR,INT', '0,20,0,100,0'),
(6, 1, 'ShopMenu', 'id,Title,Thumbnail,Description,PriceOrigin,PriceCurrent,Star,Duration,Images,Video', 'INT,VARCHAR,VARCHAR,VARCHAR,FLOAT,FLOAT,FLOAT,INT,VARCHAR,VARCHAR', '0,100,150,200,0,0,0,0,150,150'),
(7, 1, 'ShopCombo', 'id,Name,Price', 'INT,VARCHAR,FLOAT', '0,100,0'),
(8, 1, 'ShopComboDetail', 'id,IdShopCombo,IdShopMenu', 'INT,INT,INT', '0,0,0'),
(9, 1, 'ShopComment', 'id,IdShopMenu,IdUser,IdCommentStatus,Content,CreatedAt', 'INT,INT,INT,INT,TEXT,DATETIME', '0,0,0,0,0,0'),
(10, 1, 'ShopPDF', 'id,IdShopMenu,Link,CreatedAt', 'INT,INT,VARCHAR,DATETIME', '0,0,100,0'),
(11, 1, 'MealPlanType', 'id,Name', 'INT,VARCHAR', '0,50'),
(12, 1, 'Package', 'id,IdMealPlanType,Name,NumberDay,Price,CreatedAt,Image', 'INT,INT,VARCHAR,INT,FLOAT,DATETIME,VARCHAR', '0,0,100,0,0,0,200'),
(13, 1, 'IndexUser', 'id,IdUser,Height,Weight,Target,CreatedAt,UpdatedAt,IndexHeight,IndexWeight', 'INT,INT,FLOAT,FLOAT,FLOAT,DATETIME,DATETIME,FLOAT,FLOAT', '0,0,0,0,0,0,0,0,0'),
(14, 1, 'Allergic', 'id,IdMaterial,IdIndexUser', 'INT,INT,INT', '0,0,0'),
(15, 1, 'BlogCategories', 'id,Name', 'INT,VARCHAR', '0,100'),
(16, 1, 'Blog', 'id,IdBlogCategories,Title,Thumbnail,Description,Content,NumberView,CreatedAt,UpdatedAt', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,TEXT,INT,DATETIME,DATETIME', '0,0,100,150,150,0,0,0,0'),
(17, 1, 'SearchManager', 'id,Keyword,SearchAt', 'INT,VARCHAR,DATETIME', '0,100,0'),
(18, 1, 'CommunityCategories', 'id,Name', 'INT,VARCHAR', '0,50'),
(19, 1, 'Community', 'id,IdCommunityCategories,Title,Thumbnail,Description,Content,Image,NumberView,CreatedAt', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,TEXT,VARCHAR,INT,DATETIME', '0,0,100,150,150,0,150,0,0'),
(20, 1, 'ContactInfo', 'id,Address,Phone,Mail,Working,Facebook,Instagram,Youtube,Twitter,Map', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,TEXT', '0,150,15,30,200,100,100,100,100,0'),
(21, 1, 'ContactStatus', 'id,Name', 'INT,VARCHAR', '0,20'),
(22, 1, 'ContactUs', 'id,IdStatus,Name,Email,Message', 'INT,INT,VARCHAR,VARCHAR,VARCHAR', '0,0,100,30,500'),
(23, 1, 'RecipeCategories', 'id,Name', 'INT,VARCHAR', '0,50'),
(24, 1, 'CookingDifficulty', 'id,Name', 'INT,VARCHAR', '0,50'),
(25, 1, 'Recipe', 'id,IdCookingDifficulty,IdRecipeCategories,Title,NumberView,TotalTime,PrepareTime,Thumbnail,ImageBanner,ImageEnergy,PdfLink,CreatedAt,UpdatedAt,Description,IdDiet', 'INT,INT,INT,VARCHAR,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,DATETIME,TEXT,INT', '0,0,0,100,0,20,20,0,150,150,150,0,0,0,0'),
(26, 1, 'Material', 'id,Name,Image,Weight,Calo', 'INT,VARCHAR,VARCHAR,FLOAT,FLOAT', '0,100,150,0,0'),
(27, 1, 'Ingredient', 'id,IdRecipe,IdMaterial,Description,Weight,Postion', 'INT,INT,INT,VARCHAR,FLOAT,INT', '0,0,0,150,0,0'),
(28, 1, 'Instruction', 'id,IdRecipe,image,DesciptionStep,Postion', 'INT,INT,VARCHAR,TEXT,INT', '0,0,0,0,0'),
(29, 1, 'UsersStatus', 'id,Name', 'INT,VARCHAR', '0,50'),
(30, 1, 'RoleUser', 'id,Name', 'INT,VARCHAR', '0,50'),
(31, 1, 'Users', 'id,IdUserStatus,Fullname,Email,Password,Avatar,IdRole,CreatedAt,UpdatedAt,IdAccount', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,INT,DATETIME,DATETIME,INT', '0,0,30,30,50,150,0,0,0,0'),
(32, 1, 'UsersDashboard', 'id,IdUser,IdPackage,Weight,CreatedAt,UpdatedAt', 'INT,INT,INT,FLOAT,DATETIME,DATETIME', '0,0,0,0,0,0'),
(33, 1, 'Promotion', 'id,Name,PromotionCode,Percent,MoneyDiscount,StartDate,EndDate', 'INT,VARCHAR,VARCHAR,FLOAT,FLOAT,DATE,DATE', '0,100,30,0,0,0,0'),
(34, 1, 'OrderStatus', 'id,Name', 'INT,VARCHAR', '0,50'),
(35, 1, 'PaymentStatus', 'id,Name', 'INT,VARCHAR', '0,50'),
(36, 1, 'PaymentType', 'id,Name', 'INT,VARCHAR', '0,50'),
(37, 1, 'City', 'id,Name', 'INT,VARCHAR', '0,30'),
(38, 1, 'District', 'id,IdCity,Name', 'INT,INT,VARCHAR', '0,0,70'),
(39, 1, 'ProductType', 'id,Name', 'INT,VARCHAR', '0,50'),
(40, 1, 'OrderMenu', 'id,IdProductType,IdUser,IdOrderStatus,IdCity,IdDistrict,IdPaymentStatus,IdPaymentType,TotalPrice,PromotionCode,Name,Email,Phone,Address,Note,CreatedAt,UpdatedAt', 'INT,INT,INT,INT,INT,INT,INT,INT,FLOAT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,DATETIME', '0,0,0,0,0,0,0,0,0,50,30,30,15,150,100,0,0'),
(41, 1, 'OrderDetail', 'id,IdOrderMenu,IdShopMenu,IdPackage,Amount,CreatedAt,UpdatedAt', 'INT,INT,INT,INT,INT,DATETIME,DATETIME', '0,0,0,0,0,0,0'),
(42, 1, 'About', 'id,Banner,Title1,Description1,Image1,Title2,Description2,Image2,Title3,Description3,Image3,Belive1,Belive2,Belive3', 'INT,VARCHAR,VARCHAR,TEXT,VARCHAR,VARCHAR,TEXT,VARCHAR,VARCHAR,TEXT,VARCHAR,TEXT,TEXT,TEXT', '0,150,100,0,150,100,0,150,100,0,150,0,0,0'),
(43, 1, 'CommentStatus', 'id,Name', 'INT,VARCHAR', '0,50'),
(44, 1, 'RoleStaff', 'id,Name', 'INT,VARCHAR', '0,50'),
(45, 1, 'TimeCare', 'id,Name', 'INT,VARCHAR', '0,50'),
(46, 1, 'StatusCare', 'id,Name', 'INT,VARCHAR', '0,50'),
(47, 1, 'CustomerCare', 'id,IdUser,IdOrderMenu,IdTimeCare,IdStatusCare,CreateAt,SendMail,Call,Message,JoinVip,Result,LinkShare,Address', 'INT,INT,INT,INT,INT,DATETIME,VARCHAR,VARCHAR,TEXT,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '0,0,0,0,0,0,200,200,0,50,200,150,100'),
(48, 1, 'Diet', 'id,Name,Image', 'INT,VARCHAR,VARCHAR', '0,50,200'),
(49, 1, 'Level', 'id,Name', 'INT,VARCHAR', '0,50'),
(50, 1, 'Workout', 'id,IdLevel,Title,SubTitle,Desciption,Image,Calo,TotalTime', 'INT,INT,VARCHAR,VARCHAR,TEXT,VARCHAR,FLOAT,VARCHAR', '0,0,100,300,0,200,0,10'),
(51, 1, 'WorkoutStatus', 'id,Name', 'INT,VARCHAR', '0,50'),
(52, 1, 'UserWorkoutStatus', 'id,IdUser,IdWorkoutStatus', 'INT,INT,INT', '0,0,0'),
(53, 1, 'WorkoutDetail', 'id,IdWorkout,Image,Position,Times', 'INT,INT,VARCHAR,INT,INT', '0,0,200,0,0'),
(54, 1, 'Note', 'id,IdUser,Title,Description,CreatedAt,UpdatedAt', 'INT,INT,VARCHAR,TEXT,DATETIME,DATETIME', '0,0,100,0,0,0'),
(55, 2, 'Lop', 'id,Name', 'INT,VARCHAR', '0,110'),
(56, 3, 'Lop', 'id,Name,Born,Detail,Address', 'INT,VARCHAR,DATE,TEXT,VARCHAR', '0,50,0,0,200'),
(57, 3, 'SinhVien', 'id,Idlop,Fullname', 'INT,INT,VARCHAR', '0,0,50'),
(58, 4, 'Setting', 'id,ApiURL,Version,AndroidLink,IOSLink', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '0,250,10,200,200'),
(59, 4, 'Title', 'id,Name,MinLevel,MaxLevel', 'INT,VARCHAR,INT,INT', '0,150,10,10'),
(60, 4, 'Level', 'id,Name,MinExp,MaxExp', 'INT,VARCHAR,INT,INT', '0,150,10,10'),
(61, 4, 'User', 'id,IdLevel,IdTitle,Fullname,AuthKey,Avatar,Language,Gold,Diamond,Exp,Token,LoginAt,CreatedAt,UpdatedAt', 'INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,INT,INT,INT,INT,DATE,DATE,DATE', '0,10,10,250,250,250,250,10,10,20,10,0,0,0'),
(62, 4, 'Gift', 'id,Name,Position,IdCharacter,GoldValue,DiamonValue', 'INT,VARCHAR,VARCHAR,INT,INT,INT', '0,250,100,10,10,10'),
(63, 4, 'Login7Day', 'id,IdGift,Name,Position,Requirment', 'INT,INT,VARCHAR,VARCHAR,VARCHAR', '0,10,250,100,100'),
(64, 4, 'Quest', 'id,IdGift,Name,Description,Position,Level,NumberWin,NumberPlay', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,INT,INT,INT', '0,10,250,250,100,10,10,10'),
(66, 4, 'GiftHistory', 'id,IdUser,IdLogin7Day,IdQuest,CreatedAt', 'INT,INT,INT,INT,DATE', '0,10,10,10,0'),
(67, 4, 'Class', 'id,Name,Description', 'INT,VARCHAR,VARCHAR', '0,250,250'),
(68, 4, 'Sprite', 'id,Name,Url', 'INT,VARCHAR,VARCHAR', '0,250,250'),
(69, 4, 'Character', 'id,IdClass,IdSprite,Name,Avatar,BodyImage,Background,UnlockGoldOrigin,UnlockDiamonOrigin,UnlockGold,UnlockDiamon,BaseHP,BaseAttack,BaseDefence,BaseCritical,BaseMovement,Price', 'INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,FLOAT,FLOAT,FLOAT,FLOAT,FLOAT,INT', '0,10,10,250,250,250,250,250,250,250,250,10,10,10,10,10,10'),
(70, 4, 'TypeSkill', 'id,Name', 'INT,VARCHAR', '0,250'),
(71, 4, 'Skill', 'id,IdTypeSkill,IdCharacter,IdSprite,Name,Image,Description,CountDown,HP,Attack,Defence,Critical,Movement,Range,Durration', 'INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,INT,INT,INT,INT,INT,INT,INT,INT', '0,10,10,10,250,250,250,10,10,10,10,10,10,10,10'),
(72, 4, 'GamePlay', 'id,IdUserAttack,IdUserDefence,IdUserWin,CreatedAt', 'INT,INT,INT,INT,DATE', '0,10,10,10,0'),
(73, 4, 'TypeStore', 'id,Name', 'INT,VARCHAR', '0,250'),
(74, 4, 'TypePayment', 'id,Name', 'INT,VARCHAR', '0,250'),
(75, 4, 'Store', 'id,IdGift,IdTypeStore,IdTypePayment,Name,PriceOrigin,Price,Image,FirstPurchase', 'INT,INT,INT,INT,VARCHAR,INT,INT,VARCHAR,FLOAT', '0,10,10,10,250,10,10,250,10'),
(76, 4, 'PurchaseHistory', 'id,IdUser,IdStore,CreatedAt', 'INT,INT,INT,DATE', '0,10,10,0'),
(77, 4, 'StatusInbox', 'id,Name', 'INT,VARCHAR', '0,250'),
(78, 4, 'Inbox', 'id,IdUser,IdGift,IdStatusInbox,Subject,Body,CreatedAt', 'INT,INT,INT,INT,VARCHAR,VARCHAR,DATE', '0,10,10,10,250,250,0'),
(79, 1, 'UploadVideo', 'id,Name,Url,CreatedAt', 'INT,VARCHAR,VARCHAR,DATE', '0,50,200,0'),
(80, 1, 'ShopMenuVideo', 'id,IdShopMenu,IdUploadVideo,Position,CreatedAt', 'INT,INT,INT,INT,DATE', '0,0,0,0,0'),
(84, 5, 'Teacher', 'id,Fullname,Email,Phone,Password,Role', 'INT,VARCHAR,VARCHAR,VARCHAR,TEXT,VARCHAR', '0,100,100,10,0,2'),
(85, 5, 'Learn', 'id,IdSubject,IdTeacher,IdSemester,IdClass', 'INT,INT,INT,INT,INT', '0,0,0,0,0'),
(86, 5, 'Semester', 'id,Name,StartYear,EndYear', 'INT,VARCHAR,INT,INT', '0,100,0,0'),
(87, 5, 'Subject', 'id,Name', 'INT,VARCHAR', '0,100'),
(88, 5, 'Class', 'id,Name', 'INT,VARCHAR', '0,100'),
(89, 5, 'Students', 'id,IdClass,StudentCode,Password,Fullname,Email,Sex,Phone,Address', 'INT,INT,VARCHAR,TEXT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '0,0,50,0,150,100,5,10,100'),
(90, 5, 'Survey', 'id,IdSubject,IdStudent,IdSurveyTemplate,Status,CreatedAt,Name', 'INT,INT,INT,INT,INT,DATETIME,VARCHAR', '0,0,0,0,0,0,100'),
(91, 5, 'SurveyTemplate', 'id,IdMenu,Name,CreatedAt,UpdateAt', 'INT,INT,VARCHAR,DATETIME,DATETIME', '0,0,100,0,0'),
(92, 5, 'Question', 'id,IdSurvey,IdTypeChoose,Name,Position', 'INT,INT,INT,VARCHAR,INT', '0,0,0,100,0'),
(93, 5, 'TypeChoose', 'id,Name', 'INT,VARCHAR', '0,100'),
(94, 5, 'Reply', 'id,IdQuestion,Position,Name,Value', 'INT,INT,INT,VARCHAR,VARCHAR', '0,0,0,100,500'),
(95, 5, 'Evaluate', 'id,IdQuestion,IdStudent,IdSubject,IdReply,Value', 'INT,INT,INT,INT,INT,VARCHAR', '0,0,0,0,0,500'),
(96, 5, 'Header', 'id,TextLeft1,TextLeft2,TextRight,Address,Phone,Email,CreatedAt,UpdateAt', 'INT,TEXT,TEXT,TEXT,TEXT,VARCHAR,VARCHAR,DATETIME,DATETIME', '0,0,0,0,0,15,100,0,0'),
(97, 5, 'MenuPage', 'id,Name,TypeId,Url,Arrange,CreatedAt,UpdateAt', 'INT,INT,INT,TEXT,INT,DATETIME,DATETIME', '0,0,0,0,0,0,0'),
(98, 5, 'About', 'id,Title,Content,CreatedAt,UpdateAt', 'INT,VARCHAR,TEXT,DATETIME,DATETIME', '0,250,0,0,0'),
(99, 5, 'RightBody', 'id,Title,Url,Thumbnail,CreatedAt,UpdateAt', 'INT,VARCHAR,TEXT,TEXT,DATETIME,DATETIME', '0,250,0,0,0,0'),
(100, 5, 'News', 'id,Titile,Summary,Content,Thumbnail,Url,CreatedAt,UpdateAt', 'INT,VARCHAR,VARCHAR,TEXT,TEXT,TEXT,DATETIME,DATETIME', '0,250,250,0,0,0,0,0'),
(101, 5, 'Footer', 'id,Col1,Col2,Col3,CopyRight,CreatedAt,UpdateAt', 'INT,TEXT,TEXT,TEXT,VARCHAR,DATETIME,DATETIME', '0,0,0,0,250,0,0'),
(102, 5, 'Banner', 'id,Title,Url,Arrange,CreatedAt,UpdateAt', 'INT,VARCHAR,TEXT,INT,DATETIME,DATETIME', '0,250,0,0,0,0'),
(178, 7, 'WaitLocation', 'id,Name', 'INT,VARCHAR', '11,100'),
(176, 7, 'Subject', 'id,Name', 'INT,VARCHAR', '11,100'),
(177, 7, 'LearnTime', 'id,IdBranch,TimeStart,TimeEnd,Position', 'INT,INT,VARCHAR,VARCHAR,INT', '11,11,100,100,11'),
(175, 7, 'Lesson', 'id,Name', 'INT,VARCHAR', '11,200'),
(174, 7, 'Branch', 'id,Name,Address', 'INT,VARCHAR,VARCHAR', '11,100,100'),
(267, 8, 'Signal', 'id,IdPerson,IdStatus,Message,Voice,Place,BeginLat,BeginLng,EndLat,EndLng,TotalMoney,StartTime,EndTime,PhoneCustomer,CreatedAt,UpdatedAt,Distance', 'INT,INT,INT,VARCHAR,TEXT,VARCHAR,DOUBLE,DOUBLE,DOUBLE,DOUBLE,DOUBLE,DATETIME,DATETIME,VARCHAR,DATETIME,DATETIME,INT', '0,0,0,255,0,255,0,0,0,0,0,0,0,50,0,0,0'),
(265, 8, 'Status', 'id,Name', 'INT,VARCHAR', '0,150'),
(266, 8, 'Person', 'id,IdPermission,Name,Email,Phone,Age,Gender,Username,Password,AmountOfSeat,CarType,Image,LicensePlate,Address,DeviceToken', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,INT,BOOLEAN,VARCHAR,VARCHAR,INT,VARCHAR,TEXT,VARCHAR,VARCHAR,VARCHAR', '0,0,150,150,50,0,0,150,150,0,150,0,250,250,0'),
(264, 8, 'Permission', 'id,Name', 'INT,VARCHAR', '0,150'),
(163, 6, 'AcademicLevel', 'id,Name', 'INT,VARCHAR', '11,100'),
(164, 6, 'Province', 'id,Name,Type', 'INT,VARCHAR,VARCHAR', '11,100,100'),
(165, 6, 'District', 'id,IdProvince,Name,Type', 'INT,INT,VARCHAR,VARCHAR', '11,11,100,100'),
(166, 6, 'Branch', 'id,IdCompany,IdProvince,IdDistrict,Name,Address,Phone,SortIndex,Note', 'INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,INT,VARCHAR', '11,11,11,11,100,100,20,11,100'),
(167, 6, 'AnnouncementType', 'id,IdCompany,Name', 'INT,INT,VARCHAR', '11,11,100'),
(168, 6, 'ViewInformation', 'id,IdInformation,IdEmployee,Status,IdCompany', 'INT,INT,INT,BOOLEAN,INT', '11,11,11,1,11'),
(169, 6, 'TimeKeepingSetting', 'id,IdCompany,IdEmployee,IsMultiMobile,IsLocationTracking,IsNoNeedTimekeeping,IsNoConstraintTimekeeping,IsAllowingLatelyCheckinOut,IsAllowingEarlyCheckinOut,IsAutoTimekeeping,IsAutoCheckout,IsClockinUsingImage,IsClockoutUsingImage,IsHelpCheckShift', 'INT,INT,INT,BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN', '11,11,11,1,1,1,1,1,1,1,1,1,1,1'),
(161, 6, 'Position', 'id,IdCompany,IdAcademicLevel,Name,SortIndex,Experience,Description', 'INT,INT,INT,VARCHAR,INT,DOUBLE,VARCHAR', '11,11,11,100,11,0,100'),
(162, 6, 'Department', 'id,IdCompany,Name,Description,SortIndex', 'INT,INT,VARCHAR,VARCHAR,INT', '11,11,100,100,11'),
(160, 6, 'Role', 'id,DisplayName', 'INT,VARCHAR', '11,100'),
(158, 6, 'Employee', 'id,IdBranch,IdDepartment,IdPosition,IdCompany,IdRole,IdEmployeeActive,Fullname,Phone,Email,Image,Birthday,Gender,SortIndex,CardIdentity,DateRelease,PlaceRelease,PassportCard,PassportDate,PassportExpiryDate,PassportIssueIn,BankAccountName,BankAccount,BankName,BankAddress,Password', 'INT,INT,INT,INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATE,BOOLEAN,INT,VARCHAR,DATE,VARCHAR,VARCHAR,DATE,DATE,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '11,11,11,11,11,11,11,100,20,50,150,0,1,11,20,0,50,20,0,0,50,100,20,100,100,20'),
(159, 6, 'EmployeeActive', 'id,Name', 'INT,VARCHAR', '11,100'),
(156, 6, 'Major', 'id,Name', 'INT,VARCHAR', '11,100'),
(157, 6, 'WorkType', 'id,Name', 'INT,VARCHAR', '11,100'),
(152, 6, 'SpecializedCertificateType', 'id,Name', 'INT,VARCHAR', '11,100'),
(153, 6, 'SpecializedCertificate', 'id,IdCompany,IdEmployee,IdSpecializedCertificateType,Name,FinishPlace,FinishDate,Note', 'INT,INT,INT,INT,VARCHAR,VARCHAR,DATE,VARCHAR', '11,11,11,11,100,100,0,100'),
(154, 6, 'Qualification', 'id,IdCompany,IdEmployee,IdAcademicLevel,IdMajor,TrainingPlace,FinishDate,Note', 'INT,INT,INT,INT,INT,VARCHAR,DATE,VARCHAR', '11,11,11,11,11,100,0,100'),
(155, 6, 'WorkingTime', 'id,IdCompany,IdEmployee,IdWorkType,LabourStartDate,WorkingDate,ExpiredDate,CheckinLate,CheckoutEarly,Note,QuitReason,QuitDate', 'INT,INT,INT,INT,DATE,DATE,DATE,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATE', '11,11,11,11,0,0,0,20,20,100,100,0'),
(151, 6, 'WorkHistory', 'id,IdCompany,IdEmployee,WorkPlace,Position,WorkDays,FromDate,ToDate', 'INT,INT,INT,VARCHAR,VARCHAR,INT,DATE,DATE', '11,11,11,100,100,11,0,0'),
(147, 6, 'PayType', 'id,Name', 'INT,VARCHAR', '11,100'),
(148, 6, 'Payroll', 'id,IdCompany,IdPayType,Name,Money', 'INT,INT,INT,VARCHAR,VARCHAR', '11,11,11,100,100'),
(149, 6, 'HealthyStatus', 'id,IdCompany,IdEmployee,Height,Weight,Blood,Congenital,HealthyStatus,HealthLastDate', 'INT,INT,INT,DOUBLE,DOUBLE,VARCHAR,VARCHAR,VARCHAR,DATE', '11,11,11,0,0,20,100,100,0'),
(150, 6, 'ContactInfo', 'id,IdCompany,IdEmployee,IdProvince,IdDistrict,Address,Skype,Facebook,EmergencyContact,EmergencyRelate,EmergencyTel,EmergencyPhone,EmergencyAddress', 'INT,INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '11,11,11,11,11,100,100,100,50,50,50,50,100'),
(145, 6, 'SalaryCoefficient', 'id,IdCompany,Name,Money', 'INT,INT,VARCHAR,VARCHAR', '11,11,100,100'),
(146, 6, 'Allowance', 'id,IdCompany,Name,Money,IsPersonalIncomeTax,IsInsurance', 'INT,INT,VARCHAR,VARCHAR,BOOLEAN,BOOLEAN', '11,11,100,100,1,1'),
(142, 6, 'Married', 'id,Name', 'INT,VARCHAR', '11,100'),
(143, 6, 'OtherInfo', 'id,IdEmployee,IdCompany,TaxCode,Union,Ethnic,Religion,IdMarried,Note', 'INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,INT,VARCHAR', '11,11,11,50,50,50,50,11,100'),
(144, 6, 'SalaryAllowance', 'id,IdCompany,IdEmployee,IdPayType,IdPayroll,IdAllowance,IdSalaryCoefficient,Salary', 'INT,INT,INT,INT,INT,INT,INT,VARCHAR', '11,11,11,11,11,11,11,100'),
(136, 6, 'BusinessType', 'id,Name', 'INT,VARCHAR', '11,100'),
(137, 6, 'Company', 'id,IdProvince,IdDistrict,IdBusinessType,CompanyName,Image,Address,Phone,UserName,Password,Website,Email,Scale,CharterCapital,Keyword,BankName,BankAccount,TaxCode,Note', 'INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '11,11,11,11,100,150,100,20,100,100,100,100,11,100,100,100,100,100,100'),
(138, 6, 'Insurance', 'id,IdCompany,IdEmployee,Gender,Birthday,InsuranceId,InsuranceCardId,IsSubmitInsuranceBook,CardId', 'INT,INT,INT,BOOLEAN,DATE,VARCHAR,VARCHAR,BOOLEAN,VARCHAR', '11,11,11,1,0,100,100,1,100'),
(139, 6, 'Contract', 'id,IdCompany,IdEmployee,IdContractType,IdPersonalIncomeTaxType,IdWorkType,IdPayType,ContractNumber,Signer,SignDate,ExpirationDate,TrainWorkTime,Salary,Manager,WorkLocation,WorkingDate,Settlement,Attachment,Note', 'INT,INT,INT,INT,INT,INT,INT,VARCHAR,VARCHAR,DATE,DATE,INT,VARCHAR,VARCHAR,VARCHAR,DATE,VARCHAR,VARCHAR,VARCHAR', '11,11,11,11,11,11,11,100,50,0,0,11,100,100,100,0,100,150,100'),
(140, 6, 'ContractType', 'id,Name', 'INT,VARCHAR', '11,100'),
(141, 6, 'PersonalIncomeTaxType', 'id,Name', 'INT,VARCHAR', '11,100'),
(134, 6, 'AdvancedTimekeepingSetup', 'id,IdCompany,IdShift,StartCheckinHour,StartCheckinMinute,EndCheckinHour,EndCheckinMinute,StartCheckoutHour,StartCheckoutMinute,EndCheckoutHour,EndCheckoutMinute,MinimumWorkingHour', 'INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,DOUBLE', '11,11,11,11,11,11,11,11,11,11,11,0'),
(133, 6, 'ShiftAssignment', 'id,IdCompany,IdEmployee,IdShift,IdBranch,IdDepartment,IdPosition,ActingTime,SortIndex', 'INT,INT,INT,INT,INT,INT,INT,VARCHAR,INT', '11,11,11,11,11,11,11,50,11'),
(135, 6, 'Shift', 'id,IdCompany,Name,Coefficient,StartHour,StartMinute,EndHour,EndMinute,RestStartHour,RestStartMinute,RestEndHour,RestEndMinute,CheckinStartHour,CheckinStartMinute,CheckinEndHour,CheckinEndMinute,CheckoutStartHour,CheckoutStartMinute,CheckoutEndHour,CheckoutEndMinute,IsOvertimeShift', 'INT,INT,VARCHAR,DOUBLE,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,BOOLEAN', '11,11,100,0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,1'),
(131, 6, 'ModelUserFace', 'id,IdCompany,IdEmployee,ModelTrain,CreatedAt', 'INT,INT,INT,VARCHAR,DATETIME', '11,11,11,150,0'),
(132, 6, 'CheckinLateAndCheckoutSoonSetup', 'id,IdCompany,IdShift,LatelyCheckin,MaxLateCheckinMinute,EarlyCheckout,MinSoonCheckinMinute', 'INT,INT,INT,INT,INT,INT,INT', '11,11,11,11,11,11,11'),
(129, 6, 'TimekeepingStatus', 'id,Name', 'INT,VARCHAR', '11,100'),
(130, 6, 'UserFace', 'id,IdCompany,IdEmployee,Avatar,CreatedAt', 'INT,INT,INT,VARCHAR,DATETIME', '11,11,11,150,0'),
(127, 6, 'StatusApprove', 'id,Name', 'INT,VARCHAR', '11,100'),
(128, 6, 'Timekeeping', 'id,IdCompany,IdEmployee,IdShift,CheckinLat,CheckinLng,CheckinTime,CheckinFace,CheckinFaceStatus,CheckoutLat,CheckoutLng,CheckoutFace,CheckoutFaceStatus,CheckoutTime,IdTimekeepingStatus,CreatedAt', 'INT,INT,INT,INT,DOUBLE,DOUBLE,DATETIME,VARCHAR,VARCHAR,DOUBLE,DOUBLE,VARCHAR,VARCHAR,DATETIME,INT,DATETIME', '11,11,11,11,0,0,0,350,350,0,0,350,350,0,11,0'),
(122, 6, 'BonusType', 'id,Name', 'INT,VARCHAR', '11,100'),
(123, 6, 'Bonus', 'id,IdBonusType,Title,Amount,IdEmployee,AllowanceDay,ReasonContent,IdStatusApprove,CreatedAt', 'INT,INT,VARCHAR,VARCHAR,INT,DATE,VARCHAR,INT,DATETIME', '11,11,100,100,11,0,100,11,0'),
(125, 6, 'AdvanceSalaryType', 'id,Name', 'INT,VARCHAR', '11,100'),
(126, 6, 'AdvanceSalary', 'id,IdCompany,IdEmployee,Money,Title,AllowanceDay,Reason,IdStatusApprove,IdAdvanceSalaryType,CreatedAt,IsAdvanceSalary', 'INT,INT,INT,VARCHAR,VARCHAR,DATE,VARCHAR,INT,INT,DATETIME,BOOLEAN', '11,11,11,100,100,0,100,11,11,0,1'),
(124, 6, 'OvertimeType', 'id,IdCompany,Name,Keyword,Rate,LimitOvertimeHoursMonth,LimitOvertimeHoursYear,Description', 'INT,INT,VARCHAR,VARCHAR,INT,INT,INT,VARCHAR', '11,11,100,100,11,11,11,100'),
(121, 6, 'Overtime', 'id,IdCompany,IdEmployee,IdOvertimeType,FromTime,EndTime,IdStatusApprove,Reason,Content,OvertimeHour,OvertimeBranch,CreatedAt,IsOvertime', 'INT,INT,INT,INT,DATETIME,DATETIME,INT,VARCHAR,VARCHAR,FLOAT,VARCHAR,DATETIME,BOOLEAN', '11,11,11,11,0,0,11,100,100,0,100,0,1'),
(120, 6, 'BusinessTrip', 'id,IdCompany,IdEmployee,IdStatusApprove,FromTime,EndTime,Place,TrackingPlace,IdShift,Reason,CreatedAt,IsBusinessTrip', 'INT,INT,INT,INT,DATETIME,DATETIME,VARCHAR,VARCHAR,INT,VARCHAR,DATETIME,BOOLEAN', '11,11,11,111,0,0,100,100,11,100,0,1'),
(119, 6, 'CheckinLate', 'id,IdCompany,IdEmployee,IdShift,CheckinDate,CheckinTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckinLate', 'INT,INT,INT,INT,DATE,VARCHAR,INT,VARCHAR,VARCHAR,VARCHAR,DATETIME,BOOLEAN', '11,11,11,11,0,20,11,100,100,100,0,1'),
(117, 6, 'TypeLeave', 'id,Name', 'INT,VARCHAR', '11,100'),
(118, 6, 'CheckoutSoon', 'id,IdCompany,IdEmployee,IdShift,CheckoutDate,CheckoutTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckoutSoon', 'INT,INT,INT,INT,DATE,VARCHAR,INT,VARCHAR,VARCHAR,VARCHAR,DATETIME,BOOLEAN', '11,11,11,11,0,20,11,100,100,100,0,1'),
(115, 6, 'FormWork', 'id,Name', 'INT,VARCHAR', '11,100'),
(116, 6, 'OnLeaveType', 'id,IdFormWork,Name,Keyword,DayPerYear,IdPayType,IsDeliverEnough,Year,ExpirationDate,Description,IsAccumulatingByMonth,IdCompany', 'INT,INT,VARCHAR,VARCHAR,INT,INT,BOOLEAN,INT,DATE,VARCHAR,BOOLEAN,INT', '11,11,100,100,11,11,1,11,0,100,1,11'),
(179, 7, 'StatusRegisterLearn', 'id,Name', 'INT,VARCHAR', '11,100'),
(180, 7, 'StatusApproveRegister', 'id,Name', 'INT,VARCHAR', '11,100'),
(181, 7, 'LocationExam', 'id,Name', 'INT,VARCHAR', '11,100'),
(182, 7, 'DateExam', 'id,IdLocationExam,StartDate', 'INT,INT,DATE', '11,11,0'),
(183, 7, 'TypePackage', 'id,Name', 'INT,VARCHAR', '11,100'),
(184, 7, 'Package', 'id,IdTypePackage,Name,PricePackage,NumberPeriod', 'INT,INT,VARCHAR,DOUBLE,INT', '11,11,100,0,11'),
(185, 7, 'StatusPackagePayment', 'id,Name', 'INT,VARCHAR', '11,100'),
(186, 7, 'RegisterPackage', 'id,IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice,CreatedAt,SoTiet,HanDongTien,SoTienThieu,PriceOfPackage', 'INT,INT,INT,INT,INT,DOUBLE,DATETIME,INT,DATE,INT,INT', '11,11,11,11,11,0,0,11,0,11,11'),
(187, 7, 'RegisterLearn', 'id,IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt', 'INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,INT,VARCHAR,DATE,DATE', '11,11,11,11,11,11,11,11,11,11,11,100,0,0'),
(188, 7, 'Review', 'id,IdRegisterLearn,Rate,Content,IdBranch', 'INT,INT,DOUBLE,VARCHAR,INT', '11,11,0,100,11'),
(189, 7, 'TypeSource', 'id,Name', 'INT,VARCHAR', '11,100'),
(190, 7, 'StatusStudent', 'id,Name', 'INT,VARCHAR', '11,100'),
(191, 7, 'Student', 'id,IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy,IdBranch,TokenDevice', 'INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,VARCHAR,DATE,INT,DATETIME,INT,TEXT', '11,11,11,100,100,200,100,10,200,0,100,0,11,0,11,0'),
(192, 7, 'TypeEmployee', 'id,Name', 'INT,VARCHAR', '11,100'),
(193, 7, 'StatusEmployee', 'id,Name', 'INT,VARCHAR', '11,100'),
(194, 7, 'Certification', 'id,Name,DateCertification', 'INT,VARCHAR,DATETIME', '11,100,0'),
(195, 7, 'CertificationEmployee', 'id,IdCertification,IdEmployee', 'INT,INT,INT', '11,11,11'),
(196, 7, 'Employee', 'id,IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch,AccountId', 'INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,INT,INT', '11,11,11,100,100,200,100,10,200,200,0,11,11'),
(197, 7, 'BookCategories', 'id,Name', 'INT,VARCHAR', '11,100'),
(198, 7, 'Book', 'id,IdBookCategories,Name,Image,Author,Price,CreatedAt', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,DOUBLE,DATETIME', '11,11,100,200,100,0,0'),
(199, 7, 'WarehouseBook', 'id,IdBook,Total', 'INT,INT,INT', '11,11,11'),
(200, 7, 'ImportBook', 'id,IdBook,Total,CreatedAt', 'INT,INT,INT,DATETIME', '11,11,11,0'),
(201, 7, 'BorrowBook', 'id,IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt', 'INT,INT,INT,INT,DATETIME,DATETIME,DATETIME', '11,11,11,11,0,0,0'),
(202, 7, 'TypeNoticeBoard', 'id,Name,Image', 'INT,VARCHAR,VARCHAR', '11,100,200'),
(203, 7, 'NoticeBoard', 'id,IdTypeNoticeBoard,Name,Image,Description,Position', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,INT', '11,11,100,500,100,11'),
(204, 7, 'UsefulKnowledgeCategories', 'id,Name,Image', 'INT,VARCHAR,VARCHAR', '11,100,200'),
(205, 7, 'UsefulKnowledge', 'id,IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '11,11,100,200,100,100,100'),
(206, 7, 'VideoCategories', 'id,Name,Image', 'INT,VARCHAR,VARCHAR', '11,100,200'),
(207, 7, 'Video', 'id,IdVideoCategories,Name,Link', 'INT,INT,VARCHAR,VARCHAR', '11,11,100,200'),
(208, 7, 'TypeQuestion', 'id,Name', 'INT,VARCHAR', '11,100'),
(209, 7, 'Topic', 'id,IdTypeQuestion,Name', 'INT,INT,VARCHAR', '11,11,100'),
(210, 7, 'TypeAnswerResult', 'id,Name', 'INT,VARCHAR', '11,100'),
(211, 7, 'Question', 'id,IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain', 'INT,INT,INT,VARCHAR,VARCHAR', '11,11,11,100,100'),
(212, 7, 'QuestionHistory', 'id,IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent', 'INT,INT,INT,INT,INT', '11,11,11,11,11'),
(213, 7, 'HistorySenta', 'id,IdStudent,CreatedAt,IdBranch', 'INT,INT,DATETIME,INT', '11,11,0,11'),
(214, 7, 'CostCategories', 'id,Name', 'INT,VARCHAR', '11,100'),
(215, 7, 'Cost', 'id,IdBranch,IdCostCategories,Money,Note,CreatedAt', 'INT,INT,INT,DOUBLE,VARCHAR,DATE', '11,11,11,0,100,0'),
(216, 7, 'StatusChangeCompanny', 'id,Name', 'INT,VARCHAR', '11,100'),
(217, 7, 'StatusShako', 'id,Name', 'INT,VARCHAR', '11,100'),
(218, 7, 'StatusVehicleRegistration', 'id,Name', 'INT,VARCHAR', '11,100'),
(219, 7, 'CarCategories', 'id,Name', 'INT,VARCHAR', '11,100'),
(220, 7, 'CarManufacturer', 'id,Name,Logo', 'INT,VARCHAR,VARCHAR', '11,100,100'),
(221, 7, 'Car', 'id,IdCarCategories,IdCarManufacturer,NameCar,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost,PriceFullPackage,CylinderCapacity,DetailsCar,IdCarColor,Equipment,Avatar', 'INT,INT,INT,VARCHAR,VARCHAR,DATETIME,VARCHAR,INT,DOUBLE,DOUBLE,INT,INT,DATETIME,INT,INT,INT,INT,VARCHAR,TEXT,VARCHAR,VARCHAR,VARCHAR', '11,11,11,100,50,0,50,11,0,0,11,11,0,11,11,11,11,36,0,36,5000,5000'),
(222, 7, 'AuctionFloor', 'id,NumberCode,Name', 'INT,VARCHAR,VARCHAR', '11,50,100'),
(223, 7, 'SourceCar', 'id,Name', 'INT,VARCHAR', '11,100'),
(224, 7, 'Transport', 'id,Name', 'INT,VARCHAR', '11,100'),
(225, 7, 'Customer', 'id,FullName,Born,Address,Phone', 'INT,VARCHAR,DATETIME,VARCHAR,VARCHAR', '11,100,0,100,10'),
(226, 7, 'StatusPrepareTransferCar', 'id,Name', 'INT,VARCHAR', '11,100'),
(227, 7, 'WarehouseCar', 'id,IdCar,Total,CreatedAt', 'INT,INT,INT,DATETIME', '11,11,11,0'),
(228, 7, 'InputCar', 'id,IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport', 'INT,INT,INT,INT,DATETIME,DATETIME,INT,VARCHAR', '11,11,11,11,0,0,11,100'),
(229, 7, 'OutputCar', 'id,IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note', 'INT,INT,INT,INT,DOUBLE,DOUBLE,INT,INT,DATETIME,INT,VARCHAR,VARCHAR', '11,11,11,11,0,0,11,11,0,11,50,100'),
(230, 7, 'RegistrationShyaken', 'id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', 'INT,VARCHAR,DATETIME,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,DOUBLE', '11,100,0,100,15,50,50,0,0'),
(231, 7, 'RegistrationInsurrance', 'id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', 'INT,VARCHAR,DATETIME,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,DOUBLE', '11,100,0,50,15,50,50,0,0'),
(232, 7, 'RegistrationCar', 'id,FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,INT,VARCHAR,DATETIME,VARCHAR,VARCHAR,VARCHAR', '11,100,100,15,15,11,15,0,10,50,50'),
(233, 7, 'Notification', 'id,IdNotificationType,Title,Content,Author,DateCreate', 'INT,INT,VARCHAR,VARCHAR,VARCHAR,DATETIME', '11,11,50,50,50,0'),
(234, 7, 'NotificationType', 'id,Name', 'INT,VARCHAR', '11,100'),
(235, 7, 'BasicSalary', 'id,MoneyBasicInMonth,MoneyInHour', 'INT,DOUBLE,DOUBLE', '11,0,0'),
(236, 7, 'Subsidize', 'id,Name,Money', 'INT,VARCHAR,DOUBLE', '11,50,0'),
(237, 7, 'EmployeeReward', 'id,IdEmployee,Name,Money,CreatedAt', 'INT,INT,VARCHAR,DOUBLE,DATETIME', '11,11,100,0,0'),
(238, 7, 'EmployeeSubsidize', 'id,IdEmployee,IdSubsidize,StartDate,EndDate', 'INT,INT,INT,DATETIME,DATETIME', '11,11,11,0,0'),
(239, 7, 'EmployeeOvertime', 'id,IdEmployee,NumberHour,CreatedAt', 'INT,INT,VARCHAR,DATETIME', '11,11,10,0'),
(240, 7, 'EmployeeBasicSalary', 'id,IdEmployee,IdBasicSalary,IdTypeEmployee', 'INT,INT,INT,INT', '11,11,11,11'),
(241, 7, 'StatusDayOf', 'id,Name', 'INT,VARCHAR', '11,100'),
(242, 7, 'StatusApproved', 'id,Name', 'INT,VARCHAR', '11,100'),
(243, 7, 'DayOff', 'id,IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt', 'INT,INT,INT,INT,DATETIME,DATETIME,DATETIME', '11,11,11,11,0,0,0'),
(244, 7, 'StatusTakeImage', 'id,Name', 'INT,VARCHAR', '11,100'),
(245, 7, 'StatusPost', 'id,Name', 'INT,VARCHAR', '11,100'),
(246, 7, 'PaymentType', 'id,Name', 'INT,VARCHAR', '11,100'),
(247, 7, 'Payment', 'id,Name', 'INT,VARCHAR', '11,100'),
(248, 7, 'StatusPayment', 'id,Name', 'INT,VARCHAR', '11,100'),
(249, 7, 'StatusTransferCar', 'id,Name', 'INT,VARCHAR', '11,100'),
(250, 7, 'Sex', 'id,Name', 'INT,VARCHAR', '11,20'),
(251, 7, 'StatusPass', 'id,Name', 'INT,VARCHAR', '11,50'),
(252, 7, 'DrivingTest', 'id,IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note,IdLocationExam', 'INT,INT,INT,INT,INT,INT,DATE,VARCHAR,VARCHAR,INT', '11,11,11,11,11,11,0,100,100,11'),
(253, 7, 'ReviewStudent', 'id,IdRegisterLearn,Rate,Content', 'INT,INT,DOUBLE,VARCHAR', '11,11,0,150'),
(254, 7, 'ImageSlide', 'id,Link', 'INT,TEX', '11,0'),
(255, 7, 'CarColor', 'id,Name', 'INT,VARCHAR', '11,36'),
(256, 7, 'CarImages', 'id,IdCar,img', 'INT,INT,VARCHAR', '11,11,500'),
(257, 7, 'CarEquipment', 'id,Name', 'INT,VARCHAR', '11,36'),
(258, 7, 'CarEquipmentDetails', 'id,IdCar,EquipmentId', 'INT,INT,INT', '11,11,11'),
(259, 7, 'Index', 'id,title1,details1,image1,title2,details2,image2,title3,details3,image3', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '11,500,500,500,500,500,500,500,500,500'),
(260, 7, 'Contact', 'id,Address,Phone,SwitchboardSupport,Email,Website', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '11,200,36,200,36,200'),
(261, 7, 'Recruiment', 'id,Title,Image,Details,StartDate,EndDate', 'INT,VARCHAR,VARCHAR,TEXT,DATE,DAT', '11,200,200,0,0,0'),
(262, 7, 'ChatPrivate', 'id,SentId,ReciverId,Message,CreatedAt', 'INT,INT,INT,TEXT,DATE', '0,0,0,0,0'),
(263, 7, 'ChatRoom', 'id,EmployeeId,Message,CreatedAt', 'INT,INT,TEXT,DATE', '0,0,0,0'),
(114, 6, 'OnLeave', 'id,IdCompany,IdEmployee,IdTypeLeave,IdOnLeaveType,IdShift,IdStatusApprove,FromDate,ToDate,StartTime,EndTime,Reason,HandoverTo,Phone,Content,CreatedAt,IsOnLeave', 'INT,INT,INT,INT,INT,INT,INT,DATE,DATE,DATETIME,DATETIME,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,BOOLEAN', '11,11,11,11,11,11,11,0,0,0,0,100,100,20,100,0,1'),
(113, 6, 'SalaryTable', 'id,IdCompany,Name,Keyword,Month,IdSalaryTemplate,IdSalaryDateType,StartDate,EndDate,IsClientInvisible', 'INT,INT,VARCHAR,VARCHAR,INT,BOOLEAN,BOOLEAN,DATE,DATE,BOOLEAN', '11,11,100,100,11,1,1,0,0,1'),
(111, 6, 'SalaryDateType', 'id,Name', 'INT,VARCHAR', '11,100'),
(112, 6, 'OnHoliday', 'id,IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient', 'INT,INT,INT,VARCHAR,DATE,DATE,DOUBLE', '11,11,11,100,0,0,0'),
(110, 6, 'SalaryTemplate', 'id,IdCompany,IdEmployee,IdBranch,IdPayType,Name,Description,Keyword,LocationApplies', 'INT,INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '11,11,11,11,11,100,100,100,100'),
(109, 6, 'Announcement', 'id,IdCompany,IdEmployee,IdBranch,IdAnnouncementType,Title,Document,Content,Status,CreatAt', 'INT,INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,BOOLEAN,DATETIME', '11,11,11,11,11,100,150,100,1,0'),
(107, 6, 'Information', 'id,IdCompany,IdEmployee,IdBranch,IdDepartment,Document,Title,Content,IsInfoPins,ViewedRequest,Status,CreatAt', 'INT,INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,TINYINT,TINYINT,TINYINT,DATETIME', '11,11,11,11,11,150,100,100,1,1,1,0'),
(108, 6, 'Rule', 'id,IdCompany,Title,IsShow,Image,Description,Document,Status', 'INT,INT,VARCHAR,BOOLEAN,VARCHAR,VARCHAR,VARCHAR,BOOLEAN', '11,11,100,1,150,150,150,1'),
(287, 8, 'Setting', 'id,DistanceSignal,DistanceReceiver', 'INT,INT,INT', '0,0,0'),
(269, 8, 'Receiver', 'id,IdPerson,IdStatus,IdSignal,Place,Lat,Lng,CreatedAt,UpdatedAt,Distance', 'INT,INT,INT,INT,VARCHAR,DOUBLE,DOUBLE,DATETIME,DATETIME,INT', '0,0,0,250,0,0,0,0,0,0'),
(270, 8, 'RuleTotalMoney', 'id,StartDistance,EndDistance,UnitPrice,AmountOfSeat', 'INT,DOUBLE,DOUBLE,DOUBLE,INT', '0,0,0,0,0'),
(271, 8, 'Notification', 'id,IdPerson,IdSignal,Title,Image,IsView,CreatedAt,UpdatedAt,IdReceiver', 'INT,INT,INT,VARCHAR,TEXT,BOOLEAN,DATETIME,DATETIME,INT', '0,0,0,250,0,0,0,0,0'),
(272, 9, 'khach_hang', 'id,username,address,phone,cmnd', 'INT,VARCHAR,VARCHAR,INT,INT', '0,36,36,11,11'),
(273, 9, 'tran_dau', 'id,name,information,thoi_gian_tao,so_ghe_trong,image,so_ghe_da_ban', 'INT,VARCHAR,VARCHAR,DATETIME,INT,INT,INT', '0,36,500,0,0,0,0'),
(274, 9, 'admin', 'id,username,password,fullname,image,address', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '0,36,36,36,0,0'),
(275, 9, 'hoa_don', 'id,id_user,Id_tran_dau,so_ve,trang_thai,ngay_dat_mua', 'INT,INT,INT,INT,INT,DATETIME', '0,0,0,0,0,0'),
(276, 7, 'CourseInformation', 'id,title,description,price,information,image', 'INT,VARCHAR,VARCHAR,INT,TEXT,VARCHAR', '0,500,500,0,0,500'),
(277, 10, 'Student', 'id,Name,Age,IdClass', 'INT,VARCHAR,INT,INT', '0,36,0,0'),
(278, 10, 'class', 'id,Name', 'INT,VARCHAR', '0,36'),
(279, 11, 'User', 'id,IdHocVan,AuthKey,Email,Password,Fullname,Phone,Born', 'INT,TEXT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATE', '0,10,50,50,10,15,15,8'),
(280, 11, 'CommonCategories', 'id,Title,Description,View,ImageURL', 'INT,VARCHAR,VARCHAR,INT,VARCHAR', '0,10,50,0,200'),
(281, 11, 'CommonVocabulary', 'id,IdCommonCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', 'INT,VARCHAR,VARCHAR,TEXT,TEXT,VARCHAR,VARCHAR', '0,10,200,10,15,10,50'),
(282, 11, 'SpecializedCategories', 'id,Title,Description ,View,ImageURL', 'INT,VARCHAR,VARCHAR,INT,VARCHAR', '0,10,50,0,200'),
(283, 11, 'SpecializedVocabulary', 'id,IdSpecializedCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', 'INT,VARCHAR,VARCHAR,TEXT,TEXT,VARCHAR,VARCHAR', '0,10,200,10,15,10,50'),
(284, 11, 'HocVan', 'id,EducationName', 'INT,VARCHAR', '0,10'),
(285, 11, 'Dictionary', 'id,EnglishName,EnglishExplain,EnglishSpelling,VietNamName,VietNamExplain,ImageURL', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '0,10,50,10,10,50,200'),
(286, 11, 'History', 'id,IdUser,ExpressName,ExpressDescribe,CreatedAt', 'INT,INT,VARCHAR,VARCHAR,DATETIME', '0,0,15,30,0'),
(288, 12, 'User', 'id,Username,Password', 'INT,VARCHAR,VARCHAR', '0,50,50'),
(289, 14, 'Person', 'id,Name,Email,Phone,Password,Address,Sex,Brithday,IdImage,IdPermission,DeviceToken,CreatedAt,UpdatedAt', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,INT,VARCHAR,INT,INT,TEXT,DATETIME,DATETIME', '0,150,150,100,250,250,0,100,0,0,0,0,0'),
(290, 14, 'City', 'id,Name', 'INT,VARCHAR', '0,150'),
(291, 14, 'District', 'id,IdCity,Name', 'INT,INT,VARCHAR', '0,0,150'),
(292, 14, 'Status', 'id,Name', 'INT,VARCHAR', '0,100'),
(293, 14, 'ConstructionItems', 'id,Name,IdImage', 'INT,VARCHAR,INT', '0,250,0'),
(294, 14, 'LinkFile', 'id,Name,Url,CreatedAt', 'INT,VARCHAR,VARCHAR,DATETIME', '0,250,250,250'),
(295, 14, 'Image', 'id,Url,CreatedAt', 'INT,VARCHAR,DATETIME', '0,250,0'),
(296, 14, 'Permission', 'id,Name', 'INT,VARCHAR', '0,150'),
(297, 14, 'News', 'id,IdPerson,IdImage,Title,Summary,Content,View,CreatedAt,UpdatedAt', 'INT,INT,INT,VARCHAR,VARCHAR,TEXT,INT,DATETIME,DATETIME', '0,0,0,250,250,0,0,0,0'),
(298, 14, 'Work', 'id,IdPerson,IdStatusWork,IdProjectStaff,Title,StartDate,EndDate,Content,CreatedAt,UpdatedAt', 'INT,INT,INT,INT,VARCHAR,DATETIME,DATETIME,TEXT,DATETIME,DATETIME', '0,0,0,0,250,0,0,0,0,0'),
(299, 14, 'Timekeeping', 'id,IdPerson,IdProjectStaff,StartCheckingTime,EngCheckingTime,IdCity,IdDistrict,Address,Report,Location,UpdatedAt,CreatedAt', 'INT,INT,INT,DATETIME,DATETIME,INT,INT,VARCHAR,VARCHAR,TEXT,DATETIME,DATETIME', '0,0,0,0,0,0,0,250,250,0,0,0'),
(300, 14, 'ProjectStaff', 'id,IdConstructionItems,IdImage,Title,IdCity,IdDistrict,Address,CreatedAt,UpdatedAt', 'INT,INT,INT,VARCHAR,INT,INT,VARCHAR,DATETIME,DATETIME', '0,0,0,250,0,0,250,0,0'),
(301, 14, 'ManagerCost', 'id,IdTypeCost,Title,Note,Money,CreatedAt,UpdatedAt,IdPerson', 'INT,INT,VARCHAR,VARCHAR,INT,DATETIME,DATETIME,INT', '0,0,250,250,0,0,0,0'),
(302, 14, 'TypeCost', 'id,Name', 'INT,VARCHAR', '0,150'),
(303, 14, 'Project', 'id,IdProjectStatus,CustomerId,BuilderId,ListConstructionItems,Title,Content,PriceBuilder,PriceAppoved,StartDate,EndDate,Deadline,ListFile,IdCity,IdDistrict,Address,IdImage,IsDeposit,MoneyDeposit,PriceCustomer,ListImage,NoteReport,CreatedAt,UpdatedAt', 'INT,INT,INT,INT,VARCHAR,VARCHAR,TEXT,INT,INT,DATETIME,DATETIME,VARCHAR,VARCHAR,INT,INT,INT,INT,INT,INT,INT,VARCHAR,VARCHAR,DATETIME,DATETIME', '0,0,0,0,150,250,0,0,0,0,0,100,150,0,0,0,0,0,0,0,250,250,0,0'),
(304, 14, 'ProjectStatus', 'id,Name', 'INT,VARCHAR', '0,100'),
(305, 14, 'Report', 'id,IdProject,IdPerson,Title,Content,ListImage,CreatedAt,UpdatedAt', 'INT,INT,INT,VARCHAR,TEXT,VARCHAR,DATETIME,DATETIME', '0,0,0,250,0,150,0,0'),
(306, 14, 'Product', 'id,IdCategories,Name,Description,Content,ListImage,CreatedAt,UpdatedAt', 'INT,INT,VARCHAR,VARCHAR,TEXT,VARCHAR,DATETIME,DATETIME', '0,0,250,250,0,250,0,0'),
(307, 14, 'Categories', 'id,Name', 'INT,VARCHAR', '0,250'),
(308, 14, 'ProjectFinsh', 'id,Title,Content,IdCity,IdDistrict,Address,ListImage,Investor,TimeFinish,CreatedAt,UpdatedAt', 'INT,VARCHAR,TEXT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,DATETIME', '0,250,0,0,0,250,250,250,250,0,0'),
(309, 14, 'TransactionHistory', 'id,CustomerId,NameReceiver,AccoutNumber,Content,Money,IdProject,NameBank,CreatedAt,UpdatedAt', 'INT,INT,VARCHAR,VARCHAR,TEXT,INT,INT,VARCHAR,,', '0,0,250,150,0,0,0,250,0,0'),
(310, 14, 'Feedback', 'id,IdPerson,Titile,Content,CreatedAt,UpdatedAt,ListImage', 'INT,INT,VARCHAR,TEXT,DATETIME,DATETIME,VARCHAR', '0,0,250,0,0,0,250'),
(311, 14, 'Notification', 'id,IdTypeNotification,IdStatus,IdImage,Title,Content,CreatedAt,UpdatedAt,IdProject', 'INT,INT,INT,INT,VARCHAR,TEXT,DATETIME,DATETIME,INT', '0,0,0,0,250,0,0,0,0'),
(312, 14, 'TypeNotification', 'id,Name', 'INT,VARCHAR', '0,250'),
(313, 14, 'RegistrationFiveStar', 'id,IdCity,IdDistrict,IdConstructionItems,Address,Name,Phone,NumberCode,BeforeImageCode,AfterImageCode,AmountPeople,Experience,Content,CreatedAt,UpdatedAt,Appoved', 'INT,INT,INT,INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,TEXT,DATETIME,DATETIME,INT', '0,0,0,0,250,250,100,100,250,250,100,100,0,0,0,0'),
(314, 14, 'RegistrationApp', 'id,IdCity,IdDistrict,Address,StartDate,EndDate,Content,CreatedAt,UpdatedAt', 'INT,INT,INT,VARCHAR,DATETIME,DATETIME,TEXT,DATETIME,DATETIME', '0,0,0,250,0,0,0,0,0'),
(315, 14, 'Setting', 'id,PhoneContact,NameAccountBank,NumberAccountBank,NameBank,Address,NotePay1,NotePay2,CreatedAt,UpdatedAt', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,DATETIME', '0,100,100,100,250,250,250,250,0,0'),
(316, 14, 'Banner', 'id,IdTypeBanner,Tilte,IdImage,CreatedAt,UpdatedAt', 'INT,INT,VARCHAR,INT,DATETIME,DATETIME', '0,0,250,0,0,0'),
(317, 14, 'TypeBanner', 'id,Name', 'INT,VARCHAR', '0,100'),
(318, 14, 'StatusWork', 'id,Name', 'INT,VARCHAR', '0,250'),
(319, 13, 'Admin', 'id,Name,Email,Password', 'INT,VARCHAR,VARCHAR,VARCHAR', '0,50,50,255'),
(320, 13, 'User', 'id,Name,Phone,Email,Password,Avatar_Url,Token', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,TEXT,VARCHAR', '0,50,13,50,255,0,255'),
(321, 13, 'Owner', 'id,Name,Phone,Email,Password,Avatar_Url,Token', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,TEXT,VARCHAR', '0,50,13,50,255,0,255'),
(322, 13, 'FootbalField', 'id,Owner_Id,Name,Price_Per_Hour,OpenAt,CloseAt,Address,Description,Avatar_Url', 'INT,INT,VARCHAR,INT,DATETIME,DATETIME,VARCHAR,TEXT,TEXT', '0,0,50,0,0,0,255,0,0'),
(323, 13, 'Booking', 'id,User_Id,Field_Id,Book_Day,Start,End,Status,Total,Message', 'INT,INT,INT,DATE,DATETIME,DATETIME,BOOLEAN,INT,TEXT', '0,0,0,0,0,0,0,0,0'),
(324, 13, 'ImageField', 'id,Field_Id,Img_Url', 'INT,INT,TEXT', '0,0,0'),
(325, 14, 'InfomationPriceQuote', 'id,IdProject,IdProjectStatus,IdPerson,Money,Deadline,CreatedAt,UpdatedAt', 'INT,INT,INT,INT,INT,VARCHAR,DATETIME,DATETIME', '0,0,0,0,0,250,0,0'),
(326, 15, 'Teamplate', 'id,value', 'INT,VARCHAR', '0,500'),
(327, 15, 'User', 'id,Fullname,Company,Address,Position,Avatar,NumberOfView,IdTeamplate,Token', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,INT,INT,VARCHAR', '0,36,36,36,36,500,0,0,500'),
(328, 15, 'Information', 'id,Name,Image,Type', 'INT,VARCHAR,VARCHAR,INT', '0,36,500,0'),
(329, 15, 'UserConection', 'id,IdUser,IdInformation,NumberInteraction,Value', 'INT,INT,INT,INT,VARCHAR', '0,0,0,0,500'),
(330, 16, 'User', 'id,email,password,role,avatar', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR', '0,150,100,250,250'),
(331, 16, 'Label', 'id,name,idProject', 'INT,VARCHAR,INT', '0,150,0'),
(347, 19, 'PhanLoaiDuAn', 'id,idDuAn,idUser', 'INT,INT,INT', '0,0,0'),
(332, 16, 'Task', 'id,name,state,startTime,endTime,idLabel,idProject', 'INT,VARCHAR,INT,DATE,DATE,INT,INT', '0,150,1,10,10,0,0'),
(341, 17, 'User', 'id,email', 'INT,VARCHAR', '0,150'),
(345, 19, 'DuAn', 'id,name,tgbatdau,tgketthuc,trangthai,mieuta', 'INT,TEXT,DATE,DATE,INT,TEXT', '0,250,0,0,0,250'),
(346, 19, 'User', 'id,name,email,password,role,avatar', 'INT,TEXT,TEXT,VARCHAR,TEXT,TEXT', '0,150,150,20,100,250'),
(336, 16, 'Project', 'id,name,startTime,endTime,desc,idUser', 'INT,VARCHAR,DATE,DATE,TEXT,INT', '0,150,10,10,250,0'),
(349, 19, 'Nhan', 'id,name,idDuAn', 'INT,TEXT,INT', '0,150,0'),
(348, 19, 'NhiemVu', 'id,name,tgbatdau,tgketthuc,trangthai,idNhan,mieuta,idDuAn', 'INT,TEXT,DATE,DATE,INT,INT,TEXT,INT', '0,150,0,0,0,0,250,0'),
(350, 19, 'PhanLoaiNhiemVu', 'id,idNhiemVu,idUser', 'INT,INT,INT', '0,0,0'),
(351, 19, 'BinhLuanDuAn', 'id,idDuAn,ThongDiep,idUser,thoiGian', 'INT,INT,TEXT,INT,DATETIME', '0,0,250,0,0'),
(352, 19, 'BinhLuanNhiemVu', 'id,idNhiemVu,ThongDiep,idUser,ThoiGian', 'INT,INT,TEXT,INT,DATETIME', '0,0,250,0,0'),
(353, 19, 'LoaiThongBao', 'id,Loai', 'INT,TEXT', '0,50'),
(354, 19, 'ThongBao', 'id,idUser,idDuAn,idLoaiThongBao', 'INT,INT,INT,INT', '0,0,0,0'),
(355, 8, 'SaveLocation', 'id,IdSignal,Lat,Lng,Distance,CreatedAt', 'INT,INT,DOUBLE,DOUBLE,INT,DATETIME', '0,0,0,0,0,0'),
(356, 20, 'user', 'id', 'INT', '0'),
(369, 22, 'products', 'id,name,image,price,discount,quantity,created_at,updated_at', 'INT,VARCHAR,VARCHAR,INT,INT,INT,DATETIME,DATETIME', '0,255,255,0,0,0,0,0'),
(370, 22, 'posts', 'id,title,image,details,created_at,updated_at', 'INT,VARCHAR,VARCHAR,TEXT,DATETIME,DATETIME', '0,255,255,0,0,0'),
(371, 22, 'comments', 'id,user_id,post_id,details,created_at,updated_at', 'INT,INT,INT,TEXT,DATETIME,DATETIME', '0,0,0,0,0,0'),
(364, 21, 'user', 'id,lname,fname,name,email,phone,address,password,created_at,updated_at', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,DATETIME', '0,255,255,255,255,15,255,255,0,0'),
(368, 22, 'user', 'id,lname,fname,user_name,email,phone,address,password,created_at,updated_at', 'INT,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATETIME,DATETIME', '0,255,255,255,255,255,255,255,0,0');

-- --------------------------------------------------------

--
-- Table structure for table `a1crud_crud`
--

CREATE TABLE `a1crud_crud` (
  `createdate` date NOT NULL,
  `id` int(11) NOT NULL,
  `idproject` int(11) NOT NULL,
  `idclass` int(11) NOT NULL,
  `actiontype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `titles` longtext COLLATE utf8_unicode_ci NOT NULL,
  `properties` longtext COLLATE utf8_unicode_ci NOT NULL,
  `alias` longtext COLLATE utf8_unicode_ci NOT NULL,
  `refers` longtext COLLATE utf8_unicode_ci NOT NULL,
  `titlecreate` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `titleupdate` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `inputtypes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `validates` longtext COLLATE utf8_unicode_ci NOT NULL,
  `formats` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `a1crud_crud`
--

INSERT INTO `a1crud_crud` (`createdate`, `id`, `idproject`, `idclass`, `actiontype`, `titles`, `properties`, `alias`, `refers`, `titlecreate`, `titleupdate`, `inputtypes`, `validates`, `formats`) VALUES
('2021-07-27', 3863, 16, 338, '0', 'id,IdTask,IdUser,IdProject', 'id,IdTask,IdUser,IdProject', 'id,IdTask,IdUser,IdProject', '::,332:id:,330:id:,336:id:', '', '', '', '', ',,,'),
('2021-07-28', 3906, 16, 337, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3907, 16, 339, '0', 'id,IdUser,IdNotification', 'id,IdUser,IdNotification', 'id,IdUser,IdNotification', '::,330:id:id,335:id:id', '', '', '', '', ',,'),
('2020-11-01', 4, 1, 2, '0', 'id,Title,SubTitle,Content', 'id,Title,SubTitle,Content', 'ID,Tiêu ??,Tiêu ?? ph?,N?i dung', '::,::,::,::', '', '', '', '', ',,,'),
('2020-11-01', 5, 1, 2, '1', 'Tiêu ??,Tiêu ?? ph?,N?i dung', 'Title,SubTitle,Content', '', '::,::,::', 'Thêm about meal Plan', '', 'varchar,varchar,ckeditor', 'required,required,required', ''),
('2020-11-01', 6, 1, 2, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 7, 1, 3, '0', 'Content1,Content2,Content3,AndroidLink,IosLink', 'Content1,Content2,Content3,AndroidLink,IosLink', 'N?i dung 1,N?i dung 2,N?i dung 3,Android Link,IOS Link', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2020-11-01', 8, 1, 3, '1', 'N?i dung 1,N?i dung 2,N?i dung 3,Android link,IOS link', 'Content1,Content2,Content3,AndroidLink,IosLink', '', '::,::,::,::,::', 'Thêm footer', '', 'ckeditor,ckeditor,ckeditor,varchar,varchar', ',,,,', ''),
('2020-11-01', 9, 1, 3, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 10, 1, 4, '0', 'Phone,Mail', 'Phone,Mail', 'S? ?i?n tho?i,Email', '::,::', '', '', '', '', ','),
('2020-11-01', 11, 1, 4, '1', 'S? ?i?n tho?i,Email', 'Phone,Mail', '', '::,::', 'Thêm thông tin menu top', '', 'varchar,varchar', 'required,required', ''),
('2020-11-01', 12, 1, 4, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 18, 1, 5, '1', 'Tên menu,V? trí,Liên k?t,Ch?n menu cha', 'Name,Position,Link,IdParent', '', '::,::,::,5:id:Name', 'Thêm menu chính', '', 'varchar,number,varchar,select', 'required,required,required,', ''),
('2020-11-01', 17, 1, 5, '0', 'Name,Position,Link,IdParent', 'Name,Position,Link,IdParent', 'Name,V? trí s?p x?p,Link,Menu cha', '::,::,::,5:id:Name', '', '', '', '', ',,,'),
('2020-11-01', 19, 1, 5, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 20, 1, 6, '0', 'Title,PriceOrigin,PriceCurrent,Star,Duration', 'Title,PriceOrigin,PriceCurrent,Star,Duration', 'Tên th?c ??n,Giá g?c,Giá bán,?ánh giá,S? ngày dùng', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2020-11-01', 21, 1, 6, '1', 'Title,Thumbnail,Mô t?,Giá g?c,Giá bán,S? sao ?ánh giá,Th?i gian dùng,Hình ?nh,Video', 'Title,Thumbnail,Description,PriceOrigin,PriceCurrent,Star,Duration,Images,Video', '', '::,::,::,::,::,::,::,::,::', 'Thêm th?c ??n', '', 'varchar,varchar,ckeditor,number,number,number,number,image,varchar', 'required,required,required,required,required,required,required,required,required', ''),
('2020-11-01', 22, 1, 6, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 23, 1, 7, '0', 'id,Name,Price', 'id,Name,Price', 'ID,Tên gói combo,T?ng ti?n', '::,::,::', '', '', '', '', ',,'),
('2020-11-01', 24, 1, 7, '1', 'Tên combo,T?ng ti?n', 'Name,Price', '', '::,::', 'Thêm combo', '', 'varchar,number', 'required,required', ''),
('2020-11-01', 25, 1, 7, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 26, 1, 8, '0', 'id,IdShopCombo,IdShopMenu', 'id,IdShopCombo,IdShopMenu', 'ID,Combo,Th?c ??n', '::,7:id:Name,6:id:Title', '', '', '', '', ',,'),
('2020-11-01', 27, 1, 8, '1', 'Ch?n combo,Ch?n th?c ??n', 'IdShopCombo,IdShopMenu', '', '7:id:Name,6:id:Title', 'Thêm th?c ??n vào combo', '', 'select,select', 'required,required', ''),
('2020-11-01', 28, 1, 8, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 29, 1, 9, '0', 'IdShopMenu,IdUser,IdCommentStatus,Content', 'IdShopMenu,IdUser,IdCommentStatus,Content', 'Th?c ??n,Ng??i dùng,Tr?ng thái,N?i dung', '6:id:Title,31:id:Fullname,43:id:Name,::', '', '', '', '', ',,,'),
('2020-11-01', 30, 1, 9, '1', 'Th?c ??n,Ng??i dùng,Tr?ng thái,N?i dung,T?o lúc', 'IdShopMenu,IdUser,IdCommentStatus,Content,CreatedAt', '', '6:id:Title,31:id:Fullname,43:id:Name,::,::', 'Thêm bình lu?n', '', 'select,select,select,ckeditor,datetime', 'required,required,required,required,required', ''),
('2020-11-01', 31, 1, 9, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 32, 1, 10, '0', 'id,IdShopMenu,Link,CreatedAt', 'id,IdShopMenu,Link,CreatedAt', 'ID,Th?c ??n,???ng d?n,T?o lúc', '::,6:id:Title,::,::', '', '', '', '', ',,,'),
('2020-11-01', 33, 1, 10, '1', 'Th?c ??n,File PDF,CreatedAt', 'IdShopMenu,Link,CreatedAt', '', '6:id:Title,::,::', 'Thêm PDF th?c ??n', '', 'select,varchar,datetime', 'required,required,required', ''),
('2020-11-01', 34, 1, 10, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 35, 1, 11, '0', 'id,Name', 'id,Name', 'ID,Tên lo?i th?c ??n', '::,::', '', '', '', '', ','),
('2020-11-01', 36, 1, 11, '1', 'Tên lo?i th?c ??n', 'Name', '', '::', 'Thêm lo?i th?c ??n', '', 'varchar', 'required', ''),
('2020-11-01', 37, 1, 11, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 38, 1, 12, '0', 'id,IdMealPlanType,Name,NumberDay,Price,CreatedAt', 'id,IdMealPlanType,Name,NumberDay,Price,CreatedAt', 'ID,Lo?i th?c ??n,Tên gói,S? ngày,Giá ti?n,T?o lúc', '::,11:id:Name,::,::,::,::', '', '', '', '', ',,,,,'),
('2020-11-01', 39, 1, 12, '1', 'Gói th?c ??n,Tên gói,S? ngày dùng,Giá ti?n,T?o lúc', 'IdMealPlanType,Name,NumberDay,Price,CreatedAt', '', '11:id:Name,::,::,::,::', 'Thêm gói công th?c', '', 'select,varchar,number,number,datetime', 'required,required,required,required,required', ''),
('2020-11-01', 40, 1, 12, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 41, 1, 13, '0', 'IdUser,Height,Weight,Target,CreatedAt,UpdatedAt', 'IdUser,Height,Weight,Target,CreatedAt,UpdatedAt', 'Ng??i dùng,Chi?u cao hi?n t?i,Cân n?ng hi?n t?i,M?c tiêu,T?o lúc,C?p nh?t lúc', '31:id:Fullname,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2020-11-01', 42, 1, 13, '1', 'Ng??i dùng,Chi?u cao hi?n t?i,Cân n?ng hi?n t?i,M?c tiêu,T?o lúc,C?p nh?t lúc', 'IdUser,Height,Weight,Target,CreatedAt,UpdatedAt', '', '31:id:Fullname,::,::,::,::,::', 'Thêm ch? s? c?a ng??i dùng', '', 'select,number,number,number,datetime,datetime', 'required,required,required,,,', ''),
('2020-11-01', 43, 1, 13, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 45, 1, 14, '0', 'id,IdMaterial,IdIndexUser', 'id,IdMaterial,IdIndexUser', 'ID,Nguyên li?u,ID Ng??i dùng', '::,26:id:Name,13:id:IdUser', '', '', '', '', ',,'),
('2020-11-01', 46, 1, 14, '1', 'Nguyên li?u,ID Ng??i dùng', 'IdMaterial,IdIndexUser', '', '26:id:Name,13:id:IdUser', 'Thêm d? ?ng cho ng??i dùng', '', 'select,select', 'required,required', ''),
('2020-11-01', 47, 1, 14, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 48, 1, 15, '0', 'id,Name', 'id,Name', 'ID,Tên danh m?c Blog', '::,::', '', '', '', '', ','),
('2020-11-01', 49, 1, 15, '1', 'Tên danh m?c', 'Name', '', '::', 'Thêm danh m?c blog', '', 'varchar', 'required', ''),
('2020-11-01', 50, 1, 15, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 51, 1, 16, '0', 'IdBlogCategories,Title,Description,NumberView,CreatedAt,UpdatedAt', 'IdBlogCategories,Title,Description,NumberView,CreatedAt,UpdatedAt', 'Danh m?c,Tiêu ??,Mô t?,S? l??t xem,T?o lúc,C?p nh?t lúc', '15:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2020-11-01', 52, 1, 16, '1', 'Ch?n danh m?c,Tiêu ??,Thumbnail,Mô t?,N?i dung,Sô l??t xem,T?o lúc,C?p nh?t lúc', 'IdBlogCategories,Title,Thumbnail,Description,Content,NumberView,CreatedAt,UpdatedAt', '', '15:id:Name,::,::,::,::,::,::,::', 'Thêm bài vi?t', '', 'select,varchar,image,ckeditor,ckeditor,number,datetime,datetime', 'required,required,required,required,required,required,required,required', ''),
('2020-11-01', 53, 1, 16, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 54, 1, 17, '0', 'id,Keyword,SearchAt', 'id,Keyword,SearchAt', 'ID,T? khóa,Tìm ki?m lúc', '::,::,::', '', '', '', '', ',,'),
('2020-11-01', 55, 1, 17, '1', 'T? khóa tìm ki?m,Tìm lúc', 'Keyword,SearchAt', '', '::,::', 'Thêm t? khóa tìm ki?m', '', 'varchar,datetime', 'required,required', ''),
('2020-11-01', 56, 1, 17, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 57, 1, 18, '0', 'id,Name', 'id,Name', 'ID,Tên danh m?c c?ng ??ng', '::,::', '', '', '', '', ','),
('2020-11-01', 58, 1, 18, '1', 'Tên danh m?c c?ng ??ng', 'Name', '', '::', 'Thêm danh m?c c?ng ??ng', '', 'varchar', 'required', ''),
('2020-11-01', 59, 1, 18, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 60, 1, 19, '0', 'IdCommunityCategories,Title,Description,NumberView,CreatedAt', 'IdCommunityCategories,Title,Description,NumberView,CreatedAt', 'Danh m?c,Tiêu ??,Mô t?,S? l??t xem,T?o lúc', '18:id:Name,::,::,::,::', '', '', '', '', ',,,,'),
('2020-11-01', 61, 1, 19, '1', 'Danh m?c,Tiêu ??,Thumbnail,Mô t?,N?i dung,Hình ?nh,Sô l??t xem,T?o lúc', 'IdCommunityCategories,Title,Thumbnail,Description,Content,Image,NumberView,CreatedAt', '', '18:id:Name,::,::,::,::,::,::,::', 'Thêm bài vi?t c?ng ??ng', '', 'select,varchar,image,ckeditor,ckeditor,image,number,datetime', 'required,required,required,required,required,required,required,required', ''),
('2020-11-01', 62, 1, 19, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 63, 1, 20, '0', 'Address,Phone,Mail,Working', 'Address,Phone,Mail,Working', 'Address,Phone,Mail,Working', '::,::,::,::', '', '', '', '', ',,,'),
('2020-11-01', 64, 1, 20, '1', 'Address,Phone,Mail,Working,Facebook,Instagram,Youtube,Twitter,Map', 'Address,Phone,Mail,Working,Facebook,Instagram,Youtube,Twitter,Map', '', '::,::,::,::,::,::,::,::,::', 'Thêm thông tin liên h?', '', 'varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,ckeditor', ',,,,,,,,required', ''),
('2020-11-01', 65, 1, 20, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 66, 1, 21, '0', 'id,Name', 'id,Name', 'ID,Tr?ng thái liên h?', '::,::', '', '', '', '', ','),
('2020-11-01', 67, 1, 21, '1', 'Tên tr?ng thái', 'Name', '', '::', 'Thêm tr?ng thái liên h?', '', 'varchar', 'required', ''),
('2020-11-01', 68, 1, 21, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 69, 1, 22, '0', 'IdStatus,Name,Email,Message', 'IdStatus,Name,Email,Message', 'Tr?ng thái,Tên ng??i liên h?,Email,Tin nh?n', '21:id:Name,::,::,::', '', '', '', '', ',,,'),
('2020-11-01', 70, 1, 22, '1', 'Tr?ng thái,Name,Email,Message', 'IdStatus,Name,Email,Message', '', '21:id:Name,::,::,::', 'Thông tin liên h?', '', 'select,varchar,varchar,varchar', 'required,required,required,required', ''),
('2020-11-01', 71, 1, 22, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 72, 1, 23, '0', 'id,Name', 'id,Name', 'ID,Tên danh m?c', '::,::', '', '', '', '', ','),
('2020-11-01', 73, 1, 23, '1', 'Name', 'Name', '', '::', 'Thêm danh m?c cho công th?c', '', 'varchar', 'required', ''),
('2020-11-01', 74, 1, 23, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 75, 1, 24, '0', 'id,Name', 'id,Name', 'ID,?? khó', '::,::', '', '', '', '', ','),
('2020-11-01', 76, 1, 24, '1', '?? khó', 'Name', '', '::', 'Thêm ?? khó', '', 'varchar', 'required', ''),
('2020-11-01', 77, 1, 24, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-10', 78, 1, 25, '0', 'IdCookingDifficulty,IdRecipeCategories,Title,NumberView,CreatedAt,UpdatedAt,IdDiet', 'IdCookingDifficulty,IdRecipeCategories,Title,NumberView,CreatedAt,UpdatedAt,IdDiet', '?? khó,Danh m?c,Tiêu ??,S? l??t xem,T?o lúc,C?p nh?t lúc,Bửa ăn', '24:id:Name,23:id:Name,::,::,::,::,48:id:Name', '', '', '', '', ',,,,,,'),
('2020-12-10', 79, 1, 25, '1', '?? khó,Danh m?c th?c ??n,Tiêu ??,S? l??t xem,T?ng th?i gian,Th?i gian chu?n b?,Thumbnail,?nh banner,Hình ?nh Nutrition,Pdf Link,T?o lúc,C?p nh?t lúc,Mô tả,Chế độ ăn', 'IdCookingDifficulty,IdRecipeCategories,Title,NumberView,TotalTime,PrepareTime,Thumbnail,ImageBanner,ImageEnergy,PdfLink,CreatedAt,UpdatedAt,Description,IdDiet', '', '24:id:Name,23:id:Name,::,::,::,::,::,::,::,::,::,::,::,48:id:Name', 'Thêm công th?c', '', 'select,select,varchar,number,varchar,varchar,image,image,image,varchar,datetime,datetime,,select', 'required,required,required,required,required,required,,,,required,required,required,required,required', ''),
('2020-11-01', 80, 1, 25, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 81, 1, 26, '0', 'id,Name,Image,Weight,Calo', 'id,Name,Image,Weight,Calo', 'ID,Tên nguyên li?u,Hình ?nh,Cân n?ng (g),Calo', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2020-11-01', 82, 1, 26, '1', 'Name,Image,Weight,Calo', 'Name,Image,Weight,Calo', '', '::,::,::,::', 'Thêm nguyên li?u', '', 'varchar,image,number,number', 'required,required,required,required', ''),
('2020-11-01', 83, 1, 26, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 84, 1, 27, '0', 'IdRecipe,IdMaterial,Description,Weight,Postion', 'IdRecipe,IdMaterial,Description,Weight,Postion', 'Th?c ??n,Nguyên li?u,Mô t?,Cân n?ng(g),V? trí', '25:id:Title,26:id:Name,::,::,::', '', '', '', '', ',,,,'),
('2020-11-01', 85, 1, 27, '1', 'IdRecipe,IdMaterial,Description,Weight,Postion', 'IdRecipe,IdMaterial,Description,Weight,Postion', '', '25:id:Title,26:id:Name,::,::,::', 'Thêm nguyên li?u cho Công Th?c', '', 'select,select,ckeditor,number,number', 'required,required,required,required,required', ''),
('2020-11-01', 86, 1, 27, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-01', 87, 1, 28, '0', 'IdRecipe,image,Postion', 'IdRecipe,image,Postion', 'Th?c ??n,Hình ?nh,V? trí', '25:id:Title,::,::', '', '', '', '', ',,'),
('2020-11-01', 88, 1, 28, '1', 'Ch?n th?c ??n,Hình ?nh,Mô t?,V? trí', 'IdRecipe,image,DesciptionStep,Postion', '', '25:id:Title,::,::,::', 'Thêm các b??c th?c hi?n th?c ??n', '', 'select,image,ckeditor,number', 'required,required,required,required', ''),
('2020-11-01', 89, 1, 28, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 90, 1, 29, '0', 'id,Name', 'id,Name', 'ID,Tr?ng thái ng??i dùng', '::,::', '', '', '', '', ','),
('2020-11-02', 91, 1, 29, '1', 'Tên tr?ng thái', 'Name', '', '::', 'Thêm tr?ng thái ng??i dùng', '', 'varchar', 'required', ''),
('2020-11-02', 92, 1, 29, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 93, 1, 30, '0', 'id,Name', 'id,Name', 'ID,Tên nhóm quy?n', '::,::', '', '', '', '', ','),
('2020-11-02', 94, 1, 30, '1', 'Tên quy?n', 'Name', '', '::', 'Thêm quy?n cho ng??i dùng', '', 'varchar', 'required', ''),
('2020-11-02', 95, 1, 30, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 96, 1, 31, '0', 'IdUserStatus,Fullname,Email,IdRole,CreatedAt', 'IdUserStatus,Fullname,Email,IdRole,CreatedAt', 'Tr?ng thái,H? và tên,Email,Quy?n h?n,T?o lúc', '29:id:Name,::,::,30:id:Name,::', '', '', '', '', ',,,,'),
('2020-11-03', 97, 1, 31, '1', 'Tr?ng thái,H? và tên,Email,M?t kh?u,Avatar,Quy?n,T?o lúc,C?p nh?t lúc,Nhân viên Ch?m sóc', 'IdUserStatus,Fullname,Email,Password,Avatar,IdRole,CreatedAt,UpdatedAt,IdAccount', '', '29:id:Name,::,::,::,::,30:id:Name,::,::,1:id:Image', 'Thêm ng??i dùng', '', 'select,varchar,varchar,varchar,image,select,datetime,datetime,select', 'required,required,required,required,,required,required,required,', ''),
('2020-11-02', 98, 1, 31, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 99, 1, 32, '0', 'IdUser,IdPackage,Weight,CreatedAt,UpdatedAt', 'IdUser,IdPackage,Weight,CreatedAt,UpdatedAt', 'Ng??i dùng,Gói công th?c,Cân n?ng,T?o lúc,C?p nh?t lúc', '31:id:Fullname,12:id:Name,::,::,::', '', '', '', '', ',,,,'),
('2020-11-02', 100, 1, 32, '1', 'Ng??i dùng,Gói công th?c,Cân n?ng,T?o lúc,C?p nh?t lúc', 'IdUser,IdPackage,Weight,CreatedAt,UpdatedAt', '', '31:id:Fullname,12:id:Name,::,::,::', 'Thông tin cân n?ng ng??i dùng', '', 'select,select,number,datetime,datetime', 'required,required,required,required,required', ''),
('2020-11-02', 101, 1, 32, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 103, 1, 33, '0', 'Name,PromotionCode,Percent,MoneyDiscount,StartDate,EndDate', 'Name,PromotionCode,Percent,MoneyDiscount,StartDate,EndDate', 'Tên ch??ng trình,Mã gi?m giá,Ph?n tr?m gi?m,S? ti?n gi?m,Ngày b?t ??u,Ngày h?t h?n', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2020-11-02', 104, 1, 33, '1', 'Tên ch??ng trình,Mã khuy?n m?i,Ph?n tr?m gi?m,S? ti?n gi?m,Ngày b?t ??u,Ngày h?t h?n', 'Name,PromotionCode,Percent,MoneyDiscount,StartDate,EndDate', '', '::,::,::,::,::,::', 'Thêm ch??ng trình gi?m giá', '', 'varchar,varchar,number,number,date,date', 'required,required,required,required,required,required', ''),
('2020-11-02', 105, 1, 33, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 106, 1, 34, '0', 'id,Name', 'id,Name', 'ID,Tr?ng thái ??t hàng', '::,::', '', '', '', '', ','),
('2020-11-02', 107, 1, 34, '1', 'Tên tr?ng thái', 'Name', '', '::', 'Thêm tr?ng thái ??t hàng', '', 'varchar', 'required', ''),
('2020-11-02', 108, 1, 34, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 109, 1, 35, '0', 'id,Name', 'id,Name', 'ID,Tr?ng thái thanh toán', '::,::', '', '', '', '', ','),
('2020-11-02', 110, 1, 35, '1', 'Tr?ng thái', 'Name', '', '::', 'Thêm tr?ng thái thanh toán', '', 'varchar', 'required', ''),
('2020-11-02', 111, 1, 35, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 112, 1, 36, '0', 'id,Name', 'id,Name', 'ID,Lo?i thanh toán', '::,::', '', '', '', '', ','),
('2020-11-02', 113, 1, 36, '1', 'Lo?i thanh toán', 'Name', '', '::', 'Thêm lo?i thanh toán', '', 'varchar', 'required', ''),
('2020-11-02', 114, 1, 36, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 115, 1, 37, '0', 'id,Name', 'id,Name', 'ID,Tên thành ph?', '::,::', '', '', '', '', ','),
('2020-11-02', 116, 1, 37, '1', 'Tên thành ph?', 'Name', '', '::', 'Thêm thành ph?', '', 'varchar', 'required', ''),
('2020-11-02', 117, 1, 37, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 118, 1, 38, '0', 'id,IdCity,Name', 'id,IdCity,Name', 'ID,Thu?c Thành Ph?,Tên Qu?n, Huy?n', '::,::,::', '', '', '', '', ',,'),
('2020-11-02', 119, 1, 38, '1', 'Ch?n thành ph?,Tên Qu?n, Huy?n', 'IdCity,Name', '', '37:id:Name,::', 'Thêm Qu?n, Huy?n', '', 'select,varchar', 'required,required', ''),
('2020-11-02', 120, 1, 38, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 121, 1, 39, '0', 'id,Name', 'id,Name', 'ID,Tên lo?i s?n ph?m', '::,::', '', '', '', '', ','),
('2020-11-02', 122, 1, 39, '1', 'Tên lo?i', 'Name', '', '::', 'Thêm lo?i s?n ph?m', '', 'varchar', 'required', ''),
('2020-11-02', 123, 1, 39, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 124, 1, 40, '0', 'IdProductType,IdUser,IdOrderStatus,IdCity,IdDistrict,IdPaymentStatus,IdPaymentType,TotalPrice,Email,Phone,Address,Note,CreatedAt', 'IdProductType,IdUser,IdOrderStatus,IdCity,IdDistrict,IdPaymentStatus,IdPaymentType,TotalPrice,Email,Phone,Address,Note,CreatedAt', 'Lo?i S?n ph?m,Ng??i mua,Tr?ng thái ??t hàng,Thành ph?,Qu?n, Huy?n,Tr?ng thái thanh toán,Lo?i thanh toán,T?ng ti?n,Email,S? ?i?n tho?i,??a ch?,Ghi chú,T?o lúc', '39:id:Name,31:id:Fullname,34:id:Name,37:id:Name,38:id:Name,35:id:Name,36:id:Name,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,'),
('2020-11-03', 125, 1, 40, '1', 'Lo?i s?n ph?m,Ng??i dùng,Tr?ng thái ??t hàng,Thành ph?,Qu?n/Huy?n,Tr?ng thái thanh toán,Lo?i thanh toán,T?ng ti?n,Mã gi?m giá,H? và tên,Email,?i?n tho?i,??a ch?,Ghi chú,Ngày t?o,Ngày c?p nh?t', 'IdProductType,IdUser,IdOrderStatus,IdCity,IdDistrict,IdPaymentStatus,IdPaymentType,TotalPrice,PromotionCode,Name,Email,Phone,Address,Note,CreatedAt,UpdatedAt', '', '39:id:Name,31:id:Fullname,34:id:Name,37:id:Name,38:id:Name,35:id:Name,36:id:Name,::,::,::,::,::,::,::,::,::', 'Thêm ??n hàng', '', 'select,select,select,select,select,select,select,number,varchar,varchar,varchar,varchar,varchar,varchar,datetime,datetime', 'required,required,required,required,required,required,required,required,,required,required:email,required,required,,required,required', ''),
('2020-11-02', 126, 1, 40, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 127, 1, 41, '0', 'IdOrderMenu,IdShopMenu,IdPackage,Amount,CreatedAt,UpdatedAt', 'IdOrderMenu,IdShopMenu,IdPackage,Amount,CreatedAt,UpdatedAt', '??n hàng,Th?c ??n,Gói công th?c,S? l??ng,T?o lúc,C?p nh?t lúc', '40:id:Name,6:id:Title,12:id:Name,::,::,::', '', '', '', '', ',,,,,'),
('2020-11-03', 128, 1, 41, '1', '??n hàng th?c ??n,Th?c ??n,Công th?c,S? l??ng,T?o lúc,C?p nh?t lúc', 'IdOrderMenu,IdShopMenu,IdPackage,Amount,CreatedAt,UpdatedAt', '', '40:id:Phone,6:id:Title,12:id:Name,::,::,::', 'Xem chi ti?t ??n hàng', '', 'select,select,select,number,datetime,datetime', 'required,,,required,required,required', ''),
('2020-11-02', 129, 1, 41, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 130, 1, 42, '0', 'id,Banner,Title1', 'id,Banner,Title1', 'ID,Banner,Tiêu ??', '::,::,::', '', '', '', '', ',,'),
('2020-11-02', 131, 1, 42, '1', 'Banner,Tiêu ?? 1,Mô t? 1,Hình ?nh 1,Tiêu ?? 2,Mô t? 2,Hình ?nh 2,Tiêu ?? 3,Mô t? 3,Hình ?nh 3,Belive 1,Belive 2,Belive 3', 'Banner,Title1,Description1,Image1,Title2,Description2,Image2,Title3,Description3,Image3,Belive1,Belive2,Belive3', '', '::,::,::,::,::,::,::,::,::,::,::,::,::', 'Thêm thông tin trang about', '', 'image,varchar,ckeditor,image,varchar,ckeditor,image,varchar,ckeditor,image,ckeditor,ckeditor,ckeditor', ',,,,,,,,,,,,', ''),
('2020-11-02', 132, 1, 42, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-02', 133, 1, 43, '0', 'id,Name', 'id,Name', 'ID,Tr?ng thái bình lu?n', '::,::', '', '', '', '', ','),
('2020-11-02', 134, 1, 43, '1', 'Tr?ng thái', 'Name', '', '::', 'Tr?ng thái bình lu?n', '', 'varchar', 'required', ''),
('2020-11-02', 135, 1, 43, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-03', 136, 1, 44, '0', 'id,Name', 'id,Name', 'ID,Phòng Ban', '::,::', '', '', '', '', ','),
('2020-11-03', 137, 1, 44, '1', 'Tên phòng ban', 'Name', '', '::', 'Thêm phòng ban', '', 'varchar', 'required', ''),
('2020-11-03', 138, 1, 44, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-03', 139, 1, 45, '0', 'id,Name', 'id,Name', 'ID,Th?i gian ch?m sóc', '::,::', '', '', '', '', ','),
('2020-11-03', 140, 1, 45, '1', 'S? ngày', 'Name', '', '::', 'Thêm m?c th?i gian ch?m sóc', '', 'number', 'required', ''),
('2020-11-03', 141, 1, 45, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-03', 142, 1, 46, '0', 'id,Name', 'id,Name', 'ID,Tr?ng thái ch?m sóc', '::,::', '', '', '', '', ','),
('2020-11-03', 143, 1, 46, '1', 'Tr?ng thái ch?m sóc', 'Name', '', '::', 'Thêm tr?ng thái ch?m sóc', '', 'varchar', 'required', ''),
('2020-11-03', 144, 1, 46, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-03', 145, 1, 47, '0', 'IdUser,IdOrderMenu,IdTimeCare,IdStatusCare,SendMail,Call,JoinVip,Result', 'IdUser,IdOrderMenu,IdTimeCare,IdStatusCare,SendMail,Call,JoinVip,Result', 'Ng??i dùng,??n hàng,M?c ch?m sóc,IdStatusCare,G?i mail,G?i ?i?n,Vip,K?t qu?', '31:id:Fullname,40:id:Phone,45:id:Name,46:id:Name,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2020-11-03', 146, 1, 47, '1', 'Khách hàng,??n hàng,M?c ch?m sóc,Tr?ng thái ch?m sóc,Ngày b?t ??u,G?i mail,G?i ?i?n,N?i dung tin nh?n,Có ph?i khách VIP,K?t qu?,Link chia s?,??a ch? g?i quà', 'IdUser,IdOrderMenu,IdTimeCare,IdStatusCare,CreateAt,SendMail,Call,Message,JoinVip,Result,LinkShare,Address', '', '31:id:Fullname,40:id:Phone,45:id:Name,46:id:Name,::,::,::,::,::,::,::,::', 'Thêm ch?m sóc khách hàng', '', 'select,select,select,select,datetime,varchar,varchar,ckeditor,varchar,varchar,varchar,varchar', 'required,required,required,required,,,,,,,,', ''),
('2020-11-03', 147, 1, 47, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-18', 148, 1, 48, '0', 'id,Name,Image', 'id,Name,Image', 'ID,Tên món,Hình ?nh', '::,::,::', '', '', '', '', ',,'),
('2020-11-18', 149, 1, 48, '1', 'Tên b?a ?n,Hình ?nh', 'Name,Image', '', '::,::', 'Thông tin b?a ?n', '', 'varchar,image', 'required,', ''),
('2020-11-18', 150, 1, 48, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-18', 151, 1, 49, '0', 'id,Name', 'id,Name', 'ID,Name', '::,::', '', '', '', '', ','),
('2020-11-18', 152, 1, 49, '1', 'Name', 'Name', '', '::', 'C?p ??', '', 'varchar', 'required', ''),
('2020-11-18', 153, 1, 49, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-18', 154, 1, 50, '0', 'IdLevel,Title,Image,Calo,TotalTime', 'IdLevel,Title,Image,Calo,TotalTime', 'C?p ??,Tiêu ??,Hình ?nh,Calo,T?ng th?i gian', '49:id:Name,::,::,::,::', '', '', '', '', ',,,,'),
('2020-11-18', 155, 1, 50, '1', 'C?p ??,Tiêu ??,Tiêu ?? ph?,Mô t?,Hình ?nh,Calo,T?ng th?i gian', 'IdLevel,Title,SubTitle,Desciption,Image,Calo,TotalTime', '', '49:id:Name,::,::,::,::,::,::', 'Thông tin th? hình', '', 'select,varchar,varchar,ckeditor,image,number,varchar', 'required,required,required,required,,required,required', ''),
('2020-11-18', 156, 1, 50, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-18', 157, 1, 51, '0', 'id,Name', 'id,Name', 'ID,Name', '::,::', '', '', '', '', ','),
('2020-11-18', 158, 1, 51, '1', 'Name', 'Name', '', '::', 'Tr?ng thái t?p', '', 'varchar', 'required', ''),
('2020-11-18', 159, 1, 51, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-18', 160, 1, 52, '0', 'id,IdUser,IdWorkoutStatus', 'id,IdUser,IdWorkoutStatus', 'ID,Khách hàng,Tr?ng thái', '::,::,::', '', '', '', '', ',,'),
('2020-11-18', 161, 1, 52, '1', 'Khách hàng,Tr?ng thái', 'IdUser,IdWorkoutStatus', '', '31:id:Fullname,51:id:Name', 'Tr?ng thái ng??i dùng t?p luy?n', '', 'select,select', 'required,required', ''),
('2020-11-18', 162, 1, 52, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-18', 163, 1, 53, '0', 'id,IdWorkout,Image,Times', 'id,IdWorkout,Image,Times', 'ID,Bài t?p,Hình ?nh,S? l?n', '::,50:id:Title,::,::', '', '', '', '', ',,,'),
('2020-11-18', 164, 1, 53, '1', 'Bài t?p,Hình ?nh,V? trí,S? l?n', 'IdWorkout,Image,Position,Times', '', '50:id:Title,::,::,::', 'Chi ti?t bài t?p', '', 'select,image,number,number', 'required,,required,required', ''),
('2020-11-18', 165, 1, 53, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-18', 166, 1, 54, '0', 'IdUser,Title,CreatedAt,UpdatedAt', 'IdUser,Title,CreatedAt,UpdatedAt', 'Khách hàng,Tiêu ??,T?o lúc,C?p nh?t lúc', '31:id:Fullname,::,::,::', '', '', '', '', ',,,'),
('2020-11-18', 167, 1, 54, '1', 'Khách hàng,Tiêu ??,Mô t?,T?o lúc,C?p nh?t', 'IdUser,Title,Description,CreatedAt,UpdatedAt', '', '31:id:Fullname,::,::,::,::', 'Thông tin ghi chú', '', 'select,varchar,ckeditor,date,date', 'required,required,required,,', ''),
('2020-11-18', 168, 1, 54, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-26', 169, 2, 55, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2020-11-26', 170, 2, 55, '3', '', '', '', '', '', '', '', '', ''),
('2020-11-26', 171, 2, 55, '1', 'Name', 'Name', '', '::', 'Lớp thông tin', '', 'varchar', '', ''),
('2020-12-01', 172, 3, 56, '0', 'id,Name,Born,Address', 'id,Name,Born,Address', 'ID,Tên,Ngày sinh,Địa chỉ', '::,::,::,::', '', '', '', '', ',,,'),
('2020-12-01', 173, 3, 56, '1', 'Name,Born,Detail,Address', 'Name,Born,Detail,Address', '', '::,::,::,::', 'Thông tin Xuân Thành', '', 'varchar,date,ckeditor,varchar', 'required,required,required,min:required', ''),
('2020-12-01', 174, 3, 56, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-17', 175, 3, 57, '0', 'id,IdXuanThanh,Fullname', 'id,IdXuanThanh,Fullname', 'ID,Xuân thành,Họ và tên', '::,56:id:Name,::', '', '', '', '', ',,'),
('2021-02-17', 176, 3, 57, '1', 'Chọn xuân thành,Họ và tên', 'IdXuanThanh,Fullname', '', '56:id:Name,::', 'Văn thành', '', 'select,varchar', 'required,required', ''),
('2020-12-01', 177, 3, 57, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-07', 178, 4, 58, '0', 'id,ApiURL,Version,AndroidLink,IOSLink', 'id,ApiURL,Version,AndroidLink,IOSLink', 'STT,ApiURL,Version,AndroidLink,IOSLink', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2020-12-07', 179, 4, 58, '1', 'ApiURL,Version,AndroidLink,IOSLink', 'ApiURL,Version,AndroidLink,IOSLink', '', '::,::,::,::', 'Thêm mới', '', 'varchar,varchar,varchar,varchar', 'required,required,required,required', ''),
('2020-12-07', 180, 4, 58, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-07', 181, 4, 59, '0', 'id,Name,MinLevel,MaxLevel', 'id,Name,MinLevel,MaxLevel', 'id,Name,MinLevel,MaxLevel', '::,::,::,::', '', '', '', '', ',,,'),
('2020-12-07', 182, 4, 59, '1', 'Name,MinLevel,MaxLevel', 'Name,MinLevel,MaxLevel', '', '::,::,::', 'Thêm mới', '', 'varchar,number,number', 'required,required,max', ''),
('2020-12-07', 183, 4, 59, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-07', 184, 4, 60, '0', 'id,Name,MinExp,MaxExp', 'id,Name,MinExp,MaxExp', 'id,Name,MinExp,MaxExp', '::,::,::,::', '', '', '', '', ',,,'),
('2020-12-07', 185, 4, 60, '1', 'Name,MinExp,MaxExp', 'Name,MinExp,MaxExp', '', '::,::,::', 'Thêm mới', '', 'varchar,number,number', 'required,required,required', ''),
('2020-12-07', 186, 4, 60, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-07', 187, 4, 61, '0', 'id,IdLevel,IdTitle,Fullname,Gold,Diamond,Exp,CreatedAt,UpdatedAt', 'id,IdLevel,IdTitle,Fullname,Gold,Diamond,Exp,CreatedAt,UpdatedAt', 'STT,IdLevel,IdTitle,Fullname,Gold,Diamond,Exp,CreatedAt,UpdatedAt', '::,60:id:Name,59:id:Name,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2020-12-07', 188, 4, 61, '1', 'IdLevel,IdTitle,Fullname,Avatar,Language,Gold,Diamond,Exp,CreatedAt,UpdatedAt', 'IdLevel,IdTitle,Fullname,Avatar,Language,Gold,Diamond,Exp,CreatedAt,UpdatedAt', '', '60:id:Name,59:id:Name,::,::,::,::,::,::,::,::', 'Thêm mới', '', 'select,select,varchar,varchar,varchar,number,number,number,date,date', 'required,required,required,required,required,required,required,required,required,required', ''),
('2020-12-07', 189, 4, 61, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-07', 190, 4, 62, '0', 'id,Name,Position,IdCharacter,GoldValue,DiamonValue', 'id,Name,Position,IdCharacter,GoldValue,DiamonValue', 'id,Name,Position,IdCharacter,GoldValue,DiamonValue', '::,::,::,69:id:Name,::,::', '', '', '', '', ',,,,,'),
('2020-12-07', 191, 4, 62, '1', 'Name,Position,IdCharacter,GoldValue,DiamonValue', 'Name,Position,IdCharacter,GoldValue,DiamonValue', '', '::,::,69:id:Name,::,::', 'Thêm mới', '', 'varchar,varchar,select,number,number', 'required,required,required,required,required', ''),
('2020-12-07', 192, 4, 62, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-07', 193, 4, 63, '0', 'id,IdGift,Name,Position,Requirment', 'id,IdGift,Name,Position,Requirment', 'id,IdGift,Name,Position,Requirment', '::,62:id:Name,::,::,::', '', '', '', '', ',,,,'),
('2020-12-07', 194, 4, 63, '1', 'IdGift,Name,Position,Requirment', 'IdGift,Name,Position,Requirment', '', '62:id:Name,::,::,::', 'Thêm mới', '', 'select,varchar,varchar,varchar', 'required,required,required,required', ''),
('2020-12-07', 195, 4, 63, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-07', 196, 4, 64, '0', 'id,IdGift,Description,Position,Level,NumberWin,NumberPlay', 'id,IdGift,Description,Position,Level,NumberWin,NumberPlay', 'id,IdGift,Description,Position,Level,NumberWin,NumberPlay', '::,62:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2020-12-07', 197, 4, 64, '1', 'IdGift,Name,Description,Position,Level,NumberWin,NumberPlay', 'IdGift,Name,Description,Position,Level,NumberWin,NumberPlay', '', '62:id:Name,::,::,::,::,::,::', 'Thêm mới', '', 'select,varchar,varchar,varchar,number,number,number', 'required,required,required,required,required,required,required', ''),
('2020-12-07', 198, 4, 64, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-07', 199, 4, 66, '0', 'id,IdUser,IdLogin7Day,IdQuest,CreatedAt', 'id,IdUser,IdLogin7Day,IdQuest,CreatedAt', 'id,IdUser,IdLogin7Day,IdQuest,CreatedAt', '::,61:id:Fullname,63:id:Name,64:id:Name,::', '', '', '', '', ',,,,'),
('2020-12-07', 200, 4, 66, '1', 'IdUser,IdLogin7Day,IdQuest,CreatedAt', 'IdUser,IdLogin7Day,IdQuest,CreatedAt', '', '61:id:Fullname,63:id:Name,64:id:Name,::', 'Thêm mới', '', 'select,select,select,date', 'required,required,required,required', ''),
('2020-12-07', 201, 4, 66, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 202, 4, 67, '0', 'id,Name,Description', 'id,Name,Description', 'id,Name,Description', '::,::,::', '', '', '', '', ',,'),
('2020-12-08', 203, 4, 67, '1', 'Name,Description', 'Name,Description', '', '::,::', 'Thêm mới', '', 'varchar,varchar', 'required,required', ''),
('2020-12-08', 205, 4, 67, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 206, 4, 68, '0', 'id,Name,Url', 'id,Name,Url', 'id,Name,Url', '::,::,::', '', '', '', '', ',,'),
('2020-12-08', 207, 4, 68, '1', 'Name,Url', 'Name,Url', '', '::,::', 'Thêm mới', '', 'varchar,varchar', 'required,required', ''),
('2020-12-08', 208, 4, 68, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 209, 4, 69, '0', 'id,IdClass,IdSprite,Name,Avatar,Price', 'id,IdClass,IdSprite,Name,Avatar,Price', 'id,IdClass,IdSprite,Name,Avatar,Price', '::,67:id:Name,68:id:Name,::,::,::', '', '', '', '', ',,,,,'),
('2020-12-08', 210, 4, 69, '1', 'IdClass,IdSprite,Name,Avatar,BodyImage,Background,UnlockGoldOrigin,UnlockDiamonOrigin,UnlockGold,UnlockDiamon,BaseHP,BaseAttack,BaseDefence,BaseCritical,BaseMovement,Price', 'IdClass,IdSprite,Name,Avatar,BodyImage,Background,UnlockGoldOrigin,UnlockDiamonOrigin,UnlockGold,UnlockDiamon,BaseHP,BaseAttack,BaseDefence,BaseCritical,BaseMovement,Price', '', '67:id:Name,68:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::,::', 'Thêm mới', '', 'select,select,varchar,image,image,image,varchar,varchar,varchar,varchar,number,number,number,number,number,number', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2020-12-08', 211, 4, 69, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 212, 4, 70, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2020-12-08', 213, 4, 70, '1', 'Name', 'Name', '', '::', 'Thêm mới', '', 'varchar', 'required', ''),
('2020-12-08', 214, 4, 71, '0', 'id,IdTypeSkill,IdCharacter,IdSprite,Name,Image,Description,CountDown,HP,Attack,Defence,Critical', 'id,IdTypeSkill,IdCharacter,IdSprite,Name,Image,Description,CountDown,HP,Attack,Defence,Critical', 'id,IdTypeSkill,IdCharacter,IdSprite,Name,Image,Description,CountDown,HP,Attack,Defence,Critical', '::,70:id:Name,69:id:Name,68:id:Name,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2020-12-08', 215, 4, 71, '1', 'IdTypeSkill,IdCharacter,IdSprite,Name,Image,Description,CountDown,HP,Attack,Defence,Critical,Movement,Range,Durration', 'IdTypeSkill,IdCharacter,IdSprite,Name,Image,Description,CountDown,HP,Attack,Defence,Critical,Movement,Range,Durration', '', '70:id:Name,69:id:Name,68:id:Name,::,::,::,::,::,::,::,::,::,::,::', 'Thêm mới', '', 'select,select,select,varchar,image,ckeditor,number,number,number,number,number,number,number,number', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2020-12-08', 216, 4, 71, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 240, 4, 72, '1', 'IdUserAttack,IdUserDefence,IdUserWin,CreatedAt', 'IdUserAttack,IdUserDefence,IdUserWin,CreatedAt', '', '61:id:Fullname,61:id:Fullname,61:id:Fullname,::', 'Thêm mới', '', 'select,select,select,date', 'required,required,required,required', ''),
('2020-12-08', 239, 4, 72, '0', 'id,IdUserAttack,IdUserDefence,IdUserWin,CreatedAt', 'id,IdUserAttack,IdUserDefence,IdUserWin,CreatedAt', 'id,IdUserAttack,IdUserDefence,IdUserWin,CreatedAt', '::,61:id:Fullname,61:id:Fullname,61:id:Fullname,::', '', '', '', '', ',,,,'),
('2020-12-08', 219, 4, 72, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 220, 4, 73, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2020-12-08', 221, 4, 73, '1', 'Name', 'Name', '', '::', 'Thêm mới', '', 'varchar', 'required', ''),
('2020-12-08', 223, 4, 73, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 224, 4, 74, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2020-12-08', 225, 4, 74, '1', 'Name', 'Name', '', '::', 'Thêm mới', '', 'varchar', 'required', ''),
('2020-12-08', 226, 4, 74, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 227, 4, 75, '0', 'id,IdGift,IdTypeStore,IdTypePayment,Name,PriceOrigin,Price,Image,FirstPurchase', 'id,IdGift,IdTypeStore,IdTypePayment,Name,PriceOrigin,Price,Image,FirstPurchase', 'id,IdGift,IdTypeStore,IdTypePayment,Name,PriceOrigin,Price,Image,FirstPurchase', '::,62:id:Name,73:id:Name,74:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2020-12-08', 228, 4, 75, '1', 'IdGift,IdTypeStore,IdTypePayment,Name,PriceOrigin,Price,Image,FirstPurchase', 'IdGift,IdTypeStore,IdTypePayment,Name,PriceOrigin,Price,Image,FirstPurchase', '', '62:id:Name,73:id:Name,74:id:Name,::,::,::,::,::', 'Thêm mới', '', 'select,select,select,varchar,number,number,image,number', 'required,required,required,required,required,required,required,required', ''),
('2020-12-08', 229, 4, 75, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 230, 4, 76, '0', 'id,IdUser,IdStore,CreatedAt', 'id,IdUser,IdStore,CreatedAt', 'id,IdUser,IdStore,CreatedAt', '::,61:id:Fullname,75:id:Name,::', '', '', '', '', ',,,'),
('2020-12-08', 231, 4, 76, '1', 'IdUser,IdStore,CreatedAt', 'IdUser,IdStore,CreatedAt', '', '61:id:Fullname,75:id:Name,::', 'Thêm mới', '', 'select,select,date', 'required,required,required', ''),
('2020-12-08', 232, 4, 76, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 233, 4, 77, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2020-12-08', 234, 4, 77, '1', 'Name', 'Name', '', '::', 'Thêm mới', '', 'varchar', 'required', ''),
('2020-12-08', 235, 4, 77, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-08', 236, 4, 78, '0', 'id,IdUser,IdGift,IdStatusInbox,Subject,Body,CreatedAt', 'id,IdUser,IdGift,IdStatusInbox,Subject,Body,CreatedAt', 'id,IdUser,IdGift,IdStatusInbox,Subject,Body,CreatedAt', '::,61:id:Fullname,62:id:Name,77:id:Name,::,::,::', '', '', '', '', ',,,,,,'),
('2020-12-08', 237, 4, 78, '1', 'IdUser,IdGift,IdStatusInbox,Subject,Body,CreatedAt', 'IdUser,IdGift,IdStatusInbox,Subject,Body,CreatedAt', '', '61:id:Fullname,62:id:Name,77:id:Name,::,::,::', 'Thêm mới', '', 'select,select,select,varchar,ckeditor,date', 'required,required,required,required,required,required', ''),
('2020-12-08', 238, 4, 78, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-10', 241, 1, 79, '0', 'Name,Url,CreatedAt', 'Name,Url,CreatedAt', 'Name,Url,Tạo lúc', '::,::,::', '', '', '', '', ',,'),
('2020-12-10', 242, 1, 79, '1', 'Name,Upload Video,Tạo lúc', 'Name,Url,CreatedAt', '', '::,::,::', 'Upload Video', '', 'varchar,file,date', 'required,,required', ''),
('2020-12-10', 243, 1, 79, '3', '', '', '', '', '', '', '', '', ''),
('2020-12-10', 244, 1, 80, '0', 'IdShopMenu,IdUploadVideo,Position,CreatedAt', 'IdShopMenu,IdUploadVideo,Position,CreatedAt', 'Shop menu,Video,Vị Trí,Tạo lúc', '6:id:Title,79:id:Name,::,::', '', '', '', '', ',,,'),
('2020-12-10', 245, 1, 80, '1', 'Thực đơn,Video,Vị Trí,Tạo lúc', 'IdShopMenu,IdUploadVideo,Position,CreatedAt', '', '6:id:Title,79:id:Name,::,::', 'Thêm Video cho Thực Đơn', '', 'select,select,number,date', ',,required,required', ''),
('2020-12-10', 246, 1, 80, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 247, 5, 84, '0', 'STT,Tên,Email,Phone,Role', 'id,Fullname,Email,Phone,Role', 'id,Fullname,Email,Phone,Role', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-02-18', 248, 5, 84, '1', 'Tên,Email,Phone,Password,Role', 'Fullname,Email,Phone,Password,Role', '', '::,::,::,::,::', 'Thêm mới', '', 'varchar,varchar,varchar,password,varchar', 'required:maxLength,required:email,required:maxLength,nullValidator,nullValidator', ''),
('2021-02-18', 249, 5, 84, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 250, 5, 85, '0', 'STT,Môn học,Giáo viên,Học kì,Lớp', 'id,IdSubject,IdTeacher,IdSemester,IdClass', 'id,IdSubject,IdTeacher,IdSemester,IdClass', '::,87:id:Name,84:id:Fullname,86:id:Name,88:id:Name', '', '', '', '', ',,,,'),
('2021-02-18', 251, 5, 85, '1', 'Môn học,Giảng viên,Học kì,Lớp', 'IdSubject,IdTeacher,IdSemester,IdClass', '', '87:id:Name,84:id:Fullname,86:id:Name,88:id:Name', 'Thêm mới', '', 'select,select,select,select', 'required,required,required,required', ''),
('2021-02-18', 252, 5, 85, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 253, 5, 86, '0', 'STT,Tên,Năm bắt đầu,Năm kết thúc', 'id,Name,StartYear,EndYear', 'id,Name,StartYear,EndYear', '::,::,::,::', '', '', '', '', ',,,'),
('2021-02-18', 254, 5, 86, '1', 'Tên,Năm bắt đầu học kì,Năm kết thúc học kì', 'Name,StartYear,EndYear', '', '::,::,::', 'Thêm mới', '', 'varchar,number,number', 'required:maxLength,required,required', ''),
('2021-02-18', 255, 5, 86, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 256, 5, 87, '0', 'STT,Tên', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-02-18', 257, 5, 87, '1', 'Tên', 'Name', '', '::', 'Thêm mới', '', 'varchar', 'required:maxLength', ''),
('2021-02-18', 258, 5, 87, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 259, 5, 88, '0', 'STT,Tên', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-02-18', 260, 5, 88, '1', 'Tên', 'Name', '', '::', 'Thêm mới', '', 'varchar', 'required:maxLength', ''),
('2021-02-18', 261, 5, 88, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 262, 5, 89, '0', 'STT,Lớp,Mã SV,Tên,Email,Giới tính,Phone,Address', 'id,IdClass,StudentCode,Fullname,Email,Sex,Phone,Address', 'id,IdClass,StudentCode,Fullname,Email,Sex,Phone,Address', '::,88:id:Name,::,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-02-18', 263, 5, 89, '1', 'Lớp,Mã SV,Password,Tên,Email,Giới tính,Phone,Address', 'IdClass,StudentCode,Password,Fullname,Email,Sex,Phone,Address', '', '88:id:Name,::,::,::,::,::,::,::', 'Thêm mới', '', 'select,varchar,password,varchar,email,varchar,varchar,varchar', 'required,required:maxLength,required,required:maxLength,required,required,required:maxLength,required:maxLength', ''),
('2021-02-18', 264, 5, 89, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 265, 5, 90, '0', 'STT,Môn học,Sinh viên,Loại khảo sát,Trạng thái,CreatedAt,Name', 'id,IdSubject,IdStudent,IdSurveyTemplate,Status,CreatedAt,Name', 'id,IdSubject,IdStudent,IdSurveyTemplate,Status,CreatedAt,Name', '::,87:id:Name,89:id:Fullname,91:id:Name,::,::,::', '', '', '', '', ',,,,,,'),
('2021-02-18', 266, 5, 90, '1', 'Môn học,Sinh viên,Loại khảo sát,Trạng thái,CreatedAt,Name', 'IdSubject,IdStudent,IdSurveyTemplate,Status,CreatedAt,Name', '', '87:id:Name,89:id:Fullname,91:id:Name,84:id:Fullname,::,::', 'Thêm mới', '', 'select,select,select,select,datetime,varchar', 'required,required,required,required,required,required:maxLength', ''),
('2021-02-18', 267, 5, 90, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 268, 5, 91, '0', 'STT,Menu,Tên,CreatedAt,UpdateAt', 'id,IdMenu,Name,CreatedAt,UpdateAt', 'id,IdMenu,Name,CreatedAt,UpdateAt', '::,97:id:Name,::,::,::', '', '', '', '', ',,,,'),
('2021-02-18', 269, 5, 91, '1', 'Menu,Tên,CreatedAt,UpdateAt', 'IdMenu,Name,CreatedAt,UpdateAt', '', '97:id:Name,::,::,::', 'Thêm mới', '', 'select,varchar,datetime,datetime', 'required,required:maxLength,required,required', ''),
('2021-02-18', 270, 5, 91, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 271, 5, 92, '0', 'Khảo sát,Loại trả lời,Tên,Vị trí', 'IdSurvey,IdTypeChoose,Name,Position', 'IdSurvey,IdTypeChoose,Name,Position', '90:id:Name,93:id:Name,::,::', '', '', '', '', ',,,'),
('2021-02-18', 272, 5, 92, '1', 'Khảo sát,Loại trả lời,Tên,Vị trí', 'IdSurvey,IdTypeChoose,Name,Position', '', '90:id:Name,93:id:Name,::,::', 'Thêm mới', '', 'select,select,varchar,number', 'required,required,required:maxLength,required', ''),
('2021-02-18', 273, 5, 92, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 274, 5, 93, '0', 'STT,Tên', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-02-18', 275, 5, 93, '1', 'Tên', 'Name', '', '::', 'Thêm mới', '', 'varchar', 'required:maxLength', ''),
('2021-02-18', 276, 5, 93, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 277, 5, 94, '0', 'STT,Câu hỏi,Vị trí,Tên,Nội dung', 'id,IdQuestion,Position,Name,Value', 'id,IdQuestion,Position,Name,Value', '::,92:id:Name,::,::,::', '', '', '', '', ',,,,'),
('2021-02-18', 278, 5, 94, '1', 'Câu hỏi,Vị trí,Tên,Nội dung', 'IdQuestion,Position,Name,Value', '', '92:id:Name,::,::,::', 'Thêm mới', '', 'select,number,varchar,varchar', 'required:maxLength,required,required:maxLength,required:maxLength', ''),
('2021-02-18', 279, 5, 94, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 280, 5, 95, '0', 'STT,Câu hỏi,Sinh viên,Môn học,Trả lời,Nội dung', 'id,IdQuestion,IdStudent,IdSubject,IdReply,Value', 'id,IdQuestion,IdStudent,IdSubject,IdReply,Value', '::,92:id:Name,89:id:Fullname,87:id:Name,94:id:Name,::', '', '', '', '', ',,,,,'),
('2021-02-18', 281, 5, 95, '1', 'Câu hỏi,Sinh viên,Môn học,Trả lời,Nội dung', 'IdQuestion,IdStudent,IdSubject,IdReply,Value', '', '92:id:Name,89:id:Fullname,87:id:Name,94:id:Name,::', 'Thêm mới', '', 'select,select,select,select,varchar', 'required,required,required,required,required:maxLength', ''),
('2021-02-18', 282, 5, 95, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 283, 5, 96, '0', 'STT,TextLeft1,TextLeft2,TextRight,Address,Phone,Email,CreatedAt', 'id,TextLeft1,TextLeft2,TextRight,Address,Phone,Email,CreatedAt', 'id,TextLeft1,TextLeft2,TextRight,Address,Phone,Email,CreatedAt', '::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-02-18', 284, 5, 96, '1', 'TextLeft1,TextLeft2,TextRight,Address,Phone,Email,CreatedAt,UpdateAt', 'TextLeft1,TextLeft2,TextRight,Address,Phone,Email,CreatedAt,UpdateAt', '', '::,::,::,::,::,::,::,::', 'Thêm mới', '', 'varchar,varchar,varchar,varchar,varchar,varchar,date,date', 'required:maxLength,required:maxLength,required:maxLength,required:maxLength,required:maxLength,email,required,required', ''),
('2021-02-18', 285, 5, 96, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 286, 5, 97, '0', 'id,Tên,Link,Vị trí,CreatedAt,UpdateAt', 'id,Name,Url,Arrange,CreatedAt,UpdateAt', 'id,Name,Url,Arrange,CreatedAt,UpdateAt', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-02-18', 287, 5, 97, '1', 'Tên,Loại,Link,Vị trí,CreatedAt,UpdateAt', 'Name,TypeId,Url,Arrange,CreatedAt,UpdateAt', '', '::,84:id:Fullname,::,::,::,::', 'Thêm mới', '', 'varchar,select,varchar,number,datetime,datetime', 'required,required,required:maxLength,required,required,required', ''),
('2021-02-18', 288, 5, 97, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 289, 5, 98, '0', 'STT,Tiêu đề,Nội dung,CreatedAt,UpdateAt', 'id,Title,Content,CreatedAt,UpdateAt', 'id,Title,Content,CreatedAt,UpdateAt', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-02-18', 290, 5, 98, '1', 'Tiêu đề,Nội dung,CreatedAt,UpdateAt', 'Title,Content,CreatedAt,UpdateAt', '', '::,::,::,::', 'Thêm mới', '', 'varchar,ckeditor,datetime,datetime', 'required:maxLength,required,required,required', ''),
('2021-02-18', 291, 5, 98, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 292, 5, 99, '0', 'STT,Tiêu đề,Link,Hình ảnh,CreatedAt,UpdateAt', 'id,Title,Url,Thumbnail,CreatedAt,UpdateAt', 'id,Title,Url,Thumbnail,CreatedAt,UpdateAt', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-02-18', 293, 5, 99, '1', 'Tiêu đề,Link,Hình ảnh,CreatedAt,UpdateAt', 'Title,Url,Thumbnail,CreatedAt,UpdateAt', '', '::,::,::,::,::', 'Thêm mới', '', 'varchar,varchar,image,date,date', 'required:maxLength,required,required,required,required', ''),
('2021-02-18', 294, 5, 99, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 295, 5, 100, '0', 'STT,Tiêu đề,Tóm tắt,Nội dung,Hình ảnh,Link,CreatedAt,UpdateAt', 'id,Titile,Summary,Content,Thumbnail,Url,CreatedAt,UpdateAt', 'id,Titile,Summary,Content,Thumbnail,Url,CreatedAt,UpdateAt', '::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-02-18', 296, 5, 100, '1', 'Tiêu đề,Tóm tắt,Nội dung,Hình ảnh,Link,CreatedAt,UpdateAt', 'Titile,Summary,Content,Thumbnail,Url,CreatedAt,UpdateAt', '', '::,::,::,::,::,::,::', 'Thêm mới', '', 'varchar,varchar,ckeditor,image,varchar,datetime,datetime', 'required:maxLength,required:maxLength,required,required,required,required,required', ''),
('2021-02-18', 297, 5, 100, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 298, 5, 101, '0', 'STT,Cột 1,Cột 2,Cột 3,CopyRight,CreatedAt,UpdateAt', 'id,Col1,Col2,Col3,CopyRight,CreatedAt,UpdateAt', 'id,Col1,Col2,Col3,CopyRight,CreatedAt,UpdateAt', '::,::,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-02-18', 299, 5, 101, '1', 'Cột 1,Cột 2,Cột 3,CopyRight,CreatedAt,UpdateAt', 'Col1,Col2,Col3,CopyRight,CreatedAt,UpdateAt', '', '::,::,::,::,::,::', 'Thêm mới', '', 'ckeditor,ckeditor,ckeditor,varchar,datetime,date', 'required,required,,required:maxLength,required,required', ''),
('2021-02-18', 300, 5, 101, '3', '', '', '', '', '', '', '', '', ''),
('2021-02-18', 301, 5, 102, '0', 'STT,Tiêu đề,Link,Vị trí,CreatedAt,UpdateAt', 'id,Title,Url,Arrange,CreatedAt,UpdateAt', 'id,Title,Url,Arrange,CreatedAt,UpdateAt', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-02-19', 302, 5, 102, '1', 'Tiêu đề,Link,Vị trí,CreatedAt,UpdateAt', 'Title,Url,Arrange,CreatedAt,UpdateAt', '', '::,::,::,::,::', 'Thêm mới', '', 'varchar,image,number,datetime,datetime', 'required:maxLength,required,required,required,required', ''),
('2021-02-18', 303, 5, 102, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3060, 7, 247, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3061, 7, 248, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3062, 7, 248, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSPAYMENT', 'INFOMATION STATUSPAYMENT', 'varchar', 'required', ''),
('2021-06-16', 3063, 7, 249, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3064, 7, 248, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3065, 7, 249, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSTRANSFERCAR', 'INFOMATION STATUSTRANSFERCAR', 'varchar', 'required', ''),
('2021-06-16', 3066, 7, 249, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3067, 7, 251, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3068, 7, 250, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3069, 7, 250, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3070, 7, 250, '1', 'Name', 'Name', '', '::', 'INFOMATION SEX', 'INFOMATION SEX', 'varchar', 'required', '');
INSERT INTO `a1crud_crud` (`createdate`, `id`, `idproject`, `idclass`, `actiontype`, `titles`, `properties`, `alias`, `refers`, `titlecreate`, `titleupdate`, `inputtypes`, `validates`, `formats`) VALUES
('2021-06-16', 3477, 6, 169, '1', 'IdCompany,IdEmployee,IsMultiMobile,IsLocationTracking,IsNoNeedTimekeeping,IsNoConstraintTimekeeping,IsAllowingLatelyCheckinOut,IsAllowingEarlyCheckinOut,IsAutoTimekeeping,IsAutoCheckout,IsClockinUsingImage,IsClockoutUsingImage,IsHelpCheckShift', 'IdCompany,IdEmployee,IsMultiMobile,IsLocationTracking,IsNoNeedTimekeeping,IsNoConstraintTimekeeping,IsAllowingLatelyCheckinOut,IsAllowingEarlyCheckinOut,IsAutoTimekeeping,IsAutoCheckout,IsClockinUsingImage,IsClockoutUsingImage,IsHelpCheckShift', '', '137:id:IdProvince,158:id:IdBranch,::,::,::,::,::,::,::,::,::,::,::', 'INFOMATION TIMEKEEPINGSETTING', 'INFOMATION TIMEKEEPINGSETTING', 'select,select,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3077, 7, 253, '1', 'IdRegisterLearn,Rate,Content', 'IdRegisterLearn,Rate,Content', '', '187:id:IdBranch,::,::', 'INFOMATION REVIEWSTUDENT', 'INFOMATION REVIEWSTUDENT', 'select,varchar,varchar', 'required,required,required', ''),
('2021-06-16', 3479, 6, 138, '0', 'id,IdCompany,IdEmployee,Gender,Birthday,InsuranceId,InsuranceCardId,IsSubmitInsuranceBook,CardId', 'id,IdCompany,IdEmployee,Gender,Birthday,InsuranceId,InsuranceCardId,IsSubmitInsuranceBook,CardId', 'id,IdCompany,IdEmployee,Gender,Birthday,InsuranceId,InsuranceCardId,IsSubmitInsuranceBook,CardId', '::,137:id:IdProvince,158:id:IdBranch,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3478, 6, 169, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3071, 7, 251, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSPASS', 'INFOMATION STATUSPASS', 'varchar', 'required', ''),
('2021-06-16', 3072, 7, 251, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3073, 7, 252, '1', 'IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note,IdLocationExam', 'IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note,IdLocationExam', '', '174:id:Name,175:id:Name,191:id:IdStatusStudent,182:id:IdLocationExam,251:id:Name,::,::,::,181:id:Name', 'INFOMATION DRIVINGTEST', 'INFOMATION DRIVINGTEST', 'select,select,select,select,select,date,varchar,varchar,select', 'required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3074, 7, 252, '0', 'id,IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note,IdLocationExam', 'id,IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note,IdLocationExam', 'id,IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note,IdLocationExam', '::,174:id:Name,175:id:Name,191:id:IdStatusStudent,182:id:IdLocationExam,251:id:Name,::,::,::,181:id:Name', '', '', '', '', ',,,,,,,,,'),
('2021-07-20', 3622, 13, 323, '1', 'User_Id,Field_Id,Book_Day,Start,End,Status,Total,Message', 'User_Id,Field_Id,Book_Day,Start,End,Status,Total,Message', '', '320:id:Name,322:id:Name,::,::,::,::,::,::', '', '', 'number,number,date,datetime,datetime,radio,number,varchar', 'required,required,required,required,required,required,required,', ''),
('2021-06-16', 3480, 6, 138, '1', 'IdCompany,IdEmployee,Gender,Birthday,InsuranceId,InsuranceCardId,IsSubmitInsuranceBook,CardId', 'IdCompany,IdEmployee,Gender,Birthday,InsuranceId,InsuranceCardId,IsSubmitInsuranceBook,CardId', '', '137:id:IdProvince,158:id:IdBranch,::,::,::,::,::,::', 'INFOMATION INSURANCE', 'INFOMATION INSURANCE', 'select,select,varchar,date,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3457, 6, 163, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3458, 6, 162, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3459, 6, 163, '1', 'Name', 'Name', '', '::', 'INFOMATION ACADEMICLEVEL', 'INFOMATION ACADEMICLEVEL', 'varchar', 'required', ''),
('2021-06-16', 3460, 6, 163, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3461, 6, 164, '0', 'id,Name,Type', 'id,Name,Type', 'id,Name,Type', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 3462, 6, 164, '1', 'Name,Type', 'Name,Type', '', '::,::', 'INFOMATION PROVINCE', 'INFOMATION PROVINCE', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 3463, 6, 164, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3464, 6, 165, '0', 'id,IdProvince,Name,Type', 'id,IdProvince,Name,Type', 'id,IdProvince,Name,Type', '::,164:id:Name,::,::', '', '', '', '', ',,,'),
('2021-06-16', 3465, 6, 165, '1', 'IdProvince,Name,Type', 'IdProvince,Name,Type', '', '164:id:Name,::,::', 'INFOMATION DISTRICT', 'INFOMATION DISTRICT', 'select,varchar,varchar', 'required,required,required', ''),
('2021-06-16', 3466, 6, 165, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3467, 6, 166, '0', 'id,IdCompany,IdProvince,IdDistrict,Name,Address,Phone,SortIndex,Note', 'id,IdCompany,IdProvince,IdDistrict,Name,Address,Phone,SortIndex,Note', 'id,IdCompany,IdProvince,IdDistrict,Name,Address,Phone,SortIndex,Note', '::,137:id:IdProvince,164:id:Name,165:id:IdProvince,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3468, 6, 166, '1', 'IdCompany,IdProvince,IdDistrict,Name,Address,Phone,SortIndex,Note', 'IdCompany,IdProvince,IdDistrict,Name,Address,Phone,SortIndex,Note', '', '137:id:IdProvince,164:id:Name,165:id:IdProvince,::,::,::,::,::', 'INFOMATION BRANCH', 'INFOMATION BRANCH', 'select,select,select,varchar,varchar,varchar,number,varchar', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3469, 6, 166, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3470, 6, 167, '0', 'id,IdCompany,Name', 'id,IdCompany,Name', 'id,IdCompany,Name', '::,137:id:IdProvince,::', '', '', '', '', ',,'),
('2021-06-16', 3471, 6, 167, '1', 'IdCompany,Name', 'IdCompany,Name', '', '137:id:IdProvince,::', 'INFOMATION ANNOUNCEMENTTYPE', 'INFOMATION ANNOUNCEMENTTYPE', 'select,varchar', 'required,required', ''),
('2021-06-16', 3472, 6, 167, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3473, 6, 168, '1', 'IdInformation,IdEmployee,Status,IdCompany', 'IdInformation,IdEmployee,Status,IdCompany', '', '107:id:IdCompany,158:id:IdBranch,::,137:id:IdProvince', 'INFOMATION VIEWINFORMATION', 'INFOMATION VIEWINFORMATION', 'select,select,varchar,select', 'required,required,required,required', ''),
('2021-06-16', 3474, 6, 168, '0', 'id,IdInformation,IdEmployee,Status,IdCompany', 'id,IdInformation,IdEmployee,Status,IdCompany', 'id,IdInformation,IdEmployee,Status,IdCompany', '::,107:id:IdCompany,158:id:IdBranch,::,137:id:IdProvince', '', '', '', '', ',,,,'),
('2021-06-16', 3453, 6, 161, '1', 'IdCompany,IdAcademicLevel,Name,SortIndex,Experience,Description', 'IdCompany,IdAcademicLevel,Name,SortIndex,Experience,Description', '', '137:id:IdProvince,163:id:Name,::,::,::,::', 'INFOMATION POSITION', 'INFOMATION POSITION', 'select,select,varchar,number,varchar,varchar', 'required,required,required,required,required,required', ''),
('2021-06-16', 3454, 6, 161, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3455, 6, 162, '0', 'id,IdCompany,Name,Description,SortIndex', 'id,IdCompany,Name,Description,SortIndex', 'id,IdCompany,Name,Description,SortIndex', '::,137:id:IdProvince,::,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 3456, 6, 162, '1', 'IdCompany,Name,Description,SortIndex', 'IdCompany,Name,Description,SortIndex', '', '137:id:IdProvince,::,::,::', 'INFOMATION DEPARTMENT', 'INFOMATION DEPARTMENT', 'select,varchar,varchar,number', 'required,required,required,required', ''),
('2021-06-16', 3438, 6, 147, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3439, 6, 158, '0', 'id,IdBranch,IdDepartment,IdPosition,IdCompany,IdRole,IdEmployeeActive,Fullname,Phone,Email,Image,Birthday,Gender,SortIndex,CardIdentity,DateRelease,PlaceRelease,PassportCard,PassportDate,PassportExpiryDate,PassportIssueIn,BankAccountName,BankAccount,BankName,BankAddress,Password', 'id,IdBranch,IdDepartment,IdPosition,IdCompany,IdRole,IdEmployeeActive,Fullname,Phone,Email,Image,Birthday,Gender,SortIndex,CardIdentity,DateRelease,PlaceRelease,PassportCard,PassportDate,PassportExpiryDate,PassportIssueIn,BankAccountName,BankAccount,BankName,BankAddress,Password', 'id,IdBranch,IdDepartment,IdPosition,IdCompany,IdRole,IdEmployeeActive,Fullname,Phone,Email,Image,Birthday,Gender,SortIndex,CardIdentity,DateRelease,PlaceRelease,PassportCard,PassportDate,PassportExpiryDate,PassportIssueIn,BankAccountName,BankAccount,BankName,BankAddress,Password', '::,166:id:IdCompany,162:id:IdCompany,161:id:IdCompany,137:id:IdProvince,160:id:DisplayName,159:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,,,,,,,,,,,,'),
('2021-06-16', 3475, 6, 168, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3476, 6, 169, '0', 'id,IdCompany,IdEmployee,IsMultiMobile,IsLocationTracking,IsNoNeedTimekeeping,IsNoConstraintTimekeeping,IsAllowingLatelyCheckinOut,IsAllowingEarlyCheckinOut,IsAutoTimekeeping,IsAutoCheckout,IsClockinUsingImage,IsClockoutUsingImage,IsHelpCheckShift', 'id,IdCompany,IdEmployee,IsMultiMobile,IsLocationTracking,IsNoNeedTimekeeping,IsNoConstraintTimekeeping,IsAllowingLatelyCheckinOut,IsAllowingEarlyCheckinOut,IsAutoTimekeeping,IsAutoCheckout,IsClockinUsingImage,IsClockoutUsingImage,IsHelpCheckShift', 'id,IdCompany,IdEmployee,IsMultiMobile,IsLocationTracking,IsNoNeedTimekeeping,IsNoConstraintTimekeeping,IsAllowingLatelyCheckinOut,IsAllowingEarlyCheckinOut,IsAutoTimekeeping,IsAutoCheckout,IsClockinUsingImage,IsClockoutUsingImage,IsHelpCheckShift', '::,137:id:IdProvince,158:id:IdBranch,::,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,'),
('2021-06-16', 3447, 6, 148, '1', 'IdCompany,IdPayType,Name,Money', 'IdCompany,IdPayType,Name,Money', '', '137:id:IdProvince,147:id:Name,::,::', 'INFOMATION PAYROLL', 'INFOMATION PAYROLL', 'select,select,varchar,varchar', 'required,required,required,required', ''),
('2021-06-16', 3451, 6, 160, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3452, 6, 161, '0', 'id,IdCompany,IdAcademicLevel,Name,SortIndex,Experience,Description', 'id,IdCompany,IdAcademicLevel,Name,SortIndex,Experience,Description', 'id,IdCompany,IdAcademicLevel,Name,SortIndex,Experience,Description', '::,137:id:IdProvince,163:id:Name,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-16', 3450, 6, 160, '1', 'DisplayName', 'DisplayName', '', '::', 'INFOMATION ROLE', 'INFOMATION ROLE', 'varchar', 'required', ''),
('2021-06-16', 3449, 6, 160, '0', 'id,DisplayName', 'id,DisplayName', 'id,DisplayName', '::,::', '', '', '', '', ','),
('2021-06-16', 3448, 6, 159, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3446, 6, 159, '1', 'Name', 'Name', '', '::', 'INFOMATION EMPLOYEEACTIVE', 'INFOMATION EMPLOYEEACTIVE', 'varchar', 'required', ''),
('2021-06-16', 3445, 6, 149, '1', 'IdCompany,IdEmployee,Height,Weight,Blood,Congenital,HealthyStatus,HealthLastDate', 'IdCompany,IdEmployee,Height,Weight,Blood,Congenital,HealthyStatus,HealthLastDate', '', '137:id:IdProvince,158:id:IdBranch,::,::,::,::,::,::', 'INFOMATION HEALTHYSTATUS', 'INFOMATION HEALTHYSTATUS', 'select,select,varchar,varchar,varchar,varchar,varchar,date', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3444, 6, 148, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3443, 6, 159, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3436, 6, 157, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3437, 6, 148, '0', 'id,IdCompany,IdPayType,Name,Money', 'id,IdCompany,IdPayType,Name,Money', 'id,IdCompany,IdPayType,Name,Money', '::,137:id:IdProvince,147:id:Name,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 3424, 6, 146, '0', 'id,IdCompany,Name,Money,IsPersonalIncomeTax,IsInsurance', 'id,IdCompany,Name,Money,IsPersonalIncomeTax,IsInsurance', 'id,IdCompany,Name,Money,IsPersonalIncomeTax,IsInsurance', '::,137:id:IdProvince,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-06-16', 3425, 6, 155, '1', 'IdCompany,IdEmployee,IdWorkType,LabourStartDate,WorkingDate,ExpiredDate,CheckinLate,CheckoutEarly,Note,QuitReason,QuitDate', 'IdCompany,IdEmployee,IdWorkType,LabourStartDate,WorkingDate,ExpiredDate,CheckinLate,CheckoutEarly,Note,QuitReason,QuitDate', '', '137:id:IdProvince,158:id:IdBranch,157:id:Name,::,::,::,::,::,::,::,::', 'INFOMATION WORKINGTIME', 'INFOMATION WORKINGTIME', 'select,select,select,date,date,date,varchar,varchar,varchar,varchar,date', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3441, 6, 158, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3442, 6, 149, '0', 'id,IdCompany,IdEmployee,Height,Weight,Blood,Congenital,HealthyStatus,HealthLastDate', 'id,IdCompany,IdEmployee,Height,Weight,Blood,Congenital,HealthyStatus,HealthLastDate', 'id,IdCompany,IdEmployee,Height,Weight,Blood,Congenital,HealthyStatus,HealthLastDate', '::,137:id:IdProvince,158:id:IdBranch,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3440, 6, 158, '1', 'IdBranch,IdDepartment,IdPosition,IdCompany,IdRole,IdEmployeeActive,Fullname,Phone,Email,Image,Birthday,Gender,SortIndex,CardIdentity,DateRelease,PlaceRelease,PassportCard,PassportDate,PassportExpiryDate,PassportIssueIn,BankAccountName,BankAccount,BankName,BankAddress,Password', 'IdBranch,IdDepartment,IdPosition,IdCompany,IdRole,IdEmployeeActive,Fullname,Phone,Email,Image,Birthday,Gender,SortIndex,CardIdentity,DateRelease,PlaceRelease,PassportCard,PassportDate,PassportExpiryDate,PassportIssueIn,BankAccountName,BankAccount,BankName,BankAddress,Password', '', '166:id:IdCompany,162:id:IdCompany,161:id:IdCompany,137:id:IdProvince,160:id:DisplayName,159:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::', 'INFOMATION EMPLOYEE', 'INFOMATION EMPLOYEE', 'select,select,select,select,select,select,varchar,varchar,varchar,varchar,date,varchar,number,varchar,date,varchar,varchar,date,date,varchar,varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3435, 6, 157, '1', 'Name', 'Name', '', '::', 'INFOMATION WORKTYPE', 'INFOMATION WORKTYPE', 'varchar', 'required', ''),
('2021-06-16', 3433, 6, 156, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3434, 6, 157, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3427, 6, 146, '1', 'IdCompany,Name,Money,IsPersonalIncomeTax,IsInsurance', 'IdCompany,Name,Money,IsPersonalIncomeTax,IsInsurance', '', '137:id:IdProvince,::,::,::,::', 'INFOMATION ALLOWANCE', 'INFOMATION ALLOWANCE', 'select,varchar,varchar,varchar,varchar', 'required,required,required,required,required', ''),
('2021-06-16', 3428, 6, 146, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3429, 6, 155, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3430, 6, 156, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3431, 6, 156, '1', 'Name', 'Name', '', '::', 'INFOMATION MAJOR', 'INFOMATION MAJOR', 'varchar', 'required', ''),
('2021-06-16', 3432, 6, 147, '1', 'Name', 'Name', '', '::', 'INFOMATION PAYTYPE', 'INFOMATION PAYTYPE', 'varchar', 'required', ''),
('2021-06-16', 3426, 6, 147, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3423, 6, 145, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3422, 6, 155, '0', 'id,IdCompany,IdEmployee,IdWorkType,LabourStartDate,WorkingDate,ExpiredDate,CheckinLate,CheckoutEarly,Note,QuitReason,QuitDate', 'id,IdCompany,IdEmployee,IdWorkType,LabourStartDate,WorkingDate,ExpiredDate,CheckinLate,CheckoutEarly,Note,QuitReason,QuitDate', 'id,IdCompany,IdEmployee,IdWorkType,LabourStartDate,WorkingDate,ExpiredDate,CheckinLate,CheckoutEarly,Note,QuitReason,QuitDate', '::,137:id:IdProvince,158:id:IdBranch,157:id:Name,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2021-06-16', 3418, 6, 154, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3419, 6, 144, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3420, 6, 145, '0', 'id,IdCompany,Name,Money', 'id,IdCompany,Name,Money', 'id,IdCompany,Name,Money', '::,137:id:IdProvince,::,::', '', '', '', '', ',,,'),
('2021-06-16', 3421, 6, 145, '1', 'IdCompany,Name,Money', 'IdCompany,Name,Money', '', '137:id:IdProvince,::,::', 'INFOMATION SALARYCOEFFICIENT', 'INFOMATION SALARYCOEFFICIENT', 'select,varchar,varchar', 'required,required,required', ''),
('2021-06-16', 3415, 6, 154, '1', 'IdCompany,IdEmployee,IdAcademicLevel,IdMajor,TrainingPlace,FinishDate,Note', 'IdCompany,IdEmployee,IdAcademicLevel,IdMajor,TrainingPlace,FinishDate,Note', '', '137:id:IdProvince,158:id:IdBranch,163:id:Name,156:id:Name,::,::,::', 'INFOMATION QUALIFICATION', 'INFOMATION QUALIFICATION', 'select,select,select,select,varchar,date,varchar', 'required,required,required,required,required,required,required', ''),
('2021-06-16', 3417, 6, 144, '1', 'IdCompany,IdEmployee,IdPayType,IdPayroll,IdAllowance,IdSalaryCoefficient,Salary', 'IdCompany,IdEmployee,IdPayType,IdPayroll,IdAllowance,IdSalaryCoefficient,Salary', '', '137:id:IdProvince,158:id:IdBranch,147:id:Name,148:id:IdCompany,146:id:IdCompany,145:id:IdCompany,::', 'INFOMATION SALARYALLOWANCE', 'INFOMATION SALARYALLOWANCE', 'select,select,select,select,select,select,varchar', 'required,required,required,required,required,required,required', ''),
('2021-06-16', 3416, 6, 144, '0', 'id,IdCompany,IdEmployee,IdPayType,IdPayroll,IdAllowance,IdSalaryCoefficient,Salary', 'id,IdCompany,IdEmployee,IdPayType,IdPayroll,IdAllowance,IdSalaryCoefficient,Salary', 'id,IdCompany,IdEmployee,IdPayType,IdPayroll,IdAllowance,IdSalaryCoefficient,Salary', '::,137:id:IdProvince,158:id:IdBranch,147:id:Name,148:id:IdCompany,146:id:IdCompany,145:id:IdCompany,::', '', '', '', '', ',,,,,,,'),
('2021-06-16', 3411, 6, 153, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3412, 6, 143, '1', 'IdEmployee,IdCompany,TaxCode,Union,Ethnic,Religion,IdMarried,Note', 'IdEmployee,IdCompany,TaxCode,Union,Ethnic,Religion,IdMarried,Note', '', '158:id:IdBranch,137:id:IdProvince,::,::,::,::,142:id:Name,::', 'INFOMATION OTHERINFO', 'INFOMATION OTHERINFO', 'select,select,varchar,varchar,varchar,varchar,select,varchar', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3413, 6, 143, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3414, 6, 154, '0', 'id,IdCompany,IdEmployee,IdAcademicLevel,IdMajor,TrainingPlace,FinishDate,Note', 'id,IdCompany,IdEmployee,IdAcademicLevel,IdMajor,TrainingPlace,FinishDate,Note', 'id,IdCompany,IdEmployee,IdAcademicLevel,IdMajor,TrainingPlace,FinishDate,Note', '::,137:id:IdProvince,158:id:IdBranch,163:id:Name,156:id:Name,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-06-16', 3392, 6, 140, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3393, 6, 151, '0', 'id,IdCompany,IdEmployee,WorkPlace,Position,WorkDays,FromDate,ToDate', 'id,IdCompany,IdEmployee,WorkPlace,Position,WorkDays,FromDate,ToDate', 'id,IdCompany,IdEmployee,WorkPlace,Position,WorkDays,FromDate,ToDate', '::,137:id:IdProvince,158:id:IdBranch,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-06-16', 3394, 6, 139, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3395, 6, 151, '1', 'IdCompany,IdEmployee,WorkPlace,Position,WorkDays,FromDate,ToDate', 'IdCompany,IdEmployee,WorkPlace,Position,WorkDays,FromDate,ToDate', '', '137:id:IdProvince,158:id:IdBranch,::,::,::,::,::', 'INFOMATION WORKHISTORY', 'INFOMATION WORKHISTORY', 'select,select,varchar,varchar,number,date,date', 'required,required,required,required,required,required,required', ''),
('2021-06-16', 3396, 6, 151, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3397, 6, 140, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3398, 6, 152, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3399, 6, 141, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3400, 6, 152, '1', 'Name', 'Name', '', '::', 'INFOMATION SPECIALIZEDCERTIFICATETYPE', 'INFOMATION SPECIALIZEDCERTIFICATETYPE', 'varchar', 'required', ''),
('2021-06-16', 3401, 6, 140, '1', 'Name', 'Name', '', '::', 'INFOMATION CONTRACTTYPE', 'INFOMATION CONTRACTTYPE', 'varchar', 'required', ''),
('2021-06-16', 3402, 6, 152, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3403, 6, 153, '0', 'id,IdCompany,IdEmployee,IdSpecializedCertificateType,Name,FinishPlace,FinishDate,Note', 'id,IdCompany,IdEmployee,IdSpecializedCertificateType,Name,FinishPlace,FinishDate,Note', 'id,IdCompany,IdEmployee,IdSpecializedCertificateType,Name,FinishPlace,FinishDate,Note', '::,137:id:IdProvince,158:id:IdBranch,152:id:Name,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-06-16', 3404, 6, 141, '1', 'Name', 'Name', '', '::', 'INFOMATION PERSONALINCOMETAXTYPE', 'INFOMATION PERSONALINCOMETAXTYPE', 'varchar', 'required', ''),
('2021-06-16', 3405, 6, 142, '1', 'Name', 'Name', '', '::', 'INFOMATION MARRIED', 'INFOMATION MARRIED', 'varchar', 'required', ''),
('2021-06-16', 3406, 6, 142, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3407, 6, 142, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3408, 6, 143, '0', 'id,IdEmployee,IdCompany,TaxCode,Union,Ethnic,Religion,IdMarried,Note', 'id,IdEmployee,IdCompany,TaxCode,Union,Ethnic,Religion,IdMarried,Note', 'id,IdEmployee,IdCompany,TaxCode,Union,Ethnic,Religion,IdMarried,Note', '::,158:id:IdBranch,137:id:IdProvince,::,::,::,::,142:id:Name,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3409, 6, 153, '1', 'IdCompany,IdEmployee,IdSpecializedCertificateType,Name,FinishPlace,FinishDate,Note', 'IdCompany,IdEmployee,IdSpecializedCertificateType,Name,FinishPlace,FinishDate,Note', '', '137:id:IdProvince,158:id:IdBranch,152:id:Name,::,::,::,::', 'INFOMATION SPECIALIZEDCERTIFICATE', 'INFOMATION SPECIALIZEDCERTIFICATE', 'select,select,select,varchar,varchar,date,varchar', 'required,required,required,required,required,required,required', ''),
('2021-06-16', 3391, 6, 139, '1', 'IdCompany,IdEmployee,IdContractType,IdPersonalIncomeTaxType,IdWorkType,IdPayType,ContractNumber,Signer,SignDate,ExpirationDate,TrainWorkTime,Salary,Manager,WorkLocation,WorkingDate,Settlement,Attachment,Note', 'IdCompany,IdEmployee,IdContractType,IdPersonalIncomeTaxType,IdWorkType,IdPayType,ContractNumber,Signer,SignDate,ExpirationDate,TrainWorkTime,Salary,Manager,WorkLocation,WorkingDate,Settlement,Attachment,Note', '', '137:id:IdProvince,158:id:IdBranch,140:id:Name,141:id:Name,157:id:Name,147:id:Name,::,::,::,::,::,::,::,::,::,::,::,::', 'INFOMATION CONTRACT', 'INFOMATION CONTRACT', 'select,select,select,select,select,select,varchar,varchar,date,date,number,varchar,varchar,varchar,date,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3410, 6, 141, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3385, 6, 138, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3386, 6, 150, '1', 'IdCompany,IdEmployee,IdProvince,IdDistrict,Address,Skype,Facebook,EmergencyContact,EmergencyRelate,EmergencyTel,EmergencyPhone,EmergencyAddress', 'IdCompany,IdEmployee,IdProvince,IdDistrict,Address,Skype,Facebook,EmergencyContact,EmergencyRelate,EmergencyTel,EmergencyPhone,EmergencyAddress', '', '137:id:IdProvince,158:id:IdBranch,164:id:Name,165:id:IdProvince,::,::,::,::,::,::,::,::', 'INFOMATION CONTACTINFO', 'INFOMATION CONTACTINFO', 'select,select,select,select,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3387, 6, 149, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3388, 6, 150, '0', 'id,IdCompany,IdEmployee,IdProvince,IdDistrict,Address,Skype,Facebook,EmergencyContact,EmergencyRelate,EmergencyTel,EmergencyPhone,EmergencyAddress', 'id,IdCompany,IdEmployee,IdProvince,IdDistrict,Address,Skype,Facebook,EmergencyContact,EmergencyRelate,EmergencyTel,EmergencyPhone,EmergencyAddress', 'id,IdCompany,IdEmployee,IdProvince,IdDistrict,Address,Skype,Facebook,EmergencyContact,EmergencyRelate,EmergencyTel,EmergencyPhone,EmergencyAddress', '::,137:id:IdProvince,158:id:IdBranch,164:id:Name,165:id:IdProvince,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,'),
('2021-06-16', 3389, 6, 150, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3390, 6, 139, '0', 'id,IdCompany,IdEmployee,IdContractType,IdPersonalIncomeTaxType,IdWorkType,IdPayType,ContractNumber,Signer,SignDate,ExpirationDate,TrainWorkTime,Salary,Manager,WorkLocation,WorkingDate,Settlement,Attachment,Note', 'id,IdCompany,IdEmployee,IdContractType,IdPersonalIncomeTaxType,IdWorkType,IdPayType,ContractNumber,Signer,SignDate,ExpirationDate,TrainWorkTime,Salary,Manager,WorkLocation,WorkingDate,Settlement,Attachment,Note', 'id,IdCompany,IdEmployee,IdContractType,IdPersonalIncomeTaxType,IdWorkType,IdPayType,ContractNumber,Signer,SignDate,ExpirationDate,TrainWorkTime,Salary,Manager,WorkLocation,WorkingDate,Settlement,Attachment,Note', '::,137:id:IdProvince,158:id:IdBranch,140:id:Name,141:id:Name,157:id:Name,147:id:Name,::,::,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,,,,,'),
('2021-06-16', 3382, 6, 134, '1', 'IdCompany,IdShift,StartCheckinHour,StartCheckinMinute,EndCheckinHour,EndCheckinMinute,StartCheckoutHour,StartCheckoutMinute,EndCheckoutHour,EndCheckoutMinute,MinimumWorkingHour', 'IdCompany,IdShift,StartCheckinHour,StartCheckinMinute,EndCheckinHour,EndCheckinMinute,StartCheckoutHour,StartCheckoutMinute,EndCheckoutHour,EndCheckoutMinute,MinimumWorkingHour', '', '137:id:IdProvince,135:id:IdCompany,::,::,::,::,::,::,::,::,::', 'INFOMATION ADVANCEDTIMEKEEPINGSETUP', 'INFOMATION ADVANCEDTIMEKEEPINGSETUP', 'select,select,number,number,number,number,number,number,number,number,varchar', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3381, 6, 135, '0', 'id,IdCompany,Name,Coefficient,StartHour,StartMinute,EndHour,EndMinute,RestStartHour,RestStartMinute,RestEndHour,RestEndMinute,CheckinStartHour,CheckinStartMinute,CheckinEndHour,CheckinEndMinute,CheckoutStartHour,CheckoutStartMinute,CheckoutEndHour,CheckoutEndMinute,IsOvertimeShift', 'id,IdCompany,Name,Coefficient,StartHour,StartMinute,EndHour,EndMinute,RestStartHour,RestStartMinute,RestEndHour,RestEndMinute,CheckinStartHour,CheckinStartMinute,CheckinEndHour,CheckinEndMinute,CheckoutStartHour,CheckoutStartMinute,CheckoutEndHour,CheckoutEndMinute,IsOvertimeShift', 'id,IdCompany,Name,Coefficient,StartHour,StartMinute,EndHour,EndMinute,RestStartHour,RestStartMinute,RestEndHour,RestEndMinute,CheckinStartHour,CheckinStartMinute,CheckinEndHour,CheckinEndMinute,CheckoutStartHour,CheckoutStartMinute,CheckoutEndHour,CheckoutEndMinute,IsOvertimeShift', '::,137:id:IdProvince,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,,,,,,,'),
('2021-06-16', 3383, 6, 134, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3384, 6, 135, '1', 'IdCompany,Name,Coefficient,StartHour,StartMinute,EndHour,EndMinute,RestStartHour,RestStartMinute,RestEndHour,RestEndMinute,CheckinStartHour,CheckinStartMinute,CheckinEndHour,CheckinEndMinute,CheckoutStartHour,CheckoutStartMinute,CheckoutEndHour,CheckoutEndMinute,IsOvertimeShift', 'IdCompany,Name,Coefficient,StartHour,StartMinute,EndHour,EndMinute,RestStartHour,RestStartMinute,RestEndHour,RestEndMinute,CheckinStartHour,CheckinStartMinute,CheckinEndHour,CheckinEndMinute,CheckoutStartHour,CheckoutStartMinute,CheckoutEndHour,CheckoutEndMinute,IsOvertimeShift', '', '137:id:IdProvince,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::', 'INFOMATION SHIFT', 'INFOMATION SHIFT', 'select,varchar,varchar,number,number,number,number,number,number,number,number,number,number,number,number,number,number,number,number,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3379, 6, 137, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3380, 6, 134, '0', 'id,IdCompany,IdShift,StartCheckinHour,StartCheckinMinute,EndCheckinHour,EndCheckinMinute,StartCheckoutHour,StartCheckoutMinute,EndCheckoutHour,EndCheckoutMinute,MinimumWorkingHour', 'id,IdCompany,IdShift,StartCheckinHour,StartCheckinMinute,EndCheckinHour,EndCheckinMinute,StartCheckoutHour,StartCheckoutMinute,EndCheckoutHour,EndCheckoutMinute,MinimumWorkingHour', 'id,IdCompany,IdShift,StartCheckinHour,StartCheckinMinute,EndCheckinHour,EndCheckinMinute,StartCheckoutHour,StartCheckoutMinute,EndCheckoutHour,EndCheckoutMinute,MinimumWorkingHour', '::,137:id:IdProvince,135:id:IdCompany,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2021-06-16', 3365, 6, 131, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3366, 6, 131, '1', 'IdCompany,IdEmployee,ModelTrain,CreatedAt', 'IdCompany,IdEmployee,ModelTrain,CreatedAt', '', '137:id:IdProvince,158:id:IdBranch,::,::', 'INFOMATION MODELUSERFACE', 'INFOMATION MODELUSERFACE', 'select,select,varchar,date', 'required,required,required,required', ''),
('2021-06-16', 3367, 6, 132, '0', 'id,IdCompany,IdShift,LatelyCheckin,MaxLateCheckinMinute,EarlyCheckout,MinSoonCheckinMinute', 'id,IdCompany,IdShift,LatelyCheckin,MaxLateCheckinMinute,EarlyCheckout,MinSoonCheckinMinute', 'id,IdCompany,IdShift,LatelyCheckin,MaxLateCheckinMinute,EarlyCheckout,MinSoonCheckinMinute', '::,137:id:IdProvince,135:id:IdCompany,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-16', 3368, 6, 132, '1', 'IdCompany,IdShift,LatelyCheckin,MaxLateCheckinMinute,EarlyCheckout,MinSoonCheckinMinute', 'IdCompany,IdShift,LatelyCheckin,MaxLateCheckinMinute,EarlyCheckout,MinSoonCheckinMinute', '', '137:id:IdProvince,135:id:IdCompany,::,::,::,::', 'INFOMATION CHECKINLATEANDCHECKOUTSOONSETUP', 'INFOMATION CHECKINLATEANDCHECKOUTSOONSETUP', 'select,select,number,number,number,number', 'required,required,required,required,required,required', ''),
('2021-06-16', 3352, 6, 126, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3353, 6, 127, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3354, 6, 127, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3355, 6, 128, '0', 'id,IdCompany,IdEmployee,IdShift,CheckinLat,CheckinLng,CheckinTime,CheckinFace,CheckinFaceStatus,CheckoutLat,CheckoutLng,CheckoutFace,CheckoutFaceStatus,CheckoutTime,IdTimekeepingStatus,CreatedAt', 'id,IdCompany,IdEmployee,IdShift,CheckinLat,CheckinLng,CheckinTime,CheckinFace,CheckinFaceStatus,CheckoutLat,CheckoutLng,CheckoutFace,CheckoutFaceStatus,CheckoutTime,IdTimekeepingStatus,CreatedAt', 'id,IdCompany,IdEmployee,IdShift,CheckinLat,CheckinLng,CheckinTime,CheckinFace,CheckinFaceStatus,CheckoutLat,CheckoutLng,CheckoutFace,CheckoutFaceStatus,CheckoutTime,IdTimekeepingStatus,CreatedAt', '::,137:id:IdProvince,158:id:IdBranch,135:id:IdCompany,::,::,::,::,::,::,::,::,::,::,129:id:Name,::', '', '', '', '', ',,,,,,,,,,,,,,,'),
('2021-06-16', 3356, 6, 128, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3357, 6, 128, '1', 'IdCompany,IdEmployee,IdShift,CheckinLat,CheckinLng,CheckinTime,CheckinFace,CheckinFaceStatus,CheckoutLat,CheckoutLng,CheckoutFace,CheckoutFaceStatus,CheckoutTime,IdTimekeepingStatus,CreatedAt', 'IdCompany,IdEmployee,IdShift,CheckinLat,CheckinLng,CheckinTime,CheckinFace,CheckinFaceStatus,CheckoutLat,CheckoutLng,CheckoutFace,CheckoutFaceStatus,CheckoutTime,IdTimekeepingStatus,CreatedAt', '', '137:id:IdProvince,158:id:IdBranch,135:id:IdCompany,::,::,::,::,::,::,::,::,::,::,129:id:Name,::', 'INFOMATION TIMEKEEPING', 'INFOMATION TIMEKEEPING', 'select,select,select,varchar,varchar,date,varchar,varchar,varchar,varchar,varchar,varchar,date,select,date', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3358, 6, 129, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3359, 6, 129, '1', 'Name', 'Name', '', '::', 'INFOMATION TIMEKEEPINGSTATUS', 'INFOMATION TIMEKEEPINGSTATUS', 'varchar', 'required', ''),
('2021-06-16', 3360, 6, 129, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3361, 6, 130, '0', 'id,IdCompany,IdEmployee,Avatar,CreatedAt', 'id,IdCompany,IdEmployee,Avatar,CreatedAt', 'id,IdCompany,IdEmployee,Avatar,CreatedAt', '::,137:id:IdProvince,158:id:IdBranch,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 3362, 6, 130, '1', 'IdCompany,IdEmployee,Avatar,CreatedAt', 'IdCompany,IdEmployee,Avatar,CreatedAt', '', '137:id:IdProvince,158:id:IdBranch,::,::', 'INFOMATION USERFACE', 'INFOMATION USERFACE', 'select,select,varchar,date', 'required,required,required,required', ''),
('2021-06-16', 3363, 6, 130, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3364, 6, 131, '0', 'id,IdCompany,IdEmployee,ModelTrain,CreatedAt', 'id,IdCompany,IdEmployee,ModelTrain,CreatedAt', 'id,IdCompany,IdEmployee,ModelTrain,CreatedAt', '::,137:id:IdProvince,158:id:IdBranch,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 3377, 6, 137, '0', 'id,IdProvince,IdDistrict,IdBusinessType,CompanyName,Image,Address,Phone,UserName,Password,Website,Email,Scale,CharterCapital,Keyword,BankName,BankAccount,TaxCode,Note', 'id,IdProvince,IdDistrict,IdBusinessType,CompanyName,Image,Address,Phone,UserName,Password,Website,Email,Scale,CharterCapital,Keyword,BankName,BankAccount,TaxCode,Note', 'id,IdProvince,IdDistrict,IdBusinessType,CompanyName,Image,Address,Phone,UserName,Password,Website,Email,Scale,CharterCapital,Keyword,BankName,BankAccount,TaxCode,Note', '::,164:id:Name,165:id:IdProvince,136:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,,,,,'),
('2021-06-16', 3378, 6, 137, '1', 'IdProvince,IdDistrict,IdBusinessType,CompanyName,Image,Address,Phone,UserName,Password,Website,Email,Scale,CharterCapital,Keyword,BankName,BankAccount,TaxCode,Note', 'IdProvince,IdDistrict,IdBusinessType,CompanyName,Image,Address,Phone,UserName,Password,Website,Email,Scale,CharterCapital,Keyword,BankName,BankAccount,TaxCode,Note', '', '164:id:Name,165:id:IdProvince,136:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::,::,::', 'INFOMATION COMPANY', 'INFOMATION COMPANY', 'select,select,select,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,number,varchar,varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3372, 6, 133, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3373, 6, 135, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3374, 6, 136, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3375, 6, 136, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3376, 6, 136, '1', 'Name', 'Name', '', '::', 'INFOMATION BUSINESSTYPE', 'INFOMATION BUSINESSTYPE', 'varchar', 'required', ''),
('2021-06-16', 3371, 6, 133, '0', 'id,IdCompany,IdEmployee,IdShift,IdBranch,IdDepartment,IdPosition,ActingTime,SortIndex', 'id,IdCompany,IdEmployee,IdShift,IdBranch,IdDepartment,IdPosition,ActingTime,SortIndex', 'id,IdCompany,IdEmployee,IdShift,IdBranch,IdDepartment,IdPosition,ActingTime,SortIndex', '::,137:id:IdProvince,158:id:IdBranch,135:id:IdCompany,166:id:IdCompany,162:id:IdCompany,161:id:IdCompany,::,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3369, 6, 132, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3370, 6, 133, '1', 'IdCompany,IdEmployee,IdShift,IdBranch,IdDepartment,IdPosition,ActingTime,SortIndex', 'IdCompany,IdEmployee,IdShift,IdBranch,IdDepartment,IdPosition,ActingTime,SortIndex', '', '137:id:IdProvince,158:id:IdBranch,135:id:IdCompany,166:id:IdCompany,162:id:IdCompany,161:id:IdCompany,::,::', 'INFOMATION SHIFTASSIGNMENT', 'INFOMATION SHIFTASSIGNMENT', 'select,select,select,select,select,select,varchar,number', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3342, 6, 123, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3343, 6, 124, '0', 'id,IdCompany,Name,Keyword,Rate,LimitOvertimeHoursMonth,LimitOvertimeHoursYear,Description', 'id,IdCompany,Name,Keyword,Rate,LimitOvertimeHoursMonth,LimitOvertimeHoursYear,Description', 'id,IdCompany,Name,Keyword,Rate,LimitOvertimeHoursMonth,LimitOvertimeHoursYear,Description', '::,137:id:IdProvince,::,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-06-16', 3351, 6, 127, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSAPPROVE', 'INFOMATION STATUSAPPROVE', 'varchar', 'required', ''),
('2021-06-16', 3350, 6, 126, '1', 'IdCompany,IdEmployee,Money,Title,AllowanceDay,Reason,IdStatusApprove,IdAdvanceSalaryType,CreatedAt,IsAdvanceSalary', 'IdCompany,IdEmployee,Money,Title,AllowanceDay,Reason,IdStatusApprove,IdAdvanceSalaryType,CreatedAt,IsAdvanceSalary', '', '137:id:IdProvince,158:id:IdBranch,::,::,::,::,127:id:Name,125:id:Name,::,::', 'INFOMATION ADVANCESALARY', 'INFOMATION ADVANCESALARY', 'select,select,varchar,varchar,date,varchar,select,select,date,varchar', 'required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3345, 6, 124, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3346, 6, 125, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3347, 6, 125, '1', 'Name', 'Name', '', '::', 'INFOMATION ADVANCESALARYTYPE', 'INFOMATION ADVANCESALARYTYPE', 'varchar', 'required', ''),
('2021-06-16', 3348, 6, 125, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3349, 6, 126, '0', 'id,IdCompany,IdEmployee,Money,Title,AllowanceDay,Reason,IdStatusApprove,IdAdvanceSalaryType,CreatedAt,IsAdvanceSalary', 'id,IdCompany,IdEmployee,Money,Title,AllowanceDay,Reason,IdStatusApprove,IdAdvanceSalaryType,CreatedAt,IsAdvanceSalary', 'id,IdCompany,IdEmployee,Money,Title,AllowanceDay,Reason,IdStatusApprove,IdAdvanceSalaryType,CreatedAt,IsAdvanceSalary', '::,137:id:IdProvince,158:id:IdBranch,::,::,::,::,127:id:Name,125:id:Name,::,::', '', '', '', '', ',,,,,,,,,,'),
('2021-06-16', 3344, 6, 124, '1', 'IdCompany,Name,Keyword,Rate,LimitOvertimeHoursMonth,LimitOvertimeHoursYear,Description', 'IdCompany,Name,Keyword,Rate,LimitOvertimeHoursMonth,LimitOvertimeHoursYear,Description', '', '137:id:IdProvince,::,::,::,::,::,::', 'INFOMATION OVERTIMETYPE', 'INFOMATION OVERTIMETYPE', 'select,varchar,varchar,number,number,number,varchar', 'required,required,required,required,required,required,required', ''),
('2021-06-16', 3339, 6, 122, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3340, 6, 123, '0', 'id,IdBonusType,Title,Amount,IdEmployee,AllowanceDay,ReasonContent,IdStatusApprove,CreatedAt', 'id,IdBonusType,Title,Amount,IdEmployee,AllowanceDay,ReasonContent,IdStatusApprove,CreatedAt', 'id,IdBonusType,Title,Amount,IdEmployee,AllowanceDay,ReasonContent,IdStatusApprove,CreatedAt', '::,122:id:Name,::,::,158:id:IdBranch,::,::,127:id:Name,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3341, 6, 123, '1', 'IdBonusType,Title,Amount,IdEmployee,AllowanceDay,ReasonContent,IdStatusApprove,CreatedAt', 'IdBonusType,Title,Amount,IdEmployee,AllowanceDay,ReasonContent,IdStatusApprove,CreatedAt', '', '122:id:Name,::,::,158:id:IdBranch,::,::,127:id:Name,::', 'INFOMATION BONUS', 'INFOMATION BONUS', 'select,varchar,varchar,select,date,varchar,select,date', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3336, 6, 121, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3337, 6, 122, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3338, 6, 122, '1', 'Name', 'Name', '', '::', 'INFOMATION BONUSTYPE', 'INFOMATION BONUSTYPE', 'varchar', 'required', ''),
('2021-06-16', 3335, 6, 121, '1', 'IdCompany,IdEmployee,IdOvertimeType,FromTime,EndTime,IdStatusApprove,Reason,Content,OvertimeHour,OvertimeBranch,CreatedAt,IsOvertime', 'IdCompany,IdEmployee,IdOvertimeType,FromTime,EndTime,IdStatusApprove,Reason,Content,OvertimeHour,OvertimeBranch,CreatedAt,IsOvertime', '', '137:id:IdProvince,158:id:IdBranch,124:id:IdCompany,::,::,127:id:Name,::,::,::,::,::,::', 'INFOMATION OVERTIME', 'INFOMATION OVERTIME', 'select,select,select,date,date,select,varchar,varchar,varchar,varchar,date,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3333, 6, 120, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3334, 6, 121, '0', 'id,IdCompany,IdEmployee,IdOvertimeType,FromTime,EndTime,IdStatusApprove,Reason,Content,OvertimeHour,OvertimeBranch,CreatedAt,IsOvertime', 'id,IdCompany,IdEmployee,IdOvertimeType,FromTime,EndTime,IdStatusApprove,Reason,Content,OvertimeHour,OvertimeBranch,CreatedAt,IsOvertime', 'id,IdCompany,IdEmployee,IdOvertimeType,FromTime,EndTime,IdStatusApprove,Reason,Content,OvertimeHour,OvertimeBranch,CreatedAt,IsOvertime', '::,137:id:IdProvince,158:id:IdBranch,124:id:IdCompany,::,::,127:id:Name,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,'),
('2021-06-16', 3332, 6, 120, '0', 'id,IdCompany,IdEmployee,IdStatusApprove,FromTime,EndTime,Place,TrackingPlace,IdShift,Reason,CreatedAt,IsBusinessTrip', 'id,IdCompany,IdEmployee,IdStatusApprove,FromTime,EndTime,Place,TrackingPlace,IdShift,Reason,CreatedAt,IsBusinessTrip', 'id,IdCompany,IdEmployee,IdStatusApprove,FromTime,EndTime,Place,TrackingPlace,IdShift,Reason,CreatedAt,IsBusinessTrip', '::,137:id:IdProvince,158:id:IdBranch,127:id:Name,::,::,::,::,135:id:IdCompany,::,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2021-06-16', 3330, 6, 119, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3331, 6, 120, '1', 'IdCompany,IdEmployee,IdStatusApprove,FromTime,EndTime,Place,TrackingPlace,IdShift,Reason,CreatedAt,IsBusinessTrip', 'IdCompany,IdEmployee,IdStatusApprove,FromTime,EndTime,Place,TrackingPlace,IdShift,Reason,CreatedAt,IsBusinessTrip', '', '137:id:IdProvince,158:id:IdBranch,127:id:Name,::,::,::,::,135:id:IdCompany,::,::,::', 'INFOMATION BUSINESSTRIP', 'INFOMATION BUSINESSTRIP', 'select,select,select,date,date,varchar,varchar,select,varchar,date,varchar', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3328, 6, 118, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3329, 6, 119, '1', 'IdCompany,IdEmployee,IdShift,CheckinDate,CheckinTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckinLate', 'IdCompany,IdEmployee,IdShift,CheckinDate,CheckinTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckinLate', '', '137:id:IdProvince,158:id:IdBranch,135:id:IdCompany,::,::,127:id:Name,::,::,::,::,::', 'INFOMATION CHECKINLATE', 'INFOMATION CHECKINLATE', 'select,select,select,date,varchar,select,varchar,varchar,varchar,date,varchar', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3322, 6, 116, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3323, 6, 117, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3321, 6, 116, '1', 'IdFormWork,Name,Keyword,DayPerYear,IdPayType,IsDeliverEnough,Year,ExpirationDate,Description,IsAccumulatingByMonth,IdCompany', 'IdFormWork,Name,Keyword,DayPerYear,IdPayType,IsDeliverEnough,Year,ExpirationDate,Description,IsAccumulatingByMonth,IdCompany', '', '115:id:Name,::,::,::,147:id:Name,::,::,::,::,::,137:id:IdProvince', 'INFOMATION ONLEAVETYPE', 'INFOMATION ONLEAVETYPE', 'select,varchar,varchar,number,select,varchar,number,date,varchar,varchar,select', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3315, 6, 114, '1', 'IdCompany,IdEmployee,IdTypeLeave,IdOnLeaveType,IdShift,IdStatusApprove,FromDate,ToDate,StartTime,EndTime,Reason,HandoverTo,Phone,Content,CreatedAt,IsOnLeave', 'IdCompany,IdEmployee,IdTypeLeave,IdOnLeaveType,IdShift,IdStatusApprove,FromDate,ToDate,StartTime,EndTime,Reason,HandoverTo,Phone,Content,CreatedAt,IsOnLeave', '', '137:id:IdProvince,158:id:IdBranch,117:id:Name,116:id:IdFormWork,135:id:IdCompany,127:id:Name,::,::,::,::,::,::,::,::,::,::', 'INFOMATION ONLEAVE', 'INFOMATION ONLEAVE', 'select,select,select,select,select,select,date,date,date,date,varchar,varchar,varchar,varchar,date,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3327, 6, 118, '1', 'IdCompany,IdEmployee,IdShift,CheckoutDate,CheckoutTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckoutSoon', 'IdCompany,IdEmployee,IdShift,CheckoutDate,CheckoutTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckoutSoon', '', '137:id:IdProvince,158:id:IdBranch,135:id:IdCompany,::,::,127:id:Name,::,::,::,::,::', 'INFOMATION CHECKOUTSOON', 'INFOMATION CHECKOUTSOON', 'select,select,select,date,varchar,select,varchar,varchar,varchar,date,varchar', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3325, 6, 117, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3326, 6, 118, '0', 'id,IdCompany,IdEmployee,IdShift,CheckoutDate,CheckoutTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckoutSoon', 'id,IdCompany,IdEmployee,IdShift,CheckoutDate,CheckoutTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckoutSoon', 'id,IdCompany,IdEmployee,IdShift,CheckoutDate,CheckoutTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckoutSoon', '::,137:id:IdProvince,158:id:IdBranch,135:id:IdCompany,::,::,127:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2021-06-16', 3324, 6, 117, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPELEAVE', 'INFOMATION TYPELEAVE', 'varchar', 'required', ''),
('2021-06-16', 3316, 6, 114, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3317, 6, 115, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3318, 6, 115, '1', 'Name', 'Name', '', '::', 'INFOMATION FORMWORK', 'INFOMATION FORMWORK', 'varchar', 'required', ''),
('2021-06-16', 3319, 6, 115, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3320, 6, 116, '0', 'id,IdFormWork,Name,Keyword,DayPerYear,IdPayType,IsDeliverEnough,Year,ExpirationDate,Description,IsAccumulatingByMonth,IdCompany', 'id,IdFormWork,Name,Keyword,DayPerYear,IdPayType,IsDeliverEnough,Year,ExpirationDate,Description,IsAccumulatingByMonth,IdCompany', 'id,IdFormWork,Name,Keyword,DayPerYear,IdPayType,IsDeliverEnough,Year,ExpirationDate,Description,IsAccumulatingByMonth,IdCompany', '::,115:id:Name,::,::,::,147:id:Name,::,::,::,::,::,137:id:IdProvince', '', '', '', '', ',,,,,,,,,,,'),
('2021-06-16', 3306, 6, 111, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3307, 6, 112, '1', 'IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient', 'IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient', '', '137:id:IdProvince,135:id:IdCompany,::,::,::,::', 'INFOMATION ONHOLIDAY', 'INFOMATION ONHOLIDAY', 'select,select,varchar,date,date,varchar', 'required,required,required,required,required,required', ''),
('2021-06-16', 3308, 6, 112, '0', 'id,IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient', 'id,IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient', 'id,IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient', '::,137:id:IdProvince,135:id:IdCompany,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-16', 3297, 6, 110, '1', 'IdCompany,IdEmployee,IdBranch,IdPayType,Name,Description,Keyword,LocationApplies', 'IdCompany,IdEmployee,IdBranch,IdPayType,Name,Description,Keyword,LocationApplies', '', '137:id:IdProvince,158:id:IdBranch,166:id:IdCompany,147:id:Name,::,::,::,::', 'INFOMATION SALARYTEMPLATE', 'INFOMATION SALARYTEMPLATE', 'select,select,select,select,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3298, 6, 110, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3299, 6, 108, '1', 'IdCompany,Title,IsShow,Image,Description,Document,Status', 'IdCompany,Title,IsShow,Image,Description,Document,Status', '', '137:id:IdProvince,::,::,::,::,::,::', 'INFOMATION RULE', 'INFOMATION RULE', 'select,varchar,varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required,required', ''),
('2021-06-16', 3075, 7, 252, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3076, 7, 253, '0', 'id,IdRegisterLearn,Rate,Content', 'id,IdRegisterLearn,Rate,Content', 'id,IdRegisterLearn,Rate,Content', '::,187:id:IdBranch,::,::', '', '', '', '', ',,,'),
('2021-06-16', 3059, 7, 247, '1', 'Name', 'Name', '', '::', 'INFOMATION PAYMENT', 'INFOMATION PAYMENT', 'varchar', 'required', ''),
('2021-06-16', 3058, 7, 247, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3057, 7, 246, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3055, 7, 246, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3056, 7, 246, '1', 'Name', 'Name', '', '::', 'INFOMATION PAYMENTTYPE', 'INFOMATION PAYMENTTYPE', 'varchar', 'required', ''),
('2021-06-16', 3054, 7, 245, '3', '', '', '', '', '', '', '', '', '');
INSERT INTO `a1crud_crud` (`createdate`, `id`, `idproject`, `idclass`, `actiontype`, `titles`, `properties`, `alias`, `refers`, `titlecreate`, `titleupdate`, `inputtypes`, `validates`, `formats`) VALUES
('2021-06-16', 3053, 7, 245, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSPOST', 'INFOMATION STATUSPOST', 'varchar', 'required', ''),
('2021-06-16', 3051, 7, 244, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3052, 7, 245, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3050, 7, 244, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSTAKEIMAGE', 'INFOMATION STATUSTAKEIMAGE', 'varchar', 'required', ''),
('2021-06-16', 3047, 7, 243, '1', 'IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt', 'IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt', '', '196:id:IdTypeEmployee,241:id:Name,242:id:Name,::,::,::', 'INFOMATION DAYOFF', 'INFOMATION DAYOFF', 'select,select,select,date,date,date', 'required,required,required,required,required,required', ''),
('2021-06-16', 3048, 7, 243, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3049, 7, 244, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3046, 7, 243, '0', 'id,IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt', 'id,IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt', 'id,IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt', '::,196:id:IdTypeEmployee,241:id:Name,242:id:Name,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-16', 3045, 7, 242, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3043, 7, 242, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3044, 7, 242, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSAPPROVED', 'INFOMATION STATUSAPPROVED', 'varchar', 'required', ''),
('2021-06-16', 3041, 7, 241, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3042, 7, 241, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3038, 7, 240, '1', 'IdEmployee,IdBasicSalary,IdTypeEmployee', 'IdEmployee,IdBasicSalary,IdTypeEmployee', '', '196:id:IdTypeEmployee,235:id:MoneyBasicInMonth,192:id:Name', 'INFOMATION EMPLOYEEBASICSALARY', 'INFOMATION EMPLOYEEBASICSALARY', 'select,select,select', 'required,required,required', ''),
('2021-06-16', 3039, 7, 240, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3040, 7, 241, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSDAYOF', 'INFOMATION STATUSDAYOF', 'varchar', 'required', ''),
('2021-06-16', 3035, 7, 239, '1', 'IdEmployee,NumberHour,CreatedAt', 'IdEmployee,NumberHour,CreatedAt', '', '196:id:IdTypeEmployee,::,::', 'INFOMATION EMPLOYEEOVERTIME', 'INFOMATION EMPLOYEEOVERTIME', 'select,varchar,date', 'required,required,required', ''),
('2021-06-16', 3036, 7, 239, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3037, 7, 240, '0', 'id,IdEmployee,IdBasicSalary,IdTypeEmployee', 'id,IdEmployee,IdBasicSalary,IdTypeEmployee', 'id,IdEmployee,IdBasicSalary,IdTypeEmployee', '::,196:id:IdTypeEmployee,235:id:MoneyBasicInMonth,192:id:Name', '', '', '', '', ',,,'),
('2021-06-16', 3033, 7, 238, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3034, 7, 239, '0', 'id,IdEmployee,NumberHour,CreatedAt', 'id,IdEmployee,NumberHour,CreatedAt', 'id,IdEmployee,NumberHour,CreatedAt', '::,196:id:IdTypeEmployee,::,::', '', '', '', '', ',,,'),
('2021-06-16', 3032, 7, 238, '1', 'IdEmployee,IdSubsidize,StartDate,EndDate', 'IdEmployee,IdSubsidize,StartDate,EndDate', '', '196:id:IdTypeEmployee,236:id:Name,::,::', 'INFOMATION EMPLOYEESUBSIDIZE', 'INFOMATION EMPLOYEESUBSIDIZE', 'select,select,date,date', 'required,required,required,required', ''),
('2021-06-16', 3030, 7, 237, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3031, 7, 238, '0', 'id,IdEmployee,IdSubsidize,StartDate,EndDate', 'id,IdEmployee,IdSubsidize,StartDate,EndDate', 'id,IdEmployee,IdSubsidize,StartDate,EndDate', '::,196:id:IdTypeEmployee,236:id:Name,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 3029, 7, 237, '1', 'IdEmployee,Name,Money,CreatedAt', 'IdEmployee,Name,Money,CreatedAt', '', '196:id:IdTypeEmployee,::,::,::', 'INFOMATION EMPLOYEEREWARD', 'INFOMATION EMPLOYEEREWARD', 'select,varchar,varchar,date', 'required,required,required,required', ''),
('2021-06-16', 3028, 7, 236, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3027, 7, 237, '0', 'id,IdEmployee,Name,Money,CreatedAt', 'id,IdEmployee,Name,Money,CreatedAt', 'id,IdEmployee,Name,Money,CreatedAt', '::,196:id:IdTypeEmployee,::,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 3025, 7, 236, '0', 'id,Name,Money', 'id,Name,Money', 'id,Name,Money', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 3026, 7, 236, '1', 'Name,Money', 'Name,Money', '', '::,::', 'INFOMATION SUBSIDIZE', 'INFOMATION SUBSIDIZE', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 3023, 7, 235, '1', 'MoneyBasicInMonth,MoneyInHour', 'MoneyBasicInMonth,MoneyInHour', '', '::,::', 'INFOMATION BASICSALARY', 'INFOMATION BASICSALARY', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 3024, 7, 235, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3022, 7, 235, '0', 'id,MoneyBasicInMonth,MoneyInHour', 'id,MoneyBasicInMonth,MoneyInHour', 'id,MoneyBasicInMonth,MoneyInHour', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 3021, 7, 234, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3018, 7, 233, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3019, 7, 234, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3020, 7, 234, '1', 'Name', 'Name', '', '::', 'INFOMATION NOTIFICATIONTYPE', 'INFOMATION NOTIFICATIONTYPE', 'varchar', 'required', ''),
('2021-06-16', 3017, 7, 233, '1', 'IdNotificationType,Title,Content,Author,DateCreate', 'IdNotificationType,Title,Content,Author,DateCreate', '', '234:id:Name,::,::,::,::', 'INFOMATION NOTIFICATION', 'INFOMATION NOTIFICATION', 'select,varchar,varchar,varchar,date', 'required,required,required,required,required', ''),
('2021-06-16', 3015, 7, 232, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3016, 7, 233, '0', 'id,IdNotificationType,Title,Content,Author,DateCreate', 'id,IdNotificationType,Title,Content,Author,DateCreate', 'id,IdNotificationType,Title,Content,Author,DateCreate', '::,234:id:Name,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-06-16', 3013, 7, 227, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3014, 7, 232, '1', 'FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited', 'FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited', '', '::,::,::,::,220:id:Name,::,::,::,::,::', 'INFOMATION REGISTRATIONCAR', 'INFOMATION REGISTRATIONCAR', 'varchar,varchar,varchar,varchar,select,varchar,date,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3012, 7, 232, '0', 'id,FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited', 'id,FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited', 'id,FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited', '::,::,::,::,::,220:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,'),
('2021-06-16', 3011, 7, 231, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3010, 7, 231, '1', 'FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', 'FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', '', '::,::,::,::,::,::,::,::', 'INFOMATION REGISTRATIONINSURRANCE', 'INFOMATION REGISTRATIONINSURRANCE', 'varchar,date,varchar,varchar,varchar,varchar,date,varchar', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3009, 7, 231, '0', 'id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', 'id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', 'id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', '::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3008, 7, 230, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3007, 7, 227, '1', 'IdCar,Total,CreatedAt', 'IdCar,Total,CreatedAt', '', '221:id:IdCarCategories,::,::', 'INFOMATION WAREHOUSECAR', 'INFOMATION WAREHOUSECAR', 'select,number,date', 'required,required,required', ''),
('2021-06-16', 3006, 7, 230, '1', 'FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', 'FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', '', '::,::,::,::,::,::,::,::', 'INFOMATION REGISTRATIONSHYAKEN', 'INFOMATION REGISTRATIONSHYAKEN', 'varchar,date,varchar,varchar,varchar,varchar,date,varchar', 'required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3005, 7, 227, '0', 'id,IdCar,Total,CreatedAt', 'id,IdCar,Total,CreatedAt', 'id,IdCar,Total,CreatedAt', '::,221:id:IdCarCategories,::,::', '', '', '', '', ',,,'),
('2021-06-16', 3004, 7, 230, '0', 'id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', 'id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', 'id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice', '::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3003, 7, 226, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3002, 7, 229, '1', 'IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note', 'IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note', '', '221:id:IdCarCategories,196:id:IdTypeEmployee,225:id:FullName,::,::,246:id:Name,226:id:Name,::,249:id:Name,::,::', 'INFOMATION OUTPUTCAR', 'INFOMATION OUTPUTCAR', 'select,select,select,varchar,varchar,select,select,date,select,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3001, 7, 229, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3000, 7, 229, '0', 'id,IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note', 'id,IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note', 'id,IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note', '::,221:id:IdCarCategories,196:id:IdTypeEmployee,225:id:FullName,::,::,246:id:Name,226:id:Name,::,249:id:Name,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2021-06-16', 2997, 7, 226, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2998, 7, 225, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2999, 7, 226, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSPREPARETRANSFERCAR', 'INFOMATION STATUSPREPARETRANSFERCAR', 'varchar', 'required', ''),
('2021-06-16', 2996, 7, 224, '1', 'Name', 'Name', '', '::', 'INFOMATION TRANSPORT', 'INFOMATION TRANSPORT', 'varchar', 'required', ''),
('2021-06-16', 2995, 7, 225, '1', 'FullName,Born,Address,Phone', 'FullName,Born,Address,Phone', '', '::,::,::,::', 'INFOMATION CUSTOMER', 'INFOMATION CUSTOMER', 'varchar,date,varchar,varchar', 'required,required,required,required', ''),
('2021-06-16', 2994, 7, 228, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2992, 7, 224, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2993, 7, 228, '1', 'IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport', 'IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport', '', '221:id:IdCarCategories,222:id:NumberCode,223:id:Name,::,::,224:id:Name,::', 'INFOMATION INPUTCAR', 'INFOMATION INPUTCAR', 'select,select,select,date,date,select,varchar', 'required,required,required,required,required,required,required', ''),
('2021-06-16', 2990, 7, 224, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2991, 7, 225, '0', 'id,FullName,Born,Address,Phone', 'id,FullName,Born,Address,Phone', 'id,FullName,Born,Address,Phone', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 2989, 7, 223, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2988, 7, 228, '0', 'id,IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport', 'id,IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport', 'id,IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport', '::,221:id:IdCarCategories,222:id:NumberCode,223:id:Name,::,::,224:id:Name,::', '', '', '', '', ',,,,,,,'),
('2021-06-16', 2987, 7, 223, '1', 'Name', 'Name', '', '::', 'INFOMATION SOURCECAR', 'INFOMATION SOURCECAR', 'varchar', 'required', ''),
('2021-06-16', 2986, 7, 222, '1', 'NumberCode,Name', 'NumberCode,Name', '', '::,::', 'INFOMATION AUCTIONFLOOR', 'INFOMATION AUCTIONFLOOR', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 2982, 7, 221, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2983, 7, 222, '0', 'id,NumberCode,Name', 'id,NumberCode,Name', 'id,NumberCode,Name', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 2984, 7, 222, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2985, 7, 223, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2981, 7, 220, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2977, 7, 219, '1', 'Name', 'Name', '', '::', 'INFOMATION CARCATEGORIES', 'INFOMATION CARCATEGORIES', 'varchar', 'required', ''),
('2021-06-16', 2978, 7, 219, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2979, 7, 220, '0', 'id,Name,Logo', 'id,Name,Logo', 'id,Name,Logo', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 2980, 7, 220, '1', 'Name,Logo', 'Name,Logo', '', '::,::', 'INFOMATION CARMANUFACTURER', 'INFOMATION CARMANUFACTURER', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 2976, 7, 219, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2975, 7, 218, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2973, 7, 218, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2974, 7, 218, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSVEHICLEREGISTRATION', 'INFOMATION STATUSVEHICLEREGISTRATION', 'varchar', 'required', ''),
('2021-06-16', 2966, 7, 215, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2967, 7, 216, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2968, 7, 216, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSCHANGECOMPANNY', 'INFOMATION STATUSCHANGECOMPANNY', 'varchar', 'required', ''),
('2021-06-16', 2969, 7, 217, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2970, 7, 217, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSSHAKO', 'INFOMATION STATUSSHAKO', 'varchar', 'required', ''),
('2021-06-16', 2971, 7, 216, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2972, 7, 217, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2965, 7, 215, '1', 'IdBranch,IdCostCategories,Money,Note,CreatedAt', 'IdBranch,IdCostCategories,Money,Note,CreatedAt', '', '174:id:Name,214:id:Name,::,::,::', 'INFOMATION COST', 'INFOMATION COST', 'select,select,varchar,varchar,date', 'required,required,required,required,required', ''),
('2021-06-16', 2964, 7, 215, '0', 'id,IdBranch,IdCostCategories,Money,Note,CreatedAt', 'id,IdBranch,IdCostCategories,Money,Note,CreatedAt', 'id,IdBranch,IdCostCategories,Money,Note,CreatedAt', '::,174:id:Name,214:id:Name,::,::,::', '', '', '', '', ',,,,,'),
('2021-06-16', 2963, 7, 214, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2962, 7, 214, '1', 'Name', 'Name', '', '::', 'INFOMATION COSTCATEGORIES', 'INFOMATION COSTCATEGORIES', 'varchar', 'required', ''),
('2021-06-16', 2956, 7, 212, '1', 'IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent', 'IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent', '', '211:id:IdTypeAnswerResult,210:id:Name,209:id:IdTypeQuestion,191:id:IdStatusStudent', 'INFOMATION QUESTIONHISTORY', 'INFOMATION QUESTIONHISTORY', 'select,select,select,select', 'required,required,required,required', ''),
('2021-06-16', 2957, 7, 212, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2958, 7, 213, '0', 'id,IdStudent,CreatedAt,IdBranch', 'id,IdStudent,CreatedAt,IdBranch', 'id,IdStudent,CreatedAt,IdBranch', '::,191:id:IdStatusStudent,::,174:id:Name', '', '', '', '', ',,,'),
('2021-06-16', 2959, 7, 213, '1', 'IdStudent,CreatedAt,IdBranch', 'IdStudent,CreatedAt,IdBranch', '', '191:id:IdStatusStudent,::,174:id:Name', 'INFOMATION HISTORYSENTA', 'INFOMATION HISTORYSENTA', 'select,date,select', 'required,required,required', ''),
('2021-06-16', 2960, 7, 213, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2961, 7, 214, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2953, 7, 211, '1', 'IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain', 'IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain', '', '210:id:Name,209:id:IdTypeQuestion,::,::', 'INFOMATION QUESTION', 'INFOMATION QUESTION', 'select,select,varchar,varchar', 'required,required,required,required', ''),
('2021-06-16', 2954, 7, 211, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2955, 7, 212, '0', 'id,IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent', 'id,IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent', 'id,IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent', '::,211:id:IdTypeAnswerResult,210:id:Name,209:id:IdTypeQuestion,191:id:IdStatusStudent', '', '', '', '', ',,,,'),
('2021-06-16', 2952, 7, 211, '0', 'id,IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain', 'id,IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain', 'id,IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain', '::,210:id:Name,209:id:IdTypeQuestion,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 2943, 7, 203, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2944, 7, 210, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2945, 7, 204, '0', 'id,Name,Image', 'id,Name,Image', 'id,Name,Image', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 2946, 7, 204, '1', 'Name,Image', 'Name,Image', '', '::,::', 'INFOMATION USEFULKNOWLEDGECATEGORIES', 'INFOMATION USEFULKNOWLEDGECATEGORIES', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 2947, 7, 204, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2948, 7, 210, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPEANSWERRESULT', 'INFOMATION TYPEANSWERRESULT', 'varchar', 'required', ''),
('2021-06-16', 2949, 7, 210, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2950, 7, 205, '0', 'id,IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description', 'id,IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description', 'id,IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description', '::,204:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-16', 2951, 7, 205, '1', 'IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description', 'IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description', '', '204:id:Name,::,::,::,::,::', 'INFOMATION USEFULKNOWLEDGE', 'INFOMATION USEFULKNOWLEDGE', 'select,varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required', ''),
('2021-06-16', 2942, 7, 209, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2941, 7, 203, '0', 'id,IdTypeNoticeBoard,Name,Image,Description,Position', 'id,IdTypeNoticeBoard,Name,Image,Description,Position', 'id,IdTypeNoticeBoard,Name,Image,Description,Position', '::,202:id:Name,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-06-16', 2940, 7, 209, '1', 'IdTypeQuestion,Name', 'IdTypeQuestion,Name', '', '208:id:Name,::', 'INFOMATION TOPIC', 'INFOMATION TOPIC', 'select,varchar', 'required,required', ''),
('2021-06-16', 2939, 7, 209, '0', 'id,IdTypeQuestion,Name', 'id,IdTypeQuestion,Name', 'id,IdTypeQuestion,Name', '::,208:id:Name,::', '', '', '', '', ',,'),
('2021-06-16', 2938, 7, 203, '1', 'IdTypeNoticeBoard,Name,Image,Description,Position', 'IdTypeNoticeBoard,Name,Image,Description,Position', '', '202:id:Name,::,::,::,::', 'INFOMATION NOTICEBOARD', 'INFOMATION NOTICEBOARD', 'select,varchar,varchar,varchar,number', 'required,required,required,required,required', ''),
('2021-06-16', 2936, 7, 208, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2937, 7, 202, '1', 'Name,Image', 'Name,Image', '', '::,::', 'INFOMATION TYPENOTICEBOARD', 'INFOMATION TYPENOTICEBOARD', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 2934, 7, 202, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2935, 7, 202, '0', 'id,Name,Image', 'id,Name,Image', 'id,Name,Image', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 2933, 7, 201, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2932, 7, 208, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPEQUESTION', 'INFOMATION TYPEQUESTION', 'varchar', 'required', ''),
('2021-06-16', 2931, 7, 201, '0', 'id,IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt', 'id,IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt', 'id,IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt', '::,198:id:IdBookCategories,191:id:IdStatusStudent,196:id:IdTypeEmployee,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-16', 2930, 7, 201, '1', 'IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt', 'IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt', '', '198:id:IdBookCategories,191:id:IdStatusStudent,196:id:IdTypeEmployee,::,::,::', 'INFOMATION BORROWBOOK', 'INFOMATION BORROWBOOK', 'select,select,select,date,date,date', 'required,required,required,required,required,required', ''),
('2021-06-16', 2926, 7, 200, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2927, 7, 208, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2928, 7, 200, '1', 'IdBook,Total,CreatedAt', 'IdBook,Total,CreatedAt', '', '198:id:IdBookCategories,::,::', 'INFOMATION IMPORTBOOK', 'INFOMATION IMPORTBOOK', 'select,number,date', 'required,required,required', ''),
('2021-06-16', 2929, 7, 200, '0', 'id,IdBook,Total,CreatedAt', 'id,IdBook,Total,CreatedAt', 'id,IdBook,Total,CreatedAt', '::,198:id:IdBookCategories,::,::', '', '', '', '', ',,,'),
('2021-06-16', 2925, 7, 207, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2920, 7, 199, '0', 'id,IdBook,Total', 'id,IdBook,Total', 'id,IdBook,Total', '::,198:id:IdBookCategories,::', '', '', '', '', ',,'),
('2021-06-16', 2921, 7, 207, '1', 'IdVideoCategories,Name,Link', 'IdVideoCategories,Name,Link', '', '206:id:Name,::,::', 'INFOMATION VIDEO', 'INFOMATION VIDEO', 'select,varchar,varchar', 'required,required,required', ''),
('2021-06-16', 2922, 7, 198, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2923, 7, 199, '1', 'IdBook,Total', 'IdBook,Total', '', '198:id:IdBookCategories,::', 'INFOMATION WAREHOUSEBOOK', 'INFOMATION WAREHOUSEBOOK', 'select,number', 'required,required', ''),
('2021-06-16', 2924, 7, 199, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2917, 7, 198, '1', 'IdBookCategories,Name,Image,Author,Price,CreatedAt', 'IdBookCategories,Name,Image,Author,Price,CreatedAt', '', '197:id:Name,::,::,::,::,::', 'INFOMATION BOOK', 'INFOMATION BOOK', 'select,varchar,varchar,varchar,varchar,date', 'required,required,required,required,required,required', ''),
('2021-06-16', 2918, 7, 206, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2919, 7, 207, '0', 'id,IdVideoCategories,Name,Link', 'id,IdVideoCategories,Name,Link', 'id,IdVideoCategories,Name,Link', '::,206:id:Name,::,::', '', '', '', '', ',,,'),
('2021-06-16', 2916, 7, 206, '1', 'Name,Image', 'Name,Image', '', '::,::', 'INFOMATION VIDEOCATEGORIES', 'INFOMATION VIDEOCATEGORIES', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 2915, 7, 206, '0', 'id,Name,Image', 'id,Name,Image', 'id,Name,Image', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 2910, 7, 197, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2911, 7, 196, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2912, 7, 197, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2913, 7, 197, '1', 'Name', 'Name', '', '::', 'INFOMATION BOOKCATEGORIES', 'INFOMATION BOOKCATEGORIES', 'varchar', 'required', ''),
('2021-06-16', 2914, 7, 198, '0', 'id,IdBookCategories,Name,Image,Author,Price,CreatedAt', 'id,IdBookCategories,Name,Image,Author,Price,CreatedAt', 'id,IdBookCategories,Name,Image,Author,Price,CreatedAt', '::,197:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-16', 2909, 7, 205, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2907, 7, 195, '1', 'IdCertification,IdEmployee', 'IdCertification,IdEmployee', '', '194:id:Name,196:id:IdTypeEmployee', 'INFOMATION CERTIFICATIONEMPLOYEE', 'INFOMATION CERTIFICATIONEMPLOYEE', 'select,select', 'required,required', ''),
('2021-06-16', 2908, 7, 195, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2904, 7, 194, '1', 'Name,DateCertification', 'Name,DateCertification', '', '::,::', 'INFOMATION CERTIFICATION', 'INFOMATION CERTIFICATION', 'varchar,date', 'required,required', ''),
('2021-06-16', 2905, 7, 194, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2906, 7, 195, '0', 'id,IdCertification,IdEmployee', 'id,IdCertification,IdEmployee', 'id,IdCertification,IdEmployee', '::,194:id:Name,196:id:IdTypeEmployee', '', '', '', '', ',,'),
('2021-06-16', 2900, 7, 193, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2901, 7, 193, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSEMPLOYEE', 'INFOMATION STATUSEMPLOYEE', 'varchar', 'required', ''),
('2021-06-16', 2902, 7, 193, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2903, 7, 194, '0', 'id,Name,DateCertification', 'id,Name,DateCertification', 'id,Name,DateCertification', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 2899, 7, 192, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2896, 7, 191, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2897, 7, 192, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2898, 7, 192, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPEEMPLOYEE', 'INFOMATION TYPEEMPLOYEE', 'varchar', 'required', ''),
('2021-06-16', 2895, 7, 191, '1', 'IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy,IdBranch,TokenDevice', 'IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy,IdBranch,TokenDevice', '', '190:id:Name,189:id:Name,::,::,::,::,::,::,::,::,::,250:id:Name,::,174:id:Name,::', 'INFOMATION STUDENT', 'INFOMATION STUDENT', 'select,select,varchar,varchar,varchar,varchar,varchar,varchar,date,varchar,date,select,date,select,ckeditor', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 2894, 7, 191, '0', 'id,IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy,IdBranch,TokenDevice', 'id,IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy,IdBranch,TokenDevice', 'id,IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy,IdBranch,TokenDevice', '::,190:id:Name,189:id:Name,::,::,::,::,::,::,::,::,::,250:id:Name,::,174:id:Name,::', '', '', '', '', ',,,,,,,,,,,,,,,'),
('2021-06-16', 2893, 7, 190, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2892, 7, 190, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSSTUDENT', 'INFOMATION STATUSSTUDENT', 'varchar', 'required', ''),
('2021-06-16', 2891, 7, 190, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2889, 7, 189, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPESOURCE', 'INFOMATION TYPESOURCE', 'varchar', 'required', ''),
('2021-06-16', 2890, 7, 189, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2887, 7, 188, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2888, 7, 189, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2886, 7, 188, '1', 'IdRegisterLearn,Rate,Content,IdBranch', 'IdRegisterLearn,Rate,Content,IdBranch', '', '187:id:IdBranch,::,::,174:id:Name', 'INFOMATION REVIEW', 'INFOMATION REVIEW', 'select,varchar,varchar,select', 'required,required,required,required', ''),
('2021-06-16', 2884, 7, 187, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2885, 7, 188, '0', 'id,IdRegisterLearn,Rate,Content,IdBranch', 'id,IdRegisterLearn,Rate,Content,IdBranch', 'id,IdRegisterLearn,Rate,Content,IdBranch', '::,187:id:IdBranch,::,::,174:id:Name', '', '', '', '', ',,,,'),
('2021-06-16', 2883, 7, 187, '1', 'IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt', 'IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt', '', '174:id:Name,175:id:Name,176:id:Name,177:id:IdBranch,178:id:Name,179:id:Name,180:id:Name,191:id:IdStatusStudent,196:id:IdTypeEmployee,182:id:IdLocationExam,::,::,::', 'INFOMATION REGISTERLEARN', 'INFOMATION REGISTERLEARN', 'select,select,select,select,select,select,select,select,select,select,varchar,date,date', 'required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 2881, 7, 186, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2882, 7, 187, '0', 'id,IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt', 'id,IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt', 'id,IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt', '::,174:id:Name,175:id:Name,176:id:Name,177:id:IdBranch,178:id:Name,179:id:Name,180:id:Name,191:id:IdStatusStudent,196:id:IdTypeEmployee,182:id:IdLocationExam,::,::,::', '', '', '', '', ',,,,,,,,,,,,,'),
('2021-06-16', 2880, 7, 186, '1', 'IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice,CreatedAt,SoTiet,HanDongTien,SoTienThieu,PriceOfPackage', 'IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice,CreatedAt,SoTiet,HanDongTien,SoTienThieu,PriceOfPackage', '', '184:id:IdTypePackage,191:id:IdStatusStudent,185:id:Name,174:id:Name,::,::,::,::,::,::', 'INFOMATION REGISTERPACKAGE', 'INFOMATION REGISTERPACKAGE', 'select,select,select,select,varchar,date,number,date,number,number', 'required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 2879, 7, 186, '0', 'id,IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice,CreatedAt,SoTiet,HanDongTien,SoTienThieu,PriceOfPackage', 'id,IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice,CreatedAt,SoTiet,HanDongTien,SoTienThieu,PriceOfPackage', 'id,IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice,CreatedAt,SoTiet,HanDongTien,SoTienThieu,PriceOfPackage', '::,184:id:IdTypePackage,191:id:IdStatusStudent,185:id:Name,174:id:Name,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,'),
('2021-06-16', 2878, 7, 185, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2877, 7, 185, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSPACKAGEPAYMENT', 'INFOMATION STATUSPACKAGEPAYMENT', 'varchar', 'required', ''),
('2021-06-16', 2876, 7, 185, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2874, 7, 184, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2875, 7, 180, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2873, 7, 179, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2872, 7, 184, '1', 'IdTypePackage,Name,PricePackage,NumberPeriod', 'IdTypePackage,Name,PricePackage,NumberPeriod', '', '183:id:Name,::,::,::', 'INFOMATION PACKAGE', 'INFOMATION PACKAGE', 'select,varchar,varchar,number', 'required,required,required,required', ''),
('2021-06-16', 2868, 7, 179, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2869, 7, 183, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2870, 7, 179, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSREGISTERLEARN', 'INFOMATION STATUSREGISTERLEARN', 'varchar', 'required', ''),
('2021-06-16', 2871, 7, 184, '0', 'id,IdTypePackage,Name,PricePackage,NumberPeriod', 'id,IdTypePackage,Name,PricePackage,NumberPeriod', 'id,IdTypePackage,Name,PricePackage,NumberPeriod', '::,183:id:Name,::,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 2863, 7, 263, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2864, 7, 177, '1', 'IdBranch,TimeStart,TimeEnd,Position', 'IdBranch,TimeStart,TimeEnd,Position', '', '174:id:Name,::,::,::', 'INFOMATION LEARNTIME', 'INFOMATION LEARNTIME', 'select,varchar,varchar,number', 'required,required,required,required', ''),
('2021-06-16', 2865, 7, 178, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2866, 7, 183, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2867, 7, 183, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPEPACKAGE', 'INFOMATION TYPEPACKAGE', 'varchar', 'required', ''),
('2021-06-16', 2861, 7, 178, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2862, 7, 178, '1', 'Name', 'Name', '', '::', 'INFOMATION WAITLOCATION', 'INFOMATION WAITLOCATION', 'varchar', 'required', ''),
('2021-06-16', 2860, 7, 182, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2859, 7, 182, '1', 'IdLocationExam,StartDate', 'IdLocationExam,StartDate', '', '181:id:Name,::', 'INFOMATION DATEEXAM', 'INFOMATION DATEEXAM', 'select,date', 'required,required', ''),
('2021-06-16', 2858, 7, 177, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2857, 7, 177, '0', 'id,IdBranch,TimeStart,TimeEnd,Position', 'id,IdBranch,TimeStart,TimeEnd,Position', 'id,IdBranch,TimeStart,TimeEnd,Position', '::,174:id:Name,::,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 2856, 7, 182, '0', 'id,IdLocationExam,StartDate', 'id,IdLocationExam,StartDate', 'id,IdLocationExam,StartDate', '::,181:id:Name,::', '', '', '', '', ',,'),
('2021-06-16', 2853, 7, 181, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2854, 7, 176, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2855, 7, 263, '1', 'IdEmployee,Message,CreatedAt', 'IdEmployee,Message,CreatedAt', '', '196:id:IdTypeEmployee,::,::', 'INFOMATION CHATROOM', 'INFOMATION CHATROOM', 'select,ckeditor,date', 'required,required,required', ''),
('2021-06-16', 2852, 7, 181, '1', 'Name', 'Name', '', '::', 'INFOMATION LOCATIONEXAM', 'INFOMATION LOCATIONEXAM', 'varchar', 'required', ''),
('2021-06-16', 2851, 7, 263, '0', 'id,IdEmployee,Message,CreatedAt', 'id,IdEmployee,Message,CreatedAt', 'id,IdEmployee,Message,CreatedAt', '::,196:id:IdTypeEmployee,::,::', '', '', '', '', ',,,'),
('2021-06-16', 2850, 7, 181, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2846, 7, 175, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2847, 7, 180, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2848, 7, 176, '1', 'Name', 'Name', '', '::', 'INFOMATION SUBJECT', 'INFOMATION SUBJECT', 'varchar', 'required', ''),
('2021-06-16', 2849, 7, 176, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2842, 7, 174, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2843, 7, 175, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 2844, 7, 180, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSAPPROVEREGISTER', 'INFOMATION STATUSAPPROVEREGISTER', 'varchar', 'required', ''),
('2021-06-16', 2845, 7, 175, '1', 'Name', 'Name', '', '::', 'INFOMATION LESSON', 'INFOMATION LESSON', 'varchar', 'required', ''),
('2021-06-16', 2839, 7, 261, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2840, 7, 262, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2841, 7, 174, '1', 'Name,Address', 'Name,Address', '', '::,::', 'INFOMATION BRANCH', 'INFOMATION BRANCH', 'varchar,varchar', 'required,required', ''),
('2021-06-16', 2838, 7, 261, '1', 'Title,Image,Details,StartDate,EndDate', 'Title,Image,Details,StartDate,EndDate', '', '::,::,::,::,::', 'INFOMATION RECRUIMENT', 'INFOMATION RECRUIMENT', 'varchar,varchar,ckeditor,date,varchar', 'required,required,required,required,required', ''),
('2021-06-16', 2836, 7, 260, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 2837, 7, 261, '0', 'id,Title,Image,Details,StartDate,EndDate', 'id,Title,Image,Details,StartDate,EndDate', 'id,Title,Image,Details,StartDate,EndDate', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-06-16', 2835, 7, 260, '1', 'Address,Phone,SwitchboardSupport,Email,Website', 'Address,Phone,SwitchboardSupport,Email,Website', '', '::,::,::,::,::', 'INFOMATION CONTACT', 'INFOMATION CONTACT', 'varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required', ''),
('2021-06-16', 2834, 7, 260, '0', 'id,Address,Phone,SwitchboardSupport,Email,Website', 'id,Address,Phone,SwitchboardSupport,Email,Website', 'id,Address,Phone,SwitchboardSupport,Email,Website', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-06-16', 2833, 7, 174, '0', 'id,Name,Address', 'id,Name,Address', 'id,Name,Address', '::,::,::', '', '', '', '', ',,'),
('2021-06-16', 3313, 6, 113, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3314, 6, 114, '0', 'id,IdCompany,IdEmployee,IdTypeLeave,IdOnLeaveType,IdShift,IdStatusApprove,FromDate,ToDate,StartTime,EndTime,Reason,HandoverTo,Phone,Content,CreatedAt,IsOnLeave', 'id,IdCompany,IdEmployee,IdTypeLeave,IdOnLeaveType,IdShift,IdStatusApprove,FromDate,ToDate,StartTime,EndTime,Reason,HandoverTo,Phone,Content,CreatedAt,IsOnLeave', 'id,IdCompany,IdEmployee,IdTypeLeave,IdOnLeaveType,IdShift,IdStatusApprove,FromDate,ToDate,StartTime,EndTime,Reason,HandoverTo,Phone,Content,CreatedAt,IsOnLeave', '::,137:id:IdProvince,158:id:IdBranch,117:id:Name,116:id:IdFormWork,135:id:IdCompany,127:id:Name,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,,,'),
('2021-06-16', 3311, 6, 112, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3312, 6, 111, '1', 'Name', 'Name', '', '::', 'INFOMATION SALARYDATETYPE', 'INFOMATION SALARYDATETYPE', 'varchar', 'required', ''),
('2021-06-16', 3310, 6, 119, '0', 'id,IdCompany,IdEmployee,IdShift,CheckinDate,CheckinTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckinLate', 'id,IdCompany,IdEmployee,IdShift,CheckinDate,CheckinTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckinLate', 'id,IdCompany,IdEmployee,IdShift,CheckinDate,CheckinTime,IdStatusApprove,TrackingLocationLat,TrackingLocationLng,Reason,CreatedAt,IsCheckinLate', '::,137:id:IdProvince,158:id:IdBranch,135:id:IdCompany,::,::,127:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2021-06-16', 3309, 6, 113, '1', 'IdCompany,Name,Keyword,Month,IdSalaryTemplate,IdSalaryDateType,StartDate,EndDate,IsClientInvisible', 'IdCompany,Name,Keyword,Month,IdSalaryTemplate,IdSalaryDateType,StartDate,EndDate,IsClientInvisible', '', '137:id:IdProvince,::,::,::,110:id:IdCompany,111:id:Name,::,::,::', 'INFOMATION SALARYTABLE', 'INFOMATION SALARYTABLE', 'select,varchar,varchar,number,select,select,date,date,varchar', 'required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3300, 6, 109, '1', 'IdCompany,IdEmployee,IdBranch,IdAnnouncementType,Title,Document,Content,Status,CreatAt', 'IdCompany,IdEmployee,IdBranch,IdAnnouncementType,Title,Document,Content,Status,CreatAt', '', '137:id:IdProvince,158:id:IdBranch,166:id:IdCompany,167:id:IdCompany,::,::,::,::,::', 'INFOMATION ANNOUNCEMENT', 'INFOMATION ANNOUNCEMENT', 'select,select,select,select,varchar,varchar,varchar,varchar,date', 'required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3301, 6, 109, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3302, 6, 111, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3303, 6, 110, '0', 'id,IdCompany,IdEmployee,IdBranch,IdPayType,Name,Description,Keyword,LocationApplies', 'id,IdCompany,IdEmployee,IdBranch,IdPayType,Name,Description,Keyword,LocationApplies', 'id,IdCompany,IdEmployee,IdBranch,IdPayType,Name,Description,Keyword,LocationApplies', '::,137:id:IdProvince,158:id:IdBranch,166:id:IdCompany,147:id:Name,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-06-16', 3304, 6, 113, '0', 'id,IdCompany,Name,Keyword,Month,IdSalaryTemplate,IdSalaryDateType,StartDate,EndDate,IsClientInvisible', 'id,IdCompany,Name,Keyword,Month,IdSalaryTemplate,IdSalaryDateType,StartDate,EndDate,IsClientInvisible', 'id,IdCompany,Name,Keyword,Month,IdSalaryTemplate,IdSalaryDateType,StartDate,EndDate,IsClientInvisible', '::,137:id:IdProvince,::,::,::,110:id:IdCompany,111:id:Name,::,::,::', '', '', '', '', ',,,,,,,,,'),
('2021-06-16', 3305, 6, 107, '1', 'IdCompany,IdEmployee,IdBranch,IdDepartment,Document,Title,Content,IsInfoPins,ViewedRequest,Status,CreatAt', 'IdCompany,IdEmployee,IdBranch,IdDepartment,Document,Title,Content,IsInfoPins,ViewedRequest,Status,CreatAt', '', '137:id:IdProvince,158:id:IdBranch,166:id:IdCompany,162:id:IdCompany,::,::,::,::,::,::,::', 'INFOMATION INFORMATION', 'INFOMATION INFORMATION', 'select,select,select,select,varchar,varchar,varchar,varchar,varchar,varchar,date', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3296, 6, 109, '0', 'id,IdCompany,IdEmployee,IdBranch,IdAnnouncementType,Title,Document,Content,Status,CreatAt', 'id,IdCompany,IdEmployee,IdBranch,IdAnnouncementType,Title,Document,Content,Status,CreatAt', 'id,IdCompany,IdEmployee,IdBranch,IdAnnouncementType,Title,Document,Content,Status,CreatAt', '::,137:id:IdProvince,158:id:IdBranch,166:id:IdCompany,167:id:IdCompany,::,::,::,::,::', '', '', '', '', ',,,,,,,,,'),
('2021-06-16', 3293, 6, 107, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3294, 6, 108, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3295, 6, 108, '0', 'id,IdCompany,Title,IsShow,Image,Description,Document,Status', 'id,IdCompany,Title,IsShow,Image,Description,Document,Status', 'id,IdCompany,Title,IsShow,Image,Description,Document,Status', '::,137:id:IdProvince,::,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-06-16', 3078, 7, 253, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3079, 7, 254, '0', 'id,Link', 'id,Link', 'id,Link', '::,::', '', '', '', '', ','),
('2021-06-16', 3080, 7, 254, '1', 'Link', 'Link', '', '::', 'INFOMATION IMAGESLIDE', 'INFOMATION IMAGESLIDE', 'varchar', 'required', ''),
('2021-06-16', 3081, 7, 254, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3082, 7, 255, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3083, 7, 255, '1', 'Name', 'Name', '', '::', 'INFOMATION CARCOLOR', 'INFOMATION CARCOLOR', 'varchar', 'required', ''),
('2021-06-16', 3084, 7, 255, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3085, 7, 256, '0', 'id,IdCar,img', 'id,IdCar,img', 'id,IdCar,img', '::,221:id:IdCarCategories,::', '', '', '', '', ',,'),
('2021-06-16', 3086, 7, 256, '1', 'IdCar,img', 'IdCar,img', '', '221:id:IdCarCategories,::', 'INFOMATION CARIMAGES', 'INFOMATION CARIMAGES', 'select,varchar', 'required,required', ''),
('2021-06-16', 3087, 7, 256, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3088, 7, 257, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-06-16', 3089, 7, 257, '1', 'Name', 'Name', '', '::', 'INFOMATION CAREQUIPMENT', 'INFOMATION CAREQUIPMENT', 'varchar', 'required', ''),
('2021-06-16', 3090, 7, 257, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3091, 7, 258, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3092, 7, 259, '0', 'id,title1,details1,image1,title2,details2,image2,title3,details3,image3', 'id,title1,details1,image1,title2,details2,image2,title3,details3,image3', 'id,title1,details1,image1,title2,details2,image2,title3,details3,image3', '::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,'),
('2021-06-16', 3093, 7, 259, '1', 'title1,details1,image1,title2,details2,image2,title3,details3,image3', 'title1,details1,image1,title2,details2,image2,title3,details3,image3', '', '::,::,::,::,::,::,::,::,::', 'INFOMATION INDEX', 'INFOMATION INDEX', 'varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3094, 7, 259, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-16', 3095, 7, 262, '0', 'id,SentId,ReciverId,Message,CreatedAt', 'id,SentId,ReciverId,Message,CreatedAt', 'id,SentId,ReciverId,Message,CreatedAt', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-06-16', 3096, 7, 262, '1', 'SentId,ReciverId,Message,CreatedAt', 'SentId,ReciverId,Message,CreatedAt', '', '::,::,::,::', 'INFOMATION CHATPRIVATE', 'INFOMATION CHATPRIVATE', 'number,number,ckeditor,date', 'required,required,required,required', ''),
('2021-06-16', 3097, 7, 196, '0', 'id,IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch,AccountId', 'id,IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch,AccountId', 'id,IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch,AccountId', '::,192:id:Name,193:id:Name,::,::,::,::,::,::,::,::,174:id:Name,::', '', '', '', '', ',,,,,,,,,,,,'),
('2021-06-16', 3098, 7, 196, '1', 'IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch,AccountId', 'IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch,AccountId', '', '192:id:Name,193:id:Name,::,::,::,::,::,::,::,::,174:id:Name,::', 'INFOMATION EMPLOYEE', 'INFOMATION EMPLOYEE', 'select,select,varchar,varchar,varchar,varchar,varchar,varchar,varchar,date,select,number', 'required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3099, 7, 258, '1', 'IdCar,EquipmentId', 'IdCar,EquipmentId', '', '221:id:IdCarCategories,::', 'INFOMATION CAREQUIPMENTDETAILS', 'INFOMATION CAREQUIPMENTDETAILS', 'select,number', 'required,required', ''),
('2021-06-16', 3100, 7, 221, '1', 'IdCarCategories,IdCarManufacturer,NameCar,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost,PriceFullPackage,CylinderCapacity,DetailsCar,IdCarColor,Equipment,Avatar', 'IdCarCategories,IdCarManufacturer,NameCar,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost,PriceFullPackage,CylinderCapacity,DetailsCar,IdCarColor,Equipment,Avatar', '', '219:id:Name,220:id:Name,::,::,::,::,::,::,::,218:id:Name,217:id:Name,::,216:id:Name,244:id:Name,245:id:Name,::,::,::,255:id:Name,::,::', 'INFOMATION CAR', 'INFOMATION CAR', 'select,select,varchar,varchar,date,varchar,number,varchar,varchar,select,select,date,select,select,select,number,varchar,ckeditor,select,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-16', 3101, 7, 221, '0', 'id,IdCarCategories,IdCarManufacturer,NameCar,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost,PriceFullPackage,CylinderCapacity,DetailsCar,IdCarColor,Equipment,Avatar', 'id,IdCarCategories,IdCarManufacturer,NameCar,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost,PriceFullPackage,CylinderCapacity,DetailsCar,IdCarColor,Equipment,Avatar', 'id,IdCarCategories,IdCarManufacturer,NameCar,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost,PriceFullPackage,CylinderCapacity,DetailsCar,IdCarColor,Equipment,Avatar', '::,219:id:Name,220:id:Name,::,::,::,::,::,::,::,218:id:Name,217:id:Name,::,216:id:Name,244:id:Name,245:id:Name,::,::,::,255:id:Name,::,::', '', '', '', '', ',,,,,,,,,,,,,,,,,,,,,'),
('2021-06-16', 3102, 7, 258, '0', 'id,IdCar,EquipmentId', 'id,IdCar,EquipmentId', 'id,IdCar,EquipmentId', '::,221:id:IdCarCategories,::', '', '', '', '', ',,'),
('2021-06-16', 3292, 6, 107, '0', 'id,IdCompany,IdEmployee,IdBranch,IdDepartment,Document,Title,Content,IsInfoPins,ViewedRequest,Status,CreatAt', 'id,IdCompany,IdEmployee,IdBranch,IdDepartment,Document,Title,Content,IsInfoPins,ViewedRequest,Status,CreatAt', 'id,IdCompany,IdEmployee,IdBranch,IdDepartment,Document,Title,Content,IsInfoPins,ViewedRequest,Status,CreatAt', '::,137:id:IdProvince,158:id:IdBranch,166:id:IdCompany,162:id:IdCompany,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2021-07-19', 3621, 12, 288, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-19', 3620, 12, 288, '1', 'Username,Password', 'Username,Password', '', '::,::', '', '', 'varchar,', 'required,', '');
INSERT INTO `a1crud_crud` (`createdate`, `id`, `idproject`, `idclass`, `actiontype`, `titles`, `properties`, `alias`, `refers`, `titlecreate`, `titleupdate`, `inputtypes`, `validates`, `formats`) VALUES
('2021-07-12', 3617, 8, 271, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-12', 3618, 8, 265, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-12', 3616, 8, 266, '0', 'id,IdPermission,Name,Email,Phone,Age,Gender,Username,Password,AmountOfSeat,CarType,Image,LicensePlate,Address,DeviceToken', 'id,IdPermission,Name,Email,Phone,Age,Gender,Username,Password,AmountOfSeat,CarType,Image,LicensePlate,Address,DeviceToken', 'id,IdPermission,Name,Email,Phone,Age,Gender,Username,Password,AmountOfSeat,CarType,Image,LicensePlate,Address,DeviceToken', '::,264:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,'),
('2021-07-12', 3614, 8, 287, '1', 'DistanceSignal,DistanceReceiver', 'DistanceSignal,DistanceReceiver', '', '::,::', 'INFOMATION SETTING', 'INFOMATION SETTING', 'number,number', 'required,required', ''),
('2021-07-12', 3615, 8, 270, '0', 'id,StartDistance,EndDistance,UnitPrice,AmountOfSeat', 'id,StartDistance,EndDistance,UnitPrice,AmountOfSeat', 'id,StartDistance,EndDistance,UnitPrice,AmountOfSeat', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-07-12', 3613, 8, 271, '0', 'id,IdPerson,IdSignal,Title,Image,IsView,CreatedAt,UpdatedAt,IdReceiver', 'id,IdPerson,IdSignal,Title,Image,IsView,CreatedAt,UpdatedAt,IdReceiver', 'id,IdPerson,IdSignal,Title,Image,IsView,CreatedAt,UpdatedAt,IdReceiver', '::,266:id:IdPermission,267:id:IdPerson,::,::,::,::,::,269:id:IdPerson', '', '', '', '', ',,,,,,,,'),
('2021-07-12', 3609, 8, 287, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-12', 3610, 8, 269, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-12', 3611, 8, 264, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-12', 3612, 8, 266, '1', 'IdPermission,Name,Email,Phone,Age,Gender,Username,Password,AmountOfSeat,CarType,Image,LicensePlate,Address,DeviceToken', 'IdPermission,Name,Email,Phone,Age,Gender,Username,Password,AmountOfSeat,CarType,Image,LicensePlate,Address,DeviceToken', '', '264:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::', 'INFOMATION PERSON', '', 'select,varchar,varchar,varchar,number,varchar,varchar,varchar,number,varchar,image,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-07-19', 3619, 12, 288, '0', 'Username,Password', 'Username,Password', 'Username,Password', 'undefined:id:Username,::', '', '', '', '', ','),
('2021-07-12', 3605, 8, 265, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-12', 3606, 8, 270, '1', 'StartDistance,EndDistance,UnitPrice,AmountOfSeat', 'StartDistance,EndDistance,UnitPrice,AmountOfSeat', '', '::,::,::,::', 'INFOMATION RULETOTALMONEY', 'INFOMATION RULETOTALMONEY', 'varchar,varchar,varchar,number', 'required,required,required,required', ''),
('2021-07-12', 3607, 8, 264, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-12', 3600, 8, 266, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-12', 3601, 8, 270, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-12', 3602, 8, 264, '1', 'Name', 'Name', '', '::', 'INFOMATION PERMISSION', 'INFOMATION PERMISSION', 'varchar', 'required', ''),
('2021-07-12', 3603, 8, 267, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-12', 3604, 8, 271, '1', 'IdPerson,IdSignal,Title,Image,IsView,CreatedAt,UpdatedAt,IdReceiver', 'IdPerson,IdSignal,Title,Image,IsView,CreatedAt,UpdatedAt,IdReceiver', '', '266:id:IdPermission,267:id:IdPerson,::,::,::,::,::,269:id:IdPerson', 'INFOMATION NOTIFICATION', '', 'select,select,varchar,image,varchar,date,date,select', 'required,required,required,required,required,required,required,required', ''),
('2021-07-12', 3596, 8, 269, '1', 'IdPerson,IdStatus,IdSignal,Place,Lat,Lng,CreatedAt,UpdatedAt,Distance', 'IdPerson,IdStatus,IdSignal,Place,Lat,Lng,CreatedAt,UpdatedAt,Distance', '', '266:id:IdPermission,265:id:Name,267:id:IdPerson,::,::,::,::,::,::', 'INFOMATION RECEIVER', 'INFOMATION RECEIVER', 'select,select,select,varchar,varchar,varchar,date,date,number', 'required,required,required,required,required,required,required,required,required', ''),
('2021-07-12', 3597, 8, 269, '0', 'id,IdPerson,IdStatus,IdSignal,Place,Lat,Lng,CreatedAt,UpdatedAt,Distance', 'id,IdPerson,IdStatus,IdSignal,Place,Lat,Lng,CreatedAt,UpdatedAt,Distance', 'id,IdPerson,IdStatus,IdSignal,Place,Lat,Lng,CreatedAt,UpdatedAt,Distance', '::,266:id:IdPermission,265:id:Name,267:id:IdPerson,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,'),
('2021-07-12', 3608, 8, 287, '0', 'id,DistanceSignal,DistanceReceiver', 'id,DistanceSignal,DistanceReceiver', 'id,DistanceSignal,DistanceReceiver', '::,::,::', '', '', '', '', ',,'),
('2021-07-12', 3599, 8, 267, '0', 'id,IdPerson,IdStatus,Message,Voice,Place,BeginLat,BeginLng,EndLat,EndLng,TotalMoney,StartTime,EndTime,PhoneCustomer,CreatedAt,UpdatedAt,Distance', 'id,IdPerson,IdStatus,Message,Voice,Place,BeginLat,BeginLng,EndLat,EndLng,TotalMoney,StartTime,EndTime,PhoneCustomer,CreatedAt,UpdatedAt,Distance', 'id,IdPerson,IdStatus,Message,Voice,Place,BeginLat,BeginLng,EndLat,EndLng,TotalMoney,StartTime,EndTime,PhoneCustomer,CreatedAt,UpdatedAt,Distance', '::,266:id:IdPermission,265:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,,,'),
('2021-07-12', 3598, 8, 265, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUS', 'INFOMATION STATUS', 'varchar', 'required', ''),
('2021-06-29', 3505, 7, 276, '0', 'id,title,description,price,information,image', 'id,title,description,price,information,image', 'id,Tiêu đề,Mô tả,Giá tiền,Thông tin,Hình ảnh', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-06-29', 3506, 7, 276, '1', 'Tiêu đề,Mô tả,Giá tiền,Thông tin,Hình ảnh', 'title,description,price,information,image', '', '::,::,::,::,::', 'Thông tin khóa học', '', 'varchar,varchar,number,number,image', 'required,required,required,required,required', ''),
('2021-06-29', 3507, 7, 276, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-29', 3508, 10, 277, '0', 'id,Name,Age,IdClass', 'id,Name,Age,IdClass', 'id,Name,Age,IdClass', '::,::,::,278:id:Name', '', '', '', '', ',,,'),
('2021-06-29', 3509, 10, 277, '1', 'Name,Age,IdClass', 'Name,Age,IdClass', '', '::,::,278:id:Name', 'Insert', '', 'varchar,number,number', 'required,required,required', ''),
('2021-06-29', 3510, 10, 277, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-29', 3511, 10, 278, '0', 'Name', 'Name', 'Name', 'undefined:id:Name', '', '', '', '', ''),
('2021-06-29', 3512, 10, 278, '1', 'Name', 'Name', '', 'undefined:id:Name', 'Insert', '', 'varchar', 'required', ''),
('2021-06-29', 3513, 10, 278, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-20', 3626, 13, 319, '1', 'Name,Email,Password', 'Name,Email,Password', '', '::,::,::', '', '', 'varchar,email,password', 'required,required,required', ''),
('2021-07-20', 3627, 13, 320, '1', 'Name,Phone,Email,Password,Avatar_Url,Token', 'Name,Phone,Email,Password,Avatar_Url,Token', '', '::,::,::,::,::,::', '', '', 'varchar,tel,email,password,url,varchar', 'required,required,required,required,,required', ''),
('2021-07-20', 3623, 13, 322, '0', 'id,Owner_Id,Name,Price_Per_Hour,OpenAt,CloseAt,Address,Description,Avatar_Url', 'id,Owner_Id,Name,Price_Per_Hour,OpenAt,CloseAt,Address,Description,Avatar_Url', 'id,Owner_Id,Name,Price_Per_Hour,OpenAt,CloseAt,Address,Description,Avatar_Url', '::,321:id:Name,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-07-20', 3624, 13, 323, '0', 'id,User_Id,Field_Id,Book_Day,Start,End,Status,Total,Message', 'id,User_Id,Field_Id,Book_Day,Start,End,Status,Total,Message', 'id,User_Id,Field_Id,Book_Day,Start,End,Status,Total,Message', '::,320:id:Name,322:id:Name,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-07-20', 3625, 13, 324, '0', 'id,Field_Id,Img_Url', 'id,Field_Id,Img_Url', 'id,Field_Id,Img_Url', '::,322:id:Name,::', '', '', '', '', ',,'),
('2021-06-29', 3570, 11, 283, '0', 'id,IdSpecializedCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', 'id,IdSpecializedCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', 'id,IdSpecializedCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', '::,282:id:Title,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-29', 3567, 11, 281, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-29', 3568, 11, 281, '0', 'id,IdCommonCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', 'id,IdCommonCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', 'id,IdCommonCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', '::,280:id:Title,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-07-05', 3569, 11, 283, '1', 'IdSpecializedCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', 'IdSpecializedCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', '', '282:id:Title,::,::,::,::,::', 'INFOMATION SPECIALIZEDVOCABULARY', '', 'select,varchar,varchar,varchar,varchar,varchar', 'required,,required,required,,', ''),
('2021-06-29', 3566, 11, 286, '1', 'IdUser,ExpressName,ExpressDescribe,CreatedAt', 'IdUser,ExpressName,ExpressDescribe,CreatedAt', '', '279:id:IdHocVan,::,::,::', 'INFOMATION HISTORY', 'INFOMATION HISTORY', 'select,varchar,varchar,date', 'required,required,required,required', ''),
('2021-06-29', 3565, 11, 286, '0', 'id,IdUser,ExpressName,ExpressDescribe,CreatedAt', 'id,IdUser,ExpressName,ExpressDescribe,CreatedAt', 'id,IdUser,ExpressName,ExpressDescribe,CreatedAt', '::,279:id:IdHocVan,::,::,::', '', '', '', '', ',,,,'),
('2021-06-29', 3563, 11, 280, '0', 'id,Title,Description,View,ImageURL', 'id,Title,Description,View,ImageURL', 'id,Title,Description,View,ImageURL', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-06-29', 3564, 11, 282, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-06', 3561, 11, 282, '1', 'Chuyên ngành,Mô tả chuyên ngành,Hình ảnh (URL)', 'Title,Description ,ImageURL', '', '::,::,::', 'INFOMATION SPECIALIZEDCATEGORIES', '', 'varchar,varchar,varchar', 'required,,required', ''),
('2021-07-05', 3562, 11, 285, '1', 'EnglishName,EnglishExplain,EnglishSpelling,VietNamName,VietNamExplain,ImageURL', 'EnglishName,EnglishExplain,EnglishSpelling,VietNamName,VietNamExplain,ImageURL', '', '::,::,::,::,::,::', 'INFOMATION DICTIONARY', '', 'varchar,varchar,varchar,varchar,varchar,varchar', 'required,,,required,,', ''),
('2021-06-29', 3556, 11, 283, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-29', 3557, 11, 280, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-05', 3558, 11, 281, '1', 'IdCommonCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', 'IdCommonCategories,ImageVocabulary,EnglishName,VietNamName,Spelling,Description', '', '280:id:Title,::,::,::,::,::', 'INFOMATION COMMONVOCABULARY', '', 'select,varchar,varchar,varchar,varchar,varchar', 'required,,required,required,,', ''),
('2021-06-29', 3553, 11, 285, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-29', 3554, 11, 284, '0', 'id,EducationName', 'id,EducationName', 'id,EducationName', '::,::', '', '', '', '', ','),
('2021-07-06', 3555, 11, 279, '1', 'IdHocVan,AuthKey,Email,Password,Fullname,Phone,Born', 'IdHocVan,AuthKey,Email,Password,Fullname,Phone,Born', '', '284:id:EducationName,::,::,::,::,::,::', 'INFOMATION USER', '', 'select,varchar,varchar,varchar,varchar,varchar,date', 'required,,required,required,required,required,', ''),
('2021-06-29', 3551, 11, 284, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-29', 3552, 11, 285, '0', 'id,EnglishName,EnglishExplain,EnglishSpelling,VietNamName,VietNamExplain,ImageURL', 'id,EnglishName,EnglishExplain,EnglishSpelling,VietNamName,VietNamExplain,ImageURL', 'id,EnglishName,EnglishExplain,EnglishSpelling,VietNamName,VietNamExplain,ImageURL', '::,::,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-06-29', 3548, 11, 279, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-06', 3549, 11, 280, '1', 'Title,Description,ImageURL', 'Title,Description,ImageURL', '', '::,::,::', 'INFOMATION COMMONCATEGORIES', '', 'varchar,varchar,varchar', 'required,required,required', ''),
('2021-07-12', 3595, 8, 267, '1', 'IdPerson,IdStatus,Message,Voice,Place,BeginLat,BeginLng,EndLat,EndLng,TotalMoney,StartTime,EndTime,PhoneCustomer,CreatedAt,UpdatedAt,Distance', 'IdPerson,IdStatus,Message,Voice,Place,BeginLat,BeginLng,EndLat,EndLng,TotalMoney,StartTime,EndTime,PhoneCustomer,CreatedAt,UpdatedAt,Distance', '', '266:id:IdPermission,265:id:Name,::,::,::,::,::,::,::,::,::,::,::,::,::,::', 'INFOMATION SIGNAL', 'INFOMATION SIGNAL', 'select,select,varchar,ckeditor,varchar,varchar,varchar,varchar,varchar,varchar,date,date,varchar,date,date,number', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-06-29', 3550, 11, 284, '1', 'EducationName', 'EducationName', '', '::', 'INFOMATION HOCVAN', 'INFOMATION HOCVAN', 'varchar', 'required', ''),
('2021-06-29', 3547, 11, 279, '0', 'id,IdHocVan,AuthKey,Email,Password,Fullname,Phone,Born', 'id,IdHocVan,AuthKey,Email,Password,Fullname,Phone,Born', 'id,IdHocVan,AuthKey,Email,Password,Fullname,Phone,Born', '::,284:id:EducationName,::,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-06-29', 3559, 11, 286, '3', '', '', '', '', '', '', '', '', ''),
('2021-06-29', 3560, 11, 282, '0', 'id,Title,Description ,View,ImageURL', 'id,Title,Description ,View,ImageURL', 'id,Title,Description ,View,ImageURL', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-07-20', 3628, 13, 321, '1', 'Name,Phone,Email,Password,Avatar_Url,Token', 'Name,Phone,Email,Password,Avatar_Url,Token', '', '::,::,::,::,::,::', '', '', 'varchar,tel,email,password,url,varchar', 'required,required,required,required,,required', ''),
('2021-07-20', 3629, 13, 322, '1', 'Owner_Id,Name,Price_Per_Hour,OpenAt,CloseAt,Address,Description,Avatar_Url', 'Owner_Id,Name,Price_Per_Hour,OpenAt,CloseAt,Address,Description,Avatar_Url', '', '321:id:Name,::,::,::,::,::,::,::', '', '', 'number,varchar,number,datetime,date,varchar,varchar,url', 'required,required,required,required,required,required,,', ''),
('2021-07-20', 3630, 13, 324, '1', 'Field_Id,Img_Url', 'Field_Id,Img_Url', '', '322:id:Name,::', '', '', 'number,url', 'required,', ''),
('2021-07-20', 3631, 13, 319, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-20', 3632, 13, 320, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-20', 3633, 13, 321, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-20', 3634, 13, 322, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-20', 3635, 13, 323, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-20', 3636, 13, 324, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-20', 3637, 13, 319, '0', 'id,Name,Email,Password', 'id,Name,Email,Password', 'id,Name,Email,Password', '::,::,::,::', '', '', '', '', ',,,'),
('2021-07-20', 3638, 13, 320, '0', 'id,Name,Phone,Email,Password,Avatar_Url,Token', 'id,Name,Phone,Email,Password,Avatar_Url,Token', 'id,Name,Phone,Email,Password,Avatar_Url,Token', '::,::,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-07-20', 3639, 13, 321, '0', 'id,Name,Phone,Email,Password,Avatar_Url,Token', 'id,Name,Phone,Email,Password,Avatar_Url,Token', 'id,Name,Phone,Email,Password,Avatar_Url,Token', '::,::,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-07-27', 3818, 14, 312, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPENOTIFICATION', 'INFOMATION TYPENOTIFICATION', 'varchar', 'required', ''),
('2021-07-27', 3819, 14, 304, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3820, 14, 310, '0', 'id,IdPerson,Titile,Content,CreatedAt,UpdatedAt,ListImage', 'id,IdPerson,Titile,Content,CreatedAt,UpdatedAt,ListImage', 'id,IdPerson,Titile,Content,CreatedAt,UpdatedAt,ListImage', '::,289:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-07-27', 3821, 14, 314, '0', 'id,IdCity,IdDistrict,Address,StartDate,EndDate,Content,CreatedAt,UpdatedAt', 'id,IdCity,IdDistrict,Address,StartDate,EndDate,Content,CreatedAt,UpdatedAt', 'id,IdCity,IdDistrict,Address,StartDate,EndDate,Content,CreatedAt,UpdatedAt', '::,290:id:Name,291:id:IdCity,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-07-27', 3817, 14, 318, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3816, 14, 315, '1', 'PhoneContact,NameAccountBank,NumberAccountBank,NameBank,Address,NotePay1,NotePay2,CreatedAt,UpdatedAt', 'PhoneContact,NameAccountBank,NumberAccountBank,NameBank,Address,NotePay1,NotePay2,CreatedAt,UpdatedAt', '', '::,::,::,::,::,::,::,::,::', 'INFOMATION SETTING', 'INFOMATION SETTING', 'varchar,varchar,varchar,varchar,varchar,varchar,varchar,date,date', 'required,required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3814, 14, 310, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-03', 3815, 14, 313, '0', 'id,IdCity,IdDistrict,IdConstructionItems,Address,Name,Phone,NumberCode,BeforeImageCode,AfterImageCode,AmountPeople,Experience,Content,CreatedAt,UpdatedAt,Appoved', 'id,IdCity,IdDistrict,IdConstructionItems,Address,Name,Phone,NumberCode,BeforeImageCode,AfterImageCode,AmountPeople,Experience,Content,CreatedAt,UpdatedAt,Appoved', 'id,IdCity,IdDistrict,IdConstructionItems,Address,Name,Phone,NumberCode,BeforeImageCode,AfterImageCode,AmountPeople,Experience,Content,CreatedAt,UpdatedAt,Appoved', '::,290:id:Name,291:id:IdCity,293:id:Name,::,::,::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,,'),
('2021-07-27', 3813, 14, 311, '0', 'id,IdTypeNotification,IdStatus,IdImage,Title,Content,CreatedAt,UpdatedAt,IdProject', 'id,IdTypeNotification,IdStatus,IdImage,Title,Content,CreatedAt,UpdatedAt,IdProject', 'id,IdTypeNotification,IdStatus,IdImage,Title,Content,CreatedAt,UpdatedAt,IdProject', '::,312:id:Name,292:id:Name,295:id:Url,::,::,::,::,303:id:IdProjectStatus', '', '', '', '', ',,,,,,,,'),
('2021-08-03', 3812, 14, 313, '1', 'IdCity,IdDistrict,IdConstructionItems,Address,Name,Phone,NumberCode,BeforeImageCode,AfterImageCode,AmountPeople,Experience,Content,CreatedAt,UpdatedAt,Appoved', 'IdCity,IdDistrict,IdConstructionItems,Address,Name,Phone,NumberCode,BeforeImageCode,AfterImageCode,AmountPeople,Experience,Content,CreatedAt,UpdatedAt,Appoved', '', '290:id:Name,291:id:IdCity,293:id:Name,::,::,::,::,::,::,::,::,::,::,::,::', 'INFOMATION REGISTRATIONFIVESTAR', '', 'select,select,select,varchar,varchar,varchar,varchar,image,image,varchar,varchar,ckeditor,date,date,', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,', ''),
('2021-07-27', 3811, 14, 309, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3809, 14, 316, '1', 'IdTypeBanner,Tilte,IdImage,CreatedAt,UpdatedAt', 'IdTypeBanner,Tilte,IdImage,CreatedAt,UpdatedAt', '', '317:id:Name,::,295:id:Url,::,::', 'INFOMATION BANNER', 'INFOMATION BANNER', 'select,varchar,select,date,date', 'required,required,required,required,required', ''),
('2021-07-27', 3810, 14, 316, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3808, 14, 318, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUSWORK', 'INFOMATION STATUSWORK', 'varchar', 'required', ''),
('2021-07-27', 3807, 14, 314, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3806, 14, 306, '1', 'IdCategories,Name,Description,Content,ListImage,CreatedAt,UpdatedAt', 'IdCategories,Name,Description,Content,ListImage,CreatedAt,UpdatedAt', '', '307:id:Name,::,::,::,::,::,::', 'INFOMATION PRODUCT', 'INFOMATION PRODUCT', 'select,varchar,varchar,ckeditor,varchar,date,date', 'required,required,required,required,required,required,required', ''),
('2021-07-27', 3804, 14, 315, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3805, 14, 301, '1', 'IdTypeCost,Title,Note,Money,CreatedAt,UpdatedAt,IdPerson', 'IdTypeCost,Title,Note,Money,CreatedAt,UpdatedAt,IdPerson', '', '302:id:Name,::,::,::,::,::,289:id:Name', 'INFOMATION MANAGERCOST', 'INFOMATION MANAGERCOST', 'select,varchar,varchar,number,date,date,select', 'required,required,required,required,required,required,required', ''),
('2021-07-27', 3800, 14, 307, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3801, 14, 300, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3802, 14, 304, '1', 'Name', 'Name', '', '::', 'INFOMATION PROJECTSTATUS', 'INFOMATION PROJECTSTATUS', 'varchar', 'required', ''),
('2021-07-27', 3803, 14, 301, '0', 'id,IdTypeCost,Title,Note,Money,CreatedAt,UpdatedAt,IdPerson', 'id,IdTypeCost,Title,Note,Money,CreatedAt,UpdatedAt,IdPerson', 'id,IdTypeCost,Title,Note,Money,CreatedAt,UpdatedAt,IdPerson', '::,302:id:Name,::,::,::,::,::,289:id:Name', '', '', '', '', ',,,,,,,'),
('2021-07-27', 3799, 14, 307, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3798, 14, 308, '1', 'Title,Content,IdCity,IdDistrict,Address,ListImage,Investor,TimeFinish,CreatedAt,UpdatedAt', 'Title,Content,IdCity,IdDistrict,Address,ListImage,Investor,TimeFinish,CreatedAt,UpdatedAt', '', '::,::,290:id:Name,291:id:IdCity,::,::,::,::,::,::', 'INFOMATION PROJECTFINSH', 'INFOMATION PROJECTFINSH', 'varchar,ckeditor,select,select,varchar,varchar,varchar,varchar,date,date', 'required,required,required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3797, 14, 303, '0', 'id,IdProjectStatus,CustomerId,BuilderId,ListConstructionItems,Title,Content,PriceBuilder,PriceAppoved,StartDate,EndDate,Deadline,ListFile,IdCity,IdDistrict,Address,IdImage,IsDeposit,MoneyDeposit,PriceCustomer,ListImage,NoteReport,CreatedAt,UpdatedAt', 'id,IdProjectStatus,CustomerId,BuilderId,ListConstructionItems,Title,Content,PriceBuilder,PriceAppoved,StartDate,EndDate,Deadline,ListFile,IdCity,IdDistrict,Address,IdImage,IsDeposit,MoneyDeposit,PriceCustomer,ListImage,NoteReport,CreatedAt,UpdatedAt', 'id,IdProjectStatus,CustomerId,BuilderId,ListConstructionItems,Title,Content,PriceBuilder,PriceAppoved,StartDate,EndDate,Deadline,ListFile,IdCity,IdDistrict,Address,IdImage,IsDeposit,MoneyDeposit,PriceCustomer,ListImage,NoteReport,CreatedAt,UpdatedAt', '::,304:id:Name,::,::,::,::,::,::,::,::,::,::,::,290:id:Name,291:id:IdCity,::,295:id:Url,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,,,,,,,,,,,,,'),
('2021-07-27', 3793, 14, 299, '0', 'id,IdPerson,IdProjectStaff,StartCheckingTime,EngCheckingTime,IdCity,IdDistrict,Address,Report,Location,UpdatedAt,CreatedAt', 'id,IdPerson,IdProjectStaff,StartCheckingTime,EngCheckingTime,IdCity,IdDistrict,Address,Report,Location,UpdatedAt,CreatedAt', 'id,IdPerson,IdProjectStaff,StartCheckingTime,EngCheckingTime,IdCity,IdDistrict,Address,Report,Location,UpdatedAt,CreatedAt', '::,289:id:Name,300:id:IdConstructionItems,::,::,290:id:Name,291:id:IdCity,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,,'),
('2021-07-27', 3794, 14, 318, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3795, 14, 308, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3796, 14, 325, '1', 'IdProject,IdProjectStatus,IdPerson,Money,Deadline,CreatedAt,UpdatedAt', 'IdProject,IdProjectStatus,IdPerson,Money,Deadline,CreatedAt,UpdatedAt', '', '303:id:IdProjectStatus,304:id:Name,289:id:Name,::,::,::,::', 'INFOMATION INFOMATIONPRICEQUOTE', 'INFOMATION INFOMATIONPRICEQUOTE', 'select,select,select,number,varchar,date,date', 'required,required,required,required,required,required,required', ''),
('2021-07-27', 3787, 14, 301, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3788, 14, 302, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3789, 14, 309, '0', 'id,CustomerId,NameReceiver,AccoutNumber,Content,Money,IdProject,NameBank,CreatedAt,UpdatedAt', 'id,CustomerId,NameReceiver,AccoutNumber,Content,Money,IdProject,NameBank,CreatedAt,UpdatedAt', 'id,CustomerId,NameReceiver,AccoutNumber,Content,Money,IdProject,NameBank,CreatedAt,UpdatedAt', '::,::,::,::,::,::,303:id:IdProjectStatus,::,::,::', '', '', '', '', ',,,,,,,,,'),
('2021-07-27', 3790, 14, 305, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3791, 14, 302, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3792, 14, 298, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3786, 14, 305, '1', 'IdProject,IdPerson,Title,Content,ListImage,CreatedAt,UpdatedAt', 'IdProject,IdPerson,Title,Content,ListImage,CreatedAt,UpdatedAt', '', '303:id:IdProjectStatus,289:id:Name,::,::,::,::,::', 'INFOMATION REPORT', 'INFOMATION REPORT', 'select,select,varchar,ckeditor,varchar,date,date', 'required,required,required,required,required,required,required', ''),
('2021-07-27', 3785, 14, 298, '1', 'IdPerson,IdStatusWork,IdProjectStaff,Title,StartDate,EndDate,Content,CreatedAt,UpdatedAt', 'IdPerson,IdStatusWork,IdProjectStaff,Title,StartDate,EndDate,Content,CreatedAt,UpdatedAt', '', '289:id:Name,318:id:Name,300:id:IdConstructionItems,::,::,::,::,::,::', 'INFOMATION WORK', 'INFOMATION WORK', 'select,select,select,varchar,date,date,ckeditor,date,date', 'required,required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3783, 14, 304, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3784, 14, 314, '1', 'IdCity,IdDistrict,Address,StartDate,EndDate,Content,CreatedAt,UpdatedAt', 'IdCity,IdDistrict,Address,StartDate,EndDate,Content,CreatedAt,UpdatedAt', '', '290:id:Name,291:id:IdCity,::,::,::,::,::,::', 'INFOMATION REGISTRATIONAPP', 'INFOMATION REGISTRATIONAPP', 'select,select,varchar,date,date,ckeditor,date,date', 'required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3782, 14, 310, '1', 'IdPerson,Titile,Content,CreatedAt,UpdatedAt,ListImage', 'IdPerson,Titile,Content,CreatedAt,UpdatedAt,ListImage', '', '289:id:Name,::,::,::,::,::', 'INFOMATION FEEDBACK', 'INFOMATION FEEDBACK', 'select,varchar,ckeditor,date,date,varchar', 'required,required,required,required,required,required', ''),
('2021-07-27', 3780, 14, 303, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3781, 14, 299, '1', 'IdPerson,IdProjectStaff,StartCheckingTime,EngCheckingTime,IdCity,IdDistrict,Address,Report,Location,UpdatedAt,CreatedAt', 'IdPerson,IdProjectStaff,StartCheckingTime,EngCheckingTime,IdCity,IdDistrict,Address,Report,Location,UpdatedAt,CreatedAt', '', '289:id:Name,300:id:IdConstructionItems,::,::,290:id:Name,291:id:IdCity,::,::,::,::,::', 'INFOMATION TIMEKEEPING', 'INFOMATION TIMEKEEPING', 'select,select,date,date,select,select,varchar,varchar,ckeditor,date,date', 'required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3779, 14, 300, '1', 'IdConstructionItems,IdImage,Title,IdCity,IdDistrict,Address,CreatedAt,UpdatedAt', 'IdConstructionItems,IdImage,Title,IdCity,IdDistrict,Address,CreatedAt,UpdatedAt', '', '293:id:Name,295:id:Url,::,290:id:Name,291:id:IdCity,::,::,::', 'INFOMATION PROJECTSTAFF', 'INFOMATION PROJECTSTAFF', 'select,select,varchar,select,select,varchar,date,date', 'required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3777, 14, 311, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3778, 14, 300, '0', 'id,IdConstructionItems,IdImage,Title,IdCity,IdDistrict,Address,CreatedAt,UpdatedAt', 'id,IdConstructionItems,IdImage,Title,IdCity,IdDistrict,Address,CreatedAt,UpdatedAt', 'id,IdConstructionItems,IdImage,Title,IdCity,IdDistrict,Address,CreatedAt,UpdatedAt', '::,293:id:Name,295:id:Url,::,290:id:Name,291:id:IdCity,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-07-27', 3775, 14, 296, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3776, 14, 316, '0', 'id,IdTypeBanner,Tilte,IdImage,CreatedAt,UpdatedAt', 'id,IdTypeBanner,Tilte,IdImage,CreatedAt,UpdatedAt', 'id,IdTypeBanner,Tilte,IdImage,CreatedAt,UpdatedAt', '::,317:id:Name,::,295:id:Url,::,::', '', '', '', '', ',,,,,'),
('2021-07-27', 3773, 14, 312, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3774, 14, 295, '1', 'Url,CreatedAt', 'Url,CreatedAt', '', '::,::', 'INFOMATION IMAGE', 'INFOMATION IMAGE', 'varchar,date', 'required,required', ''),
('2021-07-27', 3771, 14, 312, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3772, 14, 295, '0', 'id,Url,CreatedAt', 'id,Url,CreatedAt', 'id,Url,CreatedAt', '::,::,::', '', '', '', '', ',,'),
('2021-07-27', 3770, 14, 298, '0', 'id,IdPerson,IdStatusWork,IdProjectStaff,Title,StartDate,EndDate,Content,CreatedAt,UpdatedAt', 'id,IdPerson,IdStatusWork,IdProjectStaff,Title,StartDate,EndDate,Content,CreatedAt,UpdatedAt', 'id,IdPerson,IdStatusWork,IdProjectStaff,Title,StartDate,EndDate,Content,CreatedAt,UpdatedAt', '::,289:id:Name,318:id:Name,300:id:IdConstructionItems,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,'),
('2021-07-27', 3769, 14, 303, '1', 'IdProjectStatus,CustomerId,BuilderId,ListConstructionItems,Title,Content,PriceBuilder,PriceAppoved,StartDate,EndDate,Deadline,ListFile,IdCity,IdDistrict,Address,IdImage,IsDeposit,MoneyDeposit,PriceCustomer,ListImage,NoteReport,CreatedAt,UpdatedAt', 'IdProjectStatus,CustomerId,BuilderId,ListConstructionItems,Title,Content,PriceBuilder,PriceAppoved,StartDate,EndDate,Deadline,ListFile,IdCity,IdDistrict,Address,IdImage,IsDeposit,MoneyDeposit,PriceCustomer,ListImage,NoteReport,CreatedAt,UpdatedAt', '', '304:id:Name,::,::,::,::,::,::,::,::,::,::,::,290:id:Name,291:id:IdCity,::,295:id:Url,::,::,::,::,::,::,::', 'INFOMATION PROJECT', 'INFOMATION PROJECT', 'select,number,number,varchar,varchar,ckeditor,number,number,date,date,varchar,varchar,select,select,number,select,number,number,number,varchar,varchar,date,date', 'required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3767, 14, 294, '1', 'Name,Url,CreatedAt', 'Name,Url,CreatedAt', '', '::,::,::', 'INFOMATION LINKFILE', 'INFOMATION LINKFILE', 'varchar,varchar,date', 'required,required,required', ''),
('2021-07-27', 3768, 14, 297, '1', 'IdPerson,IdImage,Title,Summary,Content,View,CreatedAt,UpdatedAt', 'IdPerson,IdImage,Title,Summary,Content,View,CreatedAt,UpdatedAt', '', '289:id:Name,295:id:Url,::,::,::,::,::,::', 'INFOMATION NEWS', 'INFOMATION NEWS', 'select,select,varchar,varchar,ckeditor,number,date,date', 'required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3766, 14, 297, '0', 'id,IdPerson,IdImage,Title,Summary,Content,View,CreatedAt,UpdatedAt', 'id,IdPerson,IdImage,Title,Summary,Content,View,CreatedAt,UpdatedAt', 'id,IdPerson,IdImage,Title,Summary,Content,View,CreatedAt,UpdatedAt', '::,289:id:Name,295:id:Url,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-07-27', 3760, 14, 292, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3761, 14, 291, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3762, 14, 293, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3763, 14, 295, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3764, 14, 297, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3765, 14, 293, '0', 'id,Name,IdImage', 'id,Name,IdImage', 'id,Name,IdImage', '::,::,295:id:Url', '', '', '', '', ',,'),
('2021-07-27', 3757, 14, 294, '0', 'id,Name,Url,CreatedAt', 'id,Name,Url,CreatedAt', 'id,Name,Url,CreatedAt', '::,::,::,::', '', '', '', '', ',,,'),
('2021-07-27', 3758, 14, 296, '1', 'Name', 'Name', '', '::', 'INFOMATION PERMISSION', 'INFOMATION PERMISSION', 'varchar', 'required', ''),
('2021-07-27', 3759, 14, 292, '1', 'Name', 'Name', '', '::', 'INFOMATION STATUS', 'INFOMATION STATUS', 'varchar', 'required', ''),
('2021-07-27', 3756, 14, 296, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3755, 14, 291, '1', 'IdCity,Name', 'IdCity,Name', '', '290:id:Name,::', 'INFOMATION DISTRICT', 'INFOMATION DISTRICT', 'select,varchar', 'required,required', ''),
('2021-07-27', 3754, 14, 289, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3752, 14, 294, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3753, 14, 289, '1', 'Name,Email,Phone,Password,Address,Sex,Brithday,IdImage,IdPermission,DeviceToken,CreatedAt,UpdatedAt', 'Name,Email,Phone,Password,Address,Sex,Brithday,IdImage,IdPermission,DeviceToken,CreatedAt,UpdatedAt', '', '::,::,::,::,::,::,::,295:id:Url,296:id:Name,::,::,::', 'INFOMATION PERSON', 'INFOMATION PERSON', 'varchar,varchar,varchar,varchar,varchar,number,varchar,select,select,ckeditor,date,date', 'required,required,required,required,required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3751, 14, 293, '1', 'Name,IdImage', 'Name,IdImage', '', '::,295:id:Url', 'INFOMATION CONSTRUCTIONITEMS', 'INFOMATION CONSTRUCTIONITEMS', 'varchar,select', 'required,required', ''),
('2021-07-27', 3750, 14, 290, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3749, 14, 289, '0', 'id,Name,Email,Phone,Password,Address,Sex,Brithday,IdImage,IdPermission,DeviceToken,CreatedAt,UpdatedAt', 'id,Name,Email,Phone,Password,Address,Sex,Brithday,IdImage,IdPermission,DeviceToken,CreatedAt,UpdatedAt', 'id,Name,Email,Phone,Password,Address,Sex,Brithday,IdImage,IdPermission,DeviceToken,CreatedAt,UpdatedAt', '::,::,::,::,::,::,::,::,295:id:Url,296:id:Name,::,::,::', '', '', '', '', ',,,,,,,,,,,,'),
('2021-07-27', 3745, 14, 291, '0', 'id,IdCity,Name', 'id,IdCity,Name', 'id,IdCity,Name', '::,290:id:Name,::', '', '', '', '', ',,'),
('2021-07-27', 3746, 14, 292, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3747, 14, 290, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3748, 14, 290, '1', 'Name', 'Name', '', '::', 'INFOMATION CITY', 'INFOMATION CITY', 'varchar', 'required', ''),
('2021-07-23', 3733, 15, 326, '0', 'id,value', 'id,value', 'id,value', '::,::', '', '', '', '', ','),
('2021-07-23', 3734, 15, 326, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-23', 3735, 15, 326, '1', 'value', 'value', '', '::', 'INFOMATION TEAMPLATE', 'INFOMATION TEAMPLATE', 'varchar', 'required', ''),
('2021-07-23', 3736, 15, 327, '1', 'Fullname,Company,Address,Position,Avatar,NumberOfView,IdTeamplate,Token', 'Fullname,Company,Address,Position,Avatar,NumberOfView,IdTeamplate,Token', '', '::,::,::,::,::,::,326:id:value,::', 'INFOMATION USER', 'INFOMATION USER', 'varchar,varchar,varchar,varchar,varchar,number,select,varchar', 'required,required,required,required,required,required,required,required', ''),
('2021-07-23', 3737, 15, 327, '0', 'id,Fullname,Company,Address,Position,Avatar,NumberOfView,IdTeamplate,Token', 'id,Fullname,Company,Address,Position,Avatar,NumberOfView,IdTeamplate,Token', 'id,Fullname,Company,Address,Position,Avatar,NumberOfView,IdTeamplate,Token', '::,::,::,::,::,::,::,326:id:value,::', '', '', '', '', ',,,,,,,,'),
('2021-07-23', 3738, 15, 327, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-23', 3739, 15, 328, '0', 'id,Name,Image,Type', 'id,Name,Image,Type', 'id,Name,Image,Type', '::,::,::,::', '', '', '', '', ',,,'),
('2021-07-23', 3740, 15, 329, '1', 'IdUser,IdInformation,NumberInteraction,Value', 'IdUser,IdInformation,NumberInteraction,Value', '', '327:id:Fullname,328:id:Name,::,::', 'INFOMATION USERCONECTION', 'INFOMATION USERCONECTION', 'select,select,number,varchar', 'required,required,required,required', ''),
('2021-07-23', 3741, 15, 328, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-23', 3742, 15, 328, '1', 'Name,Image,Type', 'Name,Image,Type', '', '::,::,::', 'INFOMATION INFORMATION', 'INFOMATION INFORMATION', 'varchar,varchar,number', 'required,required,required', ''),
('2021-07-23', 3743, 15, 329, '0', 'id,IdUser,IdInformation,NumberInteraction,Value', 'id,IdUser,IdInformation,NumberInteraction,Value', 'id,IdUser,IdInformation,NumberInteraction,Value', '::,327:id:Fullname,328:id:Name,::,::', '', '', '', '', ',,,,'),
('2021-07-23', 3744, 15, 329, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3822, 14, 307, '1', 'Name', 'Name', '', '::', 'INFOMATION CATEGORIES', 'INFOMATION CATEGORIES', 'varchar', 'required', ''),
('2021-07-27', 3823, 14, 306, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3824, 14, 311, '1', 'IdTypeNotification,IdStatus,IdImage,Title,Content,CreatedAt,UpdatedAt,IdProject', 'IdTypeNotification,IdStatus,IdImage,Title,Content,CreatedAt,UpdatedAt,IdProject', '', '312:id:Name,292:id:Name,295:id:Url,::,::,::,::,303:id:IdProjectStatus', 'INFOMATION NOTIFICATION', 'INFOMATION NOTIFICATION', 'select,select,select,varchar,ckeditor,date,date,select', 'required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3825, 14, 325, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3826, 14, 309, '1', 'CustomerId,NameReceiver,AccoutNumber,Content,Money,IdProject,NameBank,CreatedAt,UpdatedAt', 'CustomerId,NameReceiver,AccoutNumber,Content,Money,IdProject,NameBank,CreatedAt,UpdatedAt', '', '::,::,::,::,::,303:id:IdProjectStatus,::,::,::', 'INFOMATION TRANSACTIONHISTORY', 'INFOMATION TRANSACTIONHISTORY', 'number,varchar,varchar,ckeditor,number,select,varchar,varchar,varchar', 'required,required,required,required,required,required,required,required,required', ''),
('2021-07-27', 3827, 14, 317, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPEBANNER', 'INFOMATION TYPEBANNER', 'varchar', 'required', ''),
('2021-07-27', 3828, 14, 315, '0', 'id,PhoneContact,NameAccountBank,NumberAccountBank,NameBank,Address,NotePay1,NotePay2,CreatedAt,UpdatedAt', 'id,PhoneContact,NameAccountBank,NumberAccountBank,NameBank,Address,NotePay1,NotePay2,CreatedAt,UpdatedAt', 'id,PhoneContact,NameAccountBank,NumberAccountBank,NameBank,Address,NotePay1,NotePay2,CreatedAt,UpdatedAt', '::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,'),
('2021-07-27', 3829, 14, 317, '0', 'id,Name', 'id,Name', 'id,Name', '::,::', '', '', '', '', ','),
('2021-07-27', 3830, 14, 302, '1', 'Name', 'Name', '', '::', 'INFOMATION TYPECOST', 'INFOMATION TYPECOST', 'varchar', 'required', ''),
('2021-07-27', 3831, 14, 325, '0', 'id,IdProject,IdProjectStatus,IdPerson,Money,Deadline,CreatedAt,UpdatedAt', 'id,IdProject,IdProjectStatus,IdPerson,Money,Deadline,CreatedAt,UpdatedAt', 'id,IdProject,IdProjectStatus,IdPerson,Money,Deadline,CreatedAt,UpdatedAt', '::,303:id:IdProjectStatus,304:id:Name,289:id:Name,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-07-27', 3832, 14, 313, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3833, 14, 306, '0', 'id,IdCategories,Name,Description,Content,ListImage,CreatedAt,UpdatedAt', 'id,IdCategories,Name,Description,Content,ListImage,CreatedAt,UpdatedAt', 'id,IdCategories,Name,Description,Content,ListImage,CreatedAt,UpdatedAt', '::,307:id:Name,::,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-07-27', 3834, 14, 299, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3835, 14, 305, '0', 'id,IdProject,IdPerson,Title,Content,ListImage,CreatedAt,UpdatedAt', 'id,IdProject,IdPerson,Title,Content,ListImage,CreatedAt,UpdatedAt', 'id,IdProject,IdPerson,Title,Content,ListImage,CreatedAt,UpdatedAt', '::,303:id:IdProjectStatus,289:id:Name,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-07-27', 3836, 14, 317, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-27', 3837, 14, 308, '0', 'id,Title,Content,IdCity,IdDistrict,Address,ListImage,Investor,TimeFinish,CreatedAt,UpdatedAt', 'id,Title,Content,IdCity,IdDistrict,Address,ListImage,Investor,TimeFinish,CreatedAt,UpdatedAt', 'id,Title,Content,IdCity,IdDistrict,Address,ListImage,Investor,TimeFinish,CreatedAt,UpdatedAt', '::,::,::,290:id:Name,291:id:IdCity,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,,'),
('2021-07-28', 3925, 16, 335, '1', 'Time,IdProject,notificationTypeId', 'Time,IdProject,notificationTypeId', '', '::,336:id:name,::', 'INFOMATION NOTIFICATION', 'INFOMATION NOTIFICATION', 'date,select,number', 'required,required,required', ''),
('2021-07-28', 3924, 16, 334, '0', 'id,name', 'id,name', 'id,name', '::,::', '', '', '', '', ','),
('2021-07-28', 3921, 16, 333, '1', 'idUser,message,time', 'idUser,message,time', '', '330:id:email,::,::', 'INFOMATION MESSAGE', 'INFOMATION MESSAGE', 'select,varchar,date', 'required,required,required', ''),
('2021-07-28', 3922, 16, 335, '0', 'id,Time,IdProject,notificationTypeId', 'id,Time,IdProject,notificationTypeId', 'id,Time,IdProject,notificationTypeId', '::,::,336:id:name,::', '', '', '', '', ',,,'),
('2021-07-28', 3923, 16, 334, '1', 'name', 'name', '', '::', 'INFOMATION NOTIFICATIONTYPE', 'INFOMATION NOTIFICATIONTYPE', 'varchar', 'required', ''),
('2021-07-28', 3904, 16, 337, '0', 'id,IdProject,IdUser', 'id,IdProject,IdUser', 'id,IdProject,IdUser', '::,336:id:id,330:id:id', '', '', '', '', ',,'),
('2021-07-28', 3905, 16, 337, '1', 'id,IdProject,IdUser', 'id,IdProject,IdUser', '', '::,336:id:id,330:id:id', '', '', 'number,number,number', 'required,required,required', ''),
('2021-07-28', 3920, 16, 332, '1', 'name,state,startTime,endTime,idLabel,idProject', 'name,state,startTime,endTime,idLabel,idProject', '', '::,::,::,::,331:id:name,336:id:name', 'INFOMATION TASK', 'INFOMATION TASK', 'varchar,varchar,date,date,select,select', 'required,required,required,required,required,required', ''),
('2021-07-28', 3918, 16, 333, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3919, 16, 336, '1', 'name,startTime,endTime,desc,idUser', 'name,startTime,endTime,desc,idUser', '', '::,::,::,::,330:id:id', 'INFOMATION PROJECT', '', 'varchar,date,date,varchar,number', 'required,required,required,required,required', ''),
('2021-07-28', 3887, 16, 331, '1', 'id,name,idProject', 'id,name,idProject', '', '::,::,336:id:id', '', '', 'number,varchar,number', 'required,required,required', ''),
('2021-07-28', 3886, 16, 331, '0', 'id,name,idProject', 'id,name,idProject', 'id,name,idProject', '::,::,336:id:name', '', '', '', '', ',,'),
('2021-07-28', 3911, 16, 342, '1', 'id,idUser,idTask', 'id,idUser,idTask', '', '::,330:id:id,332:id:id', '', '', 'number,number,number', 'required,required,required', ''),
('2021-07-28', 3914, 16, 332, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3915, 16, 330, '0', 'id,email,password,role,avatar', 'id,email,password,role,avatar', 'id,email,password,role,avatar', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-07-27', 3864, 16, 338, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3865, 16, 338, '1', 'id,IdTask,IdUser,IdProject', 'id,IdTask,IdUser,IdProject', '', '::,332:id:name,330:id:email,336:id:name', '', '', 'number,number,number,number', 'required,required,required,required', ''),
('2021-07-28', 3909, 16, 339, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3910, 16, 342, '0', 'id,idUser,idTask', 'id,idUser,idTask', 'id,idUser,idTask', '::,330:id:id,332:id:id', '', '', '', '', ',,'),
('2021-07-28', 3908, 16, 339, '1', 'id,IdUser,IdNotification', 'id,IdUser,IdNotification', '', '::,330:id:id,335:id:id', '', '', 'number,number,number', 'required,required,required', ''),
('2021-07-28', 3888, 16, 331, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3916, 16, 330, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3917, 16, 336, '0', 'id,name,startTime,endTime,name,idUser', 'id,name,startTime,endTime,name,idUser', 'id,name,startTime,endTime,name,idUser', '::,::,::,::,::,330:id:id', '', '', '', '', ',,,,,'),
('2021-07-28', 3913, 16, 330, '1', 'email,password,role,avatar', 'email,password,role,avatar', '', '::,::,::,::', 'INFOMATION USER', 'INFOMATION USER', 'varchar,varchar,varchar,varchar', 'required,required,required,required', ''),
('2021-07-28', 3873, 16, 340, '0', 'id,idUser,idProject,idTask', 'id,idUser,idProject,idTask', 'id,idUser,idProject,idTask', '::,330:id:email,336:id:name,332:id:name', '', '', '', '', ',,,'),
('2021-07-28', 3874, 16, 340, '1', 'id,idUser,idProject,idTask', 'id,idUser,idProject,idTask', '', '::,330:id:email,336:id:name,332:id:name', '', '', 'varchar,number,number,number', 'required,required,required,required', ''),
('2021-07-28', 3875, 16, 340, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3876, 16, 340, '2', 'id,idUser,idProject,idTask', 'id,idUser,idProject,idTask', '', '::,330:id:email,336:id:name,332:id:name', '', 'InsertJoinUserTask', ',number,number,number', ',required,required,required', ''),
('2021-07-28', 3877, 17, 341, '1', 'email', 'email', '', '::', 'INFOMATION USER', 'INFOMATION USER', 'varchar', 'required', ''),
('2021-07-28', 3878, 17, 341, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3879, 17, 341, '0', 'id,email', 'id,email', 'id,email', '::,::', '', '', '', '', ','),
('2021-07-28', 3928, 16, 333, '0', 'id,idUser,message,time', 'id,idUser,message,time', 'id,idUser,message,time', '::,330:id:email,::,::', '', '', '', '', ',,,'),
('2021-07-28', 3926, 16, 334, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3927, 16, 332, '0', 'id,name,state,startTime,endTime,idLabel,idProject', 'id,name,state,startTime,endTime,idLabel,idProject', 'id,name,state,startTime,endTime,idLabel,idProject', '::,::,::,::,::,331:id:name,336:id:name', '', '', '', '', ',,,,,,'),
('2021-07-28', 3912, 16, 342, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3929, 16, 335, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3930, 16, 336, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3931, 16, 344, '0', 'id,idProject,idUser', 'id,idProject,idUser', 'id,idProject,idUser', '::,336:id:name,330:id:email', '', '', '', '', ',,'),
('2021-07-28', 3932, 16, 343, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3933, 16, 344, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-28', 3934, 16, 344, '1', 'idProject,idUser', 'idProject,idUser', '', '336:id:name,330:id:email', 'INFOMATION  PROJECTCLASSIFICATION', '', 'select,select', 'required,required', ''),
('2021-07-28', 3935, 16, 343, '0', 'id,idUser,idTask', 'id,idUser,idTask', 'id,idUser,idTask', '::,330:id:id,332:id:id', '', '', '', '', ',,'),
('2021-07-28', 3936, 16, 343, '1', 'id,idUser,idTask', 'id,idUser,idTask', '', '::,330:id:id,332:id:id', '', '', 'number,number,number', ',,', ''),
('2021-08-01', 3937, 19, 345, '1', 'name,tgbatdau,tgketthuc,trangthai,mieuta', 'name,tgbatdau,tgketthuc,trangthai,mieuta', '', '::,::,::,::,::', 'INFOMATION DUAN', '', 'varchar,date,date,number,varchar', 'required,required,required,required,required', ''),
('2021-07-29', 3938, 19, 347, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-01', 3939, 19, 348, '1', 'name,tgbatdau,tgketthuc,trangthai,idNhan,mieuta,idDuAn', 'name,tgbatdau,tgketthuc,trangthai,idNhan,mieuta,idDuAn', '', '::,::,::,::,349:id:name,::,345:id:name', 'INFOMATION NHIEMVU', '', 'varchar,date,date,number,select,varchar,select', 'required,required,required,required,required,required,required', ''),
('2021-07-29', 3940, 19, 350, '0', 'id,idNhiemVu,idUser', 'id,idNhiemVu,idUser', 'id,idNhiemVu,idUser', '::,348:id:name,346:id:name', '', '', '', '', ',,'),
('2021-07-29', 3941, 19, 350, '1', 'idNhiemVu,idUser', 'idNhiemVu,idUser', '', '348:id:name,346:id:name', 'INFOMATION PHANLOAINHIEMVU', 'INFOMATION PHANLOAINHIEMVU', 'select,select', 'required,required', ''),
('2021-07-29', 3942, 19, 345, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3943, 19, 347, '1', 'idDuAn,idUser', 'idDuAn,idUser', '', '345:id:name,346:id:name', 'INFOMATION PHANLOAIDUAN', '', 'select,select', 'required,required', ''),
('2021-07-29', 3944, 19, 348, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3945, 19, 351, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3946, 19, 351, '1', 'idDuAn,ThongDiep,idUser,thoiGian', 'idDuAn,ThongDiep,idUser,thoiGian', '', '345:id:name,::,346:id:name,::', 'INFOMATION BINHLUAN', '', 'select,varchar,select,datetime', 'required,required,required,required', ''),
('2021-07-29', 3947, 19, 346, '1', 'name,email,password,role,avatar', 'name,email,password,role,avatar', '', '::,::,::,::,::', 'INFOMATION USER', '', 'varchar,varchar,varchar,varchar,varchar', 'required,required,required,required,required', ''),
('2021-07-29', 3948, 19, 349, '0', 'id,name,idDuAn', 'id,name,idDuAn', 'id,name,idDuAn', '::,::,345:id:name', '', '', '', '', ',,'),
('2021-07-29', 3949, 19, 348, '0', 'id,name,tgbatdau,tgketthuc,trangthai,idNhan,mieuta', 'id,name,tgbatdau,tgketthuc,trangthai,idNhan,mieuta', 'id,name,tgbatdau,tgketthuc,trangthai,idNhan,mieuta', '::,::,::,::,::,349:id:name,::', '', '', '', '', ',,,,,,'),
('2021-07-29', 3950, 19, 350, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3951, 19, 346, '0', 'id,name,email,password,role,avatar', 'id,name,email,password,role,avatar', 'id,name,email,password,role,avatar', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-07-29', 3952, 19, 346, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3953, 19, 347, '0', 'id,idDuAn,idUser', 'id,idDuAn,idUser', 'id,idDuAn,idUser', '::,345:id:name,346:id:name', '', '', '', '', ',,'),
('2021-07-29', 3954, 19, 349, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-01', 3955, 19, 349, '1', 'name,idDuAn', 'name,idDuAn', '', '::,345:id:name', 'INFOMATION NHAN', '', 'varchar,select', 'required,required', ''),
('2021-07-29', 3956, 19, 351, '0', 'id,idDuAn,ThongDiep,idUser,thoiGian', 'id,idDuAn,ThongDiep,idUser,thoiGian', 'id,idDuAn,ThongDiep,idUser,thoiGian', '::,345:id:name,::,346:id:name,::', '', '', '', '', ',,,,'),
('2021-07-29', 3957, 19, 345, '0', 'id,name,tgbatdau,tgketthuc,trangthai,mieuta', 'id,name,tgbatdau,tgketthuc,trangthai,mieuta', 'id,name,tgbatdau,tgketthuc,trangthai,mieuta', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-07-29', 3958, 19, 354, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3959, 19, 353, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3960, 19, 353, '0', 'id,Loai', 'id,Loai', 'id,Loai', '::,::', '', '', '', '', ','),
('2021-07-29', 3961, 19, 354, '1', 'idUser,idDuAn,idLoaiThongBao', 'idUser,idDuAn,idLoaiThongBao', '', '346:id:name,345:id:name,353:id:Loai', 'INFOMATION THONGBAO', 'INFOMATION THONGBAO', 'select,select,select', 'required,required,required', ''),
('2021-07-29', 3962, 19, 352, '3', '', '', '', '', '', '', '', '', ''),
('2021-07-29', 3963, 19, 352, '0', 'id,idNhiemVu,ThongDiep,idUser,ThoiGian', 'id,idNhiemVu,ThongDiep,idUser,ThoiGian', 'id,idNhiemVu,ThongDiep,idUser,ThoiGian', '::,348:id:name,::,346:id:name,::', '', '', '', '', ',,,,');
INSERT INTO `a1crud_crud` (`createdate`, `id`, `idproject`, `idclass`, `actiontype`, `titles`, `properties`, `alias`, `refers`, `titlecreate`, `titleupdate`, `inputtypes`, `validates`, `formats`) VALUES
('2021-07-29', 3964, 19, 352, '1', 'idNhiemVu,ThongDiep,idUser,ThoiGian', 'idNhiemVu,ThongDiep,idUser,ThoiGian', '', '348:id:name,::,346:id:name,::', 'INFOMATION BINHLUANNHIEMVU', '', 'select,varchar,select,datetime', 'required,required,required,required', ''),
('2021-07-29', 3965, 19, 353, '1', 'Loai', 'Loai', '', '::', 'INFOMATION LOAITHONGBAO', '', 'varchar', 'required', ''),
('2021-07-29', 3966, 19, 354, '0', 'id,idUser,idDuAn,idLoaiThongBao', 'id,idUser,idDuAn,idLoaiThongBao', 'id,idUser,idDuAn,idLoaiThongBao', '::,346:id:name,345:id:name,353:id:Loai', '', '', '', '', ',,,'),
('2021-08-01', 3967, 19, 345, '2', 'name,tgbatdau,tgketthuc,trangthai,mieuta', 'name,tgbatdau,tgketthuc,trangthai,mieuta', '', '::,::,::,::,::', '', '', 'varchar,date,date,number,varchar', ',,,,', ''),
('2021-08-01', 3968, 19, 348, '2', 'name,tgbatdau,tgketthuc,trangthai,idNhan,mieuta,idDuAn', 'name,tgbatdau,tgketthuc,trangthai,idNhan,mieuta,idDuAn', '', '::,::,::,::,349:id:name,::,345:id:name', '', '', 'varchar,date,date,number,number,varchar,number', 'required,required,required,required,required,required,required', ''),
('2021-08-09', 3969, 8, 355, '0', 'id,IdSignal,Lat,Lng,Distance,CreatedAt', 'id,IdSignal,Lat,Lng,Distance,CreatedAt', 'id,IdSignal,Lat,Lng,Distance,CreatedAt', '::,267:id:IdPerson,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-08-09', 3970, 8, 355, '1', 'IdSignal,Lat,Lng,Distance,CreatedAt', 'IdSignal,Lat,Lng,Distance,CreatedAt', '', '267:id:IdPerson,::,::,::,::', 'INFOMATION SAVELOCATION', 'INFOMATION SAVELOCATION', 'select,varchar,varchar,number,date', 'required,required,required,required,required', ''),
('2021-08-09', 3971, 8, 355, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 3972, 21, 357, '0', 'id,username,productname', 'id,username,productname', 'id,username,productname', '::,::,::', '', '', '', '', ',,'),
('2021-08-22', 3973, 21, 357, '1', 'username,productname', 'username,productname', '', '::,::', '', '', 'varchar,varchar', 'required,required', ''),
('2021-08-22', 3974, 21, 357, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 3975, 21, 357, '2', 'id,username,productname', 'id,username,productname', '', '::,::,::', '', '', ',,', ',,', ''),
('2021-08-22', 3976, 21, 358, '0', 'id,lname,fname,user_name,email,phone,address,password,created_at,updated_at', 'id,lname,fname,user_name,email,phone,address,password,created_at,updated_at', 'id,lname,fname,user_name,email,phone,address,password,created_at,updated_at', '::,::,::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,,'),
('2021-08-22', 3977, 21, 358, '1', 'lname,fname,user_name,email,password', 'lname,fname,user_name,email,password', '', '::,::,::,::,::', '', '', 'varchar,varchar,varchar,email,password', 'required,required,,required:email,required', ''),
('2021-08-22', 3978, 21, 358, '2', 'id,lname,fname,phone,email,address,password', 'id,lname,fname,phone,email,address,password', '', '::,::,::,::,::,::,::', '', '', ',varchar,varchar,tel,email,varchar,password', ',,,,,,', ''),
('2021-08-22', 3979, 21, 358, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 3980, 21, 359, '0', 'id,name,price,discount,quantity,image', 'id,name,price,discount,quantity,image', 'id,name,price,discount,quantity,image', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-08-22', 3981, 21, 359, '1', 'name,price,discount,quantity,image', 'name,price,discount,quantity,image', '', '::,::,::,::,::', '', '', 'varchar,number,number,number,image', ',,,,', ''),
('2021-08-22', 3982, 21, 359, '2', 'name,price,discount,quantity,image', 'name,price,discount,quantity,image', '', '::,::,::,::,::', '', '', ',number,number,number,image', ',,,,', ''),
('2021-08-22', 3983, 21, 359, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 3984, 21, 360, '0', 'id,user_id,customer,phone,address,total,transaction_status,status,created_at', 'id,user_id,customer,phone,address,total,transaction_status,status,created_at', 'id,user_id,customer,phone,address,total,transaction_status,status,created_at', '::,358:id:user_name,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,,'),
('2021-08-22', 3985, 21, 360, '1', 'user_id,customer,phone,address,total,created_at', 'user_id,customer,phone,address,total,created_at', '', '::,::,::,::,::,::', '', '', ',,,,,', ',,,,,', ''),
('2021-08-22', 3986, 21, 362, '0', 'id,user_id,title,details,image,created_at', 'id,user_id,title,details,image,created_at', 'id,user_id,title,details,image,created_at', '::,358:id:user_name,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-08-22', 3996, 21, 362, '1', 'user_id,title,details,image', 'user_id,title,details,image', '', '::,::,::,::', '', '', ',,,', ',,,', ''),
('2021-08-22', 3988, 21, 362, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 3989, 21, 363, '0', 'user_id,details,post_id,', 'user_id,details,post_id,', 'user_id,details,post_id,', '358:id:user_name,::,::,::', '', '', '', '', ',,,'),
('2021-08-22', 3990, 21, 363, '1', 'user_id,details,post_id', 'user_id,details,post_id', '', '::,::,::', '', '', 'number,varchar,number', ',,', ''),
('2021-08-22', 3991, 21, 363, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 3992, 21, 361, '0', 'id,order_id,product_id,quantity,price,total', 'id,order_id,product_id,quantity,price,total', 'id,order_id,product_id,quantity,price,total', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-08-22', 3993, 21, 360, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 3994, 21, 361, '1', 'order_id,product_id,quantity,price,total', 'order_id,product_id,quantity,price,total', '', '::,::,::,::,::', 'INFOMATION ORDER_DETAIL', 'INFOMATION ORDER_DETAIL', 'number,number,number,number,number', 'required,required,required,required,required', ''),
('2021-08-22', 3995, 21, 361, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 3997, 21, 364, '0', 'id,name,email,phone,address', 'id,name,email,phone,address', 'id,name,email,phone,address', 'undefined::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-08-22', 3998, 21, 364, '1', 'lname,fname,email,phone,password', 'lname,fname,email,phone,password', '', '::,::,::,undefined::,::', 'list_user', '', 'varchar,varchar,email,number,password', 'required,required,required,,required', ''),
('2021-08-22', 3999, 21, 364, '2', 'lname,fname,email,phone', 'lname,fname,email,phone', '', '::,::,::,::', '', 'update_user', 'varchar,varchar,email,number', 'required,required,required,', ''),
('2021-08-22', 4000, 21, 364, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 4001, 21, 365, '0', 'id,name,image,price,discount,quantity,created_at,updated_at', 'id,name,image,price,discount,quantity,created_at,updated_at', 'id,name,image,price,discount,quantity,created_at,updated_at', '::,::,::,::,::,::,::,::', '', '', '', '', ',,,,,,,'),
('2021-08-22', 4002, 21, 365, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 4003, 21, 365, '1', 'name,image,price,discount,quantity,created_at,updated_at', 'name,image,price,discount,quantity,created_at,updated_at', '', '::,::,::,::,::,::,::', 'INFOMATION PRODUCTS', 'INFOMATION PRODUCTS', 'varchar,varchar,number,number,number,date,date', 'required,required,required,required,required,required,required', ''),
('2021-08-22', 4004, 21, 366, '0', 'id,user_id,title,details,image,created_at,updated_at', 'id,user_id,title,details,image,created_at,updated_at', 'id,user_id,title,details,image,created_at,updated_at', '::,::,::,::,::,::,::', '', '', '', '', ',,,,,,'),
('2021-08-22', 4005, 21, 366, '1', 'user_id,title,details,image,created_at,updated_at', 'user_id,title,details,image,created_at,updated_at', '', '::,::,::,::,::,::', 'INFOMATION POSTS', 'INFOMATION POSTS', 'number,varchar,ckeditor,varchar,date,date', 'required,required,required,required,required,required', ''),
('2021-08-22', 4006, 21, 367, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 4007, 21, 366, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 4008, 21, 367, '0', 'id,user_id,post_id,details,created_at,updated_at', 'id,user_id,post_id,details,created_at,updated_at', 'id,user_id,post_id,details,created_at,updated_at', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-08-22', 4009, 21, 367, '1', 'user_id,post_id,details,created_at,updated_at', 'user_id,post_id,details,created_at,updated_at', '', '::,::,::,::,::', 'INFOMATION COMMENTS', 'INFOMATION COMMENTS', 'number,number,ckeditor,date,date', 'required,required,required,required,required', ''),
('2021-08-22', 4010, 22, 368, '0', 'id,user_name,email,phone,address', 'id,user_name,email,phone,address', 'id,user_name,email,phone,address', '::,::,::,::,::', '', '', '', '', ',,,,'),
('2021-08-22', 4011, 22, 368, '1', 'lname,fname,email,address,password', 'lname,fname,email,address,password', '', '::,::,::,::,::', 'create', '', 'varchar,varchar,,,', ',,,,', ''),
('2021-08-22', 4012, 22, 368, '2', 'lname,fname,user_name,email,phone', 'lname,fname,user_name,email,phone', '', '::,::,::,::,::', '', '', 'varchar,varchar,varchar,email,number', ',,,,', ''),
('2021-08-22', 4013, 22, 368, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 4014, 22, 369, '0', 'id,name,image,price,discount,quantity', 'id,name,image,price,discount,quantity', 'id,name,image,price,discount,quantity', '::,::,::,::,::,::', '', '', '', '', ',,,,,'),
('2021-08-22', 4015, 22, 369, '1', 'name,image,price,discount,quantity', 'name,image,price,discount,quantity', '', '::,::,::,::,::', 'create_products', '', 'varchar,image,number,number,number', 'required,,required,,required', ''),
('2021-08-22', 4016, 22, 369, '2', 'name,image,price,discount,quantity', 'name,image,price,discount,quantity', '', '::,::,::,::,::', '', '', 'varchar,image,number,number,number', 'required,required,required,,required', ''),
('2021-08-22', 4017, 22, 369, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 4018, 22, 370, '0', 'id,title,image,details', 'id,title,image,details', 'id,title,image,details', '::,::,::,::', '', '', '', '', ',,,'),
('2021-08-22', 4019, 22, 370, '1', 'title,image,details', 'title,image,details', '', '::,::,::', 'create_post', '', ',,', ',,', ''),
('2021-08-22', 4020, 22, 370, '2', 'title,image,details', 'title,image,details', '', '::,::,::', '', '', 'varchar,image,varchar', 'required,,required', ''),
('2021-08-22', 4021, 22, 370, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 4022, 22, 371, '0', 'user_id,post_id,details', 'user_id,post_id,details', 'user_id,post_id,details', '368:id:user_name,370:id:title,::', '', '', '', '', ',,'),
('2021-08-22', 4023, 22, 371, '1', 'user_id,post_id,details', 'user_id,post_id,details', '', '368:id:user_name,370:id:title,::', '', '', 'select,select,varchar', ',,required', ''),
('2021-08-22', 4024, 22, 371, '3', '', '', '', '', '', '', '', '', ''),
('2021-08-22', 4025, 22, 371, '2', 'user_id,post_id,details', 'user_id,post_id,details', '', '368:id:user_name,::,370:id:title', '', '', 'select,select,varchar', ',,', '');

-- --------------------------------------------------------

--
-- Table structure for table `a1crud_project`
--

CREATE TABLE `a1crud_project` (
  `createdate` date NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `a1crud_project`
--

INSERT INTO `a1crud_project` (`createdate`, `id`, `name`) VALUES
('2020-11-01', 1, 'rolieadmin'),
('2020-12-01', 3, 'TestCRUD'),
('2020-12-07', 4, 'gameaminedefend'),
('2021-02-18', 5, 'HCE_Survey'),
('2021-06-16', 6, 'VaoRa'),
('2021-06-16', 7, 'SohaDriving'),
('2021-06-25', 8, 'BookingCar'),
('2021-06-26', 9, 'quan_ly_ve_bong_da'),
('2021-06-29', 10, 'DuongTest'),
('2021-06-29', 11, 'EVocabulary '),
('2021-07-19', 12, 'Duong19072021Test'),
('2021-07-20', 13, 'datsanbong'),
('2021-07-20', 14, 'FiveSBS'),
('2021-07-23', 15, 'so_hoa_card_visit'),
('2021-07-27', 16, 'TestDemo'),
('2021-07-28', 18, 'DevOps'),
('2021-07-28', 17, 'Demo'),
('2021-07-29', 19, 'Quan_ly_du_an'),
('2021-08-21', 20, 'demotest'),
('2021-08-22', 21, 'PETSHOPTEST'),
('2021-08-22', 22, 'API_Pet');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add user dashboard module', 1, 'add_userdashboardmodule'),
(2, 'Can change user dashboard module', 1, 'change_userdashboardmodule'),
(3, 'Can delete user dashboard module', 1, 'delete_userdashboardmodule'),
(4, 'Can view user dashboard module', 1, 'view_userdashboardmodule'),
(5, 'Can add bookmark', 2, 'add_bookmark'),
(6, 'Can change bookmark', 2, 'change_bookmark'),
(7, 'Can delete bookmark', 2, 'delete_bookmark'),
(8, 'Can view bookmark', 2, 'view_bookmark'),
(9, 'Can add pinned application', 3, 'add_pinnedapplication'),
(10, 'Can change pinned application', 3, 'change_pinnedapplication'),
(11, 'Can delete pinned application', 3, 'delete_pinnedapplication'),
(12, 'Can view pinned application', 3, 'view_pinnedapplication'),
(13, 'Can add log entry', 4, 'add_logentry'),
(14, 'Can change log entry', 4, 'change_logentry'),
(15, 'Can delete log entry', 4, 'delete_logentry'),
(16, 'Can view log entry', 4, 'view_logentry'),
(17, 'Can add permission', 5, 'add_permission'),
(18, 'Can change permission', 5, 'change_permission'),
(19, 'Can delete permission', 5, 'delete_permission'),
(20, 'Can view permission', 5, 'view_permission'),
(21, 'Can add group', 6, 'add_group'),
(22, 'Can change group', 6, 'change_group'),
(23, 'Can delete group', 6, 'delete_group'),
(24, 'Can view group', 6, 'view_group'),
(25, 'Can add user', 7, 'add_user'),
(26, 'Can change user', 7, 'change_user'),
(27, 'Can delete user', 7, 'delete_user'),
(28, 'Can view user', 7, 'view_user'),
(29, 'Can add content type', 8, 'add_contenttype'),
(30, 'Can change content type', 8, 'change_contenttype'),
(31, 'Can delete content type', 8, 'delete_contenttype'),
(32, 'Can view content type', 8, 'view_contenttype'),
(33, 'Can add session', 9, 'add_session'),
(34, 'Can change session', 9, 'change_session'),
(35, 'Can delete session', 9, 'delete_session'),
(36, 'Can view session', 9, 'view_session'),
(37, 'Can add class', 10, 'add_class'),
(38, 'Can change class', 10, 'change_class'),
(39, 'Can delete class', 10, 'delete_class'),
(40, 'Can view class', 10, 'view_class'),
(41, 'Can add crud', 11, 'add_crud'),
(42, 'Can change crud', 11, 'change_crud'),
(43, 'Can delete crud', 11, 'delete_crud'),
(44, 'Can view crud', 11, 'view_crud'),
(45, 'Can add project', 12, 'add_project'),
(46, 'Can change project', 12, 'change_project'),
(47, 'Can delete project', 12, 'delete_project'),
(48, 'Can view project', 12, 'view_project'),
(49, 'Can add customer', 13, 'add_customer'),
(50, 'Can change customer', 13, 'change_customer'),
(51, 'Can delete customer', 13, 'delete_customer'),
(52, 'Can view customer', 13, 'view_customer'),
(53, 'Can add association', 14, 'add_association'),
(54, 'Can change association', 14, 'change_association'),
(55, 'Can delete association', 14, 'delete_association'),
(56, 'Can view association', 14, 'view_association'),
(57, 'Can add code', 15, 'add_code'),
(58, 'Can change code', 15, 'change_code'),
(59, 'Can delete code', 15, 'delete_code'),
(60, 'Can view code', 15, 'view_code'),
(61, 'Can add nonce', 16, 'add_nonce'),
(62, 'Can change nonce', 16, 'change_nonce'),
(63, 'Can delete nonce', 16, 'delete_nonce'),
(64, 'Can view nonce', 16, 'view_nonce'),
(65, 'Can add user social auth', 17, 'add_usersocialauth'),
(66, 'Can change user social auth', 17, 'change_usersocialauth'),
(67, 'Can delete user social auth', 17, 'delete_usersocialauth'),
(68, 'Can view user social auth', 17, 'view_usersocialauth'),
(69, 'Can add partial', 18, 'add_partial'),
(70, 'Can change partial', 18, 'change_partial'),
(71, 'Can delete partial', 18, 'delete_partial'),
(72, 'Can view partial', 18, 'view_partial');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_userdashboardmodule`
--

CREATE TABLE `dashboard_userdashboardmodule` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` int(10) UNSIGNED NOT NULL,
  `column` int(10) UNSIGNED NOT NULL,
  `order` int(11) NOT NULL,
  `settings` longtext COLLATE utf8_unicode_ci NOT NULL,
  `children` longtext COLLATE utf8_unicode_ci NOT NULL,
  `collapsed` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_repr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'dashboard', 'userdashboardmodule'),
(2, 'jet', 'bookmark'),
(3, 'jet', 'pinnedapplication'),
(4, 'admin', 'logentry'),
(5, 'auth', 'permission'),
(6, 'auth', 'group'),
(7, 'auth', 'user'),
(8, 'contenttypes', 'contenttype'),
(9, 'sessions', 'session'),
(10, 'a1crud', 'class'),
(11, 'a1crud', 'crud'),
(12, 'a1crud', 'project'),
(13, 'sample', 'customer'),
(14, 'social_django', 'association'),
(15, 'social_django', 'code'),
(16, 'social_django', 'nonce'),
(17, 'social_django', 'usersocialauth'),
(18, 'social_django', 'partial');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'a1crud', '0001_initial', '2020-09-06 06:20:49.946997'),
(2, 'contenttypes', '0001_initial', '2020-09-06 06:20:53.464237'),
(3, 'auth', '0001_initial', '2020-09-06 06:21:13.094103'),
(4, 'admin', '0001_initial', '2020-09-06 06:21:22.875664'),
(5, 'admin', '0002_logentry_remove_auto_add', '2020-09-06 06:21:24.216233'),
(6, 'admin', '0003_logentry_add_action_flag_choices', '2020-09-06 06:21:24.684445'),
(7, 'contenttypes', '0002_remove_content_type_name', '2020-09-06 06:21:26.131417'),
(8, 'auth', '0002_alter_permission_name_max_length', '2020-09-06 06:21:26.755440'),
(9, 'auth', '0003_alter_user_email_max_length', '2020-09-06 06:21:27.352229'),
(10, 'auth', '0004_alter_user_username_opts', '2020-09-06 06:21:27.816019'),
(11, 'auth', '0005_alter_user_last_login_null', '2020-09-06 06:21:28.391246'),
(12, 'auth', '0006_require_contenttypes_0002', '2020-09-06 06:21:28.798587'),
(13, 'auth', '0007_alter_validators_add_error_messages', '2020-09-06 06:21:29.229104'),
(14, 'auth', '0008_alter_user_username_max_length', '2020-09-06 06:21:29.895898'),
(15, 'auth', '0009_alter_user_last_name_max_length', '2020-09-06 06:21:30.494747'),
(16, 'auth', '0010_alter_group_name_max_length', '2020-09-06 06:21:31.187690'),
(17, 'auth', '0011_update_proxy_permissions', '2020-09-06 06:21:32.202651'),
(18, 'dashboard', '0001_initial', '2020-09-06 06:21:39.611296'),
(19, 'jet', '0001_initial', '2020-09-06 06:21:53.589703'),
(20, 'jet', '0002_delete_userdashboardmodule', '2020-09-06 06:21:54.255501'),
(21, 'sample', '0001_initial', '2020-09-06 06:22:05.960440'),
(22, 'sessions', '0001_initial', '2020-09-06 06:22:08.635627'),
(23, 'default', '0001_initial', '2020-09-06 06:22:24.592610'),
(24, 'social_auth', '0001_initial', '2020-09-06 06:22:25.024609'),
(25, 'default', '0002_add_related_name', '2020-09-06 06:22:26.710612'),
(26, 'social_auth', '0002_add_related_name', '2020-09-06 06:22:27.118608'),
(27, 'default', '0003_alter_email_max_length', '2020-09-06 06:22:27.743611'),
(28, 'social_auth', '0003_alter_email_max_length', '2020-09-06 06:22:28.163611'),
(29, 'default', '0004_auto_20160423_0400', '2020-09-06 06:22:28.620611'),
(30, 'social_auth', '0004_auto_20160423_0400', '2020-09-06 06:22:29.064612'),
(31, 'social_auth', '0005_auto_20160727_2333', '2020-09-06 06:22:29.669612'),
(32, 'social_django', '0006_partial', '2020-09-06 06:22:33.647937'),
(33, 'social_django', '0007_code_timestamp', '2020-09-06 06:22:35.326936'),
(34, 'social_django', '0008_partial_timestamp', '2020-09-06 06:22:37.022933'),
(35, 'social_django', '0001_initial', '2020-09-06 06:22:38.058914'),
(36, 'social_django', '0002_add_related_name', '2020-09-06 06:22:38.460663'),
(37, 'social_django', '0004_auto_20160423_0400', '2020-09-06 06:22:38.868232'),
(38, 'social_django', '0003_alter_email_max_length', '2020-09-06 06:22:39.295844'),
(39, 'social_django', '0005_auto_20160727_2333', '2020-09-06 06:22:39.715000');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jet_bookmark`
--

CREATE TABLE `jet_bookmark` (
  `id` int(11) NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` int(10) UNSIGNED NOT NULL,
  `date_add` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jet_pinnedapplication`
--

CREATE TABLE `jet_pinnedapplication` (
  `id` int(11) NOT NULL,
  `app_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` int(10) UNSIGNED NOT NULL,
  `date_add` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sample_customer`
--

CREATE TABLE `sample_customer` (
  `id` int(11) NOT NULL,
  `cmnd` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `number_ticket` int(11) NOT NULL,
  `number_ticket_go` int(11) NOT NULL,
  `number_ticket_return` int(11) NOT NULL,
  `deposit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `money` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` date NOT NULL,
  `start_date` date DEFAULT NULL,
  `start_time_train` time(6) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time_train` time(6) DEFAULT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_association`
--

CREATE TABLE `social_auth_association` (
  `id` int(11) NOT NULL,
  `server_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issued` int(11) NOT NULL,
  `lifetime` int(11) NOT NULL,
  `assoc_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_code`
--

CREATE TABLE `social_auth_code` (
  `id` int(11) NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `timestamp` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_nonce`
--

CREATE TABLE `social_auth_nonce` (
  `id` int(11) NOT NULL,
  `server_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  `salt` varchar(65) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_partial`
--

CREATE TABLE `social_auth_partial` (
  `id` int(11) NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `next_step` smallint(5) UNSIGNED NOT NULL,
  `backend` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_usersocialauth`
--

CREATE TABLE `social_auth_usersocialauth` (
  `id` int(11) NOT NULL,
  `provider` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extra_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `a1crud_class`
--
ALTER TABLE `a1crud_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `a1crud_crud`
--
ALTER TABLE `a1crud_crud`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `a1crud_project`
--
ALTER TABLE `a1crud_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissions_group_id_b120cbf9` (`group_id`),
  ADD KEY `auth_group_permissions_permission_id_84c5c92e` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  ADD KEY `auth_permission_content_type_id_2f476e4b` (`content_type_id`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_user_id_6a12ed8b` (`user_id`),
  ADD KEY `auth_user_groups_group_id_97559544` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permissions_user_id_a95ead1b` (`user_id`),
  ADD KEY `auth_user_user_permissions_permission_id_1fbb5f2c` (`permission_id`);

--
-- Indexes for table `dashboard_userdashboardmodule`
--
ALTER TABLE `dashboard_userdashboardmodule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `jet_bookmark`
--
ALTER TABLE `jet_bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jet_pinnedapplication`
--
ALTER TABLE `jet_pinnedapplication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sample_customer`
--
ALTER TABLE `sample_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_auth_association`
--
ALTER TABLE `social_auth_association`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_auth_association_server_url_handle_078befa2_uniq` (`server_url`,`handle`) USING HASH;

--
-- Indexes for table `social_auth_code`
--
ALTER TABLE `social_auth_code`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_auth_code_email_code_801b2d02_uniq` (`email`,`code`),
  ADD KEY `social_auth_code_code_a2393167` (`code`),
  ADD KEY `social_auth_code_timestamp_176b341f` (`timestamp`);

--
-- Indexes for table `social_auth_nonce`
--
ALTER TABLE `social_auth_nonce`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_auth_nonce_server_url_timestamp_salt_f6284463_uniq` (`server_url`,`timestamp`,`salt`);

--
-- Indexes for table `social_auth_partial`
--
ALTER TABLE `social_auth_partial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_auth_partial_token_3017fea3` (`token`),
  ADD KEY `social_auth_partial_timestamp_50f2119f` (`timestamp`);

--
-- Indexes for table `social_auth_usersocialauth`
--
ALTER TABLE `social_auth_usersocialauth`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_auth_usersocialauth_provider_uid_e6b5e668_uniq` (`provider`,`uid`),
  ADD KEY `social_auth_usersocialauth_user_id_17d28448` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `a1crud_class`
--
ALTER TABLE `a1crud_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=372;

--
-- AUTO_INCREMENT for table `a1crud_crud`
--
ALTER TABLE `a1crud_crud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4026;

--
-- AUTO_INCREMENT for table `a1crud_project`
--
ALTER TABLE `a1crud_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dashboard_userdashboardmodule`
--
ALTER TABLE `dashboard_userdashboardmodule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `jet_bookmark`
--
ALTER TABLE `jet_bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jet_pinnedapplication`
--
ALTER TABLE `jet_pinnedapplication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sample_customer`
--
ALTER TABLE `sample_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_association`
--
ALTER TABLE `social_auth_association`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_code`
--
ALTER TABLE `social_auth_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_nonce`
--
ALTER TABLE `social_auth_nonce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_partial`
--
ALTER TABLE `social_auth_partial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_usersocialauth`
--
ALTER TABLE `social_auth_usersocialauth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
