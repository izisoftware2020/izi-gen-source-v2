<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************pXuanThanh************************             
        // pXuanThanh(Name,Born,Detail,Address)
                                                                                 
        // Get all data from pXuanThanh                                      
        case 400: {                                                              
                $XuanThanh = new XuanthanhDA();
                $sql = $XuanThanh->XuanthanhDataAccess("400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to pXuanThanh                                         
        case 401: {                                                              
                $XuanThanh = new XuanthanhDA();
                $sql = $XuanThanh->XuanthanhDataAccess("401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data pXuanThanh                                            
        case 402: {                                                              
                $XuanThanh = new XuanthanhDA();
                $sql = $XuanThanh->XuanthanhDataAccess("402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of pXuanThanh                                         
        case 403: {                                                              
                $XuanThanh = new XuanthanhDA();
                $sql = $XuanThanh->XuanthanhDataAccess("403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id pXuanThanh                                      
        case 404: {                                                              
                $XuanThanh = new XuanthanhDA();
                $sql = $XuanThanh->XuanthanhDataAccess("404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) pXuanThanh    
        case 405: {                                                              
                $XuanThanh = new XuanthanhDA();
                $sql = $XuanThanh->XuanthanhDataAccess("405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of pXuanThanh                                   
        case 406: {                                                              
                $XuanThanh = new XuanthanhDA();
                $sql = $XuanThanh->XuanthanhDataAccess("406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
