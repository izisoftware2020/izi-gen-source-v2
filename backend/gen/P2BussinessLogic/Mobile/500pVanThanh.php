<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************pVanThanh************************             
        // pVanThanh(IdXuanThanh,Fullname)
                                                                                 
        // Get all data from pVanThanh                                      
        case 500: {                                                              
                $VanThanh = new VanthanhDA();
                $sql = $VanThanh->VanthanhDataAccess("500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to pVanThanh                                         
        case 501: {                                                              
                $VanThanh = new VanthanhDA();
                $sql = $VanThanh->VanthanhDataAccess("501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data pVanThanh                                            
        case 502: {                                                              
                $VanThanh = new VanthanhDA();
                $sql = $VanThanh->VanthanhDataAccess("502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of pVanThanh                                         
        case 503: {                                                              
                $VanThanh = new VanthanhDA();
                $sql = $VanThanh->VanthanhDataAccess("503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id pVanThanh                                      
        case 504: {                                                              
                $VanThanh = new VanthanhDA();
                $sql = $VanThanh->VanthanhDataAccess("504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) pVanThanh    
        case 505: {                                                              
                $VanThanh = new VanthanhDA();
                $sql = $VanThanh->VanthanhDataAccess("505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of pVanThanh                                   
        case 506: {                                                              
                $VanThanh = new VanthanhDA();
                $sql = $VanThanh->VanthanhDataAccess("506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
