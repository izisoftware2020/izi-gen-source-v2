<?php                                                                                      
	class RoledetailDA{				
		public function RoledetailDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************pRoleDetail************************             
                // pRoleDetail(id,IdRole,IdMenu,Status)
                // Get all data from pRoleDetail
                case 300: {                                                                        
                    return "SELECT * FROM pRoleDetail";
                }                                                                                  
                                                                                                   
                // Insert data to pRoleDetail
                case 301: {                                                                        
                    return "INSERT INTO pRoleDetail(IdRole,IdMenu,Status)
                            VALUES('$param->IdRole','$param->IdMenu','$param->Status')";                               
                }                                                                                  
                                                                                                   
                // Update data pRoleDetail
                case 302: {
                    return "UPDATE pRoleDetail SET IdRole='$param->IdRole',IdMenu='$param->IdMenu',Status='$param->Status'
                            WHERE id='$param->id'";                                         
                }                                                                                  
                                                                                                   
                // Delete data of pRoleDetail
                case 303: {                                                                        
                    return "DELETE FROM pRoleDetail
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id pRoleDetail
                case 304: {                                                                        
                    return "SELECT * FROM pRoleDetail
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) pRoleDetail
                case 305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM pRoleDetail ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN pRoleDetail T2 ON T1.id = T2.id                                               
                                $param->condition";                                                               
                }                                                                                                 
                                                                                                   
                // Count number item of pRoleDetail
                case 306: {                                                                        
                    return "SELECT COUNT(1) FROM pRoleDetail $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
