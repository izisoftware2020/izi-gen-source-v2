<?php                                                                                      
	class VanthanhDA{				
		public function VanthanhDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************pVanThanh************************             
                // pVanThanh(id,IdXuanThanh,Fullname)
                // Get all data from pVanThanh
                case 500: {                                                                        
                    return "SELECT * FROM pVanThanh";
                }                                                                                  
                                                                                                   
                // Insert data to pVanThanh
                case 501: {                                                                        
                    return "INSERT INTO pVanThanh(IdXuanThanh,Fullname)
                            VALUES('$param->IdXuanThanh','$param->Fullname')";                               
                }                                                                                  
                                                                                                   
                // Update data pVanThanh
                case 502: {
                    return "UPDATE pVanThanh SET IdXuanThanh='$param->IdXuanThanh',Fullname='$param->Fullname'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of pVanThanh
                case 503: {                                                                        
                    return "DELETE FROM pVanThanh
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id pVanThanh
                case 504: {                                                                        
                    return "SELECT * FROM pVanThanh
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) pVanThanh
                case 505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM pVanThanh $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN pVanThanh T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of pVanThanh
                case 506: {                                                                        
                    return "SELECT COUNT(1) FROM pVanThanh $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
