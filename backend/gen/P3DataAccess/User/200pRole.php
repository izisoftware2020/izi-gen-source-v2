<?php                                                                                      
	class RoleDA{				
		public function RoleDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************pRole************************             
                // pRole(id,Name)
                // Get all data from pRole
                case 200: {                                                                        
                    return "SELECT * FROM pRole";
                }                                                                                  
                                                                                                   
                // Insert data to pRole
                case 201: {                                                                        
                    return "INSERT INTO pRole(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data pRole
                case 202: {
                    return "UPDATE pRole SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of pRole
                case 203: {                                                                        
                    return "DELETE FROM pRole
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id pRole
                case 204: {                                                                        
                    return "SELECT * FROM pRole
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) pRole
                case 205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM pRole ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN pRole T2 ON T1.id = T2.id                                               
                                $param->condition";                                                               
                }                                                                                                 
                                                                                                   
                // Count number item of pRole
                case 206: {                                                                        
                    return "SELECT COUNT(1) FROM pRole $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
