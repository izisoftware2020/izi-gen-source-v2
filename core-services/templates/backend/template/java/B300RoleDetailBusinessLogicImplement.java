package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.domain.entity.E{{phuong0}};
import com.izisoftware.gencode.util.ConnectDataBase;

@Service
public class B{{phuong0}}BusinessLogicImplement {

	public List<E{{phuong0}}> function{{phuong1}}0(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			List<E{{phuong0}}> e{{phuong0}}s = connectDB.excuteSingeQuery(sql);
			
			connectDB.closeConnect();
			
			return e{{phuong0}}s;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public int function{{phuong1}}1(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			int result = connectDB.excuteInsertOrUpdate(sql);
			
			connectDB.closeConnect();
			
			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public int function{{phuong1}}2(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			int result = connectDB.excuteInsertOrUpdate(sql);
			
			connectDB.closeConnect();
			
			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public int function{{phuong1}}3(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			int result = connectDB.excuteInsertOrUpdate(sql);
			
			connectDB.closeConnect();
			
			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public List<E{{phuong0}}> function{{phuong1}}4(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			List<E{{phuong0}}> e{{phuong0}}s = connectDB.excuteSingeQuery(sql);
			
			connectDB.closeConnect();
			
			return e{{phuong0}}s;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public List<JsonNode> function{{phuong1}}5(String sql) throws Exception{
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			List<JsonNode> e{{phuong0}}s = connectDB.excuteMixedQuery(sql);
			
			connectDB.closeConnect();
			
			return e{{phuong0}}s;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public List<JsonNode> function{{phuong1}}6(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			List<JsonNode> jsonObjects = connectDB.excuteMixedQuery(sql);
			
			connectDB.closeConnect();
			
			return jsonObjects;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
}
