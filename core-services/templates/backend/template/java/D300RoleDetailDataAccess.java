package com.izisoftware.gencode.dao;

import org.springframework.stereotype.Repository;

import com.izisoftware.gencode.domain.request.R{{phuong1}}0Model;

@Repository
public class D{{phuong0}}DataAccess {
	
	// *********************((table p{{phuong0}} ))***************************
			public String getQuery(int what, R{{phuong1}}0Model r{{phuong1}}0Model) {
				
				// check condition
				String condition = r{{phuong1}}0Model.getCondition() == null ? "" : r{{phuong1}}0Model.getCondition();
				
				switch (what) {
				// Get all data from p{{phuong0}}
				case {{phuong1}}0:
					return "SELECT {{phuong2}} FROM p{{phuong0}}"
					+ " WHERE (CASE WHEN '1' != '"+r{{phuong1}}0Model.getIdCompanyAccount()+"' THEN IdCompany = '"+r{{phuong1}}0Model.getIdCompanyAccount()+"'"
						 + " ELSE IdCompany LIKE '%%' END) ";

				// Insert data to p{{phuong0}}
				case {{phuong1}}1:
					
					 return " INSERT INTO p{{phuong0}}({{phuong3}}) "
	                      + " VALUES({{phuong4}})";   

				// Update data p{{phuong0}}
				case {{phuong1}}2:
					return " UPDATE p{{phuong0}} SET {{phuong4}}"
	                        +" WHERE id='"+r{{phuong1}}0Model.getId()+"'";

				// Delete data of p{{phuong0}}
				case {{phuong1}}3:
					return " DELETE FROM p{{phuong0}} "
	                     + " WHERE id IN("+r{{phuong1}}0Model.getListid()+")"; 

				// Find data with id p{{phuong0}}
				case {{phuong1}}4:
					  return "SELECT * FROM p{{phuong0}} "
	                        + " WHERE id='"+r{{phuong1}}0Model.getId()+"'";     

				// Select with pagination(offset, number-item-in-page) p{{phuong0}}
				case {{phuong1}}5:
					return " SELECT *  "                                                                            
                          + " FROM (SELECT id FROM p{{phuong0}} ORDER BY id LIMIT "+r{{phuong1}}0Model.getOffset()+", "+r{{phuong1}}0Model.getLimit()+") T1  "   
                          + " INNER JOIN p{{phuong0}} T2 ON T1.id = T2.id "                                              
                          + " WHERE (CASE WHEN '1' != '"+r{{phuong1}}0Model.getIdCompanyAccount()+"' THEN IdCompany = '"+r{{phuong1}}0Model.getIdCompanyAccount()+"'"
						  + " ELSE IdCompany LIKE '%%' END) ";  
					  
				// Count number item of p{{phuong0}}
				case {{phuong1}}6:

					return "SELECT COUNT(1) FROM p{{phuong0}} "+condition+"";

				}

				return null;
			}
}
