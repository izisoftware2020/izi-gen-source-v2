package com.izisoftware.gencode.businesslogic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.izisoftware.gencode.domain.PageResponse;
import com.izisoftware.gencode.service.JsonObjectService;

@Service
public class BaseBusinessLogic {

	@Autowired
	JsonObjectService handlingJsonObject;

	@Autowired
	B000AccountBusinessLogic b000AccountBusinessLogic;

	@Autowired
	B100MenuBusinessLogic b100MenuBusinessLogic;

	@Autowired
	B200RoleBusinessLogic b200RoleBusinessLogic;

	@Autowired
	B300RoleDetailBusinessLogic b300RoleDetailBusinessLogic;

{{phuong0}}			  
	/**
	 * Execute Business
	 * 
	 * @param json
	 * @return
	 * @throws Exception
	 */
	public PageResponse executeBusiness(String param) throws Exception {
		int what = Integer.valueOf(handlingJsonObject.getWhatByParam(param));

		// call account business logic
		if (what >= 0 && what < 100) {
			return b000AccountBusinessLogic.execute(what, param);
		}

		// call menu business logic
		if (what >= 100 && what < 200) {
			return b100MenuBusinessLogic.execute(what, param);
		}

		// call role business logic
		if (what >= 200 && what < 300) {
			return b200RoleBusinessLogic.execute(what, param);
		}

		// call roleDetail business logic
		if (what >= 300 && what < 400) {
			return b300RoleDetailBusinessLogic.execute(what, param);
		}

{{phuong1}}		
		 
		return null;
	}

}
