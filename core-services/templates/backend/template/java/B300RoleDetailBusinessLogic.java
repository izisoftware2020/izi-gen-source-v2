package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.dao.D{{phuong0}}DataAccess;
import com.izisoftware.gencode.domain.PageResponse;
import com.izisoftware.gencode.domain.entity.E{{phuong0}};
import com.izisoftware.gencode.domain.request.R{{phuong1}}0Model;
import com.izisoftware.gencode.service.JsonObjectService;

@Service
public class B{{phuong0}}BusinessLogic {
	@Autowired
	JsonObjectService jsonObjectService;

	@Autowired
	D{{phuong0}}DataAccess d{{phuong0}}DataAccess;

	@Autowired
	B{{phuong0}}BusinessLogicImplement b{{phuong0}}BusinessLogic;
	
	public PageResponse execute(int what, String param) throws Exception {

		R{{phuong1}}0Model modelRequest = (R{{phuong1}}0Model) jsonObjectService.getRequestObjectFromParam(param);

		PageResponse pageResult = new PageResponse();

		// temporary value initialization
		int updateFiels = 0;
		String sql = "";
		List<E{{phuong0}}> e{{phuong0}}s;
		List<JsonNode> jsonObjects;
		switch (what) {

		// Get all data from p{{phuong0}}
		case {{phuong1}}0:
			try {
				sql = d{{phuong0}}DataAccess.getQuery(what, modelRequest);

				e{{phuong0}}s = b{{phuong0}}BusinessLogic.function{{phuong1}}0(sql);

				pageResult.setData(e{{phuong0}}s);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Insert data to p{{phuong0}}
		case {{phuong1}}1:
			try {
				sql = d{{phuong0}}DataAccess.getQuery(what, modelRequest);

				updateFiels = b{{phuong0}}BusinessLogic.function{{phuong1}}1(sql);
				
				pageResult.setInsertId(modelRequest.getId());
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Update data p{{phuong0}}
		case {{phuong1}}2:
			try {
				System.out.println(modelRequest.toString());
				sql = d{{phuong0}}DataAccess.getQuery(what, modelRequest);
				
				System.out.println(modelRequest.getId());
				
				updateFiels = b{{phuong0}}BusinessLogic.function{{phuong1}}2(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Delete data of p{{phuong0}}
		case {{phuong1}}3:
			try {
				sql = d{{phuong0}}DataAccess.getQuery(what, modelRequest);

				updateFiels = b{{phuong0}}BusinessLogic.function{{phuong1}}3(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Find data with id p{{phuong0}}
		case {{phuong1}}4:
			try {
				sql = d{{phuong0}}DataAccess.getQuery(what, modelRequest);

				e{{phuong0}}s = b{{phuong0}}BusinessLogic.function{{phuong1}}4(sql);

				pageResult.setData(e{{phuong0}}s);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Select with pagination(offset, number-item-in-page) p{{phuong0}}
		case {{phuong1}}5:
			try {
				sql = d{{phuong0}}DataAccess.getQuery(what, modelRequest);
			
				jsonObjects = b{{phuong0}}BusinessLogic.function{{phuong1}}5(sql);

				pageResult.setData(jsonObjects);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Count number item of p{{phuong0}}
		case {{phuong1}}6:
			try {
				sql = d{{phuong0}}DataAccess.getQuery(what, modelRequest);

				jsonObjects = b{{phuong0}}BusinessLogic.function{{phuong1}}6(sql);

				pageResult.setData(jsonObjects);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		}

		pageResult.setSql(sql);
		pageResult.setNumRowUpdate(updateFiels);
		pageResult.setStatus(true);

		return pageResult;
	}
	
}
