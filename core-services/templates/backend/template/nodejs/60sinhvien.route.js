const express = require('express');
const { {{phuong0}}Controller } = require('../../controllers');
const validate = require('../../middlewares/validate');
const { {{phuong0}}Validation } = require('../../validations');
const auth = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/')
  .get(auth(), {{phuong0}}Controller.find)
  .post(auth(), validate({{phuong0}}Validation.create), {{phuong0}}Controller.create)
  .put(auth(), validate({{phuong0}}Validation.findByIdAndUpdate), {{phuong0}}Controller.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate({{phuong0}}Validation.paginate), {{phuong0}}Controller.paginate);

router
  .route('/:id')
  .get(auth(), validate({{phuong0}}Validation.findById), {{phuong0}}Controller.findById)
  .delete(auth(), validate({{phuong0}}Validation.findByIdAndDelete), {{phuong0}}Controller.findByIdAndDelete);

module.exports = router;
