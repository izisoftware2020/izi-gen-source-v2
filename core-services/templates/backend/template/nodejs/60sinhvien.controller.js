const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { {{phuong0}}Service } = require('../services');

/**
 * find
 */
const find = catchAsync(async (req, res) => {
  const result = await {{phuong0}}Service.find();
  res.send(result);
});

/**
 * create
 */
const create = catchAsync(async (req, res) => {
  const {{phuong0}} = await {{phuong0}}Service.create(req.body);
  res.status(httpStatus.CREATED).send({{phuong0}});
});

/**
 * find By Id And Update
 */
const findByIdAndUpdate = catchAsync(async (req, res) => {
  const {{phuong0}} = await {{phuong0}}Service.findByIdAndUpdate(req.body.id, req.body);
  res.status(httpStatus.CREATED).send({{phuong0}});
});

/**
 * find By Id And Delete
 */
const findByIdAndDelete = catchAsync(async (req, res) => {
  const {{phuong0}} = await {{phuong0}}Service.findByIdAndDelete(req.params.id);
  res.status(httpStatus.CREATED).send({{phuong0}});
});

/**
 * findById
 */
const findById = catchAsync(async (req, res) => {
  const {{phuong0}} = await {{phuong0}}Service.findById(req.params.id);
  res.status(httpStatus.CREATED).send({{phuong0}});
});

/**
 * paginate
 */
const paginate = catchAsync(async (req, res) => {
  const filter = pick(req.query, [{{phuong1}}]);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  options.populate = '{{phuong2}}';

  const result = await {{phuong0}}Service.paginate(filter, options);
  res.send(result);
});

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
