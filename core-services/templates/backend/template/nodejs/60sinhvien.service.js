const { {{phuong0}} } = require('../models');

/**
 * Find all {{phuong0}}
 * @returns
 */
const find = async () => {
  return {{phuong0}}.find(){{phuong1}}
};

/**
 * Create a {{phuong0}}
 * @param {*} body
 * @returns
 */
const create = async (body) => {
  return {{phuong0}}.create(body);
};

/**
 * Update a {{phuong0}}
 * @param {*} id
 * @param {*} body
 * @returns
 */
const findByIdAndUpdate = async (id, body) => {
  return {{phuong0}}.findByIdAndUpdate({ _id: id }, body);
};

/**
 * Delete a {{phuong0}}
 * @param {*} ids
 * @returns
 */
const findByIdAndDelete = async (ids) => {
  return {{phuong0}}.deleteMany({ _id: { $in: ids.split(',') } });
};

/**
 * Find a {{phuong0}} by id
 * @param {*} id
 * @returns
 */
const findById = async (id) => {
  return {{phuong0}}.findById({ _id: id }){{phuong1}}
};

/**
 * Paginate
 * @param {*} filter
 * @param {*} options
 * @returns
 */
const paginate = async (filter, options) => {
  return {{phuong0}}.paginate(filter, options);
};

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
