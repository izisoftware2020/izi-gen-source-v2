module.exports.authService = require('./00auth.service');
module.exports.emailService = require('./00email.service');
module.exports.tokenService = require('./00token.service');
module.exports.uploadService = require('./00upload.service');
module.exports.userService = require('./10user.service');
module.exports.menuService = require('./20menu.service');
module.exports.roleService = require('./30role.service');
module.exports.roleDetailService = require('./40role-detail.service');
{{phuong0}}