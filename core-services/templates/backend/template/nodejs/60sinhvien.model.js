const mongoose = require('mongoose');

const { toJSON, paginate } = require('./plugins');

const {{phuong0}}Schema = mongoose.Schema(
  {
{{phuong2}}
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
{{phuong0}}Schema.plugin(toJSON);
{{phuong0}}Schema.plugin(paginate);

/**
 * @typedef {{phuong1}}
 */
const {{phuong1}} = mongoose.model('{{phuong1}}', {{phuong0}}Schema);

module.exports = {{phuong1}};
