package com.izisoftware.gencode.configuration;

import java.sql.*;
import org.springframework.stereotype.Component;


@Component
public abstract class JdbcConfiguration {
	// JDBC driver name and database URL
	public String jdbcDriver;
	public String dbURL;

	// Database credentials
	public String user;
	public String pass;

	public JdbcConfiguration() {
	}

	/**
	 * 
	 * @param jdbcDriver
	 * @param dbURL the url connection of database 
	 * @param user the userName of database
	 * @param pass the password of database
	 */
	public JdbcConfiguration(String jdbcDriver, String dbURL, String user, String pass) {
		this.jdbcDriver = jdbcDriver;
		this.dbURL = dbURL;
		this.user = user;
		this.pass = pass;
	}

	/**
	 * get connection of JDBC
	 * @return
	 * @throws SQLException
	 */
	public abstract Connection getConnect() throws SQLException;
	
	/**
	 * get Statement
	 * @return
	 * @throws SQLException
	 */
	public abstract Statement getStatement() throws SQLException;
	
	/**
	 * 
	 * @param sql this is SQL query
	 * @return
	 * @throws SQLException
	 */
	public abstract ResultSet getResultSet(String sql) throws SQLException;
	
	/**
	 * close connection
	 * 
	 * @param rs this is resource when query
	 * @param statement this is statement when query
	 * @param connection the connection to database
	 * @throws SQLException
	 */
	public abstract void closeConnection(ResultSet rs, Statement statement, Connection connection) throws SQLException;

}
