package com.izisoftware.gencode.dao;

import org.springframework.stereotype.Repository;
import com.izisoftware.gencode.domain.request.R000Model;

@Repository
public class D000AccountDataAccess {

	// *********************(( p000 account ))***************************
	public String getQuery(int what, R000Model modelRequest) {

		switch (what) {

		// Get all data from student
		case 0:
			return "SELECT * FROM p000account ";

		// Insert data from 000
		case 1:
			return "INSERT INTO p000account(IdRole,name,email,img,role) " + "VALUES('" + modelRequest.getIdRole()
					+ "','" + modelRequest.getName() + "','" + modelRequest.getEmail() + "','" + modelRequest.getImg()
					+ "','" + modelRequest.getRole() + "')";

		// Update data p000account
		case 2:
			return " UPDATE p000account SET   IdRole='" + modelRequest.getIdRole() + "', email='"
					+ modelRequest.getEmail() + "',  role='" + modelRequest.getRole() + "'  "
					+ " WHERE id='" + modelRequest.getId() + "' ";

		// Delete data of p000account by id
		case 3:
			return " DELETE FROM p000account " + " WHERE id IN(" + modelRequest.getListid() + ")";

		// check gmail candidate
		case 4:
			return " SELECT * FROM p000account " + " WHERE email='" + modelRequest.getEmail() + "' AND password='"
					+ modelRequest.getPassword() + "'";

		// login candidate
		case 5:
			return "SELECT * FROM p000account " + " WHERE email='" + modelRequest.getEmail() + "' AND password='"
					+ modelRequest.getPassword() + "'";

		// get staff with id
		case 6:
			return "SELECT * FROM p000account WHERE id = '" + modelRequest.getId() + "'";

		// change pass md5
		case 7:
			return "";

		// update info staff
		case 8:
			return "UPDATE p000account SET name='" + modelRequest.getName() + "' WHERE id='" + modelRequest.getId()
					+ "'";

		// update info avata
		case 9:
			return "UPDATE p000account SET img='" + modelRequest.getImg() + "' WHERE id='" + modelRequest.getId() + "'";

		// Select with pagination(offset, number-item-in-page) p000account
		case 10:
			return "SELECT * FROM p000account";

		// count number item of p000acount
		case 11:
			return "SELECT COUNT(1) FROM p000account ";

		// Get account sign up of teacher
		case 12:
			return 	"SELECT * FROM `p000account` as T1 "
					+ "WHERE NOT EXISTS (SELECT T2.id FROM `p1900teacher` as T2 WHERE T1.email = T2.Email) "
					+ "AND NOT EXISTS (SELECT T3.id FROM `p1800student` as T3 WHERE T1.email = T3.Email) "
					+ "OR T1.email = '" + modelRequest.getEmailTeacher() + "' ";
		}

		return null;
	}
}
