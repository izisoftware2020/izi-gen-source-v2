package com.izisoftware.gencode.domain;

import java.io.Serializable;

import com.izisoftware.gencode.domain.entity.E000Account;

public class JwtResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private final String jwttoken;
	
	private E000Account e000Account;
	
	/**
	 * @param jwttoken
	 * @param e000Account
	 */
	public JwtResponse(String jwttoken, E000Account e000Account) {
		this.jwttoken = jwttoken;
		this.e000Account = e000Account;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	/**
	 * @return the jwttoken
	 */
	public String getJwttoken() {
		return jwttoken;
	}


	/**
	 * @return the e000Account
	 */
	public E000Account getE000Account() {
		return e000Account;
	}


	/**
	 * @param e000Account the e000Account to set
	 */
	public void setE000Account(E000Account e000Account) {
		this.e000Account = e000Account;
	}

	
	

	
	

}
