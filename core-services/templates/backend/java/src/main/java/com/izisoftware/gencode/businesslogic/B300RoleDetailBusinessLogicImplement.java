package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.domain.entity.E300RoleDetail;
import com.izisoftware.gencode.util.ConnectDataBase;

@Service
public class B300RoleDetailBusinessLogicImplement {

	public List<E300RoleDetail> function300(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			List<E300RoleDetail> e300RoleDetails = connectDB.excuteSingeQuery(sql);
			
			connectDB.closeConnect();
			
			return e300RoleDetails;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public int function301(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			int result = connectDB.excuteInsertOrUpdate(sql);
			
			connectDB.closeConnect();
			
			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public int function302(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			int result = connectDB.excuteInsertOrUpdate(sql);
			
			connectDB.closeConnect();
			
			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public int function303(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			int result = connectDB.excuteInsertOrUpdate(sql);
			
			connectDB.closeConnect();
			
			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public List<E300RoleDetail> function304(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			List<E300RoleDetail> e300RoleDetails = connectDB.excuteSingeQuery(sql);
			
			connectDB.closeConnect();
			
			return e300RoleDetails;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public List<E300RoleDetail> function305(String sql) throws Exception{
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			List<E300RoleDetail> e300RoleDetails = connectDB.excuteSingeQuery(sql);
			
			connectDB.closeConnect();
			
			return e300RoleDetails;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public List<JsonNode> function306(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();
			
			List<JsonNode> jsonObjects = connectDB.excuteMixedQuery(sql);
			
			connectDB.closeConnect();
			
			return jsonObjects;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
		
	}
	
}
