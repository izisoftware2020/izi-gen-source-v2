package com.izisoftware.gencode.domain.request;

import com.izisoftware.gencode.domain.PageRequest;

@RTable(name = "Request300")
public class R300Model extends PageRequest{
	
	public String id;
	
	public String IdRole;
	
	public String IdMenu;
	
	public String Status;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the idRole
	 */
	public String getIdRole() {
		return IdRole;
	}

	/**
	 * @param idRole the idRole to set
	 */
	public void setIdRole(String idRole) {
		IdRole = idRole;
	}

	/**
	 * @return the idMenu
	 */
	public String getIdMenu() {
		return IdMenu;
	}

	/**
	 * @param idMenu the idMenu to set
	 */
	public void setIdMenu(String idMenu) {
		IdMenu = idMenu;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}

	
	
	
	
	
	
}
