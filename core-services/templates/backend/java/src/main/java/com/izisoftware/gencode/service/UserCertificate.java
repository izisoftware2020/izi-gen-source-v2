package com.izisoftware.gencode.service;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.izisoftware.gencode.domain.entity.E000Account;
import com.izisoftware.gencode.util.ConnectDataBase;

public class UserCertificate implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Autowired
	ConnectDataBase connectDataBase;

	public String userName;
	
	public String passWord;
	
	/**
	 * Create UserCertificate by User and Roles
	 * @param user
	 * @throws Exception
	 */
	public UserCertificate(E000Account user) throws Exception {
		this.userName = user.getEmail();
		this.passWord = user.getPassword();
	}

	/**
	 * Collection Role Of User
	 * @author ThanhVV
	 * @return Roles
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		return null;
	}

	@Override
	public String getPassword() {
		return this.passWord;
	}

	@Override
	public String getUsername() {
		return this.userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}