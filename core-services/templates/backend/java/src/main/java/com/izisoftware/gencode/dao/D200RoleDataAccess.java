package com.izisoftware.gencode.dao;

import org.springframework.stereotype.Repository;
import com.izisoftware.gencode.domain.request.R200Model;


@Repository
public class D200RoleDataAccess {
	
	// *********************((table p200Role ))***************************
		public String getQuery(int what, R200Model r200Model) {
			
			// check condition
			String condition = r200Model.getCondition() == null ? "" : r200Model.getCondition();
			
			switch (what) {
			// Get all data from p200Role
			case 200:
				 return "SELECT * FROM p200Role";

			// Insert data to p200Role
			case 201:
				return "INSERT INTO p200Role(Name) "
                      +"VALUES('"+r200Model.getName()+"')";  

			// Update data p200Role
			case 202:
				  return " UPDATE p200Role SET Name = '"+r200Model.getName()+"' "
                        +" WHERE id='"+r200Model.getId()+"'"; 

			// Delete data of p200Role
			case 203:
				return " DELETE FROM p200Role "
                     + " WHERE id IN("+r200Model.getListid()+")"; 

			// Find data with id p200Role
			case 204:
				  return "SELECT * FROM p200Role "
                        + " WHERE id='"+r200Model.getId()+"'";     

			// Select with pagination(offset, number-item-in-page) p200Role
			case 205:
				  return " SELECT * "                                                                             
                       + " FROM (SELECT id FROM p200Role ORDER BY id LIMIT "+r200Model.getOffset()+", "+r200Model.getLimit()+") T1  "   
                       + " INNER JOIN p200Role T2 ON T1.id = T2.id "                                              
                       + " "+r200Model.getCondition()+"";  
				  
			// Count number item of p200Role
			case 206:

				return "SELECT COUNT(1) FROM p200Role " + condition + "";

			}

			return null;
		}
	
}
