package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.dao.D100MenuDataAccess;
import com.izisoftware.gencode.domain.PageResponse;
import com.izisoftware.gencode.domain.entity.E100Menu;
import com.izisoftware.gencode.domain.request.R100Model;
import com.izisoftware.gencode.service.JsonObjectService;
import com.izisoftware.gencode.service.UserService;

@Service
public class B100MenuBusinessLogic {
	
	@Autowired
	JsonObjectService jsonObjectService;
	
	@Autowired
	B100MenuBusinessLogicImplement b100MenuBusinessLogic;
	
	@Autowired
	D100MenuDataAccess d100MenuDataAccess;
	
	@Autowired
	UserService userService;
	
	public PageResponse execute(int what, String param) throws Exception {
		
		R100Model modelRequest = (R100Model) jsonObjectService.getRequestObjectFromParam(param);
		
 
		PageResponse pageResult = new PageResponse();

		// temporary value initialization
		int updateFiels = 0;
		String sql = "";
		List<E100Menu>	menus;
		List<JsonNode> jsonObjects;
		switch (what) {
		
			// Get all data from p100Menu
			case 100:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					menus = b100MenuBusinessLogic.function100(sql);
					
					pageResult.setData(menus);
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				
				break;
				
			// Insert data to p100Menu
			case 101:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					updateFiels = b100MenuBusinessLogic.function101(sql);
					
					pageResult.setInsertId(modelRequest.getIdMenuModule());
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				
				break;
				
			// Update data p100Menu
			case 102:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					updateFiels = b100MenuBusinessLogic.function102(sql);
					
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				
				break;
	
			// Delete data of p100Menu
			case 103:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					updateFiels = b100MenuBusinessLogic.function103(sql);
					
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				
				break;
				
			// Find data with id p100Menu
			case 104:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					menus = b100MenuBusinessLogic.function104(sql);
					
					pageResult.setData(menus);
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				
				break;
				
			// Select with pagination(offset, number-item-in-page) p100Menu
			case 105:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					menus = b100MenuBusinessLogic.function105(sql);
					
					pageResult.setData(menus);
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				
				break;
				
			// Count number item of p100Menu
			case 106:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					jsonObjects= b100MenuBusinessLogic.function106(sql);
					
					pageResult.setData(jsonObjects);
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				break;
				
			// Get all data from p100Menu WHERE IsGroup = '1'"
			case 107:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					jsonObjects = b100MenuBusinessLogic.function107(sql);
					
					pageResult.setData(jsonObjects);
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				break;
			
			// Get all data from p100Menu recursive
			case 108:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					jsonObjects= b100MenuBusinessLogic.function108(sql);
					
					pageResult.setData(jsonObjects);
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}

				break;			
				
			// Get all data from p100Menu recursive
			case 109:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					jsonObjects= b100MenuBusinessLogic.function109(sql);
					
					pageResult.setData(jsonObjects);
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				
				break;
				
			// Get all data from p100Menu recursive
			case 110:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					
					jsonObjects= b100MenuBusinessLogic.function110(sql);
							
					pageResult.setData(jsonObjects);
				} catch (Exception e) {
					pageResult.setSql(sql);
					pageResult.setError(e.getMessage());
				}
				break;
			
			// Get get permission of component
			case 111:
				try {
					sql = d100MenuDataAccess.getQuery(what, modelRequest);
					 
					jsonObjects= b100MenuBusinessLogic.function111(sql);
					
					pageResult.setData(jsonObjects);
				} catch (Exception e) {
					pageResult.setError(e.getMessage());
				}
				
				break;
		}
		
		pageResult.setSql(sql);
		pageResult.setNumRowUpdate(updateFiels);
		pageResult.setStatus(true);
		
		return pageResult;
	}
}
