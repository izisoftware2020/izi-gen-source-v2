package com.izisoftware.gencode.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;



@Service
public class SendEmailService {
	
	@Autowired
	Configuration configuration;
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	public void sendEmail(String to, String body, String topic) throws MessagingException, TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {
		// email of recipient
		final String emailToRecipient = to;
		
		//email subject
		final String emailSubject = "Successfully Registration with MyProgramingTask";
		
		MimeMessage message = javaMailSender.createMimeMessage();
		
		// set media type
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED);
		
		// model of data template
		Map<String, Object> model = new HashMap<String, Object>();
		// this is a example
		model.put("name", "ThanhVV");
		model.put("address", to);
		
		helper.addAttachment("logo.png", new ClassPathResource("logo.png"));
		
		freemarker.template.Template template = configuration.getTemplate("email-template.ftl");
		
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
		
		helper.setTo(emailToRecipient);
		helper.setText(html, true);
		helper.setSubject(emailSubject);
		
		javaMailSender.send(message);
	}
	
}
