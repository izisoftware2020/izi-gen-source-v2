package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.domain.entity.E100Menu;
import com.izisoftware.gencode.util.ConnectDataBase;

@Service
public class B100MenuBusinessLogicImplement {

	public List<E100Menu> function100(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E100Menu> e100Menus = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e100Menus;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public int function101(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int result = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public int function102(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int result = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public int function103(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int result = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<E100Menu> function104(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E100Menu> e100Menus = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e100Menus;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<E100Menu> function105(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E100Menu> e100Menus = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e100Menus;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<JsonNode> function106(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<JsonNode> jsonObjects = connectDB.excuteMixedQuery(sql);

			connectDB.closeConnect();

			return jsonObjects;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
	}

	public List<JsonNode> function107(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<JsonNode> e100Menus = connectDB.excuteMixedQuery(sql);

			connectDB.closeConnect();

			return e100Menus;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<JsonNode> function108(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<JsonNode> jsonObjects = connectDB.excuteMixedQuery(sql);

			connectDB.closeConnect();

			return jsonObjects;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
	}

	public List<JsonNode> function109(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<JsonNode> jsonObjects = connectDB.excuteMixedQuery(sql);

			connectDB.closeConnect();

			return jsonObjects;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<JsonNode> function110(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<JsonNode> jsonObjects = connectDB.excuteMixedQuery(sql);

			connectDB.closeConnect();

			return jsonObjects;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<JsonNode> function111(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<JsonNode> jsonObjects = connectDB.excuteMixedQuery(sql);

			connectDB.closeConnect();

			return jsonObjects;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

}
