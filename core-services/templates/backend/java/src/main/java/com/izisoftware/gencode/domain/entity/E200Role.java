package com.izisoftware.gencode.domain.entity;

@ETable(name = "p200role")
public class E200Role {
	
	@EColumn(name="id")
	public int id;
	
	@EColumn(name = "Name")
	public String Name;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}
	
	
}
