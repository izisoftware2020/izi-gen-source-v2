package com.izisoftware.gencode.domain.request;

import com.izisoftware.gencode.domain.PageRequest;

@RTable(name = "Request100")
public class R100Model extends PageRequest {

	private String id;

	private String IdRole;

	private String url;

	private String IdParentMenu;

	private String IsGroup;

	private String idUser;

	private String Name;

	private String Slug;

	private String Icon;

	private String idRoleDetail;

	private int Position;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the idRole
	 */
	public String getIdRole() {
		return IdRole;
	}

	/**
	 * @param idRole the idRole to set
	 */
	public void setIdRole(String idRole) {
		IdRole = idRole;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the idParentMenu
	 */
	public String getIdParentMenu() {
		return IdParentMenu;
	}

	/**
	 * @param idParentMenu the idParentMenu to set
	 */
	public void setIdParentMenu(String idParentMenu) {
		IdParentMenu = idParentMenu;
	}

	/**
	 * @return the isGroup
	 */
	public String getIsGroup() {
		return IsGroup;
	}

	/**
	 * @param isGroup the isGroup to set
	 */
	public void setIsGroup(String isGroup) {
		IsGroup = isGroup;
	}

	/**
	 * @return the idUser
	 */
	public String getIdUser() {
		return idUser;
	}

	/**
	 * @param idUser the idUser to set
	 */
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the slug
	 */
	public String getSlug() {
		return Slug;
	}

	/**
	 * @param slug the slug to set
	 */
	public void setSlug(String slug) {
		Slug = slug;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return Icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		Icon = icon;
	}

	/**
	 * @return the idRoleDetail
	 */
	public String getIdRoleDetail() {
		return idRoleDetail;
	}

	/**
	 * @param idRoleDetail the idRoleDetail to set
	 */
	public void setIdRoleDetail(String idRoleDetail) {
		this.idRoleDetail = idRoleDetail;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return Position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		Position = position;
	}

}
