package com.izisoftware.gencode.dao;

import org.springframework.stereotype.Repository;

import com.izisoftware.gencode.domain.request.R300Model;

@Repository
public class D300RoleDetailDataAccess {
	
	// *********************((table p300RoleDetail ))***************************
			public String getQuery(int what, R300Model r300Model) {
				
				// check condition
				String condition = r300Model.getCondition() == null ? "" : r300Model.getCondition();
				
				switch (what) {
				// Get all data from p300RoleDetail
				case 300:
					return "SELECT * FROM p300RoleDetail";

				// Insert data to p300RoleDetail
				case 301:
					
					 return " INSERT INTO p300RoleDetail(IdRole,IdMenu,Status) "
	                      + " VALUES('"+r300Model.getIdRole()+"','"+r300Model.getIdMenu()+"','"+r300Model.getStatus()+"')";   

				// Update data p300RoleDetail
				case 302:
					return " UPDATE p300RoleDetail SET IdRole='"+r300Model.getIdRole()+"',IdMenu='"+r300Model.getIdMenu()+"',Status='"+r300Model.getStatus()+"' "
	                        +" WHERE id='"+r300Model.getId()+"'";

				// Delete data of p300RoleDetail
				case 303:
					return " DELETE FROM p300RoleDetail "
	                     + " WHERE id IN("+r300Model.getListid()+")"; 

				// Find data with id p300RoleDetail
				case 304:
					  return "SELECT * FROM p300RoleDetail "
	                        + " WHERE id='"+r300Model.getId()+"'";     

				// Select with pagination(offset, number-item-in-page) p300RoleDetail
				case 305:
					return " SELECT *  "                                                                            
                          + " FROM (SELECT id FROM p300RoleDetail ORDER BY id LIMIT "+r300Model.getOffset()+", "+r300Model.getLimit()+") T1  "   
                          + " INNER JOIN p300RoleDetail T2 ON T1.id = T2.id "                                              
                          + " "+condition+"";    
					  
				// Count number item of p300RoleDetail
				case 306:

					return "SELECT COUNT(1) FROM p300RoleDetail "+condition+"";

				}

				return null;
			}
}
