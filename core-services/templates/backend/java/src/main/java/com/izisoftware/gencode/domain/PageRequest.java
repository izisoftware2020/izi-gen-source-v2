package com.izisoftware.gencode.domain;

public class PageRequest {

	public String what;
	
	public String idMenuModule;
	
	public int offset;
	
	public int limit;
	
	public String condition;
	
	public String listid;
	
	/**
	 * @return the what
	 */
	public String getWhat() {
		return what;
	}

	/**
	 * @return the slugModule
	 */
	public String getIdMenuModule() {
		return idMenuModule;
	}

	/**
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * @return the condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @param condition the condition to set
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * @param what the what to set
	 */
	public void setWhat(String what) {
		this.what = what;
	}

	/**
	 * @param idMenuModule the idMenuModule to set
	 */
	public void setIdMenuModule(String idMenuModule) {
		this.idMenuModule = idMenuModule;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * @return the listid
	 */
	public String getListid() {
		return listid;
	}

	/**
	 * @param listid the listid to set
	 */
	public void setListid(String listid) {
		this.listid = listid;
	}

	
	
	
}
