package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.domain.entity.E200Role;
import com.izisoftware.gencode.util.ConnectDataBase;

@Service
public class B200RoleBusinessLogicImplement {

	public List<E200Role> function200(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E200Role> e200Roles = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e200Roles;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public int function201(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int result = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public int function202(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int result = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public int function203(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int result = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return result;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<E200Role> function204(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E200Role> e200Roles = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e200Roles;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<E200Role> function205(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E200Role> e200Roles = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e200Roles;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	public List<JsonNode> function206(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<JsonNode> jsonObjects = connectDB.excuteMixedQuery(sql);

			connectDB.closeConnect();

			return jsonObjects;

		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}
	}

}
