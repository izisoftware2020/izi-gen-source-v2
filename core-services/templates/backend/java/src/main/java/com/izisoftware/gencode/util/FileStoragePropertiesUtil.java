package com.izisoftware.gencode.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import com.izisoftware.gencode.GencodeConstant;

@Component
@ConfigurationProperties(prefix = GencodeConstant.FILE_PROPERTIES_PREFIX)
public class FileStoragePropertiesUtil {
	private String uploadDir;

	public String getUploadDir() {
		return uploadDir;
	}

	public void setUploadDir(String uploadDir) {
		this.uploadDir = uploadDir;
	}
}
