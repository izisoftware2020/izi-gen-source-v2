package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.dao.D000AccountDataAccess;
import com.izisoftware.gencode.domain.PageResponse;
import com.izisoftware.gencode.domain.entity.E000Account;
import com.izisoftware.gencode.domain.request.R000Model;
import com.izisoftware.gencode.service.JsonObjectService;

@Service
public class B000AccountBusinessLogic {

	@Autowired
	JsonObjectService jsonObjectService;

	@Autowired
	D000AccountDataAccess accountDao;

	@Autowired
	B000AccountBusinessLogicImplement accountBusinessLogic;

	public PageResponse execute(int what, String param) throws Exception {
		R000Model modelRequest = (R000Model) jsonObjectService.getRequestObjectFromParam(param);

		PageResponse pageResult = new PageResponse();
		

		int numRowUpdate = 0;
		String sql = "";
		List<E000Account> accounts;
		List<JsonNode> jsonNodes;

		switch (what) {

		// Function no need authenticate
		case 0:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				numRowUpdate = accountBusinessLogic.function1(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// Function no need authenticate
		case 1:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				numRowUpdate = accountBusinessLogic.function1(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// Function no need authenticate
		case 2:
			try {
				sql = accountDao.getQuery(what, modelRequest);
		 
				numRowUpdate = accountBusinessLogic.function2(sql);

			} catch (Exception e) {
				System.out.println("Dangloi");
				pageResult.setError(e.getMessage());
			}
			break;

		// delete data of p000account
		case 3:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				numRowUpdate = accountBusinessLogic.function3(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// check email candidate
		case 4:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				accounts = accountBusinessLogic.function4(sql);

				pageResult.setData(accounts);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// login candidate
		case 5:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				accounts = accountBusinessLogic.function5(sql);

				pageResult.setData(accounts);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// get staff with id
		case 6:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				accounts = accountBusinessLogic.function6(sql);

				pageResult.setData(accounts);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// change pass md5
		case 7:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				numRowUpdate = accountBusinessLogic.function7(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// update info staff
		case 8:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				numRowUpdate = accountBusinessLogic.function8(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// update info avatar
		case 9:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				numRowUpdate = accountBusinessLogic.function9(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// Select with pagination(offset, number-item-in-page) p000account
		case 10:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				accounts = accountBusinessLogic.function10(sql);

				pageResult.setData(accounts);
			} catch (Exception e) {

				pageResult.setError(e.getMessage());
			}

			break;

		// Count number item of p000account
		case 11:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				jsonNodes = accountBusinessLogic.function11(sql);

				pageResult.setData(jsonNodes);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}

			break;

		// Get account sign up of teacher
		case 12:
			try {
				sql = accountDao.getQuery(what, modelRequest);

				accounts = accountBusinessLogic.function12(sql);

				pageResult.setData(accounts);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
		}

		pageResult.setSql(sql);
		pageResult.setNumRowUpdate(numRowUpdate);
		pageResult.setStatus(true);

		return pageResult;
	}
}
