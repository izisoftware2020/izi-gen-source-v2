package com.izisoftware.gencode.service;

import org.springframework.stereotype.Service;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.izisoftware.gencode.GencodeConstant;
import com.izisoftware.gencode.util.FileStoragePropertiesUtil;
import com.izisoftware.gencode.util.MockMultipartFile;

@Service
public class FileStorageService {

	private Path fileStorageLocation;
	
	private static final Logger logger = Logger.getLogger(FileStorageService.class);
	
	@Autowired
	public FileStorageService(FileStoragePropertiesUtil fileStorageProperties) throws Exception {
		this.fileStorageLocation = Paths.get(GencodeConstant.LOCATE_UPLOAD_IMAGE).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			logger.debug(ex);
			throw new Exception(GencodeConstant.FILE_STORAGE_EXCEPTION_PATH_NOT_FOUND);
		}
		
	}
		
	public String insertOrUpdateFile(String src) throws FileNotFoundException {
		try {
			File file = new File(GencodeConstant.TEMP_DIR + src);
			
			FileInputStream input = new FileInputStream(file);
			
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain",
					org.apache.commons.io.IOUtils.toByteArray(input));
			
			String fileName = this.storeFile(multipartFile);
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
					.path(GencodeConstant.DOWNLOAD_PATH + GencodeConstant.RESOURCES_IMAGE + "/").path(fileName).toUriString();
			
			return fileDownloadUri;
			
		} catch (Exception e) {
			logger.debug(e);
		}
		return "";
	}
	

	public Resource loadFileAsResource(String fileName) throws Exception {
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			
			Resource resource = new UrlResource(filePath.toUri());
				
			if (resource.exists()) {
				return  resource;
			} else {
				throw new Exception(GencodeConstant.FILE_NOT_FOUND + fileName);
			}
			
		} catch (MalformedURLException ex) {
			
			logger.debug(ex);
			
			throw new Exception(GencodeConstant.FILE_NOT_FOUND + fileName, ex);
		}
	}
	
	private String storeFile(MultipartFile file) throws Exception {
		
		if (!(file.getOriginalFilename().endsWith(GencodeConstant.PNG_FILE_FORMAT)
				|| file.getOriginalFilename().endsWith(GencodeConstant.JPEG_FILE_FORMAT)
				|| file.getOriginalFilename().endsWith(GencodeConstant.JPG_FILE_FORMAT)))
			throw new Exception(GencodeConstant.INVALID_FILE_FORMAT);

		File f = new File(file.getOriginalFilename());
		
		f.createNewFile();
		
		FileOutputStream fout = new FileOutputStream(f);
		fout.write(file.getBytes());
		fout.close();
		
		BufferedImage image = ImageIO.read(f);
		
		int height = image.getHeight();
		int width = image.getWidth();
		
		if (width > 1200 || height > 1200) {
			if (f.exists())
				f.delete();
			throw new Exception(GencodeConstant.INVALID_FILE_DIMENSIONS);
		}
		
		if (f.exists())
			f.delete();

		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		
		try {
			if (fileName.contains(GencodeConstant.INVALID_FILE_DELIMITER)) 
				
				throw new Exception(GencodeConstant.INVALID_FILE_PATH_NAME + fileName);
			
			String newFileName =  GencodeConstant.FILE_SEPERATOR + fileName;
			
			Path targetLocation = this.fileStorageLocation.resolve(newFileName);
			
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			
			return newFileName;
		} catch (IOException ex) {
			logger.debug(ex);
			throw new Exception(String.format(GencodeConstant.FILE_STORAGE_EXCEPTION, fileName), ex);
		}

	}
	
	public String deleteFile(String sourceImage) throws FileNotFoundException {
		try {
			String handleSrc[] = sourceImage.split("/");
			
			String src = handleSrc[handleSrc.length-1];
			
			File file = new File(GencodeConstant.SERVER_DIR + src);
			
			if (file.delete()) {
				return GencodeConstant.SUCCESS_DELETE;
			}
			
		} catch (Exception e) {
			logger.debug(e);
		}
		return GencodeConstant.FAIL_DELETE;
	}
}
