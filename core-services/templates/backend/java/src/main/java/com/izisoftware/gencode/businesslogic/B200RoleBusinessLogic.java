package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.dao.D200RoleDataAccess;
import com.izisoftware.gencode.domain.PageResponse;
import com.izisoftware.gencode.domain.entity.E200Role;
import com.izisoftware.gencode.domain.request.R200Model;
import com.izisoftware.gencode.service.JsonObjectService;

@Service
public class B200RoleBusinessLogic {

	@Autowired
	JsonObjectService jsonObjectService;

	@Autowired
	D200RoleDataAccess d200RoleDataAccess;

	@Autowired
	B200RoleBusinessLogicImplement b200RoleBusinessLogic;

	public PageResponse execute(int what, String param) throws Exception {

		R200Model modelRequest = (R200Model) jsonObjectService.getRequestObjectFromParam(param);

		PageResponse pageResult = new PageResponse();
		
		// temporary value initialization
		int updateFiels = 0;
		String sql = "";
		List<E200Role> e200Roles;
		List<JsonNode> jsonObjects;
		switch (what) {

		// Get all data from R200Model
		case 200:
			try {
				sql = d200RoleDataAccess.getQuery(what, modelRequest);

				e200Roles = b200RoleBusinessLogic.function200(sql);

				pageResult.setData(e200Roles);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Insert data to R200Model
		case 201:
			try {
				sql = d200RoleDataAccess.getQuery(what, modelRequest);

				updateFiels = b200RoleBusinessLogic.function201(sql);
				
				pageResult.setInsertId(modelRequest.getId());
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Update data R200Model
		case 202:
			try {
				sql = d200RoleDataAccess.getQuery(what, modelRequest);

				updateFiels = b200RoleBusinessLogic.function202(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			break;

		// Delete data of R200Model
		case 203:
			try {
				sql = d200RoleDataAccess.getQuery(what, modelRequest);

				updateFiels = b200RoleBusinessLogic.function203(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Find data with id R200Model
		case 204:
			try {
				sql = d200RoleDataAccess.getQuery(what, modelRequest);

				e200Roles = b200RoleBusinessLogic.function204(sql);

				pageResult.setData(e200Roles);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Select with pagination(offset, number-item-in-page) R200Model
		case 205:
			try {
				sql = d200RoleDataAccess.getQuery(what, modelRequest);
			
				e200Roles = b200RoleBusinessLogic.function205(sql);

				pageResult.setData(e200Roles);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Count number item of R200Model
		case 206:
			try {
				sql = d200RoleDataAccess.getQuery(what, modelRequest);

				jsonObjects = b200RoleBusinessLogic.function206(sql);

				pageResult.setData(jsonObjects);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;
		}

		pageResult.setSql(sql);
		pageResult.setNumRowUpdate(updateFiels);
		pageResult.setStatus(true);

		return pageResult;
	}

}
