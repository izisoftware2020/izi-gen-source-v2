package com.izisoftware.gencode.service;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.izisoftware.gencode.domain.entity.E000Account;
import com.izisoftware.gencode.util.ConnectDataBase;

/**
 * @author ThanhVV
 *
 */
@Service
public class UserService implements UserDetailsService {
	
	
	
	private static final Logger logger = Logger.getLogger(UserService.class);
	
	/**
	 * <h1>Check user having permission to access entity</h1>
	 * @author ThanhVV
	 * @param idEntity
	 * @return
	 * @throws SQLException
	 */
	public boolean isHavingPermission(String id) throws SQLException {
//		connect.connect();
//		
//		String sql = "SELECT * FROM `p000account` as t1 "
//				+ "INNER JOIN `p300roledetail` as t2 "
//				+ "ON t1.idRole = t2.idRole "
//				+ "INNER JOIN `p100menu` as t3 "
//				+ "ON t3.id = t2.idMenu "
//				+ "WHERE t3.id = '"+id+"' AND status = '2' AND t1.email = '"+getEmailPricipalCurrent()+"'  ";
//		
//		List<String> result = connect.excuteMixedQuery(sql);
//		
//		connect.closeConnect();
//		
//		return result.size() > 0;
		return true;
	}
	
	/**
	 * get Email Pricipal Current
	 * @return
	 */
	public String getEmailPricipalCurrent() {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		
		return userName;
	}
	
	/**
	 * load Use rBy Username
	 * @param email
	 * @exception UsernameNotFoundException
	 */
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		try {
			ConnectDataBase connectDB = new ConnectDataBase();
			
			connectDB.connect();
			
			String sqlGetUser = " SELECT * FROM p000account WHERE email = '"+email+"' ";
		
			E000Account user = (E000Account) connectDB.excuteSingeQuery(sqlGetUser).get(0);
			
			connectDB.closeConnect();
			
			return new UserCertificate(user);
		} catch (Exception e) {
			logger.debug(e);
		}
		
		return null;
	}
	
	/**
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public E000Account loadE000AccountByEmail(String email) throws Exception {
		try {
			ConnectDataBase connectDB = new ConnectDataBase();
			
			connectDB.connect();
			
			String sqlGetUser = " SELECT * FROM p000account WHERE email = '"+email+"' ";
			
			E000Account user = (E000Account) connectDB.excuteSingeQuery(sqlGetUser).get(0);
			
			connectDB.closeConnect();
			
			return user;
		} catch (Exception e) {
			logger.debug(e);
		}
		return null;
	}
	
	/**
	 * get Id Account Current
	 * @return
	 * @throws SQLException
	 */
	public int getIdAccountCurrent() throws SQLException {
		ConnectDataBase connectDB = new ConnectDataBase();
		
		connectDB.connect();
		try {
			String email = SecurityContextHolder.getContext().getAuthentication().getName();
			
			String sqlGetUser = " SELECT * FROM p000account WHERE email = '"+email+"' ";
		
			E000Account user = (E000Account) connectDB.excuteSingeQuery(sqlGetUser).get(0);
			
			return user.getId();
		} catch (Exception e) {
			logger.debug(e);
		}
		
		connectDB.closeConnect();
		
		return -1;
		
	}

}
