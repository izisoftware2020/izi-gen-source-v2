package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.domain.entity.E000Account;
import com.izisoftware.gencode.util.ConnectDataBase;

@Service
public class B000AccountBusinessLogicImplement {
	
	
	// get all data from 0 staff
	public List<E000Account> function0(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E000Account> accounts = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return accounts;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// insert data to p000acount
	public int function1(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int numRecordInsert = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return numRecordInsert;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}
	}

	// update data of p000account
	public int function2(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int numRecordUpdate = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return numRecordUpdate;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// Delete data of p000account
	public int function3(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int numRecordDelete = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return numRecordDelete;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// check gmail candidate
	public List<E000Account> function4(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E000Account> e000Accounts = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e000Accounts;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// login candidate
	public List<E000Account> function5(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E000Account> e000Accounts = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e000Accounts;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// get staff with id
	public List<E000Account> function6(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E000Account> e000Accounts = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e000Accounts;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// change pass md5
	public int function7(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int numRecordUpdate = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return numRecordUpdate;

		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// update info staff
	public int function8(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int numRecordUpdate = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return numRecordUpdate;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// update info avatar
	public int function9(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			int numRecordUpdate = connectDB.excuteInsertOrUpdate(sql);

			connectDB.closeConnect();

			return numRecordUpdate;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception();
		}

	}

	// Select with pagination(offset, number-item-in-page) p000account
	public List<E000Account> function10(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E000Account> e000Accounts = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e000Accounts;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

	// Count number item of p000account
	public List<JsonNode> function11(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<JsonNode> JsonNode = connectDB.excuteMixedQuery(sql);

			connectDB.closeConnect();

			return JsonNode;

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	// Get account sign up of teacher
	public List<E000Account> function12(String sql) throws Exception {
		ConnectDataBase connectDB = new ConnectDataBase();
		try {
			connectDB.connect();

			List<E000Account> e000Accounts = connectDB.excuteSingeQuery(sql);

			connectDB.closeConnect();

			return e000Accounts;
		} catch (Exception e) {
			connectDB.closeConnect();
			throw new Exception(e.getMessage());
		}

	}

}
