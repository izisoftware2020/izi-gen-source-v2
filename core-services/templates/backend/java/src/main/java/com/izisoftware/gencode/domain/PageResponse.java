package com.izisoftware.gencode.domain;

import java.util.List;

public class PageResponse {

	private boolean status = true;

	private String insertId;

	public List<?> data;

	private int numRowUpdate = 0;

	private String error = "";

	private String sql = "";

	public PageResponse() {
		super();
	}

	public PageResponse(String insertId, List<?> data, int numRowUpdate) {
		super();
		this.status = true;
		this.insertId = insertId;
		this.data = data;
		this.numRowUpdate = numRowUpdate;
		this.error = "";
		this.sql = "";
	}

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * @return the insertId
	 */
	public String getInsertId() {
		return insertId;
	}

	/**
	 * @param insertId the insertId to set
	 */
	public void setInsertId(String insertId) {
		this.insertId = insertId;
	}

	/**
	 * @return the data
	 */
	public List<?> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<?> data) {
		this.data = data;
	}

	/**
	 * @return the numRowUpdate
	 */
	public int getNumRowUpdate() {
		return numRowUpdate;
	}

	/**
	 * @param numRowUpdate the numRowUpdate to set
	 */
	public void setNumRowUpdate(int numRowUpdate) {
		this.numRowUpdate = numRowUpdate;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the sql
	 */
	public String getSql() {
		return sql;
	}

	/**
	 * @param sql the sql to set
	 */
	public void setSql(String sql) {
		this.sql = sql;
	}

}
