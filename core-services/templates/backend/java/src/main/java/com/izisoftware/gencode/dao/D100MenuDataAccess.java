package com.izisoftware.gencode.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.izisoftware.gencode.domain.request.R100Model;
import com.izisoftware.gencode.service.UserService;

@Repository
public class D100MenuDataAccess {
	
	@Autowired
	UserService userService;
	
	// *********************((table p100Menu ))***************************
	public String getQuery(int what, R100Model r100Menu) {
				// check condition
				String condition = r100Menu.getCondition() == null ? "" : r100Menu.getCondition();
				
				switch (what) {

					// Get all data from p100Menu
					case 100:
						
						return "SELECT * FROM p100Menu";
	
					// Insert data from p100menu
					case 101:
						
						return " INSERT INTO p100Menu(IdParentMenu, IsGroup, Name, Slug, Icon, Position) "
							 + " VALUES('"+r100Menu.getIdParentMenu()+"', '"+r100Menu.getIsGroup()+"', '"+r100Menu.getName()+"','"+r100Menu.getSlug()+"','"+r100Menu.getIcon()+"','"+r100Menu.getPosition()+"')"; 
	
					// Update data by id p100menu
					case 102:
						
						return " UPDATE p100Menu SET IdParentMenu='"+r100Menu.getIdParentMenu()+"', IsGroup='"+r100Menu.getIsGroup()+"', Name='"+r100Menu.getName()+"', Slug='"+r100Menu.getSlug()+"',"
							 + " Icon='"+r100Menu.getIcon()+"', Position='"+r100Menu.getPosition()+"'"
							 + " WHERE id='"+r100Menu.getId()+"'";       
	
					// Delete data of p100Menu
					case 103:
						
						return "DELETE FROM p100Menu WHERE id IN("+r100Menu.getListid()+")";   
							
					// Find data by id p400student
					case 104:
						
						return " SELECT * FROM p100Menu "
							 + " WHERE id = '"+r100Menu.getIdRole()+"' ";   
	
					// Select with pagination(offset, number-item-in-page) from p100Menu
					case 105:
						
						return " SELECT * FROM (SELECT id FROM p100Menu ORDER BY id LIMIT '"+r100Menu.getOffset()+"', '"+r100Menu.getLimit()+"') T1 "
							 + " INNER JOIN p100Menu T2 ON T1.id = T2.id ";                                            

					
					// Count number item of p100Menu
					case 106:
						
						return "SELECT COUNT(1) FROM p100Menu "+condition+"";
						
					// Get all data from p100Menu
					case 107:
				
						return "SELECT * FROM p100Menu WHERE IsGroup = '1'";
					
				   // Get all data from p100Menu recursive
					case 108:
						
						return "SELECT  t1.*, "
								+ "t2.id as id1, "
								+ "t2.`IdParentMenu` as IdParentMenu1, "
								+ "t2.`IsGroup` as IsGroup1, "
								+ "t2.`Name` as Name1, "
								+ "t2.`Slug` as Slug1, "
								+ "t2.`Icon` as Icon1, "
								+ "t2.`Position` as Position1 "
								+ "FROM p100Menu t1 "
								+ "LEFT JOIN p100Menu t2 ON t1.id = t2.IdParentMenu "
								+ "WHERE t1.IdParentMenu = 0 "
								+ "ORDER BY t1.position, t1.id, t2.position ";
					
					// Get all data from p100Menu recursive
					case 109:
						
						return   "SELECT DISTINCT   t1.*,"
								+ "t2.id as id1, "
								+ "t2.`IdParentMenu` as IdParentMenu1, "
								+ "t2.`IsGroup` as IsGroup1, "
								+ "t2.`Name` as Name1, "
								+ "t2.`Slug` as Slug1, "
								+ "t2.`Icon` as Icon1, "
								+ "t2.`Position` as Position1, "
								+ "t3.id as IdRoleDetail, "
								+ "t4.id as IdRoleDetail1, "
								+ "t3.Status, "
								+ "t4.Status as Status1 "
								+ "FROM p100Menu t1 "
								+ "LEFT JOIN p100Menu t2 ON t1.id = t2.IdParentMenu "
								+ "LEFT JOIN (SELECT * FROM p300RoleDetail WHERE idRole = '"+r100Menu.getIdRole()+"') t3 "
								+ "ON t1.id = t3.IdMenu "
								+ "LEFT JOIN (SELECT * FROM p300RoleDetail WHERE idRole = '"+r100Menu.getIdRole()+"') t4 "
								+ "ON t2.id = t4.IdMenu "
								+ "WHERE t1.IdParentMenu = 0 "
								+ "ORDER BY t1.position, t1.id, t2.position ";
					
					// Get all data from p100Menu recursive
					case 110:
						
						return "SELECT DISTINCT   t1.*, "
								
								+ "t2.id as id1, "
								+ "t2.`IdParentMenu` as IdParentMenu1, "
								+ "t2.`IsGroup` as IsGroup1, "
								+ "t2.`Name` as Name1, "
								+ "t2.`Slug` as Slug1, "
								+ "t2.`Icon` as Icon1, "
								+ "t2.`Position` as Position1, "
								+ "t3.id as IdRoleDetail, "
								+ "t4.id as IdRoleDetail1, "
								+ "t3.Status, "
								+ "t4.Status as Status1 "
								
								+ "FROM p100Menu t1  "
								
								+ "LEFT JOIN p100Menu t2 ON t1.id = t2.IdParentMenu "
								+ "LEFT JOIN (SELECT * FROM p300RoleDetail WHERE idRole = (SELECT IdRole FROM p000account WHERE email='"+userService.getEmailPricipalCurrent()+"')) t3 "
								+ " ON t1.id = t3.IdMenu   "
								+ "LEFT JOIN (SELECT * FROM p300RoleDetail WHERE idRole = (SELECT IdRole FROM p000account WHERE email='"+userService.getEmailPricipalCurrent()+"')) t4 "
								+ " ON t2.id = t4.IdMenu "
								+ "WHERE t1.IdParentMenu = 0 "
								+ " ORDER BY t1.position, t1.id, t2.position ";
					
					// Get get permission of component	
					case 111:
					
						return "   SELECT t1.Status "
								+ "FROM p300RoleDetail t1 "
								+ "INNER JOIN p200Role t2 on t1.IdRole = t2.id "
								+ "INNER JOIN p000account t3 on t2.id = t3.IdRole "
								+ "INNER JOIN p100Menu t4 on t1.IdMenu = t4.id "
								+ "WHERE t3.id = '"+r100Menu.getIdUser()+"' "
								+ "AND t4.Slug = '"+r100Menu.getUrl()+"' ";
				}
				
				
			

				return null;
			}

}
