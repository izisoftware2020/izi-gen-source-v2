package com.izisoftware.gencode.rest;

import java.io.FileNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.izisoftware.gencode.businesslogic.BaseBusinessLogic;
import com.izisoftware.gencode.domain.JwtRequest;
import com.izisoftware.gencode.domain.JwtResponse;
import com.izisoftware.gencode.domain.PageResponse;
import com.izisoftware.gencode.domain.entity.E000Account;
import com.izisoftware.gencode.service.FileStorageService;
import com.izisoftware.gencode.service.JsonObjectService;
import com.izisoftware.gencode.service.SendEmailService;
import com.izisoftware.gencode.service.UserService;
import com.izisoftware.gencode.util.ConnectDataBase;
import com.izisoftware.gencode.util.JwtTokenUtil;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class RestApiController {

 
	ConnectDataBase connectDataBase = new ConnectDataBase();

	@Autowired
	BaseBusinessLogic businessLogic;

	@Autowired
	JsonObjectService handlingJsonObject;

	@Autowired
	JwtTokenUtil jwtTokenUtil;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserService userService;

	@Autowired
	SendEmailService sendEmailService;
	
	@Autowired
	FileStorageService fileStorageService;
	
	@Autowired
	UserService service;

	/**
	 * execute Data By What Need Authenticate
	 * @author ThanhVV
	 * @param json the JSON value include number of what and property request
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/izi")
	public PageResponse executeDataByWhatNeedAuthen(@RequestBody String param) throws Exception {
		// call business logic
		PageResponse result = businessLogic.executeBusiness(param);
		
		System.out.println(service.getEmailPricipalCurrent());
		
		return result;
	}

	/**
	 * execute Data By What No Need Authenticate
	 * @author ThanhVV
	 * @param json
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/no-auth/izi")
	public PageResponse executeDataByWhatNoNeedAuthen(@RequestBody String param) throws Exception {
		// call business logic
		int what = Integer.valueOf(handlingJsonObject.getWhatByParam(param));

		// Have to condition
		if (what < 3) {
			PageResponse result = businessLogic.executeBusiness(param);

			return result;
		}
		return null;
	}

	/**
	 * Check login
	 * 
	 * @author ThanhVV
	 * @param jwtRequest the object have two property is username with password
	 * @return
	 * @throws Exception
	 */

	@PostMapping("/login")
	public JwtResponse login(@RequestBody JwtRequest jwtRequest) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(), jwtRequest.getPassword()));

			final UserDetails userDetails = userService.loadUserByUsername(jwtRequest.getUsername());

			final String token = jwtTokenUtil.generateToken(userDetails);

			E000Account e000Account = userService.loadE000AccountByEmail(jwtRequest.getUsername());

			return new JwtResponse(token, e000Account);

		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}

	}
	
	/**
	 * @param src this is parameter of file
	 * @return
	 * @throws FileNotFoundException
	 */
	@PostMapping("/uploadImage")
	public String uploadImage(@RequestBody String src) throws FileNotFoundException {
		
		System.out.println(src);
		
		return fileStorageService.insertOrUpdateFile(src);
	}

}
