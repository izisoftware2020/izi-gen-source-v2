package com.izisoftware.gencode.businesslogic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.izisoftware.gencode.dao.D300RoleDetailDataAccess;
import com.izisoftware.gencode.domain.PageResponse;
import com.izisoftware.gencode.domain.entity.E300RoleDetail;
import com.izisoftware.gencode.domain.request.R300Model;
import com.izisoftware.gencode.service.JsonObjectService;

@Service
public class B300RoleDetailBusinessLogic {
	@Autowired
	JsonObjectService jsonObjectService;

	@Autowired
	D300RoleDetailDataAccess d300RoleDetailDataAccess;

	@Autowired
	B300RoleDetailBusinessLogicImplement b300RoleDetailBusinessLogic;
	
	public PageResponse execute(int what, String param) throws Exception {

		R300Model modelRequest = (R300Model) jsonObjectService.getRequestObjectFromParam(param);

		PageResponse pageResult = new PageResponse();

		// temporary value initialization
		int updateFiels = 0;
		String sql = "";
		List<E300RoleDetail> e300RoleDetails;
		List<JsonNode> jsonObjects;
		switch (what) {

		// Get all data from p300RoleDetail
		case 300:
			try {
				sql = d300RoleDetailDataAccess.getQuery(what, modelRequest);

				e300RoleDetails = b300RoleDetailBusinessLogic.function300(sql);

				pageResult.setData(e300RoleDetails);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Insert data to p300RoleDetail
		case 301:
			try {
				sql = d300RoleDetailDataAccess.getQuery(what, modelRequest);

				updateFiels = b300RoleDetailBusinessLogic.function301(sql);
				
				pageResult.setInsertId(modelRequest.getId());
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Update data p300RoleDetail
		case 302:
			try {
				System.out.println(modelRequest.toString());
				sql = d300RoleDetailDataAccess.getQuery(what, modelRequest);
				
				System.out.println(modelRequest.getId());
				
				updateFiels = b300RoleDetailBusinessLogic.function302(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Delete data of p300RoleDetail
		case 303:
			try {
				sql = d300RoleDetailDataAccess.getQuery(what, modelRequest);

				updateFiels = b300RoleDetailBusinessLogic.function303(sql);

			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Find data with id p300RoleDetail
		case 304:
			try {
				sql = d300RoleDetailDataAccess.getQuery(what, modelRequest);

				e300RoleDetails = b300RoleDetailBusinessLogic.function304(sql);

				pageResult.setData(e300RoleDetails);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Select with pagination(offset, number-item-in-page) p300RoleDetail
		case 305:
			try {
				sql = d300RoleDetailDataAccess.getQuery(what, modelRequest);
			
				e300RoleDetails = b300RoleDetailBusinessLogic.function305(sql);

				pageResult.setData(e300RoleDetails);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		// Count number item of p300RoleDetail
		case 306:
			try {
				sql = d300RoleDetailDataAccess.getQuery(what, modelRequest);

				jsonObjects = b300RoleDetailBusinessLogic.function306(sql);

				pageResult.setData(jsonObjects);
			} catch (Exception e) {
				pageResult.setError(e.getMessage());
			}
			
			break;

		}

		pageResult.setSql(sql);
		pageResult.setNumRowUpdate(updateFiels);
		pageResult.setStatus(true);

		return pageResult;
	}
	
}
