package com.izisoftware.gencode.domain.entity;

@ETable(name = "p300roledetail")
public class E300RoleDetail {
	
	@EColumn(name="id")
	public int id;
	
	@EColumn(name="IdRole")
	public int IdRole;
	
	@EColumn(name="IdMenu")
	public int IdMenu;
	
	@EColumn(name="Status")
	public int Status;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the idRole
	 */
	public int getIdRole() {
		return IdRole;
	}

	/**
	 * @param idRole the idRole to set
	 */
	public void setIdRole(int idRole) {
		IdRole = idRole;
	}

	/**
	 * @return the idMenu
	 */
	public int getIdMenu() {
		return IdMenu;
	}

	/**
	 * @param idMenu the idMenu to set
	 */
	public void setIdMenu(int idMenu) {
		IdMenu = idMenu;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return Status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		Status = status;
	}

	@Override
	public String toString() {
		return "E300RoleDetail [id=" + id + ", IdRole=" + IdRole + ", IdMenu=" + IdMenu + ", Status=" + Status + "]";
	}
	
	
	
	
}
