package com.izisoftware.gencode.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


/**
 * @author Administrator
 */
@Service
public class SchedulerService {
	
	/**
	 * 
	 */
	@Scheduled(fixedDelay = 3000)
	public void demoServiceWithFixedDelay() {

	}

	@Scheduled(fixedRate = 3000)
	public void demoServiceWithFixedRate() {

	}

	@Scheduled(cron = "0 0 * * * *")
	public void demoServiceWithCron() {

	}
}
