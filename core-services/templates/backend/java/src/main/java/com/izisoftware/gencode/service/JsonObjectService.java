package com.izisoftware.gencode.service;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;
import org.reflections.Reflections;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.izisoftware.gencode.domain.PageRequest;
import com.izisoftware.gencode.domain.request.RTable;


/**
 * @author ThanhVV
 */
@Service
public class JsonObjectService {

	/**
	 * <h1 get Request Object From Parameter </h1>
	 * @author ThanhVV
	 * @param paramJson
	 * @return object request
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public PageRequest getRequestObjectFromParam(String param) throws InstantiationException, IllegalAccessException {
		Gson g = new Gson();
		
		Map<String, Class<?>> resultClass = getAllAnotationOfClassRequest();
	
		String what = getWhatByParam(param);
	
		String resultToWhat = handleNumberOfWhat(what);
		
		Class<?> myClass = resultClass.get(resultToWhat);
		
		return myClass != null?(PageRequest) g.fromJson(param, myClass): null ;
	}
	
	/**
	 * <h1> Handle Number Of What </h1>
	 * @author ThanhVV
	 * @param whawRaw 
	 * @return value after processing
	 */
	public String handleNumberOfWhat(String whatRaw) {
		// get number reduction
		int numberReduction = Integer.valueOf(whatRaw) % 100;
		// what = whatFromResult -  numberReduction
		int what = Integer.valueOf(Integer.valueOf(whatRaw) - numberReduction);
		
		String value = "Request" + String.valueOf(what);
	
		return value;
	}

	/**
	 * <h1>get number of what from parameter</h1>
	 * 
	 * @param param
	 * @return number what
	 */
	public String getWhatByParam(String param) {
		JSONObject obj = new JSONObject(param);
		String what = obj.getString("what");
		return what;
	}

	/**
	 * <h1>Get all annotation of class</h1>
	 * 
	 * @author ThanhVV
	 * @return return table name and Class
	 */
	static private Map<String, Class<?>> getAllAnotationOfClassRequest() {
		// get all annotation ModelRequest on package request
		Reflections reflections = getReflectionsOfRequest();

		Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(RTable.class);

		Map<String, Class<?>> resultClass = new HashMap<String, Class<?>>();

		for (Class<?> c : annotated) {
			Annotation annotation = c.getAnnotation(RTable.class);
			
			RTable table = (RTable) annotation;
			
			resultClass.put(table.name(), c);
		}

		return resultClass;
	}

	/**
	 * <h1>get number of what from parameter</h1>
	 * 
	 * @author ThanhVV
	 * @return List Reflections of request
	 */
	private static Reflections getReflectionsOfRequest() {
		return new Reflections("com.izisoftware.gencode.domain.request");
	}
}
