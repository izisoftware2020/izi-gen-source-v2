package com.izisoftware.gencode.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.springframework.stereotype.Component;
import com.izisoftware.gencode.GencodeConstant;

@Component
public class MySQLConfiguration extends JdbcConfiguration {

	Connection conn;
	
	/**
	 * 
	 * @throws SQLException
	 */
	public MySQLConfiguration() throws SQLException {
		// inject parameter compatible with database
		this(GencodeConstant.JDBC_DRIVER, GencodeConstant.DB_URL, GencodeConstant.USER, GencodeConstant.PASSWORLD);
	}

	/**
	 * 
	 * @param jdbcDriver the jdbcDriver of database
	 * @param dbURL the URL of database
	 * @param user the userName of database
	 * @param pass the pass of database
	 * @throws SQLException
	 */
	public MySQLConfiguration(String jdbcDriver, String dbURL, String user, String pass) throws SQLException {
		super(jdbcDriver, dbURL, user, pass);
		this.conn = DriverManager.getConnection(dbURL, user, pass);
	}
	
	/**
	 * get connect
	 */
	@Override
	public Connection getConnect() {
		return this.conn;
	}
	
	/**
	 * get statement
	 */
	@Override
	public Statement getStatement() throws SQLException {
		return conn.createStatement();
	}
	
	/**
	 * get result set
	 */
	@Override
	public ResultSet getResultSet(String sql) throws SQLException {
		return getStatement().executeQuery(sql);
	}
	
	/**
	 * close connection
	 */
	@Override
	public void closeConnection(ResultSet rs, Statement statement, Connection connection) throws SQLException {
		rs.close();
		statement.close();
		connection.close();
	}

}
