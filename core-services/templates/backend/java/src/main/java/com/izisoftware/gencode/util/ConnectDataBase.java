package com.izisoftware.gencode.util;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.izisoftware.gencode.configuration.MySQLConfiguration;
import com.izisoftware.gencode.service.DataJDBCService;

/**
 * @author ThanhVV
 */

public class ConnectDataBase {
	
	// create new object DataJDBCService
	private DataJDBCService dataJDBCService = new DataJDBCService();
	
	private Connection cnn;
	private Statement stmt;
	private ResultSet result;
	
	/**
	 * connection into database
	 * @throws SQLException
	 */
	public void connect() throws SQLException {
		  
		MySQLConfiguration mySQLConfiguration = new  MySQLConfiguration();
		
		// get connection
		this.cnn = mySQLConfiguration.getConnect();
		// create statement
		this.stmt = this.cnn.createStatement();
	}

	/**
	 * excute singe query 
	 * 
	 * @param sql the sql query into database 
	 * @author ThanhVV
	 */
	public <T> List<T> excuteSingeQuery(String sql) throws Exception {
		List<T> result = new ArrayList<T>();
		
		List<String> columnNames = new ArrayList<String>();
		
		ResultSet resultSet = this.getResultFromExcuteQuery(sql);
		
		ResultSetMetaData rsmd = resultSet.getMetaData();

		String tableName = rsmd.getTableName(1);

		if (!isSingleQuery(rsmd)) {
			throw new SQLException("Can't excute nested query in singe query");
		}

		Class<?> myClass = dataJDBCService.getClassOfAnnotationByDataTable(tableName);
		if (myClass != null) {
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				columnNames.add(rsmd.getColumnLabel(i));
			}

			while (resultSet.next()) {
				// collect row data as object in a list
				List<Object> rowData = new ArrayList<Object>();

				Map<String, String> map = new HashMap<String, String>();

				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					rowData.add(resultSet.getObject(i));
				}
				for (int colIndex = 0; colIndex < rsmd.getColumnCount(); colIndex++) {
					Object columnObject = rowData.get(colIndex);
					if (columnObject != null) {
						String columnName = columnNames.get(colIndex);
						String value = columnObject.toString();
						// lưa value cho map
						map.put(columnName, value);
					}
				}
				// get all fields of instance
				Field[] fields = myClass.getDeclaredFields();
				// create object
				T object = (T) myClass.newInstance();
				for (Field field : fields) {
					field.setAccessible(true);
					// set data for field of object
					field = dataJDBCService.setDataForFieldOfObject(object, field, map);
				}

				result.add(object);
				
			}
			resultSet.close();
			return result;
		}
		return null;
	}

	/**
	 * Excute nested query
	 * 
	 * @param sqlQuery the sql query into database
	 * @author ThanhVV
	 * @throws JsonProcessingException 
	 */
	public List<JsonNode> excuteMixedQuery(String sql) throws SQLException, JsonProcessingException {
		List<JsonNode> objectValue = new ArrayList<JsonNode>();
		
		List<String> columnNames = new ArrayList<String>();
		
		ResultSet resultSet = this.getResultFromExcuteQuery(sql);
		
		ResultSetMetaData rsmd = resultSet.getMetaData();
		
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {
			columnNames.add(rsmd.getColumnLabel(i));
		}
		
		while (resultSet.next()) {
			// collect row data as object in a list
			List<Object> rowData = new ArrayList<Object>();

			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				rowData.add(this.result.getObject(i));
			}

			Map<String, Object> map = new HashMap();
			for (int colIndex = 0; colIndex < rsmd.getColumnCount(); colIndex++) {
				Object columnObject = rowData.get(colIndex);

				if (columnObject != null) {
					String columnName = columnNames.get(colIndex);
					String value = columnObject.toString();
					map.put(columnName, value);
				}
			}
			// return jsonObject string
			ObjectMapper objectMapper = new ObjectMapper();
			String stringResult = objectMapper.writeValueAsString(map);
			// convert string json to object
			JsonNode jsonNode = objectMapper.readTree(stringResult);
			objectValue.add(jsonNode);
		}
		
	 
		
		return objectValue;
	}
	
	/**
	 * 
	 * @param sql the sql query into database
	 * @return
	 * @throws SQLException
	 */
	public int excuteInsertOrUpdate(String sql) throws SQLException {
		return this.stmt.executeUpdate(sql);
	}
	
	/**
	 * 
	 * @param status
	 * @throws SQLException
	 */
	public void setAutoCommit(boolean status) throws SQLException {
		this.cnn.setAutoCommit(status);
	}
	
	
	/**
	 * 
	 * @throws SQLException
	 */
	public void commit() throws SQLException {
		this.cnn.commit();
	}
	
	/**
	 * 
	 * @throws SQLException
	 */
	public void rollback() throws SQLException {
		this.cnn.rollback();
	}
	
	/**
	 * 
	 * @throws SQLException
	 */
	public void closeConnect() throws SQLException {
		// catch because in case of update this.result will be null
		try {
			this.cnn.close();
			this.stmt.close();
			this.result.close();
		} catch (Exception e) {
			 
		}
	}
	

	/**
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	private ResultSet getResultFromExcuteQuery(String sql) throws SQLException {
		this.result = this.stmt.executeQuery(sql);
		
		return this.result;
	}

	/**
	 * Check SingleQuery
	 * 
	 * @author ThanhVV
	 * @return true if right
	 */
	private boolean isSingleQuery(ResultSetMetaData rsmd) throws SQLException {
		Set<String> nameDTable = new HashSet<String>();
		
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {
			nameDTable.add(rsmd.getTableName(i));
		}
		
		return nameDTable.size() == 1;
	}

}
