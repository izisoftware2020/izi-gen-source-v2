package com.izisoftware.gencode.domain.entity;

@ETable(name = "p000account")
public class E000Account {

	@EColumn(name = "id")
	private int id;

	@EColumn(name = "IdRole")
	private int IdRole;

	@EColumn(name = "name")
	private String name;

	@EColumn(name = "email")
	public String email;

	@EColumn(name = "password")
	public String password;

	@EColumn(name = "img")
	public String img;

	@EColumn(name = "created_date")
	public java.util.Date created_date;

	@EColumn(name = "role")
	public String role;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the idRole
	 */
	public int getIdRole() {
		return IdRole;
	}

	/**
	 * @param idRole the idRole to set
	 */
	public void setIdRole(int idRole) {
		this.IdRole = idRole;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
 

	 

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * @param img the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * @return the created_date
	 */
	public java.util.Date getCreated_date() {
		return created_date;
	}

	/**
	 * @param created_date the created_date to set
	 */
	public void setCreated_date(java.util.Date created_date) {
		this.created_date = created_date;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	

}
