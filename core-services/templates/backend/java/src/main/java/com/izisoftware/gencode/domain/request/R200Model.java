package com.izisoftware.gencode.domain.request;

import com.izisoftware.gencode.domain.PageRequest;

@RTable(name = "Request200")
public class R200Model extends PageRequest{

	public String id;
	
	public String Name;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}
	
	
	
	
}
