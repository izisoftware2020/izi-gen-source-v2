package com.izisoftware.gencode.service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.reflections.Reflections;
import com.izisoftware.gencode.domain.entity.EColumn;
import com.izisoftware.gencode.domain.entity.ETable;

/**
 * @author ThanhVV
 */
public class DataJDBCService {

	/**
	 * @author ThanhVV Set data for field of object
	 * @param T      object need set
	 * @param f      field need set value
	 * @param hValue hValue key is column of class and value is value of table
	 * @return field then set value
	 */
	public <T> Field setDataForFieldOfObject(T t, Field f, Map<String, String> hValue) throws Exception {
		// check type value of field object
		String nameType = f.getType().getName();

		String valueOfAnnatation = getValueAnntationOfJCoLumFromField(f);
		// check if valueOfAnnatation is equivalent field object
		if (hValue.containsKey(valueOfAnnatation)) {
			String value = hValue.get(valueOfAnnatation);
			if ((nameType.equals("java.lang.Byte") || nameType.equals("byte")) && value != null) {
				f.set(t, Byte.parseByte(value));
			} else if ((nameType.equals("java.lang.Character") || nameType.equals("char")) && value != null) {
				f.set(t, value);
			} else if ((nameType.equals("java.lang.Short") || nameType.equals("short")) && value != null) {
				f.set(t, Short.parseShort(value));
			} else if ((nameType.equals("java.lang.Integer") || nameType.equals("int")) && value != null) {
				f.set(t, Integer.parseInt(value));
			} else if ((nameType.equals("java.lang.Long") || nameType.equals("long")) && value != null) {
				f.set(t, Long.parseLong(value));
			} else if ((nameType.equals("java.lang.Float") || nameType.equals("float")) && value != null) {
				f.set(t, Float.parseFloat(value));
			} else if ((nameType.equals("java.lang.Double") || nameType.equals("double")) && value != null) {
				f.set(t, Double.parseDouble(value));
			} else if ((nameType.equals("java.lang.String") || nameType.equals("string")) && value != null) {
				f.set(t, value);
			} else if ((nameType.equals("java.lang.Boolean") || nameType.equals("boolean")) && value != null) {
				f.set(t, value.equals("true") || value.equals("1") ? true : false);
			} else {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");
				if (value != null) {
					java.util.Date date = formatter.parse(value);
					f.set(t, date);
				}
			}
			return f;
		}
		return f;
	}

	/**
	 * @author ThanhVV Get class of annotation by data table
	 */
	public Class<?> getClassOfAnnotationByDataTable(String tableName) {
		Map<String, Class<?>> getAllNameJTableOfAnnotation = getAllAnotationOfClassEntity();

		return getAllNameJTableOfAnnotation.get(tableName);
	}

	/**
	 * get Value Anntation Of JCoLum From Field
	 * 
	 * @author ThanhVV Get Value Annotation Of JColum From Field
	 * @return value of annotation jcolumn from field
	 */
	private String getValueAnntationOfJCoLumFromField(Field f) {
		// get value of Jcolumn from field object
		Annotation annotation = f.getAnnotation(EColumn.class);

		if (annotation instanceof EColumn) {
			EColumn a = (EColumn) annotation;

			return a.name();
		}
		return null;
	}

	/**
	 * @author ThanhVV Get getReflections
	 * @return return reflections of package entity
	 */
	private static Reflections getReflectionsOfEntity() {
		return new Reflections("com.izisoftware.gencode.domain.entity");
	}

	/**
	 * Get all annotation of class
	 * 
	 * @author ThanhVV
	 * @return return table name and Class
	 */
	private static Map<String, Class<?>> getAllAnotationOfClassEntity() {
		// get all annotation JTable on package entity
		Reflections reflections = getReflectionsOfEntity();

		Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(ETable.class);

		Map<String, Class<?>> resultClass = new HashMap<String, Class<?>>();

		for (Class<?> c : annotated) {
			Annotation annotation = c.getAnnotation(ETable.class);

			ETable table = (ETable) annotation;

			resultClass.put(table.name(), c);
		}
		return resultClass;
	}
}
