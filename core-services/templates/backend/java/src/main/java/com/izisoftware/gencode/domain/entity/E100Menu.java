package com.izisoftware.gencode.domain.entity;

@ETable(name = "p100menu")
public class E100Menu {

	@EColumn(name = "id")
	private int id;

	@EColumn(name = "IdParentMenu")
	private int IdParentMenu;

	@EColumn(name = "IsGroup")
	private int IsGroup;

	@EColumn(name = "Name")
	private String Name;

	@EColumn(name = "Slug")
	private String Slug;

	@EColumn(name = "Icon")
	private String Icon;

	@EColumn(name = "Position")
	private int Position;

	/**
	 * 
	 */
	public E100Menu() {
		super();
	}

	public E100Menu(int id, int idParentMenu, int isGroup, String name, String slug, String icon, int position) {
		super();
		this.id = id;
		IdParentMenu = idParentMenu;
		IsGroup = isGroup;
		Name = name;
		Slug = slug;
		Icon = icon;
		Position = position;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the idParentMenu
	 */
	public int getIdParentMenu() {
		return IdParentMenu;
	}

	/**
	 * @param idParentMenu the idParentMenu to set
	 */
	public void setIdParentMenu(int idParentMenu) {
		IdParentMenu = idParentMenu;
	}

	/**
	 * @return the isGroup
	 */
	public int getIsGroup() {
		return IsGroup;
	}

	/**
	 * @param isGroup the isGroup to set
	 */
	public void setIsGroup(int isGroup) {
		IsGroup = isGroup;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the slug
	 */
	public String getSlug() {
		return Slug;
	}

	/**
	 * @param slug the slug to set
	 */
	public void setSlug(String slug) {
		Slug = slug;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return Icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		Icon = icon;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return Position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		Position = position;
	}

}
