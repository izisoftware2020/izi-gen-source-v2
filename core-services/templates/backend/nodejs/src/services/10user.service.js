const httpStatus = require('http-status');
const { User, Menu, RoleDetail } = require('../models');
const ApiError = require('../utils/ApiError');
/**
 * Find all user
 * @returns
 */
const find = async () => {
  return User.find();
};

/**
 * Create a user
 * @param {*} body
 * @returns
 */
const create = async (body) => {
  return User.create(body);
};

/**
 * Update a user
 * @param {*} id
 * @param {*} body
 * @returns
 */
const findByIdAndUpdate = async (id, body) => {
  return User.findByIdAndUpdate({ _id: id }, body);
};

/**
 * Delete a user
 * @param {*} ids
 * @returns
 */
const findByIdAndDelete = async (ids) => {
  return User.deleteMany({ _id: { $in: ids.split(',') } });
};

/**
 * Find a user by id
 * @param {*} id
 * @returns
 */
const findById = async (id) => {
  return User.findById(id);
};

/**
 * Paginate
 * @param {*} filter
 * @param {*} options
 * @returns
 */
const paginate = async (filter, options) => {
  return User.paginate(filter, options);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await findById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Get Permission Of Component
 */
const getPermissionOfComponent = async (userId, slug) => {
  const user = await User.findById(userId);
  const menu = await Menu.findOne({ slug });
  const roleDetail = await RoleDetail.findOne({ role: user.idRole, menu: menu.id });
  return roleDetail;
};

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
  getUserByEmail,
  updateUserById,

  getPermissionOfComponent,
};
