const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { roleService } = require('../services');

/**
 * find
 */
const find = catchAsync(async (req, res) => {
  const result = await roleService.find();
  res.send(result);
});

/**
 * create
 */
const create = catchAsync(async (req, res) => {
  const role = await roleService.create(req.body);
  res.status(httpStatus.CREATED).send(role);
});

/**
 * find By Id And Update
 */
const findByIdAndUpdate = catchAsync(async (req, res) => {
  const role = await roleService.findByIdAndUpdate(req.body.id, req.body);

  res.status(httpStatus.CREATED).send(role);
});

/**
 * find By Id And Delete
 */
const findByIdAndDelete = catchAsync(async (req, res) => {
  const role = await roleService.findByIdAndDelete(req.params.id);
  res.status(httpStatus.CREATED).send(role);
});

/**
 * findById
 */
const findById = catchAsync(async (req, res) => {
  const role = await roleService.findById(req.params.id);
  res.status(httpStatus.CREATED).send(role);
});

/**
 * paginate
 */
const paginate = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  const result = await roleService.paginate(filter, options);
  res.send(result);
});

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
