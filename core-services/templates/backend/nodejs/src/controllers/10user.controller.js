const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { userService } = require('../services');

/**
 * find
 */
const find = catchAsync(async (req, res) => {
  const result = await userService.find();
  res.send(result);
});

/**
 * create
 */
const create = catchAsync(async (req, res) => {
  req.body.password = 'password';
  const account = await userService.create(req.body);
  res.status(httpStatus.CREATED).send(account);
});

/**
 * find By Id And Update
 */
const findByIdAndUpdate = catchAsync(async (req, res) => {
  const account = await userService.findByIdAndUpdate(req.body.id, req.body);
  res.status(httpStatus.CREATED).send(account);
});

/**
 * find By Id And Delete
 */
const findByIdAndDelete = catchAsync(async (req, res) => {
  const account = await userService.findByIdAndDelete(req.params.id);
  res.status(httpStatus.CREATED).send(account);
});

/**
 * findById
 */
const findById = catchAsync(async (req, res) => {
  const account = await userService.findById(req.params.id);
  res.status(httpStatus.CREATED).send(account);
});

/**
 * paginate
 */
const paginate = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  const result = await userService.paginate(filter, options);
  res.send(result);
});

/**
 * Change password
 */
const changePassword = catchAsync(async (req, res) => {
  // Todo
  const { userId } = req.body;
  const oldPasssword = req.body.id;
  const { newPassword } = req.body;

  const result = await userService.changePassword(userId, oldPasssword, newPassword);
  res.send(result);
});

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
  changePassword,
};
