module.exports.authController = require('./00auth.controller');
module.exports.uploadController = require('./00upload.controller'); 
module.exports.userController = require('./10user.controller');
module.exports.menuController = require('./20menu.controller');
module.exports.roleController = require('./30role.controller');
module.exports.roleDetailController = require('./40role-detail.controller');
module.exports.lopControler = require('./50lop.controller');
module.exports.sinhvienControler = require('./60sinhvien.controller');
