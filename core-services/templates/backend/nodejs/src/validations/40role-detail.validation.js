const Joi = require('joi');
const { objectId } = require('./custom.validation');

const create = {
  body: Joi.object().keys({
    role: Joi.string().custom(objectId).required(),
    menu: Joi.string().custom(objectId).required(),
    status: Joi.string().required(),
  }),
};

const findByIdAndUpdate = {
  body: Joi.object().keys({
    id: Joi.string().custom(objectId).required(),
    role: Joi.string().custom(objectId).required(),
    menu: Joi.string().custom(objectId).required(),
    status: Joi.string().required(),
  }),
};

const findByIdAndDelete = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

const paginate = {
  params: Joi.object().keys({
    page: Joi.string().optional(),
    limit: Joi.string().optional(),
    role: Joi.string().optional(),
    status: Joi.string().optional(),
  }),
};

const findById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId).required(),
  }),
};

module.exports = {
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  paginate,
  findById,
};
