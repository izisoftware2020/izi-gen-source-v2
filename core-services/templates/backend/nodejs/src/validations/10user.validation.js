const Joi = require('joi');
const { objectId } = require('./custom.validation');

const create = {
  body: Joi.object().keys({
    idUser: Joi.string().optional(),
    idRole: Joi.string().optional(),
    fullname: Joi.string().optional(),
    username: Joi.string().optional(),
    password: Joi.string().optional(),
    sex: Joi.string().optional(),
    avatar: Joi.string().optional(),
    born: Joi.string().optional(),
    phone: Joi.string().optional(),
    email: Joi.string().optional(),
    address: Joi.string().optional(),
    status: Joi.string().optional(),
    deviceToken: Joi.string().optional(),
  }),
};

const findByIdAndUpdate = {
  body: Joi.object().keys({
    id: Joi.string().custom(objectId).required(),
    idUser: Joi.string().optional(),
    idRole: Joi.string().optional(),
    fullname: Joi.string().optional(),
    username: Joi.string().optional(),
    password: Joi.string().optional(),
    sex: Joi.string().optional(),
    avatar: Joi.string().optional(),
    born: Joi.string().optional(),
    phone: Joi.string().optional(),
    email: Joi.string().optional(),
    address: Joi.string().optional(),
    status: Joi.string().optional(),
    deviceToken: Joi.string().optional(),
  }),
};

const findByIdAndDelete = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

const paginate = {
  query: Joi.object().keys({
    page: Joi.string().optional(),
    limit: Joi.number().max(100).optional(),
    idUser: Joi.string().optional(),
    idRole: Joi.string().optional(),
    idOptionalRole: Joi.string().optional(),
  }),
};

const findById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId).required(),
  }),
};

module.exports = {
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  paginate,
  findById,
};
