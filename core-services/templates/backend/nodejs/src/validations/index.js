module.exports.authValidation = require('./auth.validation');
module.exports.userValidation = require('./10user.validation');
module.exports.menuValidation = require('./20menu.validation');
module.exports.roleValidation = require('./30role.validation');
module.exports.roleDetailValidation = require('./40role-detail.validation');
module.exports.lopValidation = require('./50lop.validation');

module.exports.sinhvienValidation = require('./60sinhvien.validation');
