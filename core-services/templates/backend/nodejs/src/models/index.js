module.exports.Token = require('./00token.model');
module.exports.User = require('./10user.models');
module.exports.Menu = require('./20menu.models');
module.exports.Role = require('./30role.models');
module.exports.RoleDetail = require('./40role-detail.models');
module.exports.Lop = require('./50lop.models');
module.exports.SinhVien = require('./60sinhvien.models');
