const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const { Schema } = mongoose;

const menuSchema = mongoose.Schema(
  {
    menu: {
      type: Schema.Types.ObjectId,
      ref: 'Menu',
    },
    isGroup: { type: String, trim: true },
    name: { type: String, trim: true },
    slug: { type: String, trim: true },
    icon: { type: String, trim: true },
    position: { type: Number, trim: true },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
menuSchema.plugin(toJSON);
menuSchema.plugin(paginate);

/**
 * @typedef Menu
 */
const Menu = mongoose.model('Menu', menuSchema);

module.exports = Menu;
