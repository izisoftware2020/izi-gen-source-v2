const express = require('express');
const { menuController } = require('../../controllers');
const validate = require('../../middlewares/validate');
const auth = require('../../middlewares/auth');
const { menuValidation } = require('../../validations');

const router = express.Router();

router
  .route('/')
  .get(auth(), menuController.find)
  .post(auth(), menuController.create)
  .put(auth(), menuController.findByIdAndUpdate)
  .delete(auth(), menuController.findByIdAndDelete);

router.route('/paginate').get(auth(), validate(menuValidation.paginate), menuController.paginate);

router
  .route('/:id')
  .get(auth(), validate(menuValidation.findById), menuController.findById)
  .delete(auth(), validate(menuValidation.findByIdAndDelete), menuController.findByIdAndDelete);

module.exports = router;
