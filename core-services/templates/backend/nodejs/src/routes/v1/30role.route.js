const express = require('express');
const { roleController } = require('../../controllers');
const { roleValidation } = require('../../validations');
const auth = require('../../middlewares/auth');

const router = express.Router();
const validate = require('../../middlewares/validate');

router
  .route('/')
  .get(auth(), roleController.find)
  .post(auth(), validate(roleValidation.create), roleController.create)
  .put(auth(), validate(roleValidation.findByIdAndUpdate), roleController.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate(roleController.paginate), roleController.paginate);

router
  .route('/:id')
  .get(auth(), validate(roleValidation.findById), roleController.findById)
  .delete(auth(), validate(roleValidation.findByIdAndDelete), roleController.findByIdAndDelete);

module.exports = router;
