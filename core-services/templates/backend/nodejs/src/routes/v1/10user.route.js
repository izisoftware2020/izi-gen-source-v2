const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const { userValidation } = require('../../validations');
const { userController } = require('../../controllers');

const router = express.Router();

router
  .route('/')
  .get(auth(), userController.find)
  .post(auth(), userController.create)
  .put(auth(), userController.findByIdAndUpdate);

router.route('/paginate').get(validate(userValidation.paginate), userController.paginate);

router
  .route('/:id')
  .get(auth(), validate(userValidation.findById), userController.findById)
  .delete(auth(), validate(userValidation.findByIdAndDelete), userController.findByIdAndDelete);

module.exports = router;
