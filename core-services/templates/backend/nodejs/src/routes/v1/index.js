const express = require('express');
const docsRoute = require('./docs.route');
const config = require('../../config/config');

const authRoute = require('./00auth.route');
const userRoute = require('./10user.route');
const menuRoute = require('./20menu.route');
const roleRoute = require('./30role.route');
const roleDetailRoute = require('./40role-detail.route');
const lopRoute = require('./50lop.route');
const sinhVienRoute = require('./60sinhvien.route');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/menus',
    route: menuRoute,
  },
  {
    path: '/roles',
    route: roleRoute,
  },
  {
    path: '/role-details',
    route: roleDetailRoute,
  },
  {
    path: '/lops',
    route: lopRoute,
  },
  {
    path: '/sinhviens',
    route: sinhVienRoute,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
