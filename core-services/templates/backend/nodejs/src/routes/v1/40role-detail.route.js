const express = require('express');
const { roleDetailController } = require('../../controllers');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const { roleDetailValidation } = require('../../validations');

const router = express.Router();

router
  .route('/')
  .get(auth(), roleDetailController.find)
  .post(auth(), validate(roleDetailValidation.create), roleDetailController.create)
  .put(auth(), validate(roleDetailValidation.findByIdAndUpdate), roleDetailController.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate(roleDetailValidation.paginate), roleDetailController.paginate);

router
  .route('/:id')
  .get(auth(), validate(roleDetailValidation.findById), roleDetailController.findById)
  .delete(auth(), validate(roleDetailValidation.findByIdAndDelete), roleDetailController.findByIdAndDelete);

module.exports = router;
