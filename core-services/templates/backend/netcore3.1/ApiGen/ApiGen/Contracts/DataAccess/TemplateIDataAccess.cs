﻿using ApiGen.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiGen.Data.DataAccess
{
    public interface ID{{phuong0}}0{{phuong1}}DataAccess
    {
        Task<IEnumerable<E{{phuong0}}0{{phuong1}}>> GetAllAsync();
        Task<long> CreateAsync(E{{phuong0}}0{{phuong1}} {{phuong2}});
        Task<bool> UpdateAsync(E{{phuong0}}0{{phuong1}} {{phuong2}});
        Task<bool> DeleteAsync(object id);
        Task<E{{phuong0}}0{{phuong1}}> GetByIdAsync(object id);
        Task<IEnumerable<E{{phuong0}}0{{phuong1}}>> GetPaginationAsync(UrlQueryParameters urlQueryParameters);
        Task<IEnumerable<object>> CountNumberItem(object id);
        Task<bool> ExecuteWithTransactionScope();
        Task<IEnumerable<object>> CustomJoin(); 
    }
}