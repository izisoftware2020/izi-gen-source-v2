﻿using ApiGen.Contracts;
using ApiGen.Data.DataAccess;
using ApiGen.DTO.Response;
using AutoMapper;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace ApiGen.API.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class ProxyApiController : ControllerBase
    {
        private readonly ILogger<ProxyApiController> _logger;
        private readonly IApiConnect _apiConnect;

        private C000AccountController _c000AccountController;
        private C100MenuController _c100MenuController;
        private C200RoleController _c200RoleController;
        private C300RoleDetailController _c300roleDetailController;
{{phuong0}}

        public ProxyApiController(IApiConnect apiConnect,
            ILogger<ProxyApiController> logger,
            ID000AccountDataAccess c000AccountDataAccess,
            ID100MenuDataAccess c100MenuDataAccess,
            ID200RoleDataAccess c200RoleDataAccess,
            ID300RoleDetailDataAccess roleDetailDataAccess,
{{phuong1}}
            IMapper mapper)
        {
            _apiConnect = apiConnect;
            _logger = logger;

            _c000AccountController = new C000AccountController(c000AccountDataAccess, mapper, logger);
            _c100MenuController = new C100MenuController(c100MenuDataAccess, mapper, logger);
            _c200RoleController = new C200RoleController(c200RoleDataAccess, mapper, logger);
            _c300roleDetailController = new C300RoleDetailController(roleDetailDataAccess, mapper, logger);
{{phuong2}}
        }

        [HttpPost("SelectAllByWhat")]
        [ProducesResponseType(typeof(ApiResponse), Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), Status201Created)]
        [ProducesResponseType(typeof(SinhVienQueryResponse), Status404NotFound)]
        [ProducesResponseType(typeof(ApiResponse), Status422UnprocessableEntity)]
        [ProducesResponseType(typeof(ApiResponse), Status429TooManyRequests)]
        public async Task<string> SelectAllByWhat([FromBody] dynamic paramRequest)
        {
            int what = paramRequest.what;
            switch ((what / 100) * 100)
            {
                // Redirect to Lop controller
                case 0:
                    return await _c000AccountController.execute(what, paramRequest);

                // Redirect to Menu controller
                case 100:
                    return await _c100MenuController.execute(what, paramRequest);

                // Redirect to Role controller
                case 200:
                    return await _c200RoleController.execute(what, paramRequest);

                // Redirect to RoleDetail controller
                case 300:
                    return await _c300roleDetailController.execute(what, paramRequest);

{{phuong3}}
            }
            return null;
        }
    }
}
