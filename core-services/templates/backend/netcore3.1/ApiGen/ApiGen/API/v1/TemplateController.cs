﻿using ApiGen.Data;
using ApiGen.Data.DataAccess;
using ApiGen.Data.Entity; 
using AutoMapper; 
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiGen.API.v1
{
    public class C{{phuong0}}0{{phuong1}}Controller
    {
        private readonly ILogger<dynamic> _logger;
        private readonly IMapper _mapper;
        private ID{{phuong0}}0{{phuong1}}DataAccess _d{{phuong0}}0{{phuong1}}DataAccess;

        public C{{phuong0}}0{{phuong1}}Controller(ID{{phuong0}}0{{phuong1}}DataAccess d{{phuong0}}0{{phuong1}}DataAccess, IMapper mapper, ILogger<dynamic> logger)
        {
            _d{{phuong0}}0{{phuong1}}DataAccess = d{{phuong0}}0{{phuong1}}DataAccess;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<string> execute(int what, dynamic param)
        {
            // Get all data {{phuong1}}
            if (what == {{phuong0}}0)
            {
                // Call get all data {{phuong1}}
                IEnumerable<E{{phuong0}}0{{phuong1}}> {{phuong2}} = await _d{{phuong0}}0{{phuong1}}DataAccess.GetAllAsync();
               
                return JsonConvert.SerializeObject({{phuong2}}, Formatting.Indented);
            }

            // Insert data to table {{phuong1}}
            if (what == {{phuong0}}1)
            {
                // Auto map request param data to Entity
                var {{phuong2}} = _mapper.Map<E{{phuong0}}0{{phuong1}}>(param);

                // Call insert all data to {{phuong1}} table
                var result = await _d{{phuong0}}0{{phuong1}}DataAccess.CreateAsync({{phuong2}});

                return JsonConvert.SerializeObject(result, Formatting.Indented);
            }

            // Update data table {{phuong1}}
            if (what == {{phuong0}}2)
            {
                // Auto map request param data to Entity
                var {{phuong2}} = _mapper.Map<E{{phuong0}}0{{phuong1}}>(param);
                {{phuong2}}.id = param.id.Value;

                // Call insert all data to {{phuong1}} table
                var result = await _d{{phuong0}}0{{phuong1}}DataAccess.UpdateAsync({{phuong2}});

                return JsonConvert.SerializeObject(result, Formatting.Indented);
            }

            // Delete data {{phuong1}} by Id
            if (what == {{phuong0}}3)
            {
                // Get id {{phuong1}} need delete
                var listid = param.listid.Value;

                // Call delete all data {{phuong1}} table by list id
                var result = await _d{{phuong0}}0{{phuong1}}DataAccess.DeleteAsync(listid);

                return JsonConvert.SerializeObject(result, Formatting.Indented);
            }

            // Find data {{phuong1}} by Id
            if (what == {{phuong0}}4)
            {
                // Get id {{phuong1}} need delete
                var id = param.id.Value;

                // Call find {{phuong1}} from table by id
                var result = await _d{{phuong0}}0{{phuong1}}DataAccess.GetByIdAsync(id);

                return JsonConvert.SerializeObject(result, Formatting.Indented);
            }

            // Get data {{phuong1}} Pagination 
            if (what == {{phuong0}}5)
            {
                // Auto map request param data to Entity
                UrlQueryParameters queryParam = _mapper.Map<UrlQueryParameters>(param);
                queryParam.limit = unchecked((int)param.limit.Value);
                queryParam.offset = unchecked((int)param.offset.Value);

                // Call get all data from {{phuong1}} table have pagination
                var result = await _d{{phuong0}}0{{phuong1}}DataAccess.GetPaginationAsync(queryParam);

                return JsonConvert.SerializeObject(result, Formatting.Indented);
            }

            // Check {{phuong1}} exists by Id
            if (what == {{phuong0}}6)
            {
                // Get id {{phuong1}} need check
                var Condition = "";
                if (param.Condition != null)
                {
                    Condition = param.Condition;
                }
                // Call check {{phuong1}} in table
                var result = await _d{{phuong0}}0{{phuong1}}DataAccess.CountNumberItem(Condition);

                return JsonConvert.SerializeObject(result, Formatting.Indented);
            } 

            return null;
        }
    }
}