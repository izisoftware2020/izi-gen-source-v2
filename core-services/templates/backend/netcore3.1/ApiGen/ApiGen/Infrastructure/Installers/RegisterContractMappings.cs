﻿using ApiGen.Contracts;
using ApiGen.Data.DataAccess;
using ApiGen.Data.DataManager;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ApiGen.Infrastructure.Installers
{
    internal class RegisterContractMappings : IServiceRegistration
    {
        public void RegisterAppServices(IServiceCollection services, IConfiguration config)
        {
            // Register Interface Mappings for Repositories
            services.AddTransient<ISinhVienManager, SinhVienManager>();

            services.AddTransient<ID000AccountDataAccess, D000AccountDataAccess>();
            services.AddTransient<ID100MenuDataAccess, D100MenuDataAccess>();
            services.AddTransient<ID200RoleDataAccess, D200RoleDataAccess>();
            services.AddTransient<ID300RoleDetailDataAccess, D300RoleDetailDataAccess>();
{{phuong0}}
        }
    }
}
