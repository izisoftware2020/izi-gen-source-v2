﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGen.Data.Entity
{
    public class E100Menu : EntityBase
    {
        public long id { get; set; }
        public long IdParentMenu { get; set; }
        public string IsGroup { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Icon { get; set; }
        public string Position { get; set; }
    }
}
