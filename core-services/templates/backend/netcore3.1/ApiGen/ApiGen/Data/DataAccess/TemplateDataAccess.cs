﻿using ApiGen.Data.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ApiGen.Data.DataAccess
{
    public class D{{phuong0}}0{{phuong1}}DataAccess : DbFactoryBase, ID{{phuong0}}0{{phuong1}}DataAccess
    {
        private readonly ILogger<dynamic> _logger;

        public D{{phuong0}}0{{phuong1}}DataAccess(IConfiguration config, ILogger<dynamic> logger) : base(config)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get All {{phuong1}} Async
        /// </summary>
        /// <returns>IEnumerable<{{phuong1}}></returns>
        public async Task<IEnumerable<E{{phuong0}}0{{phuong1}}>> GetAllAsync()
        {
            return await DbQueryAsync<E{{phuong0}}0{{phuong1}}>("SELECT {{phuong3}} FROM p{{phuong0}}0{{phuong1}}");
        }

        /// <summary>
        /// Create {{phuong1}} Async
        /// </summary>
        /// <param name="{{phuong2}}"></param>
        /// <returns></returns>
        public async Task<long> CreateAsync(E{{phuong0}}0{{phuong1}} {{phuong2}})
        {
            string sqlQuery = $@"INSERT INTO p{{phuong0}}0{{phuong1}}({{phuong4}})
                                 VALUES({{phuong5}});
                                 SELECT LAST_INSERT_ID();";

            return await DbQuerySingleAsync<long>(sqlQuery, {{phuong2}});
        }

        /// <summary>
        /// Update {{phuong1}} Async
        /// </summary>
        /// <param name="{{phuong2}}"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(E{{phuong0}}0{{phuong1}} {{phuong2}})
        {
            string sqlQuery = $@"UPDATE p{{phuong0}}0{{phuong1}} SET {{phuong6}}
                                 WHERE id=@id";

            return await DbExecuteAsync<bool>(sqlQuery, {{phuong2}});
        }

        /// <summary>
        /// Delete {{phuong1}} Async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(object listid)
        {
            string sqlQuery = $@"DELETE FROM p{{phuong0}}0{{phuong1}}
                                WHERE id IN(" + listid + ")";

            return await DbExecuteAsync<bool>(sqlQuery, new { });
        }

        /// <summary>
        /// Get By Id Async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<E{{phuong0}}0{{phuong1}}> GetByIdAsync(object id)
        {
            string sqlQuery = $@"SELECT * FROM p{{phuong0}}0{{phuong1}}
                                                    WHERE id = @id";
            return await DbQuerySingleAsync<E{{phuong0}}0{{phuong1}}>(sqlQuery, new { id });
        }

        /// <summary>
        /// Get {{phuong1}}s Pagination Async
        /// </summary>
        /// <param name="urlQueryParameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<E{{phuong0}}0{{phuong1}}>> GetPaginationAsync(UrlQueryParameters urlQueryParameters)
        {
            IEnumerable<E{{phuong0}}0{{phuong1}}> {{phuong2}}s;

            var query = @"SELECT *                                                                              
                            FROM (SELECT id FROM p{{phuong0}}0{{phuong1}} ORDER BY id LIMIT @offset, @limit) T1     
                            INNER JOIN p{{phuong0}}0{{phuong1}} T2 ON T1.id = T2.id                                               
                                " + urlQueryParameters.condition;

            var parameters = new
            {
                offset = urlQueryParameters.offset,
                limit = urlQueryParameters.limit
            };

            {{phuong2}}s = await DbQueryAsync<E{{phuong0}}0{{phuong1}}>(query, parameters);

            return {{phuong2}}s;
        } 

        /// <summary>
        /// Count {{phuong1}} item
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public async Task<IEnumerable<object>> CountNumberItem(object condition)
        {
            string sqlQuery = "SELECT COUNT(1) FROM p{{phuong0}}0{{phuong1}} " + condition;
            return await DbQueryAsync<object>(sqlQuery, new { condition });
        }

        /// <summary>
        /// Execute With Transaction Scope
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ExecuteWithTransactionScope()
        {

            using (var dbCon = new SqlConnection(DbConnectionString))
            {
                await dbCon.OpenAsync();
                var transaction = await dbCon.BeginTransactionAsync();

                try
                {
                    // Do stuff here Insert, Update or Delete
                    Task q1 = dbCon.ExecuteAsync("<Your SQL Query here>");
                    Task q2 = dbCon.ExecuteAsync("<Your SQL Query here>");
                    Task q3 = dbCon.ExecuteAsync("<Your SQL Query here>");

                    await Task.WhenAll(q1, q2, q3);

                    //Commit the Transaction when all query are executed successfully

                    await transaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    // Rollback the Transaction when any query fails
                    transaction.Rollback();
                    _logger.Log(LogLevel.Error, ex, "Error when trying to execute database operations within a scope.");

                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Get All {{phuong1}} Join {{phuong1}}
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<object>> CustomJoin()
        {
            return await DbQueryAsync<object>("SELECT p{{phuong0}}0{{phuong1}}.*, Person.* FROM p{{phuong0}}0{{phuong1}} INNER JOIN Person on p{{phuong0}}0{{phuong1}}.id = Person.Id");
        }
    }
}