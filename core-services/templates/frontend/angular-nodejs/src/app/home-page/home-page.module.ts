import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { AuthGuard } from '../@core/guards'; ;

@NgModule({
  declarations: [HomePageComponent, MenuComponent, FooterComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePageComponent,
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./content/content.module').then((m) => m.ContentModule),
              canActivate: [AuthGuard],
          },
        ],
      },
    ]),
  ],
  providers: [
  ],
})
export class HomePageModule { }
