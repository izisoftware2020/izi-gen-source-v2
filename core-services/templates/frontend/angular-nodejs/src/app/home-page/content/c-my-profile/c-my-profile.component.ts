import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
// import { ApiService } from '../../../common/commonService-service/commonService.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { MustMatch } from 'src/app/common/validations/must-match.validator'; 
import { Md5 } from 'ts-md5';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/@core/services/base/00auth.service';
import { UserService } from 'src/app/@core/services/base/10user.service';
import { CommonService } from 'src/app/@core/utils/common.service';
import { RoleService } from 'src/app/@core/services/base/30role.service';
import { User } from 'src/app/@core/models/10user.models';
@Component({
  selector: 'app-i-my-profile',
  templateUrl: './c-my-profile.component.html',
  styleUrls: ['./c-my-profile.component.scss'],
})
export class CMyProfileComponent implements OnInit, OnDestroy {

  //data binding
  staff = {
    id: '',
    username: '',
    phone: '',
    address: '',
    fullname: '',
    password: '',
    avatar: '',
    role: '',
    signUpDate: '',
  };

  staffInfoLogin: User = {
    id: 0,
    username: '',
    phone: '',
    address: '',
    fullname: '',
    password: '',
    avatar: '',
    role: '',

  };

  //value hide
  hide = true;
  hide1 = true;
  hide2 = true;

  //password
  passwordMdOld: any;
  passwordMdNew: any;

  //old password
  oldpassword: string;

  //new password
  newpassword: string;

  //repassword
  repassword: string;

  // validate
  form: FormGroup;

  formresetpass: FormGroup;

  /** for table */
  subscription: Subscription[] = [];

  // flag insert
  insertFlag: boolean = false;

  // roles data
  rolesData: any = [];

  // role name
  roleName: string = '';

  // binding uploads image or file
  @ViewChild("inputImageAvatar", { static: false }) inputImageAvatar: ElementRef;
  avatarImagePath: string = "";


  /**
   * constructor
   * @param commonService
   * @param formBuilder
   */
  constructor(
    private userService: UserService,
    public authService: AuthService,
    private commonService: CommonService,
    public roleService: RoleService,
    private formBuilder: FormBuilder) {

    this.form = this.formBuilder.group({
      fullname: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      username: [null],
      phone: [null],
      address: [null],
    });

    //form validation
    this.formresetpass = this.formBuilder.group({
      newpassword: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50),],],
      repassword: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50),],],
    },

    );
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    // on Close Sidebar Mobile
    this.onCloseSidebarMobile();

    // get staff value
    // this.staff = this.commonService.accountSubject.value;



    // get list role
    this.getListRole();

    //scroll top mobile
    window.scroll({ left: 0, top: 0, behavior: 'smooth' });
  }

  /**
  * ngOnDestroy
  */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * get Data Staff Login
   */
  getDataStaffLogin() {
    // get data staff login
    const idUser = localStorage.getItem('user');
    this.subscription.push(
      this.userService.find(idUser).subscribe((data: any) => {
        this.staffInfoLogin.id = data.id;
        this.staffInfoLogin.username = data.username;
        this.staffInfoLogin.fullname = data.fullname;
        this.staffInfoLogin.address = data.address;
        this.staffInfoLogin.phone = data.phone;
        this.staffInfoLogin.role = data.idRole;
        this.staffInfoLogin.password = data.password;
        if (data.avatar == '' || data.avatar == undefined) {
          this.staffInfoLogin.avatar = '../../../assets/images/logo-admin.png';
        } else {
          this.staffInfoLogin.avatar = data.avatar
        }

        // Get name role by id
        this.getNameRoleById(data.idRole);
      })
    );
  }

  /**
   * on Close Sidebar Mobile
   */
  onCloseSidebarMobile() {
    $(document).ready(function () {
      $(".row-offcanvas-right").removeClass("active");
    });
  }

  /**
   * Get list role
   */
  getListRole() {
    this.subscription.push(
      this.roleService.get().subscribe((data) => {
        this.rolesData = data;

        // getDataStaffLogin
        this.getDataStaffLogin();
      })
    )
  }

  /**
   * on update info staff
   */
  onBtnUpdateClick() {
    this.subscription.push(
      this.userService.update(this.staffInfoLogin).subscribe((data) => {
        if (data) {
          this.commonService.showSuccess('Bạn đã thay đổi thành công');
        }
      })
    )
  }

  /**
   * on update avatar
   */
  onAvatarUploadImageClick() {
    this.commonService.uploadImageCore(this.inputImageAvatar).subscribe((data) => {
      this.staffInfoLogin.avatar = data['data'];
      if (this.staffInfoLogin.avatar != null) {
        this.userService.update(this.staffInfoLogin).subscribe((data) => {
          this.commonService.showSuccess('Chỉnh sửa hình ảnh thành công');
        })
      }
    });
  }

  /**
   * on Change Password
   */
  onChangePassClick() {
    // return if error
    if (this.formresetpass.status != 'VALID') {
      this.commonService.showWarning('Vui lòng nhập các mục đánh dấu *');
      return;
    }
    if (this.repassword != this.newpassword) {
      this.commonService.showWarning('Mật khẩu cũ chưa đúng !');
    } else {
      this.staffInfoLogin.password = this.repassword;
      this.userService.update(this.staffInfoLogin).subscribe((data) => {
        if (data) {
          this.commonService.showSuccess('Thay đổi mật khẩu thành công');
        } else {
          this.commonService.showWarning('Thay đổi mật khẩu không thành công ');
        }
      })
    }
  }

  /**
   * get name role by id
   * @param id 
   * @returns 
   */
  getNameRoleById(id) {
    console.log(this.rolesData);
    this.roleName = this.rolesData.find(e => e.id == id)?.name;
    console.log(this.roleName);
  }

  /**
   * on button Cancel
   */
  onBtnCancelClick() {
    this.formresetpass.reset();
  }
}
