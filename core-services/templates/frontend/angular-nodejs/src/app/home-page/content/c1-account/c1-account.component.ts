import { Component, OnInit, Inject, ViewChild, OnDestroy } from '@angular/core';

import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, Observer, Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { AuthService } from 'src/app/@core/services/base/00auth.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/@core/services/base/10user.service';
import { RoleService } from 'src/app/@core/services/base/30role.service';
import { CommonService } from 'src/app/@core/utils/common.service';

@Component({
  selector: 'app-c1-account',
  templateUrl: './c1-account.component.html',
  styleUrls: ['./c1-account.component.scss'],
})
export class C1AccountComponent implements OnInit, OnDestroy {
  // subscription
  subscription: Subscription[] = [];

  /** for table */
  displayedColumns: string[] = [
    'select',
    'fullname',
    'email',
    'signUpDate',
    'edit',
  ];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  // update pagination
  pageIndex = 0;
  pageLength = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 20, 50, 100];
  // condition fillter
  conditonFilter: string = '';
  /**
   * getLengthOfPage
   */
  getLengthOfPage() {

  }

  /**
   * onPageChange
   */
  onPageChange(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;

    this.loadDataAccount();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1
      }`;
  }
  /** end for table */

  // data binding value
  input = {
    fullname: '',
    email: '',
    password: '',
    createdDate: '',
    idRole: '',
  };

  // is permission menu
  isPermissionMenu1: boolean = false;

  // acoount list
  accounts: any[];
  roleDatas: any[] = [];

  // role
  staff: any;



  /**
   * constructor
   * @param commonService
   * @param dialog
   */
  constructor(
    private commonService: CommonService,
    private router: Router,
    public dialog: MatDialog,
    public authService: AuthService,
    public userService: UserService,
    public roleService: RoleService
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    // load data account
    this.loadDataAccount();

    this.onLoadPermission();

    // on Close Sidebar Mobile
    this.onCloseSidebarMobile();

    // scroll top mobile
    window.scroll({ left: 0, top: 0, behavior: 'smooth' });

    // load data reference
    this.loadDataReference();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * load Data reference
   */
  loadDataReference() {
    // load list Role
    this.getListRole();
  }

  /**
   * get list Role
   */
  getListRole() {

  }

  /**
   * on Close Sidebar Mobile
   */
  onCloseSidebarMobile() {
    $(document).ready(function () {
      $('.row-offcanvas-right').removeClass('active');
    });
  }

  /**
   * onLoadPermission
   */
  onLoadPermission() {
    // this.staff = this.commonService.accountSubject.value;
    let userId = localStorage.getItem('user');
    let slug = this.router.url;

    this.subscription.push(
      this.authService.getPermissionOfComponent(userId, slug).subscribe((data) => {
        if (data.status == '2') {
          this.isPermissionMenu1 = true;
        }
      })
    );
  }

  /**
   * load data account
   */
  loadDataAccount() {
    // get Length Of Page
    this.getLengthOfPage();

    const param = {
      offset: Number(this.pageIndex * this.pageSize),
      limit: this.pageSize,
    };

    //select all data account
    this.subscription.push(
      this.userService.paginate(this.pageIndex + 1, this.pageSize, this.conditonFilter).subscribe((data: any) => {
        this.pageLength = data['totalResults']
        data = data.results;
        this.accounts = data;
        this.dataSource = new MatTableDataSource(data);

        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);
      })
    );
  }

  /**
   * on Delete Click
   */
  onBtnDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach((item) => {
      if (listId == '') {
        listId += item.id;
      } else {
        listId += ',' + item.id;
      }
    });
    if (listId != '') {
      this.userService.delete(listId).subscribe((item) => {
        this.loadDataAccount();
        //scroll top
        window.scroll({ left: 0, top: 0, behavior: 'smooth' });
        // show toast success
        this.commonService.showSuccess('Xóa thành công.');
      })
    }

  }

  /**
   * on insert data
   * @param event
   */
  onBtnInsertDataClick() {
    const dialogRef = this.dialog.open(C1AccountDialog, {
      width: '80%',
      height: '80%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 0 },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.loadDataAccount();
      }
    });
  }

  /**
   * on update data
   * @param event
   */
  onBtnUpdateDataClick(row) {
    const dialogRef = this.dialog.open(C1AccountDialog, {
      width: '80%',
      height: '80%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 1, input: row },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.loadDataAccount();
      }
    });
  }
}

/**
 * Component show thông tin để insert hoặc update
 */
@Component({
  selector: 'app-c1-account',
  templateUrl: 'c1-account-dialog.component.html',
  styleUrls: ['./c1-account.component.scss'],
})
export class C1AccountDialog implements OnInit, OnDestroy {
  observable: Observable<any>;
  observer: Observer<any>;
  type: number = 0;

  //subscription
  subscription: Subscription[] = [];

  // init input value
  input = {
    id: '',
    idRole: '',
    fullname: '',
    email: '',
    password: 'admin@123',
    phone: '',
    address: '',
    signUpDate: new Date(),

    avatar: '',
  };

  // sex value
  sexs: any[] = [
    { value: '1', viewValue: 'Nam' },
    { value: '0', viewValue: 'Nữ' },
  ];

  // list permission in array
  roleDatas: any[] = [];

  // form
  form: FormGroup;

  // detail user
  detailUser: any;

  // isUpdate
  isUpdate: boolean = false;

  date = new Date();

  /**
   * constructor
   * @param dialogRef
   * @param data
   * @param commonService
   * @param formBuilder
   */
  constructor(
    public dialogRef: MatDialogRef<C1AccountDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private roleService: RoleService,
    private userService: UserService
  ) {
    this.type = data.type;

    // if type == 1 -> update
    if (this.type == 1) {
      this.input = data.input;
      this.input.signUpDate = new Date(this.input.signUpDate);
    }

    // add validate for controls
    this.form = this.formBuilder.group({
      fullname: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ],
      ],
      email: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(255),
          Validators.email,
        ],
      ],
      signUpDate: [null, [Validators.required]],
      role: [null, [Validators.required]],
    });

    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    // load data reference
    this.loadDataReference();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * load Data reference
   */
  loadDataReference() {
    // load list Role
    this.getListRole();
  }

  /**
   * get list Role
   */
  getListRole() {
    this.subscription.push(
      this.roleService.get().subscribe((data: any) => {
        this.roleDatas = data;
        console.log(this.roleDatas);
      })
    );
  }

  /**
   * on Button Update Click
   */
  onBtnUpdateClick(): void {
    // disable button update
    this.isUpdate = true;

    // touch all control to show error
    this.form.markAllAsTouched();


    // check form pass all validate
    if (!this.form.invalid) {
      // if type = 0 insert else update
      if (this.type == 0) {
        console.log(this.input);
        this.userService
          .add(this.input)
          .subscribe((data) => {
            this.dialogRef.close(true);
            this.commonService.showSuccess('Xử lý thành công ');
          });
      } else {
        this.userService
          .update(this.input)
          .subscribe((data) => {
            this.dialogRef.close(true);
            this.commonService.showSuccess('Xử lý thành công ');
          });
      }

    }
  }
}