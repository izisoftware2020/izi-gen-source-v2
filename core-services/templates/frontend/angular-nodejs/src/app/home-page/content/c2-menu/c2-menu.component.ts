import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  OnDestroy,
  AfterViewInit,
  ElementRef,
} from '@angular/core';
// import { commonServiceService } from '../../../common/commonService-service/commonService.service';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, Observer, Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/@core/services/base/20menu.service';
import { AuthService } from 'src/app/@core/services/base/00auth.service';
import { CommonService } from 'src/app/@core/utils/common.service';

@Component({
  selector: 'app-c2-menu',
  templateUrl: './c2-menu.component.html',
  styleUrls: ['./c2-menu.component.scss'],
})
export class C2MenuComponent implements OnInit, AfterViewInit, OnDestroy {
  //subscription
  subscription: Subscription[] = [];

  /** for table */
  displayedColumns: string[] = [
    'select',
    'id',
    'idParentMenu',
    'isGroup',
    'name',
    'slug',
    'icon',
    'position',

    'edit',
  ];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  // update pagination
  pageIndex = 0;
  pageLength = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 20, 50, 100];

  /**
   * getLengthOfPage
   */
  getLengthOfPage() {


  }

  /**
   * onPageChange
   */
  onPageChange(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;

    this.onLoadDataGrid();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1
      }`;
  }
  // end for table;

  // data reference binding
  menuDatas: any[] = [];

  // role
  staff: any;

  // data permission
  isPermissionMenu1: boolean = false;

  // condition fillter
  conditonFilter: string = '';
  conditions: any[] = [];

  /**
   * constructor
   * @param commonService
   * @param dialog
   */
  constructor(
    // private commonService: commonServiceService,
    private router: Router,
    public dialog: MatDialog,
    public menuService: MenuService,
    public authService: AuthService,
    public commonService: CommonService

  ) {
    // load permission
    this.onLoadPermission();

  }

  /**
   * ngOnInit
   */
  ngOnInit() {

    // load data reference
    this.loadDataReference();

    // load data user
    this.onLoadDataGrid();

    this.onFormatIcon('<i class="fas fa-address-card"></i>');
  }

  /**
   * ng After View Init
   */
  ngAfterViewInit(): void {
    // scroll top screen
    window.scroll({ left: 0, top: 0, behavior: 'smooth' });
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on format icon from <i class="fas fa-address-card"></i> to fas fa-address-card
   * @param icon
   */
  onFormatIcon(icon) {
    // check icon have ""
    let startCopet = icon.search('"');
    if (startCopet != -1) {
      icon = icon.substring(startCopet + 1);
      let endCopet = icon.search('"');
      icon = icon.substring(0, endCopet);
    }
    return icon;
  }

  /**
   * onLoadPermission
   */
  onLoadPermission() {
    let userId = localStorage.getItem('user');
    let slug = this.router.url;

    this.subscription.push(
      this.authService.getPermissionOfComponent(userId, slug).subscribe((data) => {
        if (data.status == '2') {
          this.isPermissionMenu1 = true;
        }
      })
    );
  }

  /**
   * load Data reference
   */
  loadDataReference() {
    // load list menu
    this.getListMenu();
  }

  /**
   * get list menu
   */
  getListMenu() {
    this.subscription.push(
      this.menuService.get().subscribe((data: any) => {
        data = data.filter(e => e?.isGroup == 1);
        data.unshift({ id: '0', name: 'Menu chính' });

        this.menuDatas = data;
      })
    );
  }

  /**
   * get name display menu by id
   */
  getDisplayMenuById(id) {
    return this.menuDatas.filter((e) => e.id == id)[0]?.name;
  }

  /**
   * on menu Selection Change
   * @param event
   */
  onMenuSelectionChange(event) {
    const condition = { key: 'menu', value: event.value };

    // add new condition to list
    this.addNewConditionToList(condition);
  }

  /**
   * add New Condition To List
   * @param condition
   */
  addNewConditionToList(condition) {
    // check exists
    let flg = false;
    let i;

    // check condition exists
    for (i = 0; i < this.conditions.length; i++) {
      if ((this.conditions[i].key == condition.key)) {
        flg = true;
        break;
      }
    }

    // remove old key
    if (flg) {
      this.conditions.splice(i, 1);
    }

    // insert new seach condition if !=0
    if (condition.value != '0') {
      this.conditions.splice(0, 0, condition);
    }

    // render new condition filter
    this.createConditionFilter();

    // load grid with new condition
    this.onLoadDataGrid();
  }

  /**
   * create Condition Filter
   */
  createConditionFilter() {
    this.conditonFilter = '';
    this.conditions.forEach((item) => {
      if (this.conditonFilter == '') {
        this.conditonFilter = item.key + "=" + item.value + "";
      } else {
        this.conditonFilter += '&' + item.key + "=" + item.value + "";
      }
    });
    if (this.conditonFilter != '') {
      this.conditonFilter = '&' + this.conditonFilter;
    }
  }

  /**
   * get data grid
   */
  onLoadDataGrid() {
    this.subscription.push(
      this.menuService.paginate(this.pageIndex + 1, 100, this.conditonFilter).subscribe((data: any) => {
        // get Length Of Page
        this.pageLength = data['totalResults']

        let menusData: any = [];

        data = data.results;

        // Sort data by position
        data.sort(function (a, b) {
          return a.position - b.position
        });

        // Add parent nodes
        data.forEach((item) => {
          if (item.isGroup == 1) {
            menusData.push(item);

            let menusChildrents = [];

            // Add childrent data
            data.forEach((childrent) => {
              // Check is childrent 
              if (childrent.menu == item.id) {
                menusChildrents.push(childrent);
              }
            })

            // Sort menu childrent
            menusChildrents.sort(function (a, b) {
              return a.position - b.position
            });

            // Push menu childrent into parent
            menusData.push(...menusChildrents);
          }
        })

        // Check if filter by role
        if (this.conditonFilter.length > 0) {
          menusData = data;
        }

        this.dataSource = new MatTableDataSource(menusData);
        this.dataSource.sort = this.sort;
      })
    );
  }

  /**
   * get data grid
   */
  onLoadDataGridFilter() {
    // // get Length Of Page
    // this.getLengthOfPage();

    // const param = {
    //   condition: this.conditonFilter,
    //   offset: Number(this.pageIndex * this.pageSize),
    //   limit: this.pageSize,
    // };

    // this.subscription.push(
    //   this.commonService.excuteAllByWhat(param, '105').subscribe((data) => {
    //     // process data
    //     data = data['data'];

    //     if (data) {
    //       // set data for table
    //       this.dataSource = new MatTableDataSource(data);
    //     } else {
    //       this.dataSource = new MatTableDataSource([]);
    //     }

    //     this.dataSource.sort = this.sort;
    //     this.selection = new SelectionModel<any>(true, []);
    //   })
    // );
  }

  /**
   * on Delete Click
   */
  onBtnDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach((item) => {
      if (listId == '') {
        listId += item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    if (listId != '') {
      this.menuService.delete(listId).subscribe((item) => {
        this.onLoadDataGrid();
        //scroll top
        window.scroll({ left: 0, top: 0, behavior: 'smooth' });
        // show toast success
        this.commonService.showSuccess('Xóa thành công.');
      })
    }
  }

  /**
   * on insert data
   * @param event
   */
  onBtnInsertDataClick() {
    const dialogRef = this.dialog.open(C2MenuDialog, {
      width: '80%',
      height: '80%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 0, id: 0 },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataGrid();
      }
    });
  }

  /**
   * on update data
   * @param event
   */
  onBtnUpdateDataClick(row) {
    const dialogRef = this.dialog.open(C2MenuDialog, {
      width: '80%',
      height: '80%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 1, input: row },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataGrid();
      }
    });
  }
}

/**
 * Component show thông tin để insert hoặc update
 */
@Component({
  selector: 'c2-menu-dialog',
  templateUrl: 'c2-menu-dialog.html',
  styleUrls: ['./c2-menu.component.scss'],
})
export class C2MenuDialog implements OnInit, OnDestroy {
  observable: Observable<any>;
  observer: Observer<any>;
  type: number;

  //subscription
  subscription: Subscription[] = [];

  // init input value
  input: any = {
    menu: '0',
    isGroup: '0',
    name: '',
    slug: '',
    icon: '',
    position: '',
  };

  //form
  form: FormGroup;

  // sex
  sexs: any[] = [
    { value: '1', viewValue: 'Nam' },
    { value: '0', viewValue: 'Nữ' },
  ];

  // data reference binding
  menuDatas: any[] = [];

  // binding uploads image or file

  // isUpdate
  isUpdate: boolean = false;

  /**
   * constructor
   * @param dialogRef
   * @param data
   * @param commonService
   * @param formBuilder
   */
  constructor(
    public dialogRef: MatDialogRef<C2MenuDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    // private commonService: commonServiceService,
    private formBuilder: FormBuilder,
    public menuService: MenuService,
    public commonService: CommonService
  ) {
    this.type = data.type;

    // nếu là update
    if (this.type == 1) {
      this.input = data.input;
    }

    // add validate for controls
    this.form = this.formBuilder.group({
      menu: [null, [Validators.required]],
      isGroup: [null, [Validators.required]],
      name: [null, [Validators.required]],
      slug: [null, []],
      icon: [null, [Validators.required]],
      position: [null, [Validators.required]],
    });

    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**
   * onInit
   */
  ngOnInit() {
    // get data reference
    this.loadDataReference();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * load Data reference
   */
  loadDataReference() {
    // load list menu
    this.getListMenu();
  }

  /**
   * get list menu
   */
  getListMenu() {
    this.subscription.push(
      this.menuService.get().subscribe((data: any) => {

        this.menuDatas = data.filter(e => e.isGroup == '1');
        this.menuDatas.unshift({ id: '0', name: 'Menu chính' });
      })
    );
  }

  /**
   * on Btn Submit Click
   */
  onBtnSubmitClick(): void {
    // disable button update
    this.isUpdate = true;

    // touch all control to show error
    this.form.markAllAsTouched();

    // Delete property object
    if (this.input.menu == '0') {
      delete this.input.menu;
    }

    // check form pass all validate
    if (!this.form.invalid) {
      // if type = 0 insert else update
      if (this.type == 0) {
        this.subscription.push(
          this.menuService
            .add(this.input)
            .subscribe((data) => {
              this.dialogRef.close(true);
              this.commonService.showSuccess('Xử lý thành công ');
            })
        );
      } else {
        this.subscription.push(
          this.menuService
            .update(this.input)
            .subscribe((data) => {
              this.dialogRef.close(true);
              this.commonService.showSuccess('Cập nhật thành công');
            })
        );
      }

    }
  }
}
