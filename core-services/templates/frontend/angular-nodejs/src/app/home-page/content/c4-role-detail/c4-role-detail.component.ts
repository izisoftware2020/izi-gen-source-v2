import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  OnDestroy,
  AfterViewInit,
  ElementRef,
} from '@angular/core';

import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, Observer, Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/@core/services/base/00auth.service';
import { RoleDetailService } from 'src/app/@core/services/base/40role-details.service';
import { RoleService } from 'src/app/@core/services/base/30role.service';
import { MenuService } from 'src/app/@core/services/base/20menu.service';
import { Role } from 'src/app/@core/models/30role.models';
import { Menu } from 'src/app/@core/models/20menu.models';
import { CommonService } from 'src/app/@core/utils/common.service';

@Component({
  selector: 'app-c4-role-detail',
  templateUrl: './c4-role-detail.component.html',
  styleUrls: ['./c4-role-detail.component.scss'],
})
export class C4RoleDetailComponent implements OnInit, AfterViewInit, OnDestroy {
  //subscription
  subscription: Subscription[] = [];

  /** for table */
  displayedColumns: string[] = [
    'select',
    'id',
    'idParentMenu',
    'isGroup',
    'name',
    'status',

    'edit',
  ];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  // update pagination
  pageIndex = 0;
  pageLength = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 20, 50, 100];

  /**
   * getLengthOfPage
   */
  getLengthOfPage() {
    // const param = { condition: this.conditonFilter };
    // this.subscription.push(
    //   this.api.excuteAllByWhat(param, '106').subscribe((data) => {
    //     data = data['data'];
    //     if (data.length > 0) {
    //       this.pageLength = data[0]['COUNT(1)'];
    //     } else {
    //       this.pageLength = 0;
    //     }
    //   })
    // );

  }

  /**
   * onPageChange
   */
  onPageChange(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;

    this.onLoadDataGrid();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1
      }`;
  }
  // end for table;

  // data reference binding
  roleDatas: any[] = [];
  menuDatas: any[] = [];
  roleDetaisData: any = [];

  // role
  staff: any;

  // data permission
  isPermissionMenu1: boolean = false;

  // condition fillter
  conditonFilter: string = '';
  conditions: any[] = [];

  // input param
  paramIdRole: string = '';

  /**
   * constructor
   * @param api
   * @param dialog
   */
  constructor(
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    public authService: AuthService,
    public roleDetaiService: RoleDetailService,
    public roleService: RoleService,
    public menuService: MenuService,
    public commonService: CommonService,
  ) {

  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    // load data reference
    this.loadDataReference();

    // get parmeter
    this.route.params.subscribe((params) => {
      this.paramIdRole = params['id'];

      // load data user
      setTimeout(() => {
        this.onLoadDataGrid();
      }, 500);

    });
  }

  /**
   * ng After View Init
   */
  ngAfterViewInit(): void {
    // scroll top screen
    window.scroll({ left: 0, top: 0, behavior: 'smooth' });
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }



  /**
   * load Data reference
   */
  loadDataReference() {
    // load list Role
    this.getListRole();

    // load list menu
    this.getListMenu();

    // load list roles details
    this.getListRoleDetails();
  }

  /**
   * get list Role
   */
  getListRole() {
    this.subscription.push(
      this.roleService.get().subscribe((data: any) => {
        this.roleDatas = data;
      })
    );
  }

  /**
   * get list menu
   */
  getListMenu() {
    this.subscription.push(
      this.menuService.get().subscribe((data: any) => {
        data.unshift({ id: '0', name: 'Menu chính' });
        this.menuDatas = data;
      })
    );
  }

  /**
   * getListRoleDetails
   */
  getListRoleDetails() {
    this.subscription.push(
      this.roleDetaiService.get().subscribe((data: any) => {
        this.roleDetaisData = data;
        console.log(this.roleDetaisData);
      })
    );
  }

  /**
   * findRoleDetailByRoleIdAndMenuId
   * @param menuId 
   * @param roleId 
   * @returns 
   */
  findRoleDetailByRoleIdAndMenuId(menuId, roleId) {
    let data = this.roleDetaisData;
    return data.find(e => e?.menu?.id == menuId && e?.role?.id == roleId);
  }

  /**
   * get name display role by id
   */
  getDisplayRoleById(id) {
    return this.roleDatas.filter((e) => e.id == id)[0]?.name;
  }

  /**
   * get name display menu by id
   */
  getDisplayMenuById(id) {
    return this.menuDatas.filter((e) => e.id == id)[0]?.name;
  }

  /**
   * get name display status by id
   */
  getDisplayStatusById(id) {
    if (id == 0) return 'Không có quyền';
    if (id == 1) return 'Chỉ xem';
    if (id == 2) return 'Quản trị';
  }

  /**
   * on role Selection Change
   * @param id
   */
  onRoleSelectionChange(id) {
    const condition = { key: 'IdRole', value: id };

    // add new condition to list
    this.addNewConditionToList(condition);
  }

  /**
   * add New Condition To List
   * @param condition
   */
  addNewConditionToList(condition) {
    // check exists
    let flg = false;
    let i;

    // check condition exists
    for (i = 0; i < this.conditions.length; i++) {
      if ((this.conditions[i].key == condition.key)) {
        flg = true;
        break;
      }
    }

    // remove old key
    if (flg) {
      this.conditions.splice(i, 1);
    }

    // insert new seach condition if !=0
    if (condition.value != '0') {
      this.conditions.splice(0, 0, condition);
    }

    // render new condition filter
    this.createConditionFilter();

    // load grid with new condition
    this.onLoadDataGrid();
  }

  /**
   * create Condition Filter
   */
  createConditionFilter() {
    this.conditonFilter = '';
    this.conditions.forEach((item) => {
      if (this.conditonFilter == '') {
        this.conditonFilter = item.key + "=" + item.value + "";
      } else {
        this.conditonFilter += '&' + item.key + "=" + item.value + "";
      }
    });
    if (this.conditonFilter != '') {
      this.conditonFilter = '&' + this.conditonFilter;
    }
  }

  /**
   * get data grid
   */
  onLoadDataGrid() {
    let roleId = this.paramIdRole;

    this.subscription.push(
      this.menuService.paginate(this.pageIndex + 1, 100, '').subscribe((data: any) => {
        this.pageLength = data['totalResults'];

        data = data.results;
        let menusData: any = [];

        // Add parent nodes
        data.forEach((item) => {
          let menu = item;
          menu.roledDetailId = this.findRoleDetailByRoleIdAndMenuId(menu.id, roleId)?.id;
          menu.status = this.findRoleDetailByRoleIdAndMenuId(menu.id, roleId)?.status;
          if (menu.isGroup == 1) {
            menusData.push(menu);
            let menusChildrents = [];

            // Add childrent data
            data.forEach((childrent) => {
              // Check is childrent 
              if (childrent.menu == item.id) {
                childrent.roledDetailId = this.findRoleDetailByRoleIdAndMenuId(childrent.id, roleId)?.id;
                childrent.status = this.findRoleDetailByRoleIdAndMenuId(childrent.id, roleId)?.status;
                menusChildrents.push(childrent);
              }
            })

            // Sort menu childrent
            menusChildrents.sort(function (a, b) {
              return a.position - b.position
            });

            // Push menu childrent into parent
            menusData.push(...menusChildrents);
          }
        })

        this.dataSource = new MatTableDataSource(menusData);
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);
      })

    );
  }

  /**
   * on Btn Role Click
   */
  onBtnRoleClick() {
    this.router.navigate(['/manager/c3-role']);
  }


  /**
   * on Role Change
   * @param row
   */
  onRoleChange(row) {
    const param = {
      id: row.roledDetailId,
      role: this.paramIdRole,
      menu: row.id,
      status: row.status,
    };
    if (param.id != null) {
      this.roleDetaiService.update(param).subscribe((item) => {
        this.commonService.showSuccess('Cập nhật quyền thành công');

        // Get role details and load data
        this.getRoleDetailsAndLoadData();
      })
    } else {
      this.roleDetaiService.add(param).subscribe((item) => {
        this.commonService.showSuccess('Cập nhật quyền thành công');

        // Get role details and load data
        this.getRoleDetailsAndLoadData();
      })
    }
  }

  /**
   * Get Role Details And Load Data
   */
  getRoleDetailsAndLoadData() {
    this.getListRoleDetails();
    setTimeout(() => {
      this.onLoadDataGrid();
    }, 500);
  }

  /**
   * on insert data
   * @param event
   */
  onBtnInsertDataClick() {
    const dialogRef = this.dialog.open(C4RoleDetailDialog, {
      width: '80%',
      height: '80%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 0, id: 0 },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataGrid();
      }
    });
  }

  /**
   * on update data
   * @param event
   */
  onBtnUpdateDataClick(row) {
    const dialogRef = this.dialog.open(C4RoleDetailDialog, {
      width: '80%',
      height: '80%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 1, input: row },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataGrid();
      }
    });
  }
}

/**
 * Component show thông tin để insert hoặc update
 */
@Component({
  selector: 'c4-role-detail-dialog',
  templateUrl: 'c4-role-detail-dialog.html',
  styleUrls: ['./c4-role-detail.component.scss'],
})
export class C4RoleDetailDialog implements OnInit, OnDestroy {
  observable: Observable<any>;
  observer: Observer<any>;
  type: number;

  //subscription
  subscription: Subscription[] = [];

  // init input value
  input: any = {
    IdRole: '',
    IdMenu: '',
    status: '',
  };

  //form
  form: FormGroup;

  // status
  status: any[] = [
    { key: '0', value: 'Không có quyền' },
    { key: '1', value: 'Chỉ xem' },
    { key: '2', value: 'Quản trị' },
  ];

  // data reference binding
  roleDatas: any[] = [];
  menuDatas: any[] = [];

  // binding uploads image or file

  // isUpdate
  isUpdate: boolean = false;

  /**
   * constructor
   * @param dialogRef
   * @param data
   * @param api
   * @param formBuilder
   */
  constructor(
    public dialogRef: MatDialogRef<C4RoleDetailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    // private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.type = data.type;

    // nếu là update
    if (this.type == 1) {
      this.input = data.input;
    }

    // add validate for controls
    this.form = this.formBuilder.group({
      IdRole: [null, [Validators.required]],
      IdMenu: [null, [Validators.required]],
      status: [null, [Validators.required]],
    });

    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**
   * onInit
   */
  ngOnInit() {
    // get data reference
    this.loadDataReference();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * load Data reference
   */
  loadDataReference() {
    // load list Role
    this.getListRole();

    // load list menu
    this.getListMenu();
  }

  /**
   * get list Role
   */
  getListRole() {
    // this.subscription.push(
    //   this.api.excuteAllByWhat({}, '200').subscribe((data) => {
    //     data = data['data'];
    //     this.roleDatas = data;
    //   })
    // );
  }

  /**
   * get list menu
   */
  getListMenu() {
    // this.subscription.push(
    //   this.api.excuteAllByWhat({}, '100').subscribe((data) => {
    //     data = data['data'];
    //     this.menuDatas = data;
    //   })
    // );
  }

  /**
   * on Btn Submit Click
   */
  onBtnSubmitClick(): void {
    // // disable button update
    // this.isUpdate = true;

    // // touch all control to show error
    // this.form.markAllAsTouched();
    // this.input.Born = this.api.formatDate(new Date(this.input.Born));

    // // check form pass all validate
    // if (!this.form.invalid) {
    //   // if type = 0 insert else update
    //   this.subscription.push(
    //     this.api
    //       .excuteAllByWhat(this.input, '' + Number(301 + this.type) + '')
    //       .subscribe((data) => {
    //         this.dialogRef.close(true);
    //         this.api.showSuccess('Xử lý thành công ');
    //       })
    //   );
    // }
  }
}
