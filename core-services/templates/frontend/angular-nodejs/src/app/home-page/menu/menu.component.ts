import { Component, OnInit } from '@angular/core';
// import { LoginCookie } from '../../common/core/login-cookie'; 
// import $ from 'jquery';
import * as $ from 'jquery';
import { Subscription } from 'rxjs';
import { MenuService } from 'src/app/@core/services/base/20menu.service';
import { RoleDetailService } from 'src/app/@core/services/base/40role-details.service';
import { AuthService } from 'src/app/@core/services/base/00auth.service';
import { UserService } from 'src/app/@core/services/base/10user.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  //menu flag
  menuFlag: boolean = false;
  menuMobileFlag: boolean = false;
  menuMobileStaff: boolean = false;
  settingButton: boolean = false;
  searchFlag: boolean = false;
  isMobile: boolean = false;
  idStaff: Number;

  // flag for menu
  navLeft: boolean = false;
  navLeftMobile: boolean = false;
  settingFlag: boolean = false;

  //data permission menu
  isPermissionMenu1: boolean = false;
  isPermissionMenu2: boolean = false;
  isPermissionMenu3: boolean = false;

  // show dropdown (logout/myprofile/changepasswork)
  isShow: boolean = false;
  info: any = null;

  //pubic is visited
  public isVisited = false;
  public checkVisited() {
    // reverse the value of property
    this.isVisited = !this.isVisited;
  }

  //data binding
  staff: any;

  staffInfoLogin = {
    id: '',
    menu: '',
    username: '',
    name: '',
    passwork: '',
    img: '',
    created_date: '',
  };

  subscription: Subscription[] = [];

  dataMenus: any[] = [];

  /**
   * constructor
   * @param login
   * @param api
   */
  constructor(
    public menuService: MenuService,
    public roleDetailService: RoleDetailService,
    public authService: AuthService,
    public userService: UserService,
  ) {
    // get staff value
    // this.staff = this.api.getAccountValue;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    // this.staff = this.api.accountSubject.value;
    // this.idStaff = this.api.accountSubject.value.id;
    this.isMobile = this.isMobileDevice();
    // load data menu
    this.onLoadDataMenu();

    // this.onLoadPermission();

    // toggle Sidebar Click
    this.toggleSidebarClick();

    // toggle Sidebar Mobile Click
    this.toggleSidebarMobileClick();

    // getDataStaffLogin
    this.getDataStaffLogin();
  }

  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() {
    // process click of menu response
    $(document).ready(function ($) {
      'use strict';
      //Open submenu on hover in compact sidebar mode and horizontal menu mode
      $(document).on(
        'mouseenter mouseleave',
        '.sidebar .nav-item',
        function (ev) {
          var body = $('#body');
          var sidebarIconOnly = body.hasClass('sidebar-icon-only');
          var horizontalMenu = body.hasClass('horizontal-menu');
          var sidebarFixed = body.hasClass('sidebar-fixed');
          if (!('ontouchstart' in document.documentElement)) {
            if (sidebarIconOnly || horizontalMenu) {
              if (sidebarFixed) {
                if (ev.type === 'mouseenter') {
                  body.removeClass('sidebar-icon-only');
                }
              } else {
                var $menuItem = $(this);
                if (ev.type === 'mouseenter') {
                  $menuItem.addClass('hover-open');
                } else {
                  $menuItem.removeClass('hover-open');
                }
              }
            }
          }
        }
      );
    });
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }

  /**
   * on format icon from <i class="fas fa-address-card"></i> to fas fa-address-card
   * @param icon
   */
  onFormatIcon(icon) {
    // check icon have ""
    let startCopet = icon.search('"');
    if (startCopet != -1) {
      icon = icon.substring(startCopet + 1);
      let endCopet = icon.search('"');
      icon = icon.substring(0, endCopet);
    }
    return icon;
  }

  /**
   * get Data Staff Login
   */
  getDataStaffLogin() {
    // get data staff login
    const idUser = localStorage.getItem('user');
    this.subscription.push(
      this.userService.find(idUser).subscribe((data: any) => {
        this.staffInfoLogin.id = data.id;
        this.staffInfoLogin.username = data.username;
        this.staffInfoLogin.name = data.fullname;
        if (data.avatar == '' || data.avatar == undefined) {
          this.staffInfoLogin.img = '../../../assets/images/logo-admin.png';
        } else {
          this.staffInfoLogin.img = data.avatar
        }
      })
    );
  }

  /**
   * get data grid
   */
  onLoadDataMenu() {
    let roleId = localStorage.getItem('role');
    let filter = '&role=' + roleId;
    this.subscription.push(
      this.roleDetailService.paginate(1, 1000, filter).subscribe((data: any) => {
        data = data.results;
        // Sort data by position
        let menusData = [];

        // add parent nodes
        data.forEach((item) => {
          let menu = item.menu;
          if (menu != null) {
            menu.nodes = [];
            menu.status = item.status == null ? 0 : item.status;
            if (menu.isGroup == 1) {
              menusData.push(menu);
            }
          }
        })

        // add menu childrent into parent
        menusData.forEach((item) => {
          let menuChildrents = [];
          data.forEach((item1) => {
            let menuChildrent = item1.menu;

            if (menuChildrent != null) {
              if (item.id == menuChildrent.menu) {
                menuChildrent.status == null ? 0 : item1.status;
                menuChildrents.push(menuChildrent);
              }
            }
          })

          // Sort data by position
          menuChildrents.sort(function (a, b) {
            return a.position - b.position
          });

          item.nodes.push(...menuChildrents);
        })

        menusData.sort(function (a, b) {
          return a.position - b.position
        });

        this.dataMenus = menusData;
      })
    );
  }

  /**
   * onLoadPermission
   */
  onLoadPermission() {
    // load permission
    if (this.staff.role.search('a:1') < 0) {
      this.isPermissionMenu1 = true;
    }

    if (this.staff.role.search('b:1') < 0) {
      this.isPermissionMenu2 = true;
    }

    if (this.staff.role.search('c:1') < 0) {
      this.isPermissionMenu3 = true;
    }
  }

  /**
   * is Mobile Device
   */
  isMobileDevice() {
    return (
      typeof window.orientation !== 'undefined' ||
      navigator.userAgent.indexOf('IEMobile') !== -1
    );
  }

  /**
   * onBtnLogOutStaffClick
   */
  onBtnLogOutStaffClick() {
    // this.api.logoutAccount();
    this.authService.logout('');
  }

  /**
   * logOut Staff
   */
  toggleSidebarClick() {
    $(document).ready(function () {
      $('.icon-toggle-sidebar').click(function () {
        $('html body').toggleClass('sidebar-icon-only');
      });
    });
  }

  /**
   * logOut Staff
   */
  toggleSidebarMobileClick() {
    $(document).ready(function () {
      $('.navbar-toggler-right').click(function () {
        $('.row-offcanvas-right').toggleClass('active');
      });
    });
  }

  /**
   * onToggleButtonDesktopClick
   */
  onToggleButtonDesktopClick() {
    this.navLeft = !this.navLeft;
  }

  /**
   * onToggleButtonMobileClick
   */
  onToggleButtonMobileClick() {
    this.navLeftMobile = !this.navLeftMobile;
  }

  onSettingButtonClick() {
    this.settingFlag = !this.settingFlag;
  }
}
