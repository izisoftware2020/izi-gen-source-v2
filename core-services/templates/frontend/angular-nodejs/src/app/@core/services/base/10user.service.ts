import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { User } from '../../models/10user.models';
import { CONSTAINT_UTILS } from '../../utils/constaint.utils';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  // Define API
  apiURL = CONSTAINT_UTILS.api.frontend_url;

  constructor(private http: HttpClient) { }

  /*========================================
    Begin CRUD Methods for consuming RESTful API
  =========================================*/

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  /**
   * HttpClient API get() method => Fetch users list
   * @returns 
   */
  get(): Observable<User> {
    return this.http.get<User>(this.apiURL + '/users')
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API post() method => Create user
   * @param user 
   * @returns 
   */
  add(user: any): Observable<User> {
    return this.http.post<User>(this.apiURL + '/users', JSON.stringify(user), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API put() method => Update user
   * @param id 
   * @param user 
   * @returns 
   */
  update(user: any): Observable<User> {
    return this.http.put<User>(this.apiURL + '/users/', JSON.stringify(user), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API delete() method => Delete user
   * @param id 
   * @returns 
   */
  delete(id: any) {
    console.log(id);
    return this.http.delete<User>(this.apiURL + '/users/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
  * HttpClient API get() method => Fetch user
  * @param id 
  * @returns 
  */
  find(id: any): Observable<User> {
    return this.http.get<User>(this.apiURL + '/users/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
  * HttpClient API get() method => paginate user with start page = 1 and limit
  * @param id 
  * @returns 
  */
  paginate(page: Number, limit: Number, filter: any): Observable<User> {
    return this.http.get<User>(this.apiURL + '/users/paginate?page=' + page + '&limit=' + limit, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /*========================================
    Begin Custom Methods for RESTful API
  =========================================*/
  /**
   * HttpClient API get() method => Fetch users list
   * @returns 
   */
  findByUsername(username: string): Observable<User> {
    return this.http.get<User>(this.apiURL + '/users/username/' + username)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * Get count username
   * @returns 
   */
  getCountUsername() {
    return this.http.get<User>(this.apiURL + '/users/count')
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * Find by date start to date end
   */
  findByDateStartToDateEnd(dateStart, dateEnd) {
    let url = this.apiURL + '/users/dateStart-dateEnd?dateStart=' + dateStart + '&dateEnd=' + dateEnd;
    return this.http.get<any>(url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * Error handling 
   * @param error 
   * @returns 
   */
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    // console.log(errorMessage);
    return throwError(errorMessage);
  }

}