import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { RoleDetail } from '../../models/40roleDetail.models';
import { CONSTAINT_UTILS } from '../../utils/constaint.utils';

@Injectable({
    providedIn: 'root'
})

export class RoleDetailService {

    // Define API
    apiURL = CONSTAINT_UTILS.api.frontend_url;

    constructor(private http: HttpClient) { }

    /*========================================
      Begin CRUD Methods for consuming RESTful API
    =========================================*/

    // Http Options
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }

    /**
     * HttpClient API get() method => Fetch role-details list
     * @returns 
     */
    get(): Observable<RoleDetail> {
        return this.http.get<RoleDetail>(this.apiURL + '/role-details')
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    /**
     * HttpClient API post() method => Create RoleDetail
     * @param RoleDetail 
     * @returns 
     */
    add(RoleDetail: RoleDetail): Observable<RoleDetail> {
        return this.http.post<RoleDetail>(this.apiURL + '/role-details', JSON.stringify(RoleDetail), this.httpOptions)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    /**
     * HttpClient API put() method => Update RoleDetail
     * @param id 
     * @param RoleDetail 
     * @returns 
     */
    update(RoleDetail: RoleDetail): Observable<RoleDetail> {
        return this.http.put<RoleDetail>(this.apiURL + '/role-details/', JSON.stringify(RoleDetail), this.httpOptions)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    /**
     * HttpClient API delete() method => Delete RoleDetail
     * @param id 
     * @returns 
     */
    delete(id: any) {
        return this.http.delete<RoleDetail>(this.apiURL + '/role-details/' + id, this.httpOptions)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    /**
    * HttpClient API get() method => Fetch RoleDetail
    * @param id 
    * @returns 
    */
    find(id: any): Observable<RoleDetail> {
        return this.http.get<RoleDetail>(this.apiURL + '/role-details/' + id)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    /**
    * HttpClient API get() method => paginate RoleDetail with start page = 1 and limit
    * @param id 
    * @returns 
    */
    paginate(page: Number, limit: Number, filter: any): Observable<RoleDetail> {
        return this.http.get<RoleDetail>(this.apiURL + '/role-details/paginate?page=' + page + '&limit=' + limit + '&' + filter)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    /*========================================
      Begin Custom Methods for RESTful API
    =========================================*/
    /**
     * Error handling 
     * @param error 
     * @returns 
     */
    handleError(error: any) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
    }

}