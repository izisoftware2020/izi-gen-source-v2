import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Menu } from '../../models/20menu.models';
import { CONSTAINT_UTILS } from '../../utils/constaint.utils';

@Injectable({
  providedIn: 'root'
})

export class MenuService {

  // Define API
  apiURL = CONSTAINT_UTILS.api.frontend_url;

  constructor(private http: HttpClient) { }

  /*========================================
    Begin CRUD Methods for consuming RESTful API
  =========================================*/

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  /**
   * HttpClient API get() method => Fetch menus list
   * @returns 
   */
  get(): Observable<Menu> {
    return this.http.get<Menu>(this.apiURL + '/menus')
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API post() method => Create menu
   * @param menu 
   * @returns 
   */
  add(menu: Menu): Observable<Menu> {
    return this.http.post<Menu>(this.apiURL + '/menus', JSON.stringify(menu), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API put() method => Update menu
   * @param id 
   * @param menu 
   * @returns 
   */
  update(menu: Menu): Observable<Menu> {
    return this.http.put<Menu>(this.apiURL + '/menus/', JSON.stringify(menu), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API delete() method => Delete menu
   * @param id 
   * @returns 
   */
  delete(id: any) {
    return this.http.delete<Menu>(this.apiURL + '/menus/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
  * HttpClient API get() method => Fetch menu
  * @param id 
  * @returns 
  */
  find(id: any): Observable<Menu> {
    return this.http.get<Menu>(this.apiURL + '/menus/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
  * HttpClient API get() method => paginate menu with start page = 1 and limit
  * @param id 
  * @returns 
  */
  paginate(page: Number, limit: Number, filter: any): Observable<Menu> {
    let url = this.apiURL + '/menus/paginate?page=' + page + '&limit=' + limit;

    // add condition filter
    if (filter != '') {
      url = this.apiURL + '/menus/paginate?page=' + page + '&limit=' + limit + filter;
    }
    return this.http.get<Menu>(url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /*========================================
    Begin Custom Methods for RESTful API
  =========================================*/





  /**
   * Error handling 
   * @param error 
   * @returns 
   */
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}