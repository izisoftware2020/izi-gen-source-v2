export class OptionalRole {
    id?: Number;
    idRole?: string;
    name?: string;
    salary?: string;
    condition?: string;
    description?: string;
    bonus?: string;
    conditionBonus?: string;
    descriptionBonus?: string     
}
