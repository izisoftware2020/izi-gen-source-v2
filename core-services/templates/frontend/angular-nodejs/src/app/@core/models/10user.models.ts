export class User {
    id?: Number;
    account?: string;
    role?: string;
    fullname?: string;
    username?: string;
    password?: string;
    sex?: string;
    avatar?: string;
    born?: string;
    phone?: string;
    email?: string;
    isEmailVerified?: string;
    address?: string;
    status?: string;
    deviceToken?: string;
    createdAt?: string;
    updatedAt?: string;
}
