export class PersonalHonor {
    id?: Number;
    idContext?: string;
    idUser?: string;
    position?: string;
    title?: string;
    content?: string     
}
