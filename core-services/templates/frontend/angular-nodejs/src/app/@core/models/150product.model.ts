export class Product {
    id?: Number;
    idCategory?: string;
    name?: string;
    resource?: string;
    madeIn?: string;
    description?: string;
    thumbnail?: string;
    images?: string;
    prices?: string;
    prriceOrigin?: string;
    createdAt?: string;
    updatedAt?: string     
}
