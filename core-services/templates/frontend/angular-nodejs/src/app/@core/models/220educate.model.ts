export class Educate {
    id?: Number;
    fullname?: string;
    born?: string;
    address?: string;
    phone?: string;
    position?: string;
    addressLearning?: string;
    content?: string;
    dateStartWorking?: string;
    dateRegistration?: string     
}
