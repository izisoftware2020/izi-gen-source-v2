export class Menu {
    id?: Number;
    menu?: string;
    isGroup?: string;
    name?: string;
    slug?: string;
    icon?: string;
    position?: string;
}
