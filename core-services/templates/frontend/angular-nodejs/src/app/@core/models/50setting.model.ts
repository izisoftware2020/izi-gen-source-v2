export class Setting {
    id?: Number;
    referenceUrl?: string;
    contactUrl?: string;
    androidUrl?: string;
    iosUrl?: string     
}
