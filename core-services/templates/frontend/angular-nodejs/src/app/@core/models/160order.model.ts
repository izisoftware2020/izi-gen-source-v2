export class Order {
    id?: Number;
    idUser?: string;
    userAccept?: string;
    idWarehouse?: string;
    description?: string;
    imagePayment?: string;
    statusOrder?: string;
    statusPayment?: string;
    totalPrice?: string;
    discountPrice?: string;
    createdAt?: string;
    updatedAt?: string;
    idProvince?: string;
    idDistrict?: string;
    address?: string     
}
