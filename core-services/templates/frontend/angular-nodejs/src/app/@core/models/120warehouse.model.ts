export class Warehouse {
    id?: Number;
    idProvince?: string;
    idDistrict?: string;
    idUser?: string;
    idWarehouse?: string;
    name?: string;
    address?: string;
    area?: string;
    oda?: string     
}
