import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { JwtInterceptor, ServerErrorInterceptor } from './interceptors';
import { AuthService } from './services/base/00auth.service';
import { LopService } from './services/base/50lop.service';
import { SinhVienService } from './services/base/60sinh-vien.service';
import { CommonService } from './utils/common.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorInterceptor,
      multi: true,
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    CommonService,
    AuthService,
    LopService,
    SinhVienService,

  ],
})
export class CoreModule { }
