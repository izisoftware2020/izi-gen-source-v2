import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { LoginCookie } from '../common/core/login-cookie';
import { AuthService } from '../@core/services/base/00auth.service';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: LoginComponent
      }
    ]),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [AuthService,
    //  LoginCookie
    ],
})
export class LoginModule { }
