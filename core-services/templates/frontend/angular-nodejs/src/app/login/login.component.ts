import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
// import { LoginCookie } from '../common/core/login-cookie';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Md5 } from 'ts-md5';
import { AuthService } from '../@core/services/base/00auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  //data binding
  input = {
    username: '',
    password: '',
    role: '',
  };

  //subscription
  subscription: Subscription[] = [];

  //md5 password
  passwordMd5: string = '';
  form: FormGroup;

  /**
   * constructor
   * @param router
   * @param api
   * @param login
   * @param formBuilder
   */
  constructor(
    private router: Router,
    private authService: AuthService,
    // private login: LoginCookie,
    private formBuilder: FormBuilder
  ) {
    //form validation
    this.form = this.formBuilder.group({
      password: [null, [Validators.maxLength(50)]],
      email: [null, [Validators.maxLength(50)]],
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.input = {
      username: '',
      password: '',
      role: '',
    };
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Login Click
   */
  onBtnLoginClick() {
    // check login
    this.subscription.push(
      this.authService.login(this.input.username, this.input.password)
        // this.authService.login('ytp3001', 'password')
        .subscribe(data => {

          // Save access token
          localStorage.setItem('access', data['tokens']['access']['token']);

          // Save refresh token
          localStorage.setItem('refresh', data['tokens']['refresh']['token']);

          // Save id user
          localStorage.setItem('user', data['user']['id']);

          // Save role of user
          localStorage.setItem('role', data['user']['idRole']);

          // image profile url
          localStorage.setItem('imageAvatar', data['user']['avatar']);

          //  access login
          this.authService.loginSuccess(data['tokens']['access']['token']);

          setTimeout(() => {
            window.location.href = '/#/manager/home';
            location.reload();
          }, 500);
        })
    );
  }

}
