import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { {{phuong0}} } from '../../models/{{phuong1}}';
import { CONSTAINT_UTILS } from '../../utils/constaint.utils';

@Injectable({
  providedIn: 'root'
})

export class {{phuong0}}Service {

  // Define API
  apiURL = CONSTAINT_UTILS.api.frontend_url;

  constructor(private http: HttpClient) { }

  /*========================================
    Begin CRUD Methods for consuming RESTful API
  =========================================*/

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  /**
   * HttpClient API get() method => Fetch {{phuong2}}s list
   * @returns 
   */
  get(): Observable<any> {
    return this.http.get<{{phuong0}}>(this.apiURL + '/{{phuong2}}s')
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API post() method => Create {{phuong3}}
   * @param {{phuong3}} 
   * @returns 
   */
  add({{phuong3}}: {{phuong0}}): Observable<{{phuong0}}> {
    return this.http.post<{{phuong0}}>(this.apiURL + '/{{phuong2}}s', JSON.stringify({{phuong3}}), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API put() method => Update {{phuong3}}
   * @param id 
   * @param {{phuong3}} 
   * @returns 
   */
  update(id: any, {{phuong3}}: {{phuong0}}): Observable<{{phuong0}}> {
    return this.http.put<{{phuong0}}>(this.apiURL + '/{{phuong2}}s', JSON.stringify({{phuong3}}), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API delete() method => Delete {{phuong3}}
   * @param id 
   * @returns 
   */
  delete(id: any) {
    return this.http.delete<{{phuong0}}>(this.apiURL + '/{{phuong2}}s/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
  * HttpClient API get() method => Fetch {{phuong3}}
  * @param id 
  * @returns 
  */
  find(id: any): Observable<{{phuong0}}> {
    return this.http.get<{{phuong0}}>(this.apiURL + '/{{phuong2}}s/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
  * HttpClient API get() method => paginate {{phuong3}} with start page = 1 and limit
  * @param id 
  * @returns 
  */
  paginate(page: Number, limit: Number, filter: any): Observable<{{phuong0}}> {
    let url = this.apiURL + '/{{phuong2}}s/paginate?page=' + page + '&limit=' + limit;

    // add condition filter
    if (filter != '') {
      url = this.apiURL + '/{{phuong2}}s/paginate?page=' + page + '&limit=' + limit + filter;
    }
    return this.http.get<{{phuong0}}>(url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /*========================================
    Begin Custom Methods for RESTful API
  =========================================*/




  /**
   * Error handling 
   * @param error 
   * @returns 
   */
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}