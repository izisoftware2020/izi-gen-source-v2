import 'package:get/get.dart';
import 'package:template/view/screen/auth/auth_controller.dart';
import 'package:template/view/screen/dashboard/dashboard_controller.dart';
import 'package:template/view/screen/introduction/intro_controller.dart';
import 'package:template/view/screen/splash/splash_controller.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SplashController>(() => SplashController()); 
    Get.lazyPut<IntroController>(() => IntroController()); 
  }
}
