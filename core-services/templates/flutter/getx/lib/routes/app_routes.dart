class AppRoutes {
  static const String SPLASH = '/';
  static const String INTRO = '/intro';
  static const String AUTH = '/auth';
  static const String DASHBOARD = '/dashboard';
  static const String ACCOUNT = '/account';
  static const String POST = '/post';
  static const String PREVIEW = '/preview';
  static const String ADD_LINK = '/add_link';
}
