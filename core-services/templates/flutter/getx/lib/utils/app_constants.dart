import 'package:template/data/model/response/language_model.dart';

const String BASE_URL = 'http://103.146.23.186:4004/';

// api url
const String region_uri = 'v1/regions';

// user api url
const String user_uri = 'v1/users';

// template api url
const String template_uri = 'v1/templates';

// information api url
const String information_uri = 'v1/informations';

// user connection api url
const String user_connection_uri = 'v1/userconnections';

const String LANG_KEY = 'lang';
const String COUNTRY_CODE = 'country_code';
const String LANGUAGE_CODE = 'language_code';
const String THEME = 'theme';

List<LanguageModel> languages = [
  LanguageModel(
      imageUrl: '',
      languageName: 'Việt Nam',
      countryCode: 'VI',
      languageCode: 'vi'),
  LanguageModel(
      imageUrl: '',
      languageName: 'English',
      countryCode: 'US',
      languageCode: 'en'),
];
