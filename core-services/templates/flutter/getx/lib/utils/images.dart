class Images {
  static const String logo_image = 'assets/images/logo.png';
  static const String splash_screen = 'assets/images/splashscreen.png';
  static const String splash_logo = 'assets/images/logo_splash.png';
  static const String no_internet = 'assets/images/opps_internet.png';
  static const String no_data = 'assets/images/no_data.png';

  static const String flutter = 'assets/images/flutter.png';
  static const String intro_img1 = 'assets/images/img1.jpg';
  static const String intro_img2 = 'assets/images/img2.jpg';
  static const String intro_img3 = 'assets/images/img3.jpg';

  static const String login_background = 'assets/images/login_background.png';

}
