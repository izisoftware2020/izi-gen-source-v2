import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/utils/device_utils.dart';

class LinkButton extends StatelessWidget {
  final Color? backgroudColor;
  final IconData? linkIcon;
  final Color? linkIconColor;
  final String linkText;
  final Color? linkColor;

  const LinkButton(
      {Key? key,
      this.backgroudColor,
      this.linkIcon,
      this.linkIconColor,
      required this.linkText,
      this.linkColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.snackbar('Facebook click', 'sdf');
      },
      child: Container(
        width: DeviceUtils.getScaledWidth(context, 0.9),
        margin: EdgeInsets.only(
            left: DeviceUtils.getScaledWidth(context, 0.05),
            right: DeviceUtils.getScaledWidth(context, 0.05),
            bottom: 10.0),
        height: 50,
        decoration: BoxDecoration(
            color: backgroudColor, borderRadius: BorderRadius.circular(12)),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // icon
              Container(
                margin: const EdgeInsets.only(right: 10),
                child: Icon(
                  linkIcon,
                  size: 35,
                  color: linkIconColor,
                ),
              ),

              // text
              Text(
                linkText,
                style: TextStyle(
                    color: linkColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
        ),
      ),
    );
  }
}
