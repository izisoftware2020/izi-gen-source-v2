import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/routes/app_routes.dart';
import 'package:template/utils/device_utils.dart';
import 'package:template/utils/dimensions.dart';

import 'components/link_button.dart';
import 'home_controller.dart';

class HomePage extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tap Go'),
        backgroundColor: const Color(0xff3E3E70),
        centerTitle: true,
        elevation: 0,
        actions: [
          GestureDetector(
            onTap: () {
              Get.toNamed(AppRoutes.PREVIEW);
            },
            child: Container(
              padding:
                  const EdgeInsets.only(right: Dimensions.PADDING_SIZE_DEFAULT),
              child: const Icon(Icons.remove_red_eye_outlined),
            ),
          )
        ],
      ),
      body: Container(
        decoration: const BoxDecoration(color: Color(0xff3E3E70)),
        child: Center(
            child: Container(
          width: DeviceUtils.getScaledWidth(context, 1),
          height: DeviceUtils.getScaledHeight(context, 1),
          decoration: const BoxDecoration(
              color: Color(0xffF7F6F7),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0))),
          child: Stack(
            children: [
              Positioned(
                  top: 0,
                  width: DeviceUtils.getScaledWidth(context, 1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // Avatar
                      Container(
                          margin: const EdgeInsets.only(
                              top: Dimensions.PADDING_SIZE_DEFAULT),
                          height: 180.0,
                          width: DeviceUtils.getScaledWidth(context, 0.9),
                          alignment: Alignment.center,
                          child: Stack(
                            children: [
                              // Background
                              Positioned.fill(
                                top: 50,
                                child: Card(
                                  elevation: 0.2,
                                  child: Container(
                                    width: DeviceUtils.getScaledWidth(
                                        context, 0.9),
                                    height: 100.0,
                                    decoration: const BoxDecoration(
                                        color: Color(0xffFFFFFF)),
                                  ),
                                ),
                              ),

                              // Image
                              Positioned(
                                  top: 0,
                                  width:
                                      DeviceUtils.getScaledWidth(context, 0.9),
                                  child: GestureDetector(
                                    // on update profile tab
                                    onTap: () =>
                                        controller.onUpdateProfileTab(),
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: ClipOval(
                                        child: Image.network(
                                          "https://i.vietgiaitri.com/2020/6/3/xinh-dep-lai-co-minh-day-cung-nguc-day-hotgirl-len-hinh-ben-bo-ma-bi-don-cap-dai-gia-19f-4981007.jpg",
                                          height: 100,
                                          width: 100,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  )),

                              // Image
                              Positioned(
                                  top: 79,
                                  left: 29,
                                  width:
                                      DeviceUtils.getScaledWidth(context, 0.9),
                                  child: GestureDetector(
                                    // on update profile tab
                                    onTap: () =>
                                        controller.onUpdateProfileTab(),
                                    child: const Icon(
                                      Icons.enhance_photo_translate,
                                      size: Dimensions.ICON_SIZE_DEFAULT,
                                      color: Color(0xff3E3E70),
                                    ),
                                  )),

                              // Fullname
                              Positioned(
                                  top: 110,
                                  width:
                                      DeviceUtils.getScaledWidth(context, 0.9),
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: const Text(
                                      'Lê Hồng Phương',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xff3D3F50),
                                          fontSize: 18),
                                    ),
                                  )),

                              // Jobs
                              Positioned(
                                  top: 135,
                                  width:
                                      DeviceUtils.getScaledWidth(context, 0.9),
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: const Text(
                                      'Giám đốc Marketing',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          color: Color(0xff85858B),
                                          fontSize: 18),
                                    ),
                                  )),
                            ],
                          )),

                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  )),

              // link button
              Container(
                margin: const EdgeInsets.only(top: 220.0),
                width: DeviceUtils.getScaledWidth(context, 1),
                child: GetBuilder<HomeController>(
                  init: HomeController(),
                  builder: (controller) {
                    if (controller.isLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    return Container();

                    // return ListView.builder(
                    //   itemCount: controller.regionsList.length,
                    //   itemBuilder: (context, index) => LinkButton(
                    //     linkText:
                    //         'Facebook ${controller.regionsList[index].name}',
                    //     linkColor: Colors.white,
                    //     backgroudColor: Colors.blue,
                    //     linkIconColor: Colors.white,
                    //     linkIcon: Icons.facebook,
                    //   ),
                    // );
                  },
                ),
              ),
            ],
          ),
        )),
      ),
    );
  }
}
