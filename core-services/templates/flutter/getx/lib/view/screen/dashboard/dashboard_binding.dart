import 'package:get/get.dart';
import 'package:template/view/screen/dashboard/dashboard_controller.dart';
import 'package:template/view/screen/home/home_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(() => DashboardController());
    Get.lazyPut<HomeController>(() => HomeController());
  }
}
