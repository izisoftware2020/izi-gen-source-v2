import 'package:get/get.dart';
import 'package:template/routes/app_routes.dart';

class DashboardController extends GetxController {
  var tabIndex = 0;

  void changeTabIndex(int index) {
    tabIndex = index;
    update();
  }

  ///
  /// on Add Link Btn Press
  ///
  void onAddLinkBtnPress() {
    Get.toNamed(AppRoutes.ADD_LINK);
  }
}
