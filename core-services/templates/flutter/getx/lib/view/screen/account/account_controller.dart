import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:template/routes/app_routes.dart';

class AccountController extends GetxController {
  bool isLoading = true;

  late TextEditingController fullnameController;
  late TextEditingController companyController;
  late TextEditingController jobController;
  late TextEditingController addressController;

  @override
  void onInit() {
    // init controller
    fullnameController = TextEditingController();
    companyController = TextEditingController();
    jobController = TextEditingController();
    addressController = TextEditingController();

    super.onInit();
  }

  ///
  /// on Save Tap
  ///
  void onSaveTap() {
    Get.snackbar('title', 'message ${fullnameController.text}');
  }

  ///
  /// on update avatar Tap
  ///
  Future<void> onUpdateAvatarTap() async {
    final ImagePicker _picker = ImagePicker();

    // Pick an image
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    print(image);
  }

  ///
  /// onBtnLogoutTap
  ///
  Future<void> onBtnLogoutTap() async {
    Get.offAllNamed(AppRoutes.AUTH);
  }
}
