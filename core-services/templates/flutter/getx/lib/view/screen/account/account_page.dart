import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/utils/device_utils.dart';
import 'package:template/utils/dimensions.dart';
import 'package:template/view/basewidget/textfield/input_text.dart';
import 'package:template/view/screen/account/account_controller.dart';

class AccountPage extends GetView<AccountController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Thông tin cá nhân'),
          backgroundColor: const Color(0xff3E3E70),
          centerTitle: true,
          elevation: 0,
          actions: [
            GestureDetector(
              onTap: () => controller.onSaveTap(),
              child: Container(
                margin: const EdgeInsets.all(Dimensions.MARGIN_SIZE_DEFAULT),
                child: const Text(
                  'Lưu',
                  style: TextStyle(fontSize: Dimensions.FONT_SIZE_LARGE),
                ),
              ),
            )
          ],
        ),
        body: GetBuilder<AccountController>(
          init: AccountController(),
          builder: (controller) {
            return Container(
              decoration: const BoxDecoration(color: Color(0xff3E3E70)),
              child: Center(
                  child: Container(
                width: DeviceUtils.getScaledWidth(context, 1),
                height: DeviceUtils.getScaledHeight(context, 1),
                decoration: const BoxDecoration(
                    color: Color(0xffF7F6F7),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0))),
                child: Container(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        // Avatar
                        Container(
                            margin: const EdgeInsets.only(
                                top: Dimensions.PADDING_SIZE_DEFAULT),
                            height: 150.0,
                            width: DeviceUtils.getScaledWidth(context, 0.9),
                            alignment: Alignment.center,
                            child: Stack(
                              children: [
                                // Background
                                Positioned.fill(
                                  top: 50,
                                  child: Card(
                                    elevation: 0.2,
                                    child: Container(
                                      width: DeviceUtils.getScaledWidth(
                                          context, 0.9),
                                      height: 100.0,
                                      decoration: const BoxDecoration(
                                          color: Color(0xffFFFFFF)),
                                    ),
                                  ),
                                ),

                                // Image
                                Positioned(
                                    top: 0,
                                    width: DeviceUtils.getScaledWidth(
                                        context, 0.9),
                                    child: GestureDetector(
                                      // on update profile tab
                                      onTap: () =>
                                          controller.onUpdateAvatarTap(),
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: ClipOval(
                                          child: Image.network(
                                            "https://i.vietgiaitri.com/2020/6/3/xinh-dep-lai-co-minh-day-cung-nguc-day-hotgirl-len-hinh-ben-bo-ma-bi-don-cap-dai-gia-19f-4981007.jpg",
                                            height: 100,
                                            width: 100,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    )),

                                // Image
                                Positioned(
                                    top: 79,
                                    left: 29,
                                    width: DeviceUtils.getScaledWidth(
                                        context, 0.9),
                                    child: GestureDetector(
                                      // on update profile tab
                                      onTap: () =>
                                          controller.onUpdateAvatarTap(),
                                      child: const Icon(
                                        Icons.enhance_photo_translate,
                                        size: Dimensions.ICON_SIZE_DEFAULT,
                                        color: Color(0xff3E3E70),
                                      ),
                                    )),

                                // Fullname
                                Positioned(
                                    top: 110,
                                    width: DeviceUtils.getScaledWidth(
                                        context, 0.9),
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: const Text(
                                        'Lê Hồng Phương',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xffA1A1A1),
                                            fontSize: 18),
                                      ),
                                    )),
                              ],
                            )),

                        // input field
                        InputText(
                            label: 'Họ và tên',
                            controller: controller.fullnameController,
                            hintText: 'Nhập họ và tên'),

                        // Company
                        InputText(
                            label: 'Công ty',
                            controller: controller.companyController,
                            hintText: 'Nhập tên công ty'),

                        // Job
                        InputText(
                            label: 'Chức vụ',
                            controller: controller.jobController,
                            hintText: 'Nhập chức vụ'),

                        // Address
                        InputText(
                            label: 'Địa chỉ',
                            controller: controller.addressController,
                            hintText: 'Nhập địa chỉ'),

                        // Logout button
                        GestureDetector(
                          onTap: ()=> controller.onBtnLogoutTap(),
                          child: Container(
                            width: DeviceUtils.getScaledWidth(context, 0.9),
                            margin: EdgeInsets.only(
                                left: DeviceUtils.getScaledWidth(context, 0.05),
                                right:
                                    DeviceUtils.getScaledWidth(context, 0.05),
                                top: Dimensions.PADDING_SIZE_LARGE),
                            height: 50,
                            decoration: BoxDecoration(
                                color: const Color(0xff3E3E70),
                                borderRadius: BorderRadius.circular(12)),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  // icon
                                  Container(
                                    margin: const EdgeInsets.only(right: 10),
                                    child: const Icon(
                                      Icons.logout,
                                      size: Dimensions.ICON_SIZE_DEFAULT,
                                      color: Colors.white,
                                    ),
                                  ),

                                  // text
                                  const Text(
                                    'Đăng xuất',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )),
            );
          },
        ));
  }
}
