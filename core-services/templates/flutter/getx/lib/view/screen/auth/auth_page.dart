import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:get/get.dart';
import 'package:template/utils/device_utils.dart';
import 'package:template/utils/images.dart';

import 'auth_controller.dart';

class AuthPage extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(Images.login_background),
                    fit: BoxFit.cover)),
            child: Stack(
              children: [
                Positioned(
                  bottom: 0,
                  width: DeviceUtils.getScaledWidth(context, 1),
                  height: DeviceUtils.getScaledHeight(context, 0.25),
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Colors.black26,
                        borderRadius:
                            BorderRadius.only(topLeft: Radius.circular(50))),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // login with google
                        SignInButton(
                          Buttons.Google,
                          elevation: 3.0,
                          padding: const EdgeInsets.only(
                              left: 30.0, right: 30.0, top: 5.0, bottom: 5.0),
                          text: "Sign up with Google",
                          onPressed: () => controller.onBtnGoogleSignInPress(),
                        ),

                        const SizedBox(height: 10),

                        // login with facebook
                        SignInButton(
                          Buttons.FacebookNew,
                          elevation: 3.0,
                          padding: const EdgeInsets.only(
                              left: 30.0, right: 30.0, top: 11.0, bottom: 11.0),
                          text: "Sign up with Facebook",
                          onPressed: () =>
                              controller.onBtnFacebookSignInPress(),
                        ),

                        const SizedBox(height: 10),

                        // login with apple
                        SignInButton(
                          Buttons.AppleDark,
                          elevation: 3.0,
                          padding: const EdgeInsets.only(
                              left: 30.0, right: 30.0, top: 13.0, bottom: 13.0),
                          text: "Sign up with Apple",
                          onPressed: () => controller.onBtnAppleSignInPress(),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )));
  }
}
