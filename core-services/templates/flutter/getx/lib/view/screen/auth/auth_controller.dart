import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:template/routes/app_routes.dart';

import 'dart:convert' show json;

import "package:http/http.dart" as http;

class AuthController extends GetxController {
  final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  @override
  void onInit() {
    super.onInit();

    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount? account) {
      print('phuong account');
      print(account);
      // setState(() {
      //   _currentUser = account;
      // });
      if (account != null) {
        _handleGetContact(account);
      }
    });
    _googleSignIn.signInSilently();
    print('phuong onInit');
  }

  Future<void> _handleGetContact(GoogleSignInAccount user) async {
    // setState(() {
    //   _contactText = "Loading contact info...";
    // });
    final http.Response response = await http.get(
      Uri.parse('https://people.googleapis.com/v1/people/me/connections'
          '?requestMask.includeField=person.names'),
      headers: await user.authHeaders,
    );
    if (response.statusCode != 200) { 
      print('People API ${response.statusCode} response: ${response.body}');
      return;
    }
    // final Map<String, dynamic> data = json.decode(response.body);
    // final String? namedContact = _pickFirstNamedContact(data);
    // setState(() {
    //   if (namedContact != null) {
    //     _contactText = "I see you know $namedContact!";
    //   } else {
    //     _contactText = "No contacts to display.";
    //   }
    // });
  }

  ///
  /// onBtnGoogleSignInPress
  ///
  Future<void> onBtnGoogleSignInPress() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
    // Get.offAndToNamed(AppRoutes.DASHBOARD);
    // Get.snackbar('Tap Go Login Google', 'Tap Go Login Google success!',
    //     icon: const Icon(Icons.login),
    //     shouldIconPulse: true,
    //     isDismissible: true,
    //     duration: const Duration(seconds: 1));
  }

  ///
  /// onBtnFacebookSignInPress
  ///
  void onBtnFacebookSignInPress() {
    Get.offAndToNamed(AppRoutes.DASHBOARD);
    Get.snackbar('Tap Go Login Facebook', 'Tap Go Login Facebook success!',
        icon: const Icon(Icons.login),
        shouldIconPulse: true,
        isDismissible: true,
        duration: const Duration(seconds: 1));
  }

  ///
  /// onBtnAppleSignInPress
  ///
  void onBtnAppleSignInPress() {
    Get.offAndToNamed(AppRoutes.DASHBOARD);
    Get.snackbar('Tap Go Login Apple', 'Tap Go Login Apple success!',
        icon: const Icon(Icons.login),
        shouldIconPulse: true,
        isDismissible: true,
        duration: const Duration(seconds: 1));
  }
}
