import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:template/routes/app_routes.dart';

class SplashController extends GetxController with SingleGetTickerProviderMixin {
  AnimationController? _animationController;
  Animation? _animation;

  @override
  void onInit() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1500),
    );
    _animation = CurvedAnimation(
        parent: _animationController!.view, curve: Curves.easeInCubic);

    _animationController!
        .forward()
        .whenComplete(() => Get.offNamed(AppRoutes.INTRO));
    super.onInit();
  }

  @override
  void onClose(){
    super.onClose();
    print('on onClose');
    _animationController!.dispose();
  }
}
