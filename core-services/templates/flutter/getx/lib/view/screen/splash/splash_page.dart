import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/utils/images.dart';

import 'splash_controller.dart';

class SplashPage extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GetBuilder<SplashController>(
        builder: (controller) {
          return Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(Images.splash_screen),
                    fit: BoxFit.cover)),
            alignment: Alignment.center,
          );
        },
      ),
    );
  }
}
