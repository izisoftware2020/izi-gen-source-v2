import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/routes/app_routes.dart';

class IntroController extends GetxController {

  void onIntroEnd() {
    Get.offNamed(AppRoutes.AUTH);
  }
}
