import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:template/utils/images.dart';

import 'intro_controller.dart';

class IntroPage extends GetView<IntroController> {
  final introKey = GlobalKey<IntroductionScreenState>();

  Widget _buildImage(String assetName, [double width = 350]) {
    return Image.asset(assetName, width: width);
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);

    const pageDecoration = PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      globalBackgroundColor: Colors.white,
      globalHeader: Align(
        alignment: Alignment.topRight,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 16, right: 16),
            child: _buildImage(Images.logo_image, 100),
          ),
        ),
      ),
      // globalFooter: SizedBox(
      //   width: double.infinity,
      //   height: 60,
      //   child: ElevatedButton( 
      //     onPressed: () => controller.onIntroEnd(),
      //     child: const Text(
      //       'Bắt đầu ngay!',
      //       style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
      //     ),
      //   ),
      // ),
      pages: [
        PageViewModel(
          title: "Tiêu đề 1",
          body:
              "Thay vì phải mua toàn bộ cổ phiếu, hãy đầu tư bất kỳ số tiền nào bạn muốn.",
          image: _buildImage(Images.intro_img1),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Tiêu đề 2",
          body:
              "Tải xuống ứng dụng Kho dự trữ và làm chủ thị trường với bài học nhỏ của chúng tôi.",
          image: _buildImage(Images.intro_img2),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Tiêu đề 3",
          body:
              "Trẻ em và thanh thiếu niên có thể theo dõi cổ phiếu của họ 24/7 và đặt các giao dịch mà bạn chấp thuận.",
          image: _buildImage(Images.intro_img3),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => controller.onIntroEnd(),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      //rtl: true, // Display as right-to-left
      skip: const Text('Bỏ qua'),
      next: const Icon(Icons.arrow_forward),
      done: const Text('Hoàn thành',
          style: TextStyle(fontWeight: FontWeight.w600)),
      curve: Curves.fastLinearToSlowEaseIn,
      controlsMargin: const EdgeInsets.all(16),
      controlsPadding: kIsWeb
          ? const EdgeInsets.all(12.0)
          : const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      dotsContainerDecorator: const ShapeDecoration(
        color: Color(0xff3E3E70),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }
}
