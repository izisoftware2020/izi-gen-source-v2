import 'package:flutter/material.dart';
import 'package:template/utils/device_utils.dart';
import 'package:template/utils/dimensions.dart';

class InputSelect extends StatelessWidget {
  final String label;
  final TextEditingController? controller;
  final String hintText;

  const InputSelect({
    Key? key,
    required this.label,
    this.controller,
    required this.hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: Dimensions.MARGIN_SIZE_DEFAULT),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // label
          Container(
            margin: const EdgeInsets.only(
                bottom: Dimensions.MARGIN_SIZE_EXTRA_SMALL),
            child: Text(
              label,
              style: const TextStyle(
                  color: Color(0xffA1A1A1),
                  fontSize: Dimensions.FONT_SIZE_DEFAULT,
                  fontWeight: FontWeight.w500),
            ),
          ),

          // Input text
          Container(
            height: 50,
            padding: const EdgeInsets.only(
                left: Dimensions.PADDING_SIZE_DEFAULT,
                right: Dimensions.PADDING_SIZE_DEFAULT),
            width: DeviceUtils.getScaledWidth(context, 0.9),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 0.5,
                  blurRadius: 1,
                  // changes position of shadow
                  offset: const Offset(0, 1),
                ),
              ],
            ),
            child: DropdownButton<String>(
              value: 'Facebook',
              icon: const Icon(Icons.arrow_downward),
              iconSize: Dimensions.ICON_SIZE_DEFAULT,
              elevation: 16,
              style: const TextStyle(color: Colors.deepPurple),
              underline: Container(
                height: 0,
                color: Colors.deepPurpleAccent,
              ),
              iconDisabledColor: Colors.white,
              iconEnabledColor: Colors.white,
              onChanged: (String? newValue) {},
              items: <String>['Facebook', 'Google', 'Intagram', 'TikTok']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(
                            right: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                        child: const Icon(
                          Icons.facebook_outlined,
                          color: Colors.deepPurple,
                          size: Dimensions.ICON_SIZE_DEFAULT,
                        ),
                      ),
                      Text(value)
                    ],
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }
}
