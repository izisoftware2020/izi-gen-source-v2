class Preferences {
  Preferences._();

  static const String jwt_token = "jwt_token";
  static const String is_dark_mode = "is_dark_mode";
  static const String current_language = "current_language";
}