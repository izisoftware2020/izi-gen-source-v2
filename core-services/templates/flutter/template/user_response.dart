{{phuong5}}
class {{phuong0}}Response {
{{phuong1}}
  String? createdAt;
  String? updatedAt;

  {{phuong0}}Response({
{{phuong2}},
      this.createdAt,
      this.updatedAt});
  
  ///
  /// From JSON
  ///
  {{phuong0}}Response.fromJson(Map<String, dynamic> json) {
{{phuong3}}
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  ///
  /// To JSON
  ///
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
{{phuong4}}
    return data;
  }
}
