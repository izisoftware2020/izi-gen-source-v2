import 'package:get_it/get_it.dart';
import 'package:template/data/datasource/remote/dio/dio_client.dart';
import 'package:template/data/datasource/remote/exception/api_error_handler.dart';
import 'package:template/data/model/request/{{phuong0}}';
import 'package:template/data/model/response/base/api_response.dart';

class {{phuong1}}Repository {
  DioClient? dioClient = GetIt.I.get<DioClient>();

  {{phuong1}}Repository();

  ///
  /// Get all {{phuong2}}s
  ///
  Future<ApiResponse> get() async {
    try {
      final response = await dioClient!.get('/{{phuong2}}s');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  ///
  /// Insert {{phuong2}} to database
  ///
  Future<ApiResponse> add({{phuong1}}Request data) async {
    try {
      final response = await dioClient!.post('/{{phuong2}}s', data: data.toJson());
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  ///
  /// Update {{phuong2}} to database
  ///
  Future<ApiResponse> update({{phuong1}}Request data) async {
    try {
      final response = await dioClient!.put('/{{phuong2}}s', data: data.toJson());
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  ///
  /// Update {{phuong2}} to database
  ///
  Future<ApiResponse> delete(String id) async {
    try {
      final response =
          await dioClient!.delete('/{{phuong2}}s/$id');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  ///
  /// Get paginate {{phuong2}}s "page": 1, "limit": 10, filter 
  ///
  Future<ApiResponse> paginate(int page, int limit, String filter) async {
    try {
      String uri = '/{{phuong2}}s/paginate?page=$page&limit=$limit'.toString();

      // add condition filter
      if (filter != '') {
        uri = '/{{phuong2}}s/paginate?page=$page&limit=$limit$filter';
      }

      final response = await dioClient!.get(uri);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  ///
  /// Find {{phuong2}} by id
  ///
  Future<ApiResponse> find(String id) async {
    try {
      final String uri = '/{{phuong2}}s/$id';
      final response = await dioClient!.get(uri);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
