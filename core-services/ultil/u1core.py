import shutil
import os
from os import path
import re
from django.core import serializers

class GenSourceCore: 

    def __init__(self):
        self.root_link = 'output/'
        self.templates_link = 'templates/'
    
    def read_file(self, path_file):
        with open(self.templates_link + path_file, 'r', encoding='utf8') as f:
            return f.readlines()

    def read_file_origin(self, path_file):
        with open(path_file, 'r', encoding='utf8') as f:
            return f.readlines()            

    def create_file(self, file_name, content=''):
        print('file_name', file_name)
        with open(self.root_link + file_name,'w+', encoding='utf8') as f:
            f.write(content)

    def create_folder(self, folder_path):
        from pathlib import Path
        Path(self.root_link + folder_path).mkdir(parents=True, exist_ok=True) 

    def copy_folder(self, source_folder, dest_folder):
        try:
            # remove old folder first 
            if os.path.exists(self.root_link + dest_folder):
                shutil.rmtree(self.root_link + dest_folder)

            # copy folder
            shutil.copytree(self.templates_link + source_folder, self.root_link + dest_folder)

        # Directories are the same
        except shutil.Error as e:
            print('Directory not copied. Error: %s' % e)
        # Any error saying that the directory doesn't exist
        except OSError as e:
            print('Directory not copied. Error: %s' % e)

    def copy_folder_custom(self, source_folder, dest_folder):
        try:
            # remove old folder first
            if os.path.exists(dest_folder):
                shutil.rmtree(dest_folder)

            # copy folder
            shutil.copytree(source_folder, dest_folder)

        # Directories are the same
        except shutil.Error as e:
            print('Directory not copied. Error: %s' % e)
        # Any error saying that the directory doesn't exist
        except OSError as e:
            print('Directory not copied. Error: %s' % e)

    def copy_file_custom(self, source_file, dest_folder):
        try:
            # copy file
            shutil.copy(source_file, dest_folder)

        # Directories are the same
        except shutil.Error as e:
            print('Directory not copied. Error: %s' % e)
        # Any error saying that the directory doesn't exist
        except OSError as e:
            print('Directory not copied. Error: %s' % e)

    def render_replace(self, input_path_file, ouput_path_file, params):
        number_param = len(params)

        # read all data in file
        datas = self.read_file(input_path_file)
        output = ''
        
        ans = 0
        
        for data in datas:  
            if data.find('phuong' + str(ans)) >= 0 and ans < number_param:
                output = output + data.replace('phuong' + str(ans), params[ans])
                ans = ans + 1
            else:
                output = output + data
        
        # save file after replace
        self.create_file(ouput_path_file, output)

    def render_replace_template(self, input_path_file, ouput_path_file, params, read_origin = False):
        number_param = len(params)

        # read all data in file
        if(read_origin): 
            datas = self.read_file_origin(input_path_file)
        else:
            datas = self.read_file(input_path_file)
        output = ''
        
        ans = 0 
        for param in params:
            data_temp = []
            for data in datas:
                data_temp.append(data.replace('{{phuong'+str(ans)+'}}', params[ans]))

            datas = data_temp
            ans = ans + 1

        output = output.join(datas)
        
        # save file after replace
        self.create_file(ouput_path_file, output) 
        return output       

    def zipdir(self, path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file in files:
                ziph.write(os.path.join(root, file))

    def remove_project(self, project_name):
        import shutil 
        input_dir_name = 'output/'+ project_name 
        if os.path.exists(input_dir_name) and os.path.isdir(input_dir_name):
            shutil.rmtree(input_dir_name)

    def zip_project(self, project_name):
        import shutil
        from datetime import datetime
        now = datetime.now()  # current date and time
        date_time = now.strftime("%m%d%Y%H%M%S")

        output_filename = 'output/'+ project_name + date_time
        input_dir_name = 'output/'+ project_name
        shutil.make_archive(output_filename , 'zip', input_dir_name)

        return '/output/'+ project_name + date_time +'.zip'

    def call_bat_file(self):
        import subprocess
        subprocess.call([r'test.bat'])

    def to_snake_case(self, name): 
        return re.sub(r'(?<!^)(?=[A-Z])', '_', name).lower()

    def to_name_file_case(self, name): 
        return re.sub(r'(?<!^)(?=[A-Z])', '-', name).lower()

    def first_lower(self, name): 
        if len(name) == 0:
            return name
        else:
            return name[0].lower() + name[1:]

    def to_pascal_case(self, name): 
        if len(name) == 0:
            return name
        else:
            return name[0].upper() + name[1:]

    def recure_populate_table(self, idRefer, tables):
        table_name = idRefer[2:]
        result = ''
        for table in tables:  
            if table_name == table.classname:
                properties = table.properties.split(',')

                # get id item
                for field in properties:
                    if field[:2] == 'Id':
                        if result == '':
                            result = '.' + self.first_lower(field)
                        else:
                            result = result + ' ' + self.first_lower(field)
        return result
        

    def gen_populate_controller(self, table_name, tables): 
        result = ''
        for table in tables:  
            if table_name == table.classname:
                properties = table.properties.split(',')

                # get id item
                for field in properties:
                    if field[:2] == 'Id':
                        if result == '':
                            result = self.first_lower(field) + self.recure_populate_table(field, tables)
                        else:
                            result = result + ', ' + self.first_lower(field)  + self.recure_populate_table(field, tables)
                            

        return result

    def recure_populate_table_service(self, idRefer, tables, result_child):
        table_name = idRefer[2:]
        result = ''
        for table in tables:  
            if table_name == table.classname:
                properties = table.properties.split(',')

                # get id item
                for field in properties:
                    if field[:2] == 'Id':
                        if result == '':
                            # nếu trùng thì ko cần thêm vào nữa
                            if self.first_lower(field) not in result_child:
                                result = ' ' + self.first_lower(field)
                        else:
                            # nếu trùng thì ko cần thêm vào nữa
                            if self.first_lower(field) not in result_child:
                                result = result + ' ' + self.first_lower(field)
        return result

    def gen_populate_service(self, table_name, tables): 
        result = ''
        result_child = ''
        for table in tables: 
            if table_name == table.classname:
                properties = table.properties.split(',')

                # get id item
                for field in properties:
                    if field[:2] == 'Id':
                        if result == '':
                            result = self.first_lower(field) 
                        else:
                            result = result + ' ' + self.first_lower(field)
                        
                        # get child path
                        result_child = result_child + self.recure_populate_table_service(field, tables, result_child)    

        return result, result_child.strip()

            
