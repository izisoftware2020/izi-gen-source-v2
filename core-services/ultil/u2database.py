from django.core import serializers

# from models import User
from django.apps import apps 

from apps.a1crud import models

from ultil.u1core import GenSourceCore 
core_gen = GenSourceCore()

class GenSourceDatabase: 
    def __init__(self):
        self.root_link = 'output/'
        self.templates_link = 'templates/'
    
    def gen_database_mongodb(self, idproject):
        params = []
        project_name = models.Project.objects.filter(id = idproject)[0].name

        # copy sample to output
        core_gen.copy_folder('database/mongodb', project_name + '/database')

        # test gen  
        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        if len(tables) > 0: 

            # phuong1 create default menu data 
            tables = models.Class.objects.filter(idproject = idproject).order_by('id')
            param1 = ''
            if len(tables) > 0: 
                ans = 6
                for table in tables:
                    classname = core_gen.to_name_file_case(table.classname)
                    param1 = param1 + '{                                                            \n'
                    param1 = param1 + '    "menu" : ObjectId("61664087dcc204257ed772b1"),           \n'
                    param1 = param1 + '    "isGroup" : "0",                                         \n'
                    param1 = param1 + '    "name" : "Quản lý '+ table.classname +'",                      \n'
                    param1 = param1 + '    "slug" : "/manager/c'+ str(ans-1) +'-'+ classname +'",   \n'
                    param1 = param1 + '    "icon" : "&lt;i class=\\"fas fa-users\\">&lt;/i>",         \n'
                    param1 = param1 + '    "position" : NumberInt(3),                               \n'
                    param1 = param1 + '    "created_at" : ISODate("2021-10-13T02:13:39.628+0000"),  \n'
                    param1 = param1 + '    "updated_at" : ISODate("2021-10-13T02:15:48.496+0000"),  \n'
                    param1 = param1 + '    "__v" : NumberInt(0)                                     \n'
                    param1 = param1 + '}                                                            \n'
                    ans = ans + 1
                     
            
            core_gen.create_folder(project_name + '/database')
            core_gen.render_replace_template('database/menus.json', project_name + '/database/menus.json', [param1])

        else:
            print('project havent class')
    
    def gen_database_mysql(self, idproject):
        params = []
        # test gen  
        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        if len(tables) > 0: 
            content = ''
            roles = 'a:3'
            ans = 1
            count = 400

            print(serializers.serialize("json",  tables)) 
            for table in tables:  
                classname = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

                classname = 'p' + str(count) + classname

                content = content + '-- \n' 
                content = content + '-- Table structure for table `'+ classname +'` \n'
                content = content + '--                      \n'
                content = content + 'CREATE TABLE `'+ classname +'` (                                          \n'
                
                for i in range(len(properties)):
                    if properties[i] == 'id':
                        content = content + '  `id` int(11) NOT NULL,\n'
                        continue

                    if attributes[i] == 'INT':
                        content = content + '  `'+ properties[i] +'` int(11) DEFAULT NULL,\n'

                    if attributes[i] == 'DOUBLE':
                        content = content + '  `'+ properties[i] +'` double DEFAULT NULL,\n'
                    
                    if attributes[i] == 'VARCHAR':
                        content = content + '  `'+ properties[i] +'` varchar('+ lengths[i] +') COLLATE utf8_unicode_ci DEFAULT NULL,\n'
                    
                    if attributes[i] == 'TEXT':
                        content = content + '  `'+ properties[i] +'` text COLLATE utf8_unicode_ci DEFAULT NULL,\n'
                    
                    if attributes[i] == 'LONGTEXT':
                        content = content + '  `'+ properties[i] +'` longtext COLLATE utf8_unicode_ci DEFAULT NULL,\n'

                    if attributes[i] == 'DATE':
                        content = content + '  `'+ properties[i] +'` date NOT NULL,\n'

                    if attributes[i] == 'DATETIME':
                        content = content + '  `'+ properties[i] +'` datetime NOT NULL DEFAULT current_timestamp(),\n'

                    if attributes[i] == 'FLOAT':
                        content = content + '  `'+ properties[i] +'` float DEFAULT NULL,\n'

                    if attributes[i] == 'BOOLEAN':
                        content = content + '  `'+ properties[i] +'` tinyint(1) DEFAULT NULL,\n'

                    if attributes[i] == 'VARBINARY':
                        content = content + '  `'+ properties[i] +'` varbinary('+ lengths[i] +') DEFAULT NULL,\n'
                
                # delete string ',' end of string
                content = content[:-2] + '\n'

                content = content + ') ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;  \n'
                content = content + '                                                               \n'
                content = content + '--                                                             \n'
                content = content + '-- Indexes for table `'+ classname +'`                                    \n'
                content = content + '--                                                             \n'
                content = content + 'ALTER TABLE `'+ classname +'` ADD PRIMARY KEY (`id`);                     \n'
                content = content + '                                                               \n'
                content = content + '--                                                             \n'
                content = content + '-- AUTO_INCREMENT for table `'+ classname +'`                             \n'
                content = content + '--                                                             \n'
                content = content + 'ALTER TABLE `'+ classname +'` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;\n' 
                content = content + '                                                               \n'

                # update permission
                roles = roles + ',' + chr(97 + ans) + ':3'
                ans = ans + 1
                count = count + 100

            # gen project folder
            project_name = models.Project.objects.filter(id=idproject)[0].name
            params.append(project_name)
            params.append(project_name)
            params.append(project_name)
            params.append(project_name)
            params.append(project_name)
            params.append(roles)
            params.append(content)

            # phuong7 create default menu data 
            tables = models.Class.objects.filter(idproject = idproject).order_by('id')
            phuong7 = ''
            if len(tables) > 0: 
                ans = 6
                for table in tables:
                    classname = table.classname
                    phuong7 = phuong7 + '('+ str(ans) +', 0, 0, \'Quản lý '+ classname +'\', \'/manager/c'+ str(ans-1) +'-'+ classname +'\', \'<i class=\"fab fa-python\"></i>\', '+ str(ans) +'),\n'
                    ans = ans + 1
                    
            phuong7 = phuong7[:-2]
            phuong7 = phuong7 + ';'
            # add param phuong7
            params.append(phuong7)
            
            core_gen.create_folder(project_name + '/database')
            core_gen.render_replace_template('database/input_mysql.sql', project_name + '/database/output.sql', params)

        else:
            print('project havent class')
    
