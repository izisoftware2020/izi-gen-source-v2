from django.core import serializers

# from models import User
from django.apps import apps 

from apps.a1crud import models

from ultil.u1core import GenSourceCore 
core_gen = GenSourceCore()

class GenSourceBackend: 
    def __init__(self):
        self.root_link = 'output/'
        self.templates_link = 'templates/'


    def gen_backend_java(self, idproject):
        project_name = models.Project.objects.filter(id = idproject)[0].name

        # copy sample to output
        core_gen.copy_folder('backend/java', project_name + '/backend')

        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        params = []
        
        if len(tables) > 0:
            content = ''
            import_services = ''
            ans = 400
            print(serializers.serialize("json",  tables)) 

            #*******************************************************
            # *********Gen Base Bussiness logic
            #*******************************************************
            param1 = ''
            param2 = '' 
            params = []

            for table in tables:  
                params = []
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print('phuongphuong', ans)
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0


                temp = str(ans) + classname_origin 

                param1 = param1+'	@Autowired                                                \n'
                param1 = param1+'	B'+ temp +'BusinessLogic b'+ temp +'BusinessLogic;  \n\n'

                param2 = param2 +'		// call roleDetail business logic                            \n'
                param2 = param2 +'		if (what >= '+ str(ans) +' && what < '+ str(ans+100) +') {                             \n'
                param2 = param2 +'			return b'+ temp +'BusinessLogic.execute(what, param); \n'
                param2 = param2 +'		}                                                            \n\n'

                ans = ans + 100  

            # gen base bussiness logic 
            source_path_template = 'backend/template/java/BaseBusinessLogic.java'
            dest_path_template = project_name + '/backend/src/main/java/com/izisoftware/gencode/businesslogic/BaseBusinessLogic.java'
            core_gen.render_replace_template(source_path_template, dest_path_template, [param1, param2])


            
            #*******************************************************
            # *********Gen Bussiness logic
            #*******************************************************
            param1 = ''
            param2 = ''
            param3 = ''
            param4 = ''
            ans = 400
            params = []

            for table in tables:  
                params = []
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print('phuongphuong', ans)
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

                # gen bussiness logic 
                source_path_template = 'backend/template/java/B300RoleDetailBusinessLogic.java'
                class_name = 'B' + str(ans) + classname_origin
                dest_path_template = project_name + '/backend/src/main/java/com/izisoftware/gencode/businesslogic/' + class_name  + 'BusinessLogic.java'

                param1 = str(ans) + classname_origin

                core_gen.render_replace_template(source_path_template, dest_path_template, [param1, str(int(ans/10))])

                # gen bussiness logic implement
                source_path_template = 'backend/template/java/B300RoleDetailBusinessLogicImplement.java'
                dest_path_template = project_name + '/backend/src/main/java/com/izisoftware/gencode/businesslogic/' + class_name  + 'BusinessLogicImplement.java'

                core_gen.render_replace_template(source_path_template, dest_path_template, [param1, str(int(ans/10))])


                ans = ans + 100  
            
            #*******************************************************
            # *********Gen DataAccess
            #*******************************************************
            ans = 400
            params = []

            for table in tables:
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0  

                params = []
                
                params.append(str(ans) + classname_origin)
                params.append(str(int(ans/10))) 
                params.append(','.join(properties))
                params.append(','.join(properties[1:]))

                # create insert value => VALUES('"+r300Model.getIdRole()+"','"+r300Model.getIdMenu()+"')
                insert_value = ','.join(list(map(lambda x: '\'"+r'+str(ans)+'Model.get'+x[:1].capitalize()+x[1:]+'()+"\'' , properties[1:])))
                params.append(insert_value)

                # create update value => IdRole='"+r300Model.getIdRole()+"',IdMenu='"+r300Model.getIdMenu()+"'
                update_value = ','.join(list(map(lambda x: x + ''+x+'=\'"+r'+str(ans)+'Model.get'+x[:1].capitalize()+x[1:]+'()+"\'' + x, properties[1:])))
                params.append(update_value) 

                # Replace param  
                source_path_template = 'backend/template/java/D300RoleDetailDataAccess.java'
                class_name = 'D' + str(ans) + classname_origin
                dest_path_template = project_name + '/backend/src/main/java/com/izisoftware/gencode/dao/' + class_name  + 'DataAccess.java'
                core_gen.render_replace_template(source_path_template, dest_path_template, params)

                ans = ans + 100

            #*******************************************************
            # *********Gen Entity
            #*******************************************************
            ans = 400
            params = [] 

            for table in tables:   
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

                params = []
                
                params.append(str(ans) + classname_origin.lower())    
                
                param1 = ''
                param2 = ''
                for index in range(0,len(properties)):
                    param1 = param1 + '	@EColumn(name="'+ properties[index] +'")\n'
                    type_variable = ''
                    if attributes[index] == 'VARCHAR' or attributes[index] == 'TEXT' or attributes[index] == 'DATETIME' or attributes[index] == 'DATE':
                        type_variable = 'String'
                    elif attributes[index] == 'BOOLEAN':
                        type_variable = 'Boolean'
                    else:
                        type_variable = attributes[index].lower()

                    param1 = param1 + '	public '+ type_variable +' '+ properties[index] +';\n\n'

                    param2 = param2 +'	/**                              \n'
                    param2 = param2 +'	 * @return the '+ properties[index] +'                \n'
                    param2 = param2 +'	 */                              \n'
                    param2 = param2 +'	public '+ type_variable +' get'+ properties[index][0:1].capitalize() + properties[index][1:] +'() {             \n'
                    param2 = param2 +'		return '+ properties[index] +';                   \n'
                    param2 = param2 +'	}                                \n'
                    param2 = param2 +'                                   \n'
                    param2 = param2 +'	/**                              \n'
                    param2 = param2 +'	 * @param '+ properties[index] +' the '+ properties[index] +' to set       \n'
                    param2 = param2 +'	 */                              \n'
                    param2 = param2 +'	public void set'+ properties[index][0:1].capitalize() + properties[index][1:] +'('+ type_variable +' '+ properties[index] +') {      \n'
                    param2 = param2 +'		this.'+ properties[index] +' = '+ properties[index] +';                \n'
                    param2 = param2 +'	}                                \n'

                params.append(param1)
                params.append(param2)  

                # create toString value => id=" + id + ", IdRole=" + IdRole + "
                to_string_value = ','.join(list(map(lambda x: ''+x+'=" + '+x+' + "' , properties)))
                params.append(to_string_value) 
                # phuong4
                params.append(str(ans) + classname_origin)  

                # Replace param  
                source_path_template = 'backend/template/java/E300RoleDetail.java'
                class_name = 'E' + str(ans) + classname_origin
                dest_path_template = project_name + '/backend/src/main/java/com/izisoftware/gencode/domain/entity/' + class_name  + '.java'
                core_gen.render_replace_template(source_path_template, dest_path_template, params)
                 
                ans = ans + 100


            #*******************************************************
            # *********Gen Request
            #*******************************************************
            ans = 400
            params = [] 

            for table in tables:   
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

                params = []
                
                params.append(str(ans))   
                
                param1 = ''
                param2 = ''
                for index in range(0,len(properties)):
                    param1 = param1 + '	public String '+ properties[index] +';\n\n'

                    param2 = param2 +'	/**                              \n'
                    param2 = param2 +'	 * @return the '+ properties[index] +'                \n'
                    param2 = param2 +'	 */                              \n'
                    param2 = param2 +'	public String get'+ properties[index][0:1].capitalize() + properties[index][1:] +'() {             \n'
                    param2 = param2 +'		return '+ properties[index] +';                   \n'
                    param2 = param2 +'	}                                \n'
                    param2 = param2 +'                                   \n'
                    param2 = param2 +'	/**                              \n'
                    param2 = param2 +'	 * @param '+ properties[index] +' the '+ properties[index] +' to set       \n'
                    param2 = param2 +'	 */                              \n'
                    param2 = param2 +'	public void set'+ properties[index][0:1].capitalize() + properties[index][1:] +'(String '+ properties[index] +') {      \n'
                    param2 = param2 +'		this.'+ properties[index] +' = '+ properties[index] +';                \n'
                    param2 = param2 +'	}                                \n'

                params.append(param1)
                params.append(param2)

                # Replace param  
                source_path_template = 'backend/template/java/R300Model.java'
                class_name = 'R' + str(ans) + 'Model'
                dest_path_template = project_name + '/backend/src/main/java/com/izisoftware/gencode/domain/request/' + class_name  + '.java'
                core_gen.render_replace_template(source_path_template, dest_path_template, params)
                 
                ans = ans + 100
 


    def gen_backend_dotnet(self, idproject):
        project_name = models.Project.objects.filter(id = idproject)[0].name

        # copy sample to output
        core_gen.copy_folder('backend/netcore3.1', project_name + '/backend')

        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        params = []
        
        if len(tables) > 0:
            content = ''
            import_services = ''
            ans = 400
            print(serializers.serialize("json",  tables)) 

            #*******************************************************
            # *********Gen api proxy 
            #*******************************************************
            param1 = ''
            param2 = ''
            param3 = ''
            param4 = ''
            params = []

            for table in tables:  
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0
                param1 = param1 + '        private C'+str(ans)+''+classname_origin+'Controller _c'+str(ans)+''+classname_origin+'Controller; \n'
                param2 = param2 + '            ID'+str(ans)+''+classname_origin+'DataAccess d'+str(ans)+''+classname_origin+'DataAccess, \n'
                param3 = param3 + '            _c'+str(ans)+''+classname_origin+'Controller = new C'+str(ans)+''+classname_origin+'Controller(d'+str(ans)+''+classname_origin+'DataAccess, mapper, logger); \n'
                param4 = param4 + '                // Redirect to '+classname_origin+' controller \n'
                param4 = param4 + '                case '+str(ans)+': \n'
                param4 = param4 + '                    return await _c'+str(ans)+''+classname_origin+'Controller.execute(what, paramRequest); \n \n'

                ans = ans + 100

            params.append(param1)
            params.append(param2)
            params.append(param3)
            params.append(param4)
            # Copy include for ProxyApiController.cs  
            source_path_template = 'backend/netcore3.1/ApiGen/ApiGen/API/v1/ProxyApiController.cs'

            # copy for ProxyApiController.cs
            dest_path_template = project_name + '/backend/ApiGen/ApiGen/API/v1/ProxyApiController.cs'
            core_gen.render_replace_template(source_path_template, dest_path_template, params)

            #*******************************************************
            # *********Gen controller
            #*******************************************************
            ans = 400
            params = []

            # copy file 
            # Copy include for TemplateController.cs
            source_path_template = 'backend/netcore3.1/ApiGen/ApiGen/API/v1/TemplateController.cs'   

            for table in tables:   
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0  

                params = []

                # Create file template controller
                dest_path_template = project_name + '/backend/ApiGen/ApiGen/API/v1/C'+str(ans)+''+classname_origin+'Controller.cs'
                core_gen.render_replace(source_path_template, dest_path_template, params) 

                
                params.append(str(ans//10))
                params.append(classname_origin)
                params.append(classname_origin[0].lower()+classname_origin[1:])

                # Replace param  
                core_gen.render_replace_template('output/' + dest_path_template, dest_path_template, params, True) 
                ans = ans + 100
            
            
            #*******************************************************
            # *********Gen DataAccess interface
            #*******************************************************
            ans = 400
            params = []

            # copy file 
            # Copy include for TemplateIDataAccess.cs
            source_path_template = 'backend/netcore3.1/ApiGen/ApiGen/Contracts/DataAccess/TemplateIDataAccess.cs'   

            for table in tables:   
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0  

                params = []

                # Create file template controller
                dest_path_template = project_name + '/backend/ApiGen/ApiGen/Contracts/DataAccess/ID'+str(ans)+''+classname_origin+'DataAccess.cs'
                core_gen.render_replace(source_path_template, dest_path_template, params) 

                
                params.append(str(ans//10))
                params.append(classname_origin)
                params.append(classname_origin[0].lower()+classname_origin[1:])

                # Replace param  
                core_gen.render_replace_template('output/' + dest_path_template, dest_path_template, params, True) 
                ans = ans + 100
            
            
            #*******************************************************
            # *********Gen DataAccess
            #*******************************************************
            ans = 400
            params = []

            # copy file 
            # Copy include for TemplateIDataAccess.cs
            source_path_template = 'backend/netcore3.1/ApiGen/ApiGen/Data/DataAccess/TemplateDataAccess.cs'   

            for table in tables:   
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0  

                params = []

                # Create file template controller
                dest_path_template = project_name + '/backend/ApiGen/ApiGen/Data/DataAccess/D'+str(ans)+''+classname_origin+'DataAccess.cs'
                core_gen.render_replace(source_path_template, dest_path_template, params) 

                
                params.append(str(ans//10))
                params.append(classname_origin)
                params.append(classname_origin[0].lower()+classname_origin[1:])
                params.append(','.join(properties))
                params.append(','.join(properties[1:]))
                # create insert value => VALUES(@Name,@Born,@Detail,@Address)
                insert_value = ','.join(list(map(lambda x: '@'+ x , properties)))
                params.append(insert_value)

                # create update value => Name=@Name,Born=@Born,Detail=@Detail
                update_value = ','.join(list(map(lambda x: x + '=@' + x, properties[1:])))
                params.append(update_value) 

                # Replace param  
                core_gen.render_replace_template('output/' + dest_path_template, dest_path_template, params, True) 
                ans = ans + 100


                
            #*******************************************************
            # *********Gen Entity
            #*******************************************************
            ans = 400
            params = []
            content_contracts = ''

            # copy file 
            # Copy include for TemplateIDataAccess.cs
            source_path_template = 'backend/netcore3.1/ApiGen/ApiGen/Data/DataAccess/TemplateDataAccess.cs'   

            for table in tables:   
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0  
                
                temp = str(ans) + classname_origin

                # create contract mapping
                content_contracts = content_contracts + '            services.AddTransient<ID'+temp+'DataAccess, D'+temp+'DataAccess>();\n'

                name_file = 'E'+str(ans)+''+classname_origin+'.cs'

                content = 'using System;                         \n'
                content = content + '                                      \n'
                content = content + 'namespace ApiGen.Data.Entity          \n'
                content = content + '{                                     \n'
                content = content + '    public class E'+str(ans)+''+classname_origin+' : EntityBase \n'
                content = content + '    {                                 \n'
                content = content + '        public long id { get; set; }  \n'
                content = content + ''.join(list(map(lambda x: '        public string ' + x +' { get; set; }\n', properties[1:])))
                content = content + '    }                                 \n'
                content = content + '}                                     \n' 

                core_gen.create_file(project_name + '/backend/ApiGen/ApiGen/Data/Entity/' + name_file, content)
                 
                ans = ans + 100

            #*******************************************************
            # *********Gen Contracts Mapping
            #*******************************************************
            path_contract = project_name + '/backend/ApiGen/ApiGen/Infrastructure/Installers/RegisterContractMappings.cs'
            core_gen.render_replace_template('output/' + path_contract, path_contract, [content_contracts], True) 






    
    def gen_backend_php(self, idproject):
        project_name = models.Project.objects.filter(id = idproject)[0].name

        # copy sample to output
        core_gen.copy_folder('backend/php.v1', project_name + '/backend')

        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        params = [] 
        
        if len(tables) > 0: 
            content = ''
            import_services = ''
            ans = 400
            print(serializers.serialize("json",  tables)) 
            for table in tables:  
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname_origin)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0 

                # gen data access
                classname = 'p' + str(ans) + classname_origin
                name_file = 'p' + classname_origin

                content = content + '<?php                                                                                      \n'
                content = content + '	class '+ classname_origin +'DA{				\n'
                content = content + '		public function '+ classname_origin +'DataAccess($what, $param){ 			\n'
                content = content + '            switch ($what) {                                                                 \n'
                content = content + '                //******************'+ classname +'************************             \n'
                content = content + '                // '+ classname +'('+ ','.join(properties) +')\n'

                properties = properties[1:]

                content = content + '                // Get all data from '+ classname +'\n'
                content = content + '                case '+ str(ans) +': {                                                                        \n'
                content = content + '                    return "SELECT * FROM '+ classname +'";\n'
                content = content + '                }                                                                                  \n'
                content = content + '                                                                                                   \n'
                content = content + '                // Insert data to '+ classname +'\n'
                content = content + '                case '+ str(ans + 1) +': {                                                                        \n'
                content = content + '                    return "INSERT INTO '+ classname +'('+ ','.join(properties) +')\n'

                # create insert value => VALUES('$param->name','$param->address')
                insert_value = ','.join(list(map(lambda x: '\'$param->'+ x +'\'', properties)))

                content = content + '                            VALUES('+ insert_value +')";                               \n'
                content = content + '                }                                                                                  \n'
                content = content + '                                                                                                   \n'
                content = content + '                // Update data '+ classname +'\n'
                content = content + '                case '+ str(ans + 2) +': {\n'

                # create update value => name='$param->name', address='$param->address'
                update_value = ','.join(list(map(lambda x: x + '=\'$param->' + x+'\'', properties)))

                content = content + '                    return "UPDATE '+ classname +' SET '+ update_value +'\n'
                content = content + '                            WHERE id=\'$param->id\'";                                                 \n'
                content = content + '                }                                                                                  \n'
                content = content + '                                                                                                   \n'
                content = content + '                // Delete data of '+ classname +'\n'
                content = content + '                case '+ str(ans + 3) +': {                                                                        \n'
                content = content + '                    return "DELETE FROM '+ classname +'\n'
                content = content + '                            WHERE id IN($param->listid)";                                           \n'
                content = content + '                }                                                                                  \n'
                content = content + '                                                                                                   \n'
                content = content + '                // Find data with id '+ classname +'\n'
                content = content + '                case '+ str(ans + 4) +': {                                                                        \n'
                content = content + '                    return "SELECT * FROM '+ classname +'\n'
                content = content + '                            WHERE id=\'$param->id\'";                                                 \n'
                content = content + '                }                                                                                  \n'
                content = content + '                                                                                                   \n'
                content = content + '                // Select with pagination(offset, number-item-in-page) '+ classname +'\n'
                content = content + '                case '+ str(ans + 5) +': {                                                                        \n'
                content = content + '                    return "SELECT *                                                                              \n'
                content = content + '                            FROM (SELECT id FROM '+ classname +' $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     \n'
                content = content + '                            INNER JOIN '+ classname +' T2 ON T1.id = T2.id";                                     \n'                
                content = content + '                }                                                                                                 \n'
                content = content + '                                                                                                   \n'
                content = content + '                // Count number item of '+ classname +'\n'
                content = content + '                case '+ str(ans + 6) +': {                                                                        \n'
                content = content + '                    return "SELECT COUNT(1) FROM '+ classname +' $param->condition";\n'
                content = content + '                }                                                                                  \n'
                content = content + '            }                                                                                      \n'
                content = content + '		}                                                                                      \n'
                content = content + '	}                                                                                      \n'
                content = content + '?>                                                                                         \n'
                
                # create service file include "include_once "000account.php"; ";
                import_services = import_services + 'include_once "'+ str(ans) + name_file +'.php";\n'
                core_gen.create_file(project_name + '/backend/P3DataAccess/Admin/' + str(ans) + name_file + '.php', content)
                core_gen.create_file(project_name + '/backend/P3DataAccess/User/' + str(ans) + name_file + '.php', content)
                core_gen.create_file(project_name + '/backend/P3DataAccess/Mobile/' + str(ans) + name_file + '.php', content)
                print('phuong end ')
                content = ''

                # gen bussiness logic
                content = content + '<?php                                                                            \n'
                content = content + '// learn php basic: https://www.w3schools.com/php/default.asp                    \n'
                content = content + '                                                                                 \n'
                content = content + 'switch ($param->what) {                                                          \n'
                content = content + '        //******************'+ classname +'************************             \n'
                content = content + '        // '+ classname +'('+ ','.join(properties) +')\n'
                content = content + '                                                                                 \n'
                content = content + '        // Get all data from '+ classname +'                                      \n'
                content = content + '        case '+ str(ans) +': {                                                              \n'
                content = content + '                $'+classname_origin+' = new '+classname_origin+'DA();\n'
                content = content + '                $sql = $'+classname_origin+'->'+classname_origin+'DataAccess("'+ str(ans) +'", $param);               \n'
                content = content + '                $result = $baseQuery->execSQL($sql);                             \n'
                content = content + '                                                                                 \n'
                content = content + '                echo json_encode($result);                                       \n'
                content = content + '                break;                                                           \n'
                content = content + '        }                                                                        \n'
                content = content + '                                                                                 \n'
                content = content + '        // Insert data to '+ classname +'                                         \n'
                content = content + '        case '+ str(ans + 1) +': {                                                              \n'
                content = content + '                $'+classname_origin+' = new '+classname_origin+'DA();\n'
                content = content + '                $sql = $'+classname_origin+'->'+classname_origin+'DataAccess("'+ str(ans + 1) +'", $param);               \n'
                content = content + '                $result = $baseQuery->execSQL($sql);                             \n'
                content = content + '                                                                                 \n'
                content = content + '                echo json_encode($result);                                       \n'
                content = content + '                break;                                                           \n'
                content = content + '        }                                                                        \n'
                content = content + '                                                                                 \n'
                content = content + '        // Update data '+ classname +'                                            \n'
                content = content + '        case '+ str(ans + 2) +': {                                                              \n'
                content = content + '                $'+classname_origin+' = new '+classname_origin+'DA();\n'
                content = content + '                $sql = $'+classname_origin+'->'+classname_origin+'DataAccess("'+ str(ans + 2) +'", $param);               \n'
                content = content + '                $result = $baseQuery->execSQL($sql);                             \n'
                content = content + '                                                                                 \n'
                content = content + '                echo json_encode($result);                                       \n'
                content = content + '                break;                                                           \n'
                content = content + '        }                                                                        \n'
                content = content + '                                                                                 \n'
                content = content + '        // Delete data of '+ classname +'                                         \n'
                content = content + '        case '+ str(ans + 3) +': {                                                              \n'
                content = content + '                $'+classname_origin+' = new '+classname_origin+'DA();\n'
                content = content + '                $sql = $'+classname_origin+'->'+classname_origin+'DataAccess("'+ str(ans + 3) +'", $param);               \n'
                content = content + '                                                                                 \n'
                content = content + '                $result = $baseQuery->execSQL($sql);                             \n'
                content = content + '                                                                                 \n'
                content = content + '                echo json_encode($result);                                       \n'
                content = content + '                break;                                                           \n'
                content = content + '        }                                                                        \n'
                content = content + '                                                                                 \n'
                content = content + '        // Find data with id '+ classname +'                                      \n'
                content = content + '        case '+ str(ans + 4) +': {                                                              \n'
                content = content + '                $'+classname_origin+' = new '+classname_origin+'DA();\n'
                content = content + '                $sql = $'+classname_origin+'->'+classname_origin+'DataAccess("'+ str(ans + 4) +'", $param);               \n'
                content = content + '                                                                                 \n'
                content = content + '                $result = $baseQuery->execSQL($sql);                             \n'
                content = content + '                                                                                 \n'
                content = content + '                echo json_encode($result);                                       \n'
                content = content + '                break;                                                           \n'
                content = content + '        }                                                                        \n'
                content = content + '                                                                                 \n'
                content = content + '        // Select with pagination(offset, number-item-in-page) '+ classname +'    \n'
                content = content + '        case '+ str(ans + 5) +': {                                                              \n'
                content = content + '                $'+classname_origin+' = new '+classname_origin+'DA();\n'
                content = content + '                $sql = $'+classname_origin+'->'+classname_origin+'DataAccess("'+ str(ans + 5) +'", $param);               \n'
                content = content + '                                                                                 \n'
                content = content + '                $result = $baseQuery->execSQL($sql);                             \n'
                content = content + '                                                                                 \n'
                content = content + '                echo json_encode($result);                                       \n'
                content = content + '                break;                                                           \n'
                content = content + '        }                                                                        \n'
                content = content + '                                                                                 \n'
                content = content + '        // Count number item of '+ classname +'                                   \n'
                content = content + '        case '+ str(ans + 6) +': {                                                              \n'
                content = content + '                $'+classname_origin+' = new '+classname_origin+'DA();\n'
                content = content + '                $sql = $'+classname_origin+'->'+classname_origin+'DataAccess("'+ str(ans + 6) +'", $param);               \n'
                content = content + '                                                                                 \n'
                content = content + '                $result = $baseQuery->execSQL($sql);                             \n'
                content = content + '                                                                                 \n'
                content = content + '                echo json_encode($result);                                       \n'
                content = content + '                break;                                                           \n'
                content = content + '        }                                                                        \n'
                content = content + '                                                                                 \n'
                content = content + '}                                                                                \n'

                core_gen.create_file(project_name + '/backend/P2BussinessLogic/Admin/' + str(ans) + name_file + '.php', content)
                core_gen.create_file(project_name + '/backend/P2BussinessLogic/User/' + str(ans) + name_file + '.php', content)
                core_gen.create_file(project_name + '/backend/P2BussinessLogic/Mobile/' + str(ans) + name_file + '.php', content)
                print('phuong end ')
                content = ''
                
                ans = ans + 100 

            params.append(import_services)

            # Copy include for _init.php BussinessLogic  
            source_path_template = 'backend/php.v1/P2BussinessLogic/Admin/_init.php'

            # copy for Admin
            dest_path_template = project_name + '/backend/P2BussinessLogic/Admin/_init.php'
            core_gen.render_replace(source_path_template, dest_path_template, params)

            # copy for Mobile
            dest_path_template = project_name + '/backend/P2BussinessLogic/Mobile/_init.php'
            core_gen.render_replace(source_path_template, dest_path_template, params)

            # copy for User
            dest_path_template = project_name + '/backend/P2BussinessLogic/User/_init.php'
            core_gen.render_replace(source_path_template, dest_path_template, params)

            # Copy include for _init.php DataAccess  
            source_path_template = 'backend/php.v1/P2BussinessLogic/Admin/_init.php'

            # copy for Admin
            dest_path_template = project_name + '/backend/P3DataAccess/Admin/_init.php'
            core_gen.render_replace(source_path_template, dest_path_template, params)

            # copy for Mobile
            dest_path_template = project_name + '/backend/P3DataAccess/Mobile/_init.php'
            core_gen.render_replace(source_path_template, dest_path_template, params)

            # copy for User
            dest_path_template = project_name + '/backend/P3DataAccess/User/_init.php'
            core_gen.render_replace(source_path_template, dest_path_template, params)

            # update param database
            params = [project_name]
            source_path_template = 'backend/php.v1/P0Ultil/U0BaseConnectionMySQL.php'
            dest_path_template = project_name + '/backend/P0Ultil/U0BaseConnectionMySQL.php'
            core_gen.render_replace(source_path_template, dest_path_template, params)

        else:
            print('project havent class')
    
