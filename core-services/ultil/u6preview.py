from django.core import serializers
import time

# from models import User
from django.apps import apps
from apps.a1crud import models
from ultil.u1core import GenSourceCore
core_gen = GenSourceCore()

class GenPreview:
    def __init__(self):
        self.output_link = 'output/'
        self.root_link = 'link/'

    def gen_source_copy(self, idproject):
        project_name = models.Project.objects.filter(id=idproject)[0].name

        # disable auto build of angular
        source_path_output = 'templates/temp/start/app.module.ts'
        dest_path_project = self.root_link + '/frontend/src/app/app.module.ts'
        core_gen.copy_file_custom(source_path_output, dest_path_project)

        # code database
        source_path_output = self.output_link + project_name + '/database/output.sql'
        dest_path_project = self.root_link + '/database'
        core_gen.copy_file_custom(source_path_output, dest_path_project)

        # copy backend
        source_path_output = self.output_link + project_name + '/backend'
        dest_path_project = self.root_link + '/backend/gen'
        core_gen.copy_folder_custom(source_path_output, dest_path_project)

        # copy front end
        # source_path_output = self.output_link + project_name + '/frontend/src/app/common'
        # dest_path_project = self.root_link + '/frontend/src/app/common'
        # core_gen.copy_folder_custom(source_path_output, dest_path_project)

        source_path_output = self.output_link + project_name + '/frontend/src/app/home-page/content'
        dest_path_project = self.root_link + '/frontend/src/app/home-page/content'
        core_gen.copy_folder_custom(source_path_output, dest_path_project)

        # exec sql bat file
        # import os
        # os.system("link/database/exec-sql.bat")

        return project_name

    def gen_source_preview(self, idproject):
        project_name = models.Project.objects.filter(id=idproject)[0].name

        # enable auto build of angular
        source_path_output = 'templates/temp/end/app.module.ts'
        dest_path_project = self.root_link + '/frontend/src/app/app.module.ts'
        core_gen.copy_file_custom(source_path_output, dest_path_project)

        return project_name
