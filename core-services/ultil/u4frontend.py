from django.core import serializers

# from models import User
from django.apps import apps 

from apps.a1crud import models

from ultil.u1core import GenSourceCore 
core_gen = GenSourceCore()

class GenSourceFrontend:
    def __init__(self):
        self.root_link = 'output/'
        self.templates_link = 'templates/'
        
    
    def gen_frontend_angular_for_nodejs(self, idproject):
        project_name = models.Project.objects.filter(id = idproject)[0].name

        print('begin copy folder frontend/angular-nodejs')
        # copy sample to output
        core_gen.copy_folder('frontend/angular-nodejs', project_name + '/frontend')
        print('end copy folder frontend/angular-nodejs')

        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        
        if len(tables) > 0: 
            #*******************************************************
            # ********* Gen Route
            #*******************************************************
            ans = 50
            ans1 = 50
            param1 = ''
            param2 = '' 
            param3 = '' 
            param4 = '' 
            params = []

            for table in tables:  
                params = []
                classname_origin = table.classname
                classname_snake = core_gen.to_snake_case(table.classname)
                classname_camel = core_gen.first_lower(table.classname)
                classname_name_file = core_gen.to_name_file_case(table.classname)
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print('phuongphuong', ans)
                print('classname_origin', classname_origin)
                print('classname_snake', classname_snake)
                print('classname_camel', classname_camel)
                print('classname_name_file', classname_name_file)
                print('properties', properties) #id,name,address
                print('attributes', attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

                param1 = classname_origin  

                # 60sinh-vien.model
                temp_model = str(ans) + classname_name_file +'.model'

                #     lop?: string;
                param2 = ';\n'.join(list(map(lambda x: '    '+core_gen.first_lower(x)+'?: string' , properties[1:])))
 
                ans = ans + 10

                # gen model
                source_path_template = 'frontend/template/angular-nodejs/60sinhvien.model.ts'
                dest_path_template = project_name + '/frontend/src/app/@core/models/'+ temp_model + '.ts'
                core_gen.render_replace_template(source_path_template, dest_path_template, [param1, param2])

                #*******************************************************
                # ********* Gen Service
                #*******************************************************
                # 60sinh-vien.service
                temp_service = str(ans1) + classname_name_file +'.service'

                #     lop?: string;
                param2 = ';\n'.join(list(map(lambda x: '    '+core_gen.first_lower(x)+'?: string' , properties[1:])))
 
                ans1 = ans1 + 10 

                # gen service
                source_path_template = 'frontend/template/angular-nodejs/60sinh-vien.service.ts'
                dest_path_template = project_name + '/frontend/src/app/@core/services/base/'+ temp_service + '.ts'
                core_gen.render_replace_template(source_path_template, dest_path_template, [param1, temp_model, classname_name_file, classname_camel])

                # core.module.ts
                param3 = param3 + "import { "+ classname_origin +"Service } from './services/base/"+ temp_service +"';\n"
                param4 = param4 + "    "+ classname_origin +"Service,\n"
            
            # gen index model
            source_path_template = 'frontend/template/angular-nodejs/core.module.ts'
            dest_path_template = project_name + '/frontend/src/app/@core/core.module.ts'
            core_gen.render_replace_template(source_path_template, dest_path_template, [param3, param4]) 


            #*******************************************************
            # ********* Gen Component
            #*******************************************************
            content = ''
            html_menu = ''
            param_permision_menu = ''
            load_permision_menu = ''
            content_module = ''

            roles = ''
            permission = ''

            ans = 4
            ans1 = 50
            # print(serializers.serialize("json",  tables))
            for table in tables:  
                classname = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

               
                # create content module 
                content_module = content_module + '          {                                                               \n'
                content_module = content_module + '            path: \'c'+  str(ans+1) +'-'+core_gen.to_name_file_case(classname)+'\',                                               \n'
                content_module = content_module + '            loadChildren: () =>                                           \n'
                name_folder_module = 'c'+  str(ans+1) +'-' + core_gen.to_name_file_case(classname)
                name_module = 'C'+  str(ans+1) +'' + classname
                content_module = content_module + '              import(\'./'+ name_folder_module +'/'+ name_folder_module +'.module\').then((m) => m.'+ name_module +'Module), \n'
                content_module = content_module + '          },                                          \n'  

                # create main module
                # create folder
                component_name = 'c' + str(ans+1) + '-' + core_gen.to_name_file_case(classname)
                module_path_template = '/frontend/angular-nodejs/src/app/home-page/content/'
                module_path = project_name + '/frontend/src/app/home-page/content/'
                core_gen.create_folder(module_path + component_name) 

                # dialog
                # load data crud for insert and update
                crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                c_inputtypes_temp = ['']
                if len(crud) > 0:
                    crud = crud[0]
                    c_titles = crud.titles.split(',')
                    c_properties = crud.properties.split(',')
                    c_alias = crud.alias.split(',')
                    c_refers = crud.refers.split(',')
                    c_inputtypes = crud.inputtypes.split(',')
                    c_inputtypes_temp.extend(c_inputtypes)
                    c_validates = crud.validates.split(',')
                    c_formats = crud.formats.split(',')
                    print('dialgo ne dialgo ne dialgo ne ')
                    print(c_properties)  # id,name,address
                    print(c_refers)  # INT,VARCHAR,VARCHAR
                    print(c_inputtypes)  # INT,VARCHAR,VARCHAR
                    print(c_validates)  # 0,255,0

                    params = []
                    param = ''
                    params.append(crud.titlecreate)

                    # add dialog controls
                    for index in range(len(c_properties)):
                        param = param + '        <!-- '+c_properties[index]+' field -->                                                           \n'
                        param = param + '        <div class="col-md-12">                                                       \n'

                        camel_propertie = core_gen.first_lower(c_properties[index])

                        # check control is varchar
                        if c_inputtypes[index] == 'varchar':
                            param = param + '          <mat-form-field class="example-full-width">                                 \n'
                            param = param + '            <mat-label i18n>'+c_titles[index]+' <span class="text-red">*</span></mat-label>           \n'
                            param = param + '            <input matInput [(ngModel)]="input.'+camel_propertie+'" formControlName="'+camel_propertie+'">          \n'
                            param = param + '            <button mat-button *ngIf="input.'+camel_propertie+'" matSuffix mat-icon-button           \n'
                            param = param + '             aria-label="Clear" (click)="input.'+camel_propertie+'=\'\'">                            \n'
                            param = param + '              <mat-icon>close</mat-icon>                                              \n'
                            param = param + '            </button>                                                                 \n'
                            param = param + '          </mat-form-field>                                                           \n'

                        # check control is date or datetime
                        if c_inputtypes[index] == 'date' or c_inputtypes[index] == 'datetime':
                            param = param + '          <mat-form-field class="example-full-width" appearance="fill">                                   \n'
                            param = param + '            <mat-label i18n>'+c_titles[index]+'<span class="text-red">*</span></mat-label>                          \n'
                            param = param + '            <input matInput [(ngModel)]="input.'+camel_propertie+'" formControlName="'+camel_propertie+'" [matDatepicker]="picker'+c_properties[index]+'"> \n'
                            param = param + '            <mat-datepicker-toggle matSuffix [for]="picker'+c_properties[index]+'"></mat-datepicker-toggle>                  \n'
                            param = param + '            <mat-datepicker touchUi #picker'+c_properties[index]+'></mat-datepicker>                                         \n'
                            param = param + '          </mat-form-field>                                                                               \n'

                        # check control is text
                        if c_inputtypes[index] == 'ckeditor':
                            param = param + '          <mat-label i18n>'+c_titles[index]+'<span class="text-red">*</span></mat-label>                             \n'
                            param = param + '          <ckeditor [(ngModel)]="input.'+camel_propertie+'" formControlName="'+camel_propertie+'" [editor]="editor">" \n'
                            param = param + '          </ckeditor>                                                                                     \n'

                        # check control is radio
                        if c_inputtypes[index] == 'radio':
                            param = param + '          <mat-label i18n>'+c_titles[index]+' <span class="text-red">*</span></mat-label>                           \n'
                            param = param + '          <mat-radio-group formControlName="'+camel_propertie+'" [(ngModel)]="input.'+camel_propertie+'" aria-label="'+c_titles[index]+'">          \n'
                            param = param + '            <mat-radio-button *ngFor="let sex of sexs" [value]="sex.value" style="padding-left: 10px;">   \n'
                            param = param + '              {{sex.viewValue}}                                                                           \n'
                            param = param + '            </mat-radio-button>                                                                           \n'
                            param = param + '          </mat-radio-group>                                                                              \n'

                        # check control is number
                        if c_inputtypes[index] == 'number':
                            param = param + '          <mat-form-field class="example-full-width">                                                     \n'
                            param = param + '            <mat-label i18n>'+c_titles[index]+' <span class="text-red">*</span></mat-label>                               \n'
                            param = param + '            <input matInput type="number" [(ngModel)]="input.'+camel_propertie+'" formControlName="'+camel_propertie+'">              \n'
                            param = param + '          </mat-form-field>                                                                               \n'

                        print('phuong oi phuong oi ', c_inputtypes)
                        # check control is select
                        if c_inputtypes[index] == 'select':
                            print('c_refersc_refersc_refersc_refers', c_refers)
                            refers = c_refers[index].split(':')
                            print('phuong1', index)
                            print('phuong1', c_inputtypes[index])
                            print('phuong1', refers)
                            if len(refers) > 2:
                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname
                                refer_classname = core_gen.first_lower(refer_classname)
                                refer_id = refers[1]
                                refer_display = refers[2]
                                refer_display = core_gen.first_lower(refer_display)

                                param = param + '          <mat-form-field class="example-full-width">                                                     \n'
                                param = param + '            <mat-label i18n>'+c_titles[index]+'<span class="text-red">*</span></mat-label>                               \n'
                                param = param + '            <mat-select formControlName="'+camel_propertie+'" [(ngModel)]="input.'+camel_propertie+'">                                \n'
                                param = param + '              <mat-option *ngFor="let '+ refer_classname +' of '+ refer_classname +'Datas" [value]="'+ refer_classname +'.'+ refer_id +'">                           \n'
                                param = param + '                {{'+ refer_classname +'.'+ refer_display +'}}                                                                            \n'
                                param = param + '              </mat-option>                                                                               \n'
                                param = param + '            </mat-select>                                                                                 \n'
                                param = param + '          </mat-form-field>                                                                               \n'

                        # check control is image
                        if c_inputtypes[index] == 'image':
                            param = param + '            <!-- images ' + c_properties[index] + ' -->  \n'
                            param = param + '            <mat-card class="example-full-width" style="box-shadow: none; padding-left: 0;">                  \n'
                            param = param + '              <mat-card-actions style="margin: 0; padding: 0;">                                               \n'
                            param = param + '                <button i18n style="border: 1px solid red; border-radius: 3px; float: left; width: 200px;"    \n'
                            param = param + '                  mat-button color="warn"                                                                     \n'
                            param = param + '                  (click)="on' + c_properties[index].capitalize() + 'UploadImageClick()">  \n'
                            param = param + '                  <mat-icon>file_upload</mat-icon>                                                            \n'
                            param = param + '                  ' + c_titles[index] + '                                                                     \n'
                            param = param + '                </button>                                                                                     \n'
                            param = param + '                <input                                                                                        \n'
                            param = param + '                  type="file"                                                                                 \n'
                            param = param + '                  formControlName="' + camel_propertie + '"  \n'
                            param = param + '                  #inputImage' + c_properties[index].capitalize() + '      \n'
                            param = param + '                  style="display: none"                                                                       \n'
                            param = param + '                  multiple="multiple"                                                                         \n'
                            param = param + '                  accept="image/*"/>                                                                          \n'
                            param = param + '              </mat-card-actions>                                                                             \n'
                            param = param + '            </mat-card><br><br>                                                                                       \n'
                            param = param + '            <img class="custom-preview-file" *ngIf="input.' + camel_propertie + '!=\'\'" [src]="input.' + camel_propertie + '" alt="izi software" width="200" height="200">\n'

                        # check control is file
                        if c_inputtypes[index] == 'file':
                            param = param + '            <!-- files ' + c_properties[index] + ' -->  \n'
                            param = param + '            <mat-card class="example-full-width" style="box-shadow: none; padding-left: 0;">                  \n'
                            param = param + '              <mat-card-actions style="margin: 0; padding: 0;">                                               \n'
                            param = param + '                <button i18n style="border: 1px solid red; border-radius: 3px; float: left; width: 200px;"    \n'
                            param = param + '                  mat-button color="warn"                                                                     \n'
                            param = param + '                  (click)="on' + c_properties[index].capitalize() + 'UploadFileClick()">  \n'
                            param = param + '                  <mat-icon>file_upload</mat-icon>                                                            \n'
                            param = param + '                  ' + c_titles[index] + '                                                                     \n'
                            param = param + '                </button>                                                                                     \n'
                            param = param + '                <input                                                                                        \n'
                            param = param + '                  type="file"                                                                                 \n'
                            param = param + '                  formControlName="' + camel_propertie + '"  \n'
                            param = param + '                  #inputFile' + c_properties[index].capitalize() + '      \n'
                            param = param + '                  style="display: none">                                                                       \n'
                            param = param + '              </mat-card-actions>                                                                             \n'
                            param = param + '            </mat-card>                                                                                       \n'
                            param = param + '            <a class="custom-preview-file" *ngIf="input.' + camel_propertie + '!=\'\'" href="{{input.' + \
                                    camel_propertie + '}}" target="_blank" style="width: 200px; text-align: center;">Upload Link</a>\n'

                        # checking validations
                        validates = c_validates[index].split(':')
                        if len(validates) > 0:
                            param = param + '                                                                                      \n'
                            param = param + '          <div class="alert alert-danger" *ngIf="form.controls[\''+camel_propertie+'\'].invalid      \n'
                            param = param + '              && (form.controls[\''+camel_propertie+'\'].dirty || form.controls[\''+camel_propertie+'\'].touched)"> \n'

                            for validate in validates:
                                if validate == 'required':
                                    param = param + '            <div *ngIf="form.controls[\''+camel_propertie+'\'].errors.required" i18n>                \n'
                                    param = param + '              Vui lòng nhập trường này.                                               \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'minLength':
                                    param = param + '            <div *ngIf="form.controls[\''+camel_propertie+'\'].errors.minlength" i18n>               \n'
                                    param = param + '              Bạn vui lòng nhập ít nhất 1 ký tự.                                      \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'maxLength':
                                    param = param + '            <div *ngIf="form.controls[\''+camel_propertie+'\'].errors.maxlength" i18n>               \n'
                                    param = param + '              Bạn không được nhập quá 100 ký tự.                                      \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'pattern':
                                    param = param + '            <div *ngIf="form.controls[\''+camel_propertie+'\'].errors.pattern" i18n>                 \n'
                                    param = param + '              Vui lòng nhập đúng định dạng.                                           \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'min':
                                    param = param + '            <div *ngIf="form.controls[\''+camel_propertie+'\'].errors.min" i18n>                     \n'
                                    param = param + '              Số không được nhỏ hơn 10.                                               \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'max':
                                    param = param + '            <div *ngIf="form.controls[\''+camel_propertie+'\'].errors.max" i18n>                     \n'
                                    param = param + '              Số không được lớn hơn 100.                                              \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'email':
                                    param = param + '            <div *ngIf="form.controls[\''+camel_propertie+'\'].errors.email" i18n>                   \n'
                                    param = param + '              Vui lòng nhập đúng định dạng email.                                     \n'
                                    param = param + '            </div>                                                                    \n'

                            param = param + '          </div>                                                                      \n'
                            param = param + '        </div>                                                                        \n\n'

                    params.append(param)
                    source_path_template = module_path_template + 'sample/sample-dialog.html'
                    dest_path_template = module_path + component_name + '/' + component_name + '-dialog.html'
                    core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # html
                # load data crud for list
                crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=0)
                if len(crud) > 0:
                    crud = crud[0]
                    c_titles = crud.titles.split(',')
                    c_properties = crud.properties.split(',')
                    c_alias = crud.alias.split(',')
                    c_refers = crud.refers.split(',')
                    c_inputtypes = crud.inputtypes.split(',')
                    c_validates = crud.validates.split(',')
                    c_formats = crud.formats.split(',')

                    print('c_inputtypes_tempc_inputtypes_temp', c_inputtypes_temp)
                    print('c_propertiesc_propertiesc_properties', c_properties)

                    params = []
                    param = ''
                    params.append(classname)

                    # check have select filter header
                    print('phuongphuongphuongphuongphuongphuong', c_properties)
                    print('phuongphuongphuongphuongphuongphuong', c_refers, c_inputtypes)
                    for index in range(len(c_properties)):
                        # check control is select
                        refers = c_refers[index].split(':')

                        camel_propertie = core_gen.first_lower(c_properties[index])

                        if c_refers[index] != '' and refers[0] != '':
                            if len(refers) > 2:
                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname.capitalize()
                                refer_id = refers[1]
                                refer_display = refers[2]

                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname

                                param = param + '    <!-- select ' + c_titles[index] + ' filter -->                     \n'
                                param = param + '    <div class="col-md-3 col-xs-12">                                   \n'
                                param = param + '      <mat-form-field appearance="fill">                               \n'
                                param = param + '        <mat-label i18n>SEARCH ' + c_titles[index] + '</mat-label>        \n'
                                param = param + '        <mat-select [(ngModel)]="' + camel_propertie + 'Model" (selectionChange)="on' + refer_classname + 'SelectionChange($event)">  \n'
                                param = param + '          <mat-option *ngFor="let item of ' + core_gen.first_lower(refer_classname) + 'Datas" [value]="item.id"> \n'
                                param = param + '            {{item.'+ core_gen.first_lower(refer_display) +'}}                                              \n'
                                param = param + '          </mat-option>                                                \n'
                                param = param + '        </mat-select>                                                  \n'
                                param = param + '      </mat-form-field>                                                \n'
                                param = param + '    </div>                                                             \n\n'

                    params.append(param)
                    param = ''

                    # check have delete action
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=3)
                    if len(crud) > 0:
                        param = '      <button type="button" class="btn btn-danger btn-del w-120" data-toggle="modal" data-target="#exampleModal" i18n><i \n'
                        param = param + '          class="mdi mdi-delete"></i>Xóa</button>                                                                        \n'
                        params.append(param)
                    else:
                        params.append('      <!--  -->')

                    # add column grid
                    param = ''
                    for index in range(len(c_properties)):
                        camel_propertie = core_gen.first_lower(c_properties[index])

                        param = param + '    <!-- '+ c_properties[index] +' Column -->                                                    \n'
                        param = param + '    <ng-container matColumnDef="'+ camel_propertie +'">                                      \n'

                        # check refer class
                        refers = c_refers[index].split(':')

                        if c_refers[index] != '' and refers[0] != '':
                            if len(refers) > 2:
                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname.capitalize()
                                refer_id = refers[1]
                                refer_display = refers[2]

                                # check alias
                                if c_alias[index] != '':
                                    param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> ' + c_alias[index] + ' </th> \n'
                                else:
                                    param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> ' + c_titles[index] + ' </th> \n'

                                # check format
                                param = param + '      <td mat-cell *matCellDef="let row"> {{getDisplay'+ refer_classname +'ById(row.'+ camel_propertie +')}} </td>\n'

                        # check control is image
                        elif c_inputtypes_temp[index] == 'image':
                            # check alias
                            if c_alias[index] != '':
                                param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> '+ c_alias[index] +' </th> \n'
                            else:
                                param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> '+ c_titles[index] +' </th> \n'

                            # check format
                            if c_formats[index] != '':
                                param = param + '      <td mat-cell *matCellDef="let row"> {{'+ c_formats[index] +'}} </td>                \n'
                            else:
                                param = param + '      <td mat-cell *matCellDef="let row"> <img src="{{row.'+ camel_propertie +'}}" style="width: 150px; height:150px;"> </td>                \n'
                        else:
                            # check alias
                            if c_alias[index] != '':
                                param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> '+ c_alias[index] +' </th> \n'
                            else:
                                param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> '+ c_titles[index] +' </th> \n'

                            # check format
                            if c_formats[index] != '':
                                param = param + '      <td mat-cell *matCellDef="let row"> {{'+ c_formats[index] +'}} </td>                \n'
                            else:
                                param = param + '      <td mat-cell *matCellDef="let row"> {{row.'+ camel_propertie +'}} </td>                \n'

                        param = param + '    </ng-container>                                                         \n\n'

                    params.append(param)

                    source_path_template = module_path_template + 'sample/sample.component.html'
                    dest_path_template = module_path + component_name + '/' + component_name + '.component.html'
                    core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # scss
                params = []
                source_path_template = module_path_template + 'sample/sample.component.scss'
                dest_path_template = module_path + component_name + '/' + component_name + '.component.scss'
                core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # ts
                # load data crud for list
                crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=0)
                if len(crud) > 0:
                    classname_camel = core_gen.first_lower(classname)
                    classname_file = core_gen.to_name_file_case(classname)
                    crud = crud[0]
                    c_titles = crud.titles.split(',')
                    c_properties = crud.properties.split(',')
                    c_alias = crud.alias.split(',')
                    c_refers = crud.refers.split(',')
                    c_inputtypes = crud.inputtypes.split(',')
                    c_validates = crud.validates.split(',')
                    c_formats = crud.formats.split(',')

                    params = []
                    param = ''
                    # update @component and export class
                    param = param + '@Component({                                                                \n'
                    param = param + '  selector: \'app-c'+ str(ans + 1) +'-'+ classname_file +'\',                                                \n'
                    param = param + '  templateUrl: \'./c'+ str(ans + 1) +'-'+ classname_file +'.component.html\',                                \n'
                    param = param + '  styleUrls: [\'./c'+ str(ans + 1) +'-'+ classname_file +'.component.scss\'],                                \n'
                    param = param + '})                                                                          \n'
                    param = param + 'export class C'+ str(ans + 1) +''+ classname +'Component implements OnInit, AfterViewInit, OnDestroy {  \n'
                    
                    # phuong0
                    params.append(param)
                    param = ''

                    # add display columns
                    for property in c_properties:
                        param = param + '    \''+ core_gen.first_lower(property) +'\',\n'

                    # phuong1
                    params.append(param)
                    param = ''

                    # update what in getLengthOfPage method
                    # phuong2
                    params.append(str(ans))

                    # add data reference binding
                    param_define = ''
                    param_call = ''
                    param_method = ''
                    param_method_filter = ''
                    for index in range(len(c_refers)):
                        c_refer = c_refers[index]
                        c_propertie = c_properties[index]

                        refers = c_refer.split(':')

                        camel_propertie = core_gen.first_lower(c_properties[index])

                        if len(refers) > 0 and refers[0] != '':
                            refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                            refer_classname = refer_class[0].classname
 
                            refer_name_file = core_gen.to_name_file_case(refer_classname)

                            # camel classname
                            camel_classname = core_gen.first_lower(refer_classname)

                            refer_id = refers[1]
                            refer_display = core_gen.first_lower(refers[2])

                            param_define = param_define + '  '+ camel_classname +'Datas: any[] = [];\n'

                            param_call = param_call + '    // load list '+ refer_classname +'\n'
                            param_call = param_call + '    this.getList'+ refer_classname.capitalize() +'();\n\n'

                            param_method = param_method + '  /**                                                            \n'
                            param_method = param_method + '   * get list '+ refer_classname +'\n'
                            param_method = param_method + '   */                                                            \n'
                            param_method = param_method + '  getList'+ refer_classname.capitalize() +'() {       \n'

                            # get what number for select
                            all_class = models.Class.objects.filter(idproject=idproject).order_by('id')
                            count = 0
                            for index in range(len(all_class)):
                                if all_class[index].classname == refer_classname:
                                    count = index
                                    break

                            param_method = param_method + '    this.subscription.push(this.'+ camel_classname +'Service.get() \n'

                            param_method = param_method + '      .subscribe((data) => {                                     \n'
                            param_method = param_method + '        data.unshift({ id: \'0\', '+ refer_display +': \'Tất cả\' });  \n'
                            param_method = param_method + '        this.'+ camel_classname +'Datas = data;  \n'
                            param_method = param_method + '      })                                                         \n'
                            param_method = param_method + '    );                                                           \n'
                            param_method = param_method + '  }                                                              \n'
                            param_method = param_method + '                                                                 \n'
                            param_method = param_method + '  /**                                                            \n'
                            param_method = param_method + '   * get name display '+ refer_classname +' by id     \n'
                            param_method = param_method + '   */                                                            \n'
                            param_method = param_method + '  getDisplay'+ refer_classname.capitalize() +'ById(id) {                                      \n'
                            param_method = param_method + '    if (id != undefined && id != null) {                                      \n'
                            param_method = param_method + '        return this.'+ camel_classname +'Datas.filter((e) => e.id == id[\'id\'])[0]?.'+ refer_display +';   \n'
                            param_method = param_method + '    }\n'
                            param_method = param_method + '    return '';\n'
                            param_method = param_method + '  }                                                              \n\n'

                            param_method_filter = param_method_filter + '  /**                                                        \n'
                            param_method_filter = param_method_filter + '   * on ' + refer_classname + ' Selection Change             \n'
                            param_method_filter = param_method_filter + '   * @param event                                            \n'
                            param_method_filter = param_method_filter + '   */                                                        \n'
                            param_method_filter = param_method_filter + '  on' + refer_classname + 'SelectionChange(event) {          \n'
                            param_method_filter = param_method_filter + '    const condition = { key: "' + camel_propertie + '", value: event.value };  \n'
                            param_method_filter = param_method_filter + '                                                             \n'
                            param_method_filter = param_method_filter + '    // add new condition to list                             \n'
                            param_method_filter = param_method_filter + '    this.addNewConditionToList(condition);                   \n'
                            param_method_filter = param_method_filter + '  }                                                          \n'

                    # phuong3
                    params.append(param_define)

                    # add for permission phuong4
                    params.append(chr(97 + ans))

                    params.append(param_call)    # phuong5
                    params.append(param_method)  # phuong6

                    # add method selection change filter # phuong7
                    params.append(param_method_filter)

                    # update what in onLoadDataGrid method # phuong8
                    params.append(str(ans))

                    # update what in onBtnDelClick method  # phuong9
                    params.append(str(ans))

                    # update name dialog in onBtnInsertDataClick method
                    params.append('C'+ str(ans + 1) +''+ classname +'Dialog')    # phuong10

                    # update name dialog in onBtnUpdateDataClick method
                    params.append('C' + str(ans + 1) + '' + classname + 'Dialog')    # phuong11

                    param = ''

                    # start for dialog component
                    # update @component and export dialog class
                    param = param + '@Component({                                              \n'
                    param = param + '  selector: \'c'+ str(ans + 1) +'-'+ classname_file +'-dialog\',                            \n'
                    param = param + '  templateUrl: \'c'+ str(ans + 1) +'-'+ classname_file +'-dialog.html\',                    \n'
                    param = param + '  styleUrls: [\'./c'+ str(ans + 1) +'-'+ classname_file +'.component.scss\'],               \n'
                    param = param + '})                                                        \n'
                    param = param + 'export class C'+ str(ans + 1) + classname +'Dialog implements OnInit, OnDestroy {  \n'

                    params.append(param)     # phuong12
                    param = ''

                    # get list for insert
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_properties = crud.properties.split(',')
                        c_validates = crud.validates.split(',')

                    # add input value for dialog insert
                    for property in c_properties:
                        param = param + '    '+ core_gen.first_lower(property) +': \'\',\n'

                    params.append(param)         # phuong13
                    param = ''

                    # add data reference binding for dialog
                    param_define = ''
                    param_call = ''
                    param_method = ''

                    # get list for insert
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_refers = crud.refers.split(',')
                        for c_refer in c_refers:
                            refers = c_refer.split(':')
                            if len(refers) > 0 and refers[0] != '':
                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname

                                refer_name_file = core_gen.to_name_file_case(refer_classname)

                                # camel classname
                                camel_classname = core_gen.first_lower(refer_classname)

                                refer_id = refers[1]
                                refer_display = core_gen.first_lower(refers[2])

                                param_define = param_define + '  ' + core_gen.first_lower(refer_classname) + 'Datas: any[] = [];\n'

                                param_call = param_call + '    // load list ' + refer_classname + '\n'
                                param_call = param_call + '    this.getList' + refer_classname.capitalize() + '();\n\n'

                                param_method = param_method + '  /**                                                            \n'
                                param_method = param_method + '   * get list ' + refer_classname + '\n'
                                param_method = param_method + '   */                                                            \n'
                                param_method = param_method + '  getList' + refer_classname.capitalize() + '() {       \n'

                                # get what number for select
                                all_class = models.Class.objects.filter(idproject=idproject).order_by('id')
                                count = 0
                                for index in range(len(all_class)):
                                    if all_class[index].classname == refer_classname:
                                        count = index
                                        break

                                param_method = param_method + '    this.subscription.push(this.'+ camel_classname +'Service.get() \n'

                                param_method = param_method + '      .subscribe((data) => {                                     \n'
                                param_method = param_method + '        this.' + core_gen.first_lower(refer_classname) + 'Datas = data;  \n'
                                param_method = param_method + '      })                                                         \n'
                                param_method = param_method + '    );                                                           \n'
                                param_method = param_method + '  }                                                              \n\n'

                    params.append(param_define)    # phuong14


                    # add define validate
                    param = ''

                    for index in range(len(c_properties)):
                        camel_propertie = core_gen.first_lower(c_properties[index])

                        param = param + '      '+ camel_propertie +': [                        \n'
                        param = param + '        null,                        \n'
                        param = param + '        [                            \n'
                        validates = c_validates[index].split(':')
                        for validate in validates:
                            if validate == 'required':
                                param = param + '          Validators.required,       \n'

                            if validate == 'minLength':
                                param = param + '          Validators.minLength(1),   \n'

                            if validate == 'maxLength':
                                param = param + '          Validators.maxLength(100), \n'

                            if validate == 'pattern':
                                param = param + '          Validators.pattern(\' * \'),   \n'

                            if validate == 'min':
                                param = param + '          Validators.min(10),        \n'

                            if validate == 'max':
                                param = param + '          Validators.max(100),       \n'

                            if validate == 'email':
                                param = param + '          Validators.email,          \n'
                        param = param + '        ],                           \n'
                        param = param + '      ],                             \n'


                    # get list for insert
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_properties = crud.properties.split(',')
                        c_inputtypes = crud.inputtypes.split(',')

                    # add image and file uploads
                    param_define = ''
                    param_method_upload = ''
                    for index in range(len(c_properties)):
                        camel_propertie = core_gen.first_lower(c_properties[index])

                        # check control is image
                        if c_inputtypes[index] == 'image':
                            param_define = param_define + '  @ViewChild("inputImage'+ c_properties[index].capitalize() +'" '', { static: false }) inputImage' + \
                                           c_properties[index].capitalize() + ': ElementRef; \n'
                            param_define = param_define + '  ' + camel_propertie + 'ImagePath: string = ""; \n\n'

                            param_method_upload = param_method_upload + '  /**                                                                             \n'
                            param_method_upload = param_method_upload + '   * on ' + c_properties[
                                index].capitalize() + ' Upload image Click                                                 \n'
                            param_method_upload = param_method_upload + '   */                                                                             \n'
                            param_method_upload = param_method_upload + '  on' + c_properties[
                                index].capitalize() + 'UploadImageClick() {                                                    \n'
                            param_method_upload = param_method_upload + '    this.commonService.uploadImageCore(this.inputImage' + \
                                                  c_properties[
                                                      index].capitalize() + ').subscribe((data) => {         \n'
                            param_method_upload = param_method_upload + '      this.input.' + c_properties[
                                index] + ' = data[\'data\']; \n'
                            param_method_upload = param_method_upload + '    });                                                                           \n'
                            param_method_upload = param_method_upload + '  } \n\n'

                        # check control is file
                        if c_inputtypes[index] == 'file':
                            param_define = param_define + '  @ViewChild("inputFile'+ c_properties[index].capitalize() +'" '', { static: false }) inputFile' + \
                                           c_properties[index].capitalize() + ': ElementRef;         \n'
                            param_define = param_define + '  ' + camel_propertie + 'FilePath: string = ""; \n'

                            param_method_upload = param_method_upload + '  /**             \n'
                            param_method_upload = param_method_upload + '   * on ' + c_properties[
                                index].capitalize() + ' Upload file Click                                           \n'
                            param_method_upload = param_method_upload + '   */                          \n'
                            param_method_upload = param_method_upload + '  on' + c_properties[
                                index].capitalize() + 'UploadFileClick() {                    \n'
                            param_method_upload = param_method_upload + '    this.commonService.uploadFileCore(this.inputFile' + \
                                                  c_properties[
                                                      index].capitalize() + ').subscribe((data) => {        \n'
                            param_method_upload = param_method_upload + '      this.input.' + c_properties[
                                index] + ' = data[\'data\']; \n'
                            param_method_upload = param_method_upload + '    });               \n'
                            param_method_upload = param_method_upload + '  }                \n\n'

                    # param define for upload image or file
                    params.append(param_define)   # phuong15

                    # update name dialog in constructor method
                    params.append('C' + str(ans + 1) + '' + classname + 'Dialog')  # phuong16

                    # add validation
                    params.append(param)  # phuong17

                    # add call reference
                    params.append(param_call) # phuong18
                    params.append(param_method) # phuong19

                    # add param for upload
                    params.append(param_method_upload) # phuong20

                    # add for mat date for field datetime
                    # get list for insert
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_properties = crud.properties.split(',')
                        c_inputtypes = crud.inputtypes.split(',')

                    format_date = ''
                    for index in range(len(c_properties)):
                        camel_propertie = core_gen.first_lower(c_properties[index])
                        if c_inputtypes[index] == 'date' or c_inputtypes[index] == 'datetime':
                            format_date = format_date + '    this.input.'+ camel_propertie +' = this.commonService.formatDate(new Date(this.input.'+ camel_propertie +')); \n'

                    params.append(format_date)  # phuong21

                    # update what in onBtnSubmitClick method phuong22
                    params.append(str(ans)) 

                    # phuong23 phuong24 phuong25 phuong30 add input model
                    param30 = ''
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=0)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_titles = crud.titles.split(',')
                        c_properties = crud.properties.split(',')
                        c_alias = crud.alias.split(',')
                        c_refers = crud.refers.split(',')
                        c_inputtypes = crud.inputtypes.split(',')
                        c_validates = crud.validates.split(',')
                        c_formats = crud.formats.split(',')

                        param23 = ''
                        param24 = ''
                        param25 = ''
                        for index in range(len(c_properties)):
                            camel_propertie = core_gen.first_lower(c_properties[index])

                            # check control is select
                            refers = c_refers[index].split(':')
                            if c_refers[index] != '' and refers[0] != '':
                                if len(refers) > 2: 
                                    param23 = param23 + '  ' + camel_propertie + 'Model: any = \'0\'; \n'
                                    param24 = param24 + '        ' + camel_propertie + 'Model: this.' + camel_propertie + 'Model,\n'
                                    param25 = param25 + '      this.input.' + camel_propertie + ' = data.' + camel_propertie + 'Model;\n'
                                    param30 = param30 + '      this.input.' + camel_propertie + ' = data.input?.' + camel_propertie + '?.id;\n'

                        # add data to params
                        params.append(param23)  # phuong23
                        params.append(param24)  # phuong24
                        params.append(param25)  # phuong25 

                    # 60sinh-vien.validation
                    classname_origin = table.classname
                    classname_camel = core_gen.first_lower(table.classname)
                    classname_name_file = core_gen.to_name_file_case(table.classname)
                    service_file = str(ans1) + classname_name_file
                    ans1 = ans1 + 10
                    params.append(classname_origin)  # phuong26
                    params.append(service_file)  # phuong27

                    # add data reference binding phuong28
                    param_import = ''
                    service_import = '' 
                    for index in range(len(c_refers)):
                        c_refer = c_refers[index]
                        c_propertie = c_properties[index]

                        refers = c_refer.split(':')

                        camel_propertie = core_gen.first_lower(c_properties[index])

                        if len(refers) > 0 and refers[0] != '':
                            refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                            refer_classname = refer_class[0].classname
                            refer_name_file = core_gen.to_name_file_case(refer_classname)

                            # camel classname
                            camel_classname = core_gen.first_lower(refer_classname) 
                              
                            # get what number for select
                            all_class = models.Class.objects.filter(idproject=idproject).order_by('id')
                            count = 0
                            for index in range(len(all_class)):
                                if all_class[index].classname == refer_classname:
                                    count = index
                                    break

                            param_import = param_import + "import { "+ refer_classname +"Service } from 'src/app/@core/services/base/"+ str(count + 5) +"0"+ refer_name_file +".service';\n"
                            service_import = service_import + "    private "+ camel_classname +"Service: "+ refer_classname +"Service,\n"

                             
                    params.append(param_import)  # phuong28
                    params.append(service_import)  # phuong29
                    params.append(param30)  # phuong30

                    source_path_template = module_path_template + 'sample/sample.component.ts'
                    dest_path_template = module_path + component_name + '/' + component_name + '.component.ts'
                    core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # module
                # load data crud for list
                params = []
                param = ''
                class_component = 'C'+ str(ans + 1) +''+ classname +'Component'
                class_dialog = 'C'+ str(ans + 1) +''+ classname +'Dialog' 
                component_link = './c'+ str(ans + 1) +'-'+ core_gen.to_name_file_case(classname) +'.component'
                class_module = 'C'+ str(ans + 1) +''+ classname +'Module'

                # import dialog and component
                param = param + 'import { '+ class_component +', '+ class_dialog +' } from \'./'+ component_link +'\';  \n'
                params.append(param)

                # add component to import
                # import dialog and component
                param = ''
                param = param + '  declarations: ['+ class_component +', '+ class_dialog +'],   \n'
                params.append(param)
                params.append(class_component)
                params.append(class_dialog)
                params.append(class_module)

                source_path_template = module_path_template + 'sample/sample.module.ts'
                dest_path_template = module_path + component_name + '/' + component_name + '.module.ts'
                core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # update permission
                roles = roles + ',' + chr(97 + ans) + ':1'

                permission = permission + '    {                          \n'
                permission = permission + '      value: \''+ chr(97+ans) +'\',            \n'
                permission = permission + '      viewValue: \'Quản lý '+ classname +' \', \n'
                permission = permission + '    },                         \n'

                ans = ans + 1
    
            # render replace
            # c1-account
            # source_path_template = 'frontend/angular/src/app/home-page/content/c1-account/c1-account.component.ts'
            # dest_path_template = project_name + '/frontend/src/app/home-page/content/c1-account/c1-account.component.ts'
            # core_gen.render_replace(source_path_template, dest_path_template, [roles, permission])

            # menu html
            # source_path_template = 'frontend/angular/src/app/home-page/menu/menu.component.html'
            # dest_path_template = project_name + '/frontend/src/app/home-page/menu/menu.component.html'
            # core_gen.render_replace(source_path_template, dest_path_template, [html_menu])

            # menu component
            # source_path_template = 'frontend/angular/src/app/home-page/menu/menu.component.ts'
            # dest_path_template = project_name + '/frontend/src/app/home-page/menu/menu.component.ts'
            # core_gen.render_replace(source_path_template, dest_path_template, [param_permision_menu, load_permision_menu])

            # content module
            source_path_template = 'frontend/angular-nodejs/src/app/home-page/content/content.module.ts'
            dest_path_template = project_name + '/frontend/src/app/home-page/content/content.module.ts'
            core_gen.render_replace(source_path_template, dest_path_template, [content_module])

        else:
            print('project havent class')

    
    def gen_frontend_angular(self, idproject):
        project_name = models.Project.objects.filter(id = idproject)[0].name

        print('begin copy folder frontend/angular')
        # copy sample to output
        core_gen.copy_folder('frontend/angular', project_name + '/frontend')
        print('end copy folder frontend/angular')

        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        
        if len(tables) > 0: 
            content = ''
            html_menu = ''
            param_permision_menu = ''
            load_permision_menu = ''
            content_module = ''

            roles = ''
            permission = ''

            ans = 4
            # print(serializers.serialize("json",  tables))
            for table in tables:  
                classname = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print(classname)
                print(properties) #id,name,address
                print(attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

                # create item left menu
                # html_menu =  html_menu + '            <!--'+ 'c' + str(ans + 1) + '-' + classname +' -->                                                                   \n'
                # html_menu =  html_menu + '            <li class="nav-item" *ngIf="isPermissionMenu' + str(ans + 1) + '" (click)="onToggleButtonMobileClick()">  \n'
                # html_menu =  html_menu + '              <a class="nav-link" routerLinkActive="active"  \n'
                # html_menu =  html_menu + '                routerLink="/manager/'+ 'c' + str(ans + 1) + '-' + classname +'">                                                 \n'
                # html_menu =  html_menu + '                <i class="icon-grid menu-icon"></i>                                           \n'
                # html_menu =  html_menu + '                <span class="menu-title" i18n>Quản lý ' + classname + '</span>                                   \n'
                # html_menu =  html_menu + '              </a>                                                                            \n'
                # html_menu =  html_menu + '            </li>                                                                             \n\n'
                
                # create variable permission menu
                # param_permision_menu = param_permision_menu + '  isPermissionMenu' + str(ans + 1) + ': boolean = false;\n'

                # create update permission menu
                # load_permision_menu = load_permision_menu + '    if (this.staff.role.search(\'' + chr(97 + ans) + ':1\') < 0) { \n'
                # load_permision_menu = load_permision_menu + '      this.isPermissionMenu' + str(ans + 1) + ' = true;\n'
                # load_permision_menu = load_permision_menu + '    }                                      \n\n'

                # create content module 
                content_module = content_module + '          {                                                               \n'
                content_module = content_module + '            path: \'c'+  str(ans+1) +'-'+classname+'\',                                               \n'
                content_module = content_module + '            loadChildren: () =>                                           \n'
                name_folder_module = 'c'+  str(ans+1) +'-' + classname
                name_module = 'C'+  str(ans+1) +'' + classname.capitalize()
                content_module = content_module + '              import(\'./'+ name_folder_module +'/'+ name_folder_module +'.module\').then((m) => m.'+ name_module +'Module), \n'
                content_module = content_module + '          },                                          \n'  

                # create main module
                # create folder
                component_name = 'c' + str(ans+1) + '-' + classname
                module_path_template = '/frontend/angular/src/app/home-page/content/'
                module_path = project_name + '/frontend/src/app/home-page/content/'
                core_gen.create_folder(module_path + component_name)


                # dialog
                # load data crud for insert and update
                crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                c_inputtypes_temp = ['']
                if len(crud) > 0:
                    crud = crud[0]
                    c_titles = crud.titles.split(',')
                    c_properties = crud.properties.split(',')
                    c_alias = crud.alias.split(',')
                    c_refers = crud.refers.split(',')
                    c_inputtypes = crud.inputtypes.split(',')
                    c_inputtypes_temp.extend(c_inputtypes)
                    c_validates = crud.validates.split(',')
                    c_formats = crud.formats.split(',')
                    print('dialgo ne dialgo ne dialgo ne ')
                    print(c_properties)  # id,name,address
                    print(c_refers)  # INT,VARCHAR,VARCHAR
                    print(c_inputtypes)  # INT,VARCHAR,VARCHAR
                    print(c_validates)  # 0,255,0

                    params = []
                    param = ''
                    params.append(crud.titlecreate)

                    # add dialog controls
                    for index in range(len(c_properties)):
                        param = param + '        <!-- '+c_properties[index]+' field -->                                                           \n'
                        param = param + '        <div class="col-md-12">                                                       \n'

                        # check control is varchar
                        if c_inputtypes[index] == 'varchar':
                            param = param + '          <mat-form-field class="example-full-width">                                 \n'
                            param = param + '            <mat-label i18n>'+c_titles[index]+' <span class="text-red">*</span></mat-label>           \n'
                            param = param + '            <input matInput [(ngModel)]="input.'+c_properties[index]+'" formControlName="'+c_properties[index]+'">          \n'
                            param = param + '            <button mat-button *ngIf="input.'+c_properties[index]+'" matSuffix mat-icon-button           \n'
                            param = param + '             aria-label="Clear" (click)="input.'+c_properties[index]+'=\'\'">                            \n'
                            param = param + '              <mat-icon>close</mat-icon>                                              \n'
                            param = param + '            </button>                                                                 \n'
                            param = param + '          </mat-form-field>                                                           \n'

                        # check control is date or datetime
                        if c_inputtypes[index] == 'date' or c_inputtypes[index] == 'datetime':
                            param = param + '          <mat-form-field class="example-full-width" appearance="fill">                                   \n'
                            param = param + '            <mat-label i18n>'+c_titles[index]+'<span class="text-red">*</span></mat-label>                          \n'
                            param = param + '            <input matInput [(ngModel)]="input.'+c_properties[index]+'" formControlName="'+c_properties[index]+'" [matDatepicker]="picker'+c_properties[index]+'"> \n'
                            param = param + '            <mat-datepicker-toggle matSuffix [for]="picker'+c_properties[index]+'"></mat-datepicker-toggle>                  \n'
                            param = param + '            <mat-datepicker touchUi #picker'+c_properties[index]+'></mat-datepicker>                                         \n'
                            param = param + '          </mat-form-field>                                                                               \n'

                        # check control is text
                        if c_inputtypes[index] == 'ckeditor':
                            param = param + '          <mat-label i18n>'+c_titles[index]+'<span class="text-red">*</span></mat-label>                             \n'
                            param = param + '          <ckeditor [(ngModel)]="input.'+c_properties[index]+'" formControlName="'+c_properties[index]+'" [config]="{uiColor: \'#16c1c3\'}" \n'
                            param = param + '            debounce="500">                                                            \n'
                            param = param + '          </ckeditor>                                                                                     \n'

                        # check control is radio
                        if c_inputtypes[index] == 'radio':
                            param = param + '          <mat-label i18n>'+c_titles[index]+' <span class="text-red">*</span></mat-label>                           \n'
                            param = param + '          <mat-radio-group formControlName="'+c_properties[index]+'" [(ngModel)]="input.'+c_properties[index]+'" aria-label="'+c_titles[index]+'">          \n'
                            param = param + '            <mat-radio-button *ngFor="let sex of sexs" [value]="sex.value" style="padding-left: 10px;">   \n'
                            param = param + '              {{sex.viewValue}}                                                                           \n'
                            param = param + '            </mat-radio-button>                                                                           \n'
                            param = param + '          </mat-radio-group>                                                                              \n'

                        # check control is number
                        if c_inputtypes[index] == 'number':
                            param = param + '          <mat-form-field class="example-full-width">                                                     \n'
                            param = param + '            <mat-label i18n>'+c_titles[index]+' <span class="text-red">*</span></mat-label>                               \n'
                            param = param + '            <input matInput type="number" [(ngModel)]="input.'+c_properties[index]+'" formControlName="'+c_properties[index]+'">              \n'
                            param = param + '          </mat-form-field>                                                                               \n'

                        print('phuong oi phuong oi ', c_inputtypes)
                        # check control is select
                        if c_inputtypes[index] == 'select':
                            print('c_refersc_refersc_refersc_refers', c_refers)
                            refers = c_refers[index].split(':')
                            print('phuong1', index)
                            print('phuong1', c_inputtypes[index])
                            print('phuong1', refers)
                            if len(refers) > 2:
                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname
                                refer_id = refers[1]
                                refer_display = refers[2]

                                param = param + '          <mat-form-field class="example-full-width">                                                     \n'
                                param = param + '            <mat-label i18n>'+c_titles[index]+'<span class="text-red">*</span></mat-label>                               \n'
                                param = param + '            <mat-select formControlName="'+c_properties[index]+'" [(ngModel)]="input.'+c_properties[index]+'">                                \n'
                                param = param + '              <mat-option *ngFor="let '+ refer_classname +' of '+ refer_classname +'Datas" [value]="'+ refer_classname +'.'+ refer_id +'">                           \n'
                                param = param + '                {{'+ refer_classname +'.'+ refer_display +'}}                                                                            \n'
                                param = param + '              </mat-option>                                                                               \n'
                                param = param + '            </mat-select>                                                                                 \n'
                                param = param + '          </mat-form-field>                                                                               \n'

                        # check control is image
                        if c_inputtypes[index] == 'image':
                            param = param + '            <!-- images ' + c_properties[index] + ' -->  \n'
                            param = param + '            <mat-card class="example-full-width" style="box-shadow: none; padding-left: 0;">                  \n'
                            param = param + '              <mat-card-actions style="margin: 0; padding: 0;">                                               \n'
                            param = param + '                <button i18n style="border: 1px solid red; border-radius: 3px; float: left; width: 200px;"    \n'
                            param = param + '                  mat-button color="warn"                                                                     \n'
                            param = param + '                  (click)="on' + c_properties[index].capitalize() + 'UploadImageClick()">  \n'
                            param = param + '                  <mat-icon>file_upload</mat-icon>                                                            \n'
                            param = param + '                  ' + c_titles[index] + '                                                                     \n'
                            param = param + '                </button>                                                                                     \n'
                            param = param + '                <input                                                                                        \n'
                            param = param + '                  type="file"                                                                                 \n'
                            param = param + '                  formControlName="' + c_properties[index] + '"  \n'
                            param = param + '                  #inputImage' + c_properties[index].capitalize() + '      \n'
                            param = param + '                  style="display: none"                                                                       \n'
                            param = param + '                  multiple="multiple"                                                                         \n'
                            param = param + '                  accept="image/*"/>                                                                          \n'
                            param = param + '              </mat-card-actions>                                                                             \n'
                            param = param + '            </mat-card><br><br>                                                                                       \n'
                            param = param + '            <img class="custom-preview-file" *ngIf="input.' + c_properties[index] + '!=\'\'" [src]="input.' + c_properties[index] + '" alt="izi software" width="200" height="200">\n'

                        # check control is file
                        if c_inputtypes[index] == 'file':
                            param = param + '            <!-- files ' + c_properties[index] + ' -->  \n'
                            param = param + '            <mat-card class="example-full-width" style="box-shadow: none; padding-left: 0;">                  \n'
                            param = param + '              <mat-card-actions style="margin: 0; padding: 0;">                                               \n'
                            param = param + '                <button i18n style="border: 1px solid red; border-radius: 3px; float: left; width: 200px;"    \n'
                            param = param + '                  mat-button color="warn"                                                                     \n'
                            param = param + '                  (click)="on' + c_properties[index].capitalize() + 'UploadFileClick()">  \n'
                            param = param + '                  <mat-icon>file_upload</mat-icon>                                                            \n'
                            param = param + '                  ' + c_titles[index] + '                                                                     \n'
                            param = param + '                </button>                                                                                     \n'
                            param = param + '                <input                                                                                        \n'
                            param = param + '                  type="file"                                                                                 \n'
                            param = param + '                  formControlName="' + c_properties[index] + '"  \n'
                            param = param + '                  #inputFile' + c_properties[index].capitalize() + '      \n'
                            param = param + '                  style="display: none">                                                                       \n'
                            param = param + '              </mat-card-actions>                                                                             \n'
                            param = param + '            </mat-card>                                                                                       \n'
                            param = param + '            <a class="custom-preview-file" *ngIf="input.' + c_properties[index] + '!=\'\'" href="{{input.' + \
                                    c_properties[index] + '}}" target="_blank" style="width: 200px; text-align: center;">Upload Link</a>\n'

                        # checking validations
                        validates = c_validates[index].split(':')
                        if len(validates) > 0:
                            param = param + '                                                                                      \n'
                            param = param + '          <div class="alert alert-danger" *ngIf="form.controls[\''+c_properties[index]+'\'].invalid      \n'
                            param = param + '              && (form.controls[\''+c_properties[index]+'\'].dirty || form.controls[\''+c_properties[index]+'\'].touched)"> \n'

                            for validate in validates:
                                if validate == 'required':
                                    param = param + '            <div *ngIf="form.controls[\''+c_properties[index]+'\'].errors.required" i18n>                \n'
                                    param = param + '              Vui lòng nhập trường này.                                               \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'minLength':
                                    param = param + '            <div *ngIf="form.controls[\''+c_properties[index]+'\'].errors.minlength" i18n>               \n'
                                    param = param + '              Bạn vui lòng nhập ít nhất 1 ký tự.                                      \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'maxLength':
                                    param = param + '            <div *ngIf="form.controls[\''+c_properties[index]+'\'].errors.maxlength" i18n>               \n'
                                    param = param + '              Bạn không được nhập quá 100 ký tự.                                      \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'pattern':
                                    param = param + '            <div *ngIf="form.controls[\''+c_properties[index]+'\'].errors.pattern" i18n>                 \n'
                                    param = param + '              Vui lòng nhập đúng định dạng.                                           \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'min':
                                    param = param + '            <div *ngIf="form.controls[\''+c_properties[index]+'\'].errors.min" i18n>                     \n'
                                    param = param + '              Số không được nhỏ hơn 10.                                               \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'max':
                                    param = param + '            <div *ngIf="form.controls[\''+c_properties[index]+'\'].errors.max" i18n>                     \n'
                                    param = param + '              Số không được lớn hơn 100.                                              \n'
                                    param = param + '            </div>                                                                    \n'

                                if validate == 'email':
                                    param = param + '            <div *ngIf="form.controls[\''+c_properties[index]+'\'].errors.email" i18n>                   \n'
                                    param = param + '              Vui lòng nhập đúng định dạng email.                                     \n'
                                    param = param + '            </div>                                                                    \n'

                            param = param + '          </div>                                                                      \n'
                            param = param + '        </div>                                                                        \n\n'

                    params.append(param)
                    source_path_template = module_path_template + 'sample/sample-dialog.html'
                    dest_path_template = module_path + component_name + '/' + component_name + '-dialog.html'
                    core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # html
                # load data crud for list
                crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=0)
                if len(crud) > 0:
                    crud = crud[0]
                    c_titles = crud.titles.split(',')
                    c_properties = crud.properties.split(',')
                    c_alias = crud.alias.split(',')
                    c_refers = crud.refers.split(',')
                    c_inputtypes = crud.inputtypes.split(',')
                    c_validates = crud.validates.split(',')
                    c_formats = crud.formats.split(',')

                    print('c_inputtypes_tempc_inputtypes_temp', c_inputtypes_temp)
                    print('c_propertiesc_propertiesc_properties', c_properties)

                    params = []
                    param = ''
                    params.append(classname)

                    # check have select filter header
                    print('phuongphuongphuongphuongphuongphuong', c_properties)
                    print('phuongphuongphuongphuongphuongphuong', c_refers, c_inputtypes)
                    for index in range(len(c_properties)):
                        # check control is select
                        refers = c_refers[index].split(':')
                        if c_refers[index] != '' and refers[0] != '':
                            if len(refers) > 2:
                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname.capitalize()
                                refer_id = refers[1]
                                refer_display = refers[2]

                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname

                                param = param + '    <!-- select ' + c_titles[index] + ' filter -->                     \n'
                                param = param + '    <div class="col-md-3 col-xs-12">                                   \n'
                                param = param + '      <mat-form-field appearance="fill">                               \n'
                                param = param + '        <mat-label i18n>SEARCH ' + c_titles[index] + '</mat-label>        \n'
                                param = param + '        <mat-select [(ngModel)]="' + c_properties[index] + 'Model" (selectionChange)="on' + refer_classname + 'SelectionChange($event)">  \n'
                                param = param + '          <mat-option *ngFor="let item of ' + refer_classname + 'Datas" [value]="item.id"> \n'
                                param = param + '            {{item.'+ refer_display +'}}                                              \n'
                                param = param + '          </mat-option>                                                \n'
                                param = param + '        </mat-select>                                                  \n'
                                param = param + '      </mat-form-field>                                                \n'
                                param = param + '    </div>                                                             \n\n'

                    params.append(param)
                    param = ''

                    # check have delete action
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=3)
                    if len(crud) > 0:
                        param = '      <button type="button" class="btn btn-danger btn-del w-120" data-toggle="modal" data-target="#exampleModal" i18n><i \n'
                        param = param + '          class="mdi mdi-delete"></i>Xóa</button>                                                                        \n'
                        params.append(param)
                    else:
                        params.append('      <!--  -->')

                    # add column grid
                    param = ''
                    for index in range(len(c_properties)):
                        param = param + '    <!-- '+ c_properties[index] +' Column -->                                                    \n'
                        param = param + '    <ng-container matColumnDef="'+ c_properties[index] +'">                                      \n'

                        # check refer class
                        refers = c_refers[index].split(':')
                        if c_refers[index] != '' and refers[0] != '':
                            if len(refers) > 2:
                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname.capitalize()
                                refer_id = refers[1]
                                refer_display = refers[2]

                                # check alias
                                if c_alias[index] != '':
                                    param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> ' + c_alias[index] + ' </th> \n'
                                else:
                                    param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> ' + c_titles[index] + ' </th> \n'

                                # check format
                                param = param + '      <td mat-cell *matCellDef="let row"> {{getDisplay'+ refer_classname +'ById(row.'+ c_properties[index] +')}} </td>\n'

                        # check control is image
                        elif c_inputtypes_temp[index] == 'image':
                            # check alias
                            if c_alias[index] != '':
                                param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> '+ c_alias[index] +' </th> \n'
                            else:
                                param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> '+ c_titles[index] +' </th> \n'

                            # check format
                            if c_formats[index] != '':
                                param = param + '      <td mat-cell *matCellDef="let row"> {{'+ c_formats[index] +'}} </td>                \n'
                            else:
                                param = param + '      <td mat-cell *matCellDef="let row"> <img src="{{row.'+ c_properties[index] +'}}" style="width: 150px; height:150px;"> </td>                \n'
                        else:
                            # check alias
                            if c_alias[index] != '':
                                param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> '+ c_alias[index] +' </th> \n'
                            else:
                                param = param + '      <th mat-header-cell *matHeaderCellDef mat-sort-header i18n> '+ c_titles[index] +' </th> \n'

                            # check format
                            if c_formats[index] != '':
                                param = param + '      <td mat-cell *matCellDef="let row"> {{'+ c_formats[index] +'}} </td>                \n'
                            else:
                                param = param + '      <td mat-cell *matCellDef="let row"> {{row.'+ c_properties[index] +'}} </td>                \n'

                        param = param + '    </ng-container>                                                         \n\n'

                    params.append(param)

                    source_path_template = module_path_template + 'sample/sample.component.html'
                    dest_path_template = module_path + component_name + '/' + component_name + '.component.html'
                    core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # scss
                params = []
                source_path_template = module_path_template + 'sample/sample.component.scss'
                dest_path_template = module_path + component_name + '/' + component_name + '.component.scss'
                core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # ts
                # load data crud for list
                crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=0)
                if len(crud) > 0:
                    crud = crud[0]
                    c_titles = crud.titles.split(',')
                    c_properties = crud.properties.split(',')
                    c_alias = crud.alias.split(',')
                    c_refers = crud.refers.split(',')
                    c_inputtypes = crud.inputtypes.split(',')
                    c_validates = crud.validates.split(',')
                    c_formats = crud.formats.split(',')

                    params = []
                    param = ''
                    # update @component and export class
                    param = param + '@Component({                                                                \n'
                    param = param + '  selector: \'app-c'+ str(ans + 1) +'-'+ classname +'\',                                                \n'
                    param = param + '  templateUrl: \'./c'+ str(ans + 1) +'-'+ classname +'.component.html\',                                \n'
                    param = param + '  styleUrls: [\'./c'+ str(ans + 1) +'-'+ classname +'.component.scss\'],                                \n'
                    param = param + '})                                                                          \n'
                    param = param + 'export class C'+ str(ans + 1) +''+ classname.capitalize() +'Component implements OnInit, AfterViewInit, OnDestroy {  \n'
                    params.append(param)
                    param = ''

                    # add display columns
                    for property in c_properties:
                        param = param + '    \''+ property +'\',\n'

                    params.append(param)
                    param = ''

                    # update what in getLengthOfPage method
                    params.append(str(ans))

                    # add data reference binding
                    param_define = ''
                    param_call = ''
                    param_method = ''
                    param_method_filter = ''
                    for index in range(len(c_refers)):
                        c_refer = c_refers[index]
                        c_propertie = c_properties[index]

                        refers = c_refer.split(':')
                        if len(refers) > 0 and refers[0] != '':
                            refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                            refer_classname = refer_class[0].classname
                            refer_id = refers[1]
                            refer_display = refers[2]

                            param_define = param_define + '  '+ refer_classname +'Datas: any[] = [];\n'

                            param_call = param_call + '    // load list '+ refer_classname +'\n'
                            param_call = param_call + '    this.getList'+ refer_classname.capitalize() +'();\n\n'

                            param_method = param_method + '  /**                                                            \n'
                            param_method = param_method + '   * get list '+ refer_classname +'\n'
                            param_method = param_method + '   */                                                            \n'
                            param_method = param_method + '  getList'+ refer_classname.capitalize() +'() {       \n'

                            # get what number for select
                            all_class = models.Class.objects.filter(idproject=idproject).order_by('id')
                            count = 0
                            for index in range(len(all_class)):
                                if all_class[index].classname == refer_classname:
                                    count = index
                                    break

                            param_method = param_method + '    this.subscription.push(this.api.excuteAllByWhat({}, \''+ str(count + 4) +'00\') \n'

                            param_method = param_method + '      .subscribe((data) => {                                     \n'
                            param_method = param_method + '        data = data[\'data\'];  \n'
                            
                            param_method = param_method + '        data.unshift({ id: \'0\', '+ refer_display +': \'Tất cả\' });  \n'
                            
                            param_method = param_method + '        this.'+ refer_classname +'Datas = data;  \n'
                            param_method = param_method + '      })                                                         \n'
                            param_method = param_method + '    );                                                           \n'
                            param_method = param_method + '  }                                                              \n'
                            param_method = param_method + '                                                                 \n'
                            param_method = param_method + '  /**                                                            \n'
                            param_method = param_method + '   * get name display '+ refer_classname +' by id     \n'
                            param_method = param_method + '   */                                                            \n'
                            param_method = param_method + '  getDisplay'+ refer_classname.capitalize() +'ById(id) {                                      \n'
                            param_method = param_method + '    return this.'+ refer_classname +'Datas.filter((e) => e.id == id)[0]?.'+ refer_display +';   \n'
                            param_method = param_method + '  }                                                              \n\n'

                            param_method_filter = param_method_filter + '  /**                                                        \n'
                            param_method_filter = param_method_filter + '   * on ' + refer_classname + ' Selection Change             \n'
                            param_method_filter = param_method_filter + '   * @param event                                            \n'
                            param_method_filter = param_method_filter + '   */                                                        \n'
                            param_method_filter = param_method_filter + '  on' + refer_classname + 'SelectionChange(event) {          \n'
                            param_method_filter = param_method_filter + '    const condition = { key: "' + c_propertie + '", value: event.value };  \n'
                            param_method_filter = param_method_filter + '                                                             \n'
                            param_method_filter = param_method_filter + '    // add new condition to list                             \n'
                            param_method_filter = param_method_filter + '    this.addNewConditionToList(condition);                   \n'
                            param_method_filter = param_method_filter + '  }                                                          \n'

                    params.append(param_define)

                    # add for permission
                    params.append(chr(97 + ans))

                    params.append(param_call)
                    params.append(param_method)

                    # add method selection change filter
                    params.append(param_method_filter)

                    # update what in onLoadDataGrid method
                    params.append(str(ans))

                    # update what in onBtnDelClick method
                    params.append(str(ans))

                    # update name dialog in onBtnInsertDataClick method
                    params.append('C'+ str(ans + 1) +''+ classname.capitalize() +'Dialog')

                    # update name dialog in onBtnUpdateDataClick method
                    params.append('C' + str(ans + 1) + '' + classname.capitalize() + 'Dialog')

                    param = ''

                    # start for dialog component
                    # update @component and export dialog class
                    param = param + '@Component({                                              \n'
                    param = param + '  selector: \'c'+ str(ans + 1) +'-'+ classname +'-dialog\',                            \n'
                    param = param + '  templateUrl: \'c'+ str(ans + 1) +'-'+ classname +'-dialog.html\',                    \n'
                    param = param + '  styleUrls: [\'./c'+ str(ans + 1) +'-'+ classname +'.component.scss\'],               \n'
                    param = param + '})                                                        \n'
                    param = param + 'export class C'+ str(ans + 1) + classname.capitalize() +'Dialog implements OnInit, OnDestroy {  \n'

                    params.append(param)
                    param = ''

                    # get list for insert
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_properties = crud.properties.split(',')
                        c_validates = crud.validates.split(',')

                    # add input value for dialog insert
                    for property in c_properties:
                        param = param + '    '+ property +': \'\',\n'

                    params.append(param)
                    param = ''

                    # add data reference binding for dialog
                    param_define = ''
                    param_call = ''
                    param_method = ''

                    # get list for insert
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_refers = crud.refers.split(',')
                        for c_refer in c_refers:
                            refers = c_refer.split(':')
                            if len(refers) > 0 and refers[0] != '':
                                refer_class = models.Class.objects.filter(id=refers[0]).order_by('id')
                                refer_classname = refer_class[0].classname
                                refer_id = refers[1]
                                refer_display = refers[2]

                                param_define = param_define + '  ' + refer_classname + 'Datas: any[] = [];\n'

                                param_call = param_call + '    // load list ' + refer_classname + '\n'
                                param_call = param_call + '    this.getList' + refer_classname.capitalize() + '();\n\n'

                                param_method = param_method + '  /**                                                            \n'
                                param_method = param_method + '   * get list ' + refer_classname + '\n'
                                param_method = param_method + '   */                                                            \n'
                                param_method = param_method + '  getList' + refer_classname.capitalize() + '() {       \n'

                                # get what number for select
                                all_class = models.Class.objects.filter(idproject=idproject).order_by('id')
                                count = 0
                                for index in range(len(all_class)):
                                    if all_class[index].classname == refer_classname:
                                        count = index
                                        break

                                param_method = param_method + '    this.subscription.push(this.api.excuteAllByWhat({}, \'' + str(count + 4) + '00\') \n'

                                param_method = param_method + '      .subscribe((data) => {                                     \n'
                                param_method = param_method + '        data = data[\'data\'];  \n'
                                param_method = param_method + '        this.' + refer_classname + 'Datas = data;  \n'
                                param_method = param_method + '      })                                                         \n'
                                param_method = param_method + '    );                                                           \n'
                                param_method = param_method + '  }                                                              \n\n'

                    params.append(param_define)


                    # add define validate
                    param = ''

                    for index in range(len(c_properties)):
                        param = param + '      '+ c_properties[index] +': [                        \n'
                        param = param + '        null,                        \n'
                        param = param + '        [                            \n'
                        validates = c_validates[index].split(':')
                        for validate in validates:
                            if validate == 'required':
                                param = param + '          Validators.required,       \n'

                            if validate == 'minLength':
                                param = param + '          Validators.minLength(1),   \n'

                            if validate == 'maxLength':
                                param = param + '          Validators.maxLength(100), \n'

                            if validate == 'pattern':
                                param = param + '          Validators.pattern(\' * \'),   \n'

                            if validate == 'min':
                                param = param + '          Validators.min(10),        \n'

                            if validate == 'max':
                                param = param + '          Validators.max(100),       \n'

                            if validate == 'email':
                                param = param + '          Validators.email,          \n'
                        param = param + '        ],                           \n'
                        param = param + '      ],                             \n'


                    # get list for insert
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_properties = crud.properties.split(',')
                        c_inputtypes = crud.inputtypes.split(',')

                    # add image and file uploads
                    param_define = ''
                    param_method_upload = ''
                    for index in range(len(c_properties)):
                        # check control is image
                        if c_inputtypes[index] == 'image':
                            param_define = param_define + '  @ViewChild("inputImage'+ c_properties[index].capitalize() +'" '', { static: false }) inputImage' + \
                                           c_properties[index].capitalize() + ': ElementRef; \n'
                            param_define = param_define + '  ' + c_properties[index] + 'ImagePath: string = ""; \n\n'

                            param_method_upload = param_method_upload + '  /**                                                                             \n'
                            param_method_upload = param_method_upload + '   * on ' + c_properties[
                                index].capitalize() + ' Upload image Click                                                 \n'
                            param_method_upload = param_method_upload + '   */                                                                             \n'
                            param_method_upload = param_method_upload + '  on' + c_properties[
                                index].capitalize() + 'UploadImageClick() {                                                    \n'
                            param_method_upload = param_method_upload + '    this.api.uploadImageCore(this.inputImage' + \
                                                  c_properties[
                                                      index].capitalize() + ').subscribe((data) => {         \n'
                            param_method_upload = param_method_upload + '      this.input.' + c_properties[
                                index] + ' = this.api.BASE_UPLOAD_URL +  "/uploads/images/" + data; \n'
                            param_method_upload = param_method_upload + '    });                                                                           \n'
                            param_method_upload = param_method_upload + '  } \n\n'

                        # check control is file
                        if c_inputtypes[index] == 'file':
                            param_define = param_define + '  @ViewChild("inputFile'+ c_properties[index].capitalize() +'" '', { static: false }) inputFile' + \
                                           c_properties[index].capitalize() + ': ElementRef;         \n'
                            param_define = param_define + '  ' + c_properties[index] + 'FilePath: string = ""; \n'

                            param_method_upload = param_method_upload + '  /**             \n'
                            param_method_upload = param_method_upload + '   * on ' + c_properties[
                                index].capitalize() + ' Upload file Click                                           \n'
                            param_method_upload = param_method_upload + '   */                          \n'
                            param_method_upload = param_method_upload + '  on' + c_properties[
                                index].capitalize() + 'UploadFileClick() {                    \n'
                            param_method_upload = param_method_upload + '    this.api.uploadFileCore(this.inputFile' + \
                                                  c_properties[
                                                      index].capitalize() + ').subscribe((data) => {        \n'
                            param_method_upload = param_method_upload + '      this.input.' + c_properties[
                                index] + ' = this.api.BASE_UPLOAD_URL +  "/uploads/files/" + data; \n'
                            param_method_upload = param_method_upload + '    });               \n'
                            param_method_upload = param_method_upload + '  }                \n\n'

                    # param define for upload image or file
                    params.append(param_define)

                    # update name dialog in constructor method
                    params.append('C' + str(ans + 1) + '' + classname.capitalize() + 'Dialog')

                    # add validation
                    params.append(param)

                    # add call reference
                    params.append(param_call)
                    params.append(param_method)

                    # add param for upload
                    params.append(param_method_upload)

                    # add for mat date for field datetime
                    # get list for insert
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=1)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_properties = crud.properties.split(',')
                        c_inputtypes = crud.inputtypes.split(',')

                    format_date = ''
                    for index in range(len(c_properties)):
                        if c_inputtypes[index] == 'date' or c_inputtypes[index] == 'datetime':
                            format_date = format_date + '    this.input.'+ c_properties[index] +' = this.api.formatDate(new Date(this.input.'+ c_properties[index] +')); \n'

                    params.append(format_date)

                    # update what in onBtnSubmitClick method phuong22
                    params.append(str(ans)) 

                    # phuong23 phuong24 phuong25 add input model
                    crud = models.CRUD.objects.filter(idproject=idproject, idclass=table.id, actiontype=0)
                    if len(crud) > 0:
                        crud = crud[0]
                        c_titles = crud.titles.split(',')
                        c_properties = crud.properties.split(',')
                        c_alias = crud.alias.split(',')
                        c_refers = crud.refers.split(',')
                        c_inputtypes = crud.inputtypes.split(',')
                        c_validates = crud.validates.split(',')
                        c_formats = crud.formats.split(',')

                        param23 = ''
                        param24 = ''
                        param25 = ''
                        for index in range(len(c_properties)):
                            # check control is select
                            refers = c_refers[index].split(':')
                            if c_refers[index] != '' and refers[0] != '':
                                if len(refers) > 2: 
                                    param23 = param23 + '  ' + c_properties[index] + 'Model: any = \'0\'; \n'
                                    param24 = param24 + '        ' + c_properties[index] + 'Model: this.' + c_properties[index] + 'Model,\n'
                                    param25 = param25 + '      this.input.' + c_properties[index] + ' = data.' + c_properties[index] + 'Model;\n'

                        # add data to params
                        params.append(param23)
                        params.append(param24)
                        params.append(param25)

                    source_path_template = module_path_template + 'sample/sample.component.ts'
                    dest_path_template = module_path + component_name + '/' + component_name + '.component.ts'
                    core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # module
                # load data crud for list
                params = []
                param = ''
                class_component = 'C'+ str(ans + 1) +''+ classname.capitalize() +'Component'
                class_dialog = 'C'+ str(ans + 1) +''+ classname.capitalize() +'Dialog'
                component_link = './c'+ str(ans + 1) +'-'+ classname +'.component'
                class_module = 'C'+ str(ans + 1) +''+ classname.capitalize() +'Module'

                # import dialog and component
                param = param + 'import { '+ class_component +', '+ class_dialog +' } from \'./'+ component_link +'\';  \n'
                params.append(param)

                # add component to import
                # import dialog and component
                param = ''
                param = param + '  declarations: ['+ class_component +', '+ class_dialog +'],   \n'
                params.append(param)
                params.append(class_component)
                params.append(class_dialog)
                params.append(class_module)

                source_path_template = module_path_template + 'sample/sample.module.ts'
                dest_path_template = module_path + component_name + '/' + component_name + '.module.ts'
                core_gen.render_replace_template(source_path_template, dest_path_template, params)

                # update permission
                roles = roles + ',' + chr(97 + ans) + ':1'

                permission = permission + '    {                          \n'
                permission = permission + '      value: \''+ chr(97+ans) +'\',            \n'
                permission = permission + '      viewValue: \'Quản lý '+ classname.capitalize() +' \', \n'
                permission = permission + '    },                         \n'

                ans = ans + 1
    
            # render replace
            # c1-account
            # source_path_template = 'frontend/angular/src/app/home-page/content/c1-account/c1-account.component.ts'
            # dest_path_template = project_name + '/frontend/src/app/home-page/content/c1-account/c1-account.component.ts'
            # core_gen.render_replace(source_path_template, dest_path_template, [roles, permission])

            # menu html
            source_path_template = 'frontend/angular/src/app/home-page/menu/menu.component.html'
            dest_path_template = project_name + '/frontend/src/app/home-page/menu/menu.component.html'
            core_gen.render_replace(source_path_template, dest_path_template, [html_menu])

            # menu component
            # source_path_template = 'frontend/angular/src/app/home-page/menu/menu.component.ts'
            # dest_path_template = project_name + '/frontend/src/app/home-page/menu/menu.component.ts'
            # core_gen.render_replace(source_path_template, dest_path_template, [param_permision_menu, load_permision_menu])

            # content module
            source_path_template = 'frontend/angular/src/app/home-page/content/content.module.ts'
            dest_path_template = project_name + '/frontend/src/app/home-page/content/content.module.ts'
            core_gen.render_replace(source_path_template, dest_path_template, [content_module])

        else:
            print('project havent class')

