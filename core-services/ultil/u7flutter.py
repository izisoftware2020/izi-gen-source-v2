from django.core import serializers

# from models import User
from django.apps import apps 

from apps.a1crud import models

from ultil.u1core import GenSourceCore 
core_gen = GenSourceCore()

class GenSourceFlutter: 
    def __init__(self):
        self.root_link = 'output/'
        self.templates_link = 'templates/' 


    def gen_flutter_getx(self, idproject):
        project_name = models.Project.objects.filter(id = idproject)[0].name

        # copy sample to output
        core_gen.copy_folder('flutter/getx', project_name + '/flutter')

        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        params = []
        
        if len(tables) > 0:
            content = ''
            import_services = ''
            ans = 50
            print(serializers.serialize("json",  tables)) 

            #*******************************************************
            # ********* Gen Model response
            #*******************************************************
            ans = 50 
            params = []

            for table in tables:
                param0 = ''
                param1 = ''
                param2 = '' 
                param3 = '' 
                param4 = '' 
                param5 = '' 
                param6 = ''   
                params = []
                param0r = ''
                param1r = ''
                param2r = '' 
                param3r = '' 
                param4r = '' 
                param5r = '' 
                param6r = ''   
                paramsr = []
                classname_origin = table.classname
                classname_snake = core_gen.to_snake_case(table.classname)
                classname_camel = core_gen.first_lower(table.classname)
                classname_name_file = core_gen.to_name_file_case(table.classname)
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print('phuongphuong', ans)
                print('classname_origin', classname_origin)
                print('classname_snake', classname_snake)
                print('classname_camel', classname_camel)
                print('classname_camel', classname_name_file)
                print('properties', properties) #id,name,address
                print('attributes', attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

                param1 = classname_origin  
                param1r = classname_origin  

                # 10user_response.dart
                temp = classname_snake +'_response.dart'

                # create insert value =>  String? fullname;
                for i in range(0, len(properties)):
                    # kiểm tra nếu là id thì sẽ refer thêm vào
                    if properties[i][:2] == 'Id':
                        param0 = param0 + 'import \''+core_gen.to_snake_case(properties[i][2:])+'_response.dart\';\n'
                        param2 = param2 + '  '+properties[i][2:]+'Response? '+core_gen.first_lower(properties[i])+';\n'
                    else:
                        param2 = param2 + '  String? '+core_gen.first_lower(properties[i])+';\n'


                # create insert value =>      this.fullname,
                param3 = ',\n'.join(list(map(lambda x: '      this.'+core_gen.first_lower(x)+'' , properties[0:])))

                # create insert value =>    fullname = json['fullname'].toString();
                for i in range(0, len(properties)):
                    # kiểm tra nếu là id thì sẽ refer thêm vào
                    if properties[i][:2] == 'Id':
                        param4 = param4 + '\n'
                        param4 = param4 + '    // mapping '+core_gen.first_lower(properties[i])+'                                                              \n'
                        param4 = param4 + '    if (json[\''+core_gen.first_lower(properties[i])+'\'] != null && json[\''+core_gen.first_lower(properties[i])+'\'].toString().length!=24) {                                                  \n'
                        param4 = param4 + '      '+core_gen.first_lower(properties[i])+' = '+properties[i][2:]+'Response.fromJson(json[\''+core_gen.first_lower(properties[i])+'\'] as Map<String, dynamic>); \n'
                        param4 = param4 + '    } else {                                                                           \n'
                        param4 = param4 + '      '+core_gen.first_lower(properties[i])+' = null;                                                               \n'
                        param4 = param4 + '    }                                                                                  \n'
                    else:
                        param4 = param4 + '    '+core_gen.first_lower(properties[i])+' = json[\''+core_gen.first_lower(properties[i])+'\'].toString();\n'

                # create insert value =>    data['fullname'] = fullname;
                for i in range(0, len(properties)):
                    param5 = param5 + '    // check null '+core_gen.first_lower(properties[i])+'\n'
                    param5 = param5 + '    if ('+core_gen.first_lower(properties[i])+' != null) data[\''+core_gen.first_lower(properties[i])+'\'] = '+core_gen.first_lower(properties[i])+'; \n\n'
 
                ans = ans + 10 

                # gen model response
                source_path_template = 'flutter/template/user_response.dart'
                dest_path_template = project_name + '/flutter/lib/data/model/response/'+ temp

                # add param prepare to generations model response 
                params.append(param1)
                params.append(param2)
                params.append(param3)
                params.append(param4)
                params.append(param5)
                params.append(param0)
                core_gen.render_replace_template(source_path_template, dest_path_template, params)


                # 10user_request.dart
                temp = classname_snake +'_request.dart'

                # create insert value =>  String? fullname;
                param2r = ';\n'.join(list(map(lambda x: '  String? '+core_gen.first_lower(x)+'' , properties[0:])))
                param2r = param2r + ';'

                # create insert value =>      this.fullname,
                param3r = ',\n'.join(list(map(lambda x: '      this.'+core_gen.first_lower(x)+'' , properties[0:])))

                # create insert value =>    fullname = json['fullname'].toString();
                param4r = ';\n'.join(list(map(lambda x: '    '+core_gen.first_lower(x)+' = json[\''+core_gen.first_lower(x)+'\'].toString()' , properties[0:])))
                param4r = param4r + ';'

                # create insert value =>    data['fullname'] = fullname;
                for i in range(0, len(properties)):
                    param5r = param5r + '    // check null '+core_gen.first_lower(properties[i])+'\n'
                    param5r = param5r + '    if ('+core_gen.first_lower(properties[i])+' != null) data[\''+core_gen.first_lower(properties[i])+'\'] = '+core_gen.first_lower(properties[i])+'; \n\n'

                # gen model request
                source_path_template = 'flutter/template/user_request.dart'
                dest_path_template = project_name + '/flutter/lib/data/model/request/'+ temp

                # add param prepare to generations model request
                paramsr.append(param1r)
                paramsr.append(param2r)
                paramsr.append(param3r)
                paramsr.append(param4r)
                paramsr.append(param5r)
                core_gen.render_replace_template(source_path_template, dest_path_template, paramsr)
            
            #*******************************************************
            # ********* Gen Repository
            #*******************************************************
            ans = 50
            param1 = ''
            param2 = '' 
            param3 = '' 
            param4 = '' 
            param5 = '' 
            param6 = '' 
            params = []

            for table in tables:  
                params = []
                classname_origin = table.classname
                classname_snake = core_gen.to_snake_case(table.classname)
                classname_camel = core_gen.first_lower(table.classname)
                classname_name_file = core_gen.to_name_file_case(table.classname)
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print('phuongphuong', ans)
                print('classname_origin', classname_origin)
                print('classname_snake', classname_snake)
                print('classname_camel', classname_camel)
                print('classname_camel', classname_name_file)
                print('properties', properties) #id,name,address
                print('attributes', attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0

                param1 = classname_origin  

                # 10user_model.dart
                temp = classname_snake +'_repository.dart'

                param2 = classname_snake +'_request.dart'
                param3 = classname_name_file

                ans = ans + 10 

                # gen repository
                source_path_template = 'flutter/template/user_repository.dart'
                dest_path_template = project_name + '/flutter/lib/data/repository/'+ temp

                params.append(param2)
                params.append(param1)
                params.append(param3) 
                core_gen.render_replace_template(source_path_template, dest_path_template, params)


            #*******************************************************
            # ********* Gen provider
            #*******************************************************
            ans = 50
            param1 = ''
            param2 = '' 
            param3 = '' 
            param4 = '' 
            param5 = '' 
            param6 = '' 
            params = []

            for table in tables:  
                params = []
                classname_origin = table.classname
                classname_snake = core_gen.to_snake_case(table.classname)
                classname_camel = core_gen.first_lower(table.classname)
                classname_name_file = core_gen.to_name_file_case(table.classname)
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print('phuongphuong', ans)
                print('classname_origin', classname_origin)
                print('classname_snake', classname_snake)
                print('classname_camel', classname_camel)
                print('classname_camel', classname_name_file)
                print('properties', properties) #id,name,address
                print('attributes', attributes) #INT,VARCHAR,VARCHAR
                print(lengths)    #0,255,0


                param1 = classname_snake
                param2 = classname_origin  
                param3 = classname_camel

                temp = classname_snake +'_provider.dart'  
 
                ans = ans + 10 

                # gen repository
                source_path_template = 'flutter/template/user_provider.dart'
                dest_path_template = project_name + '/flutter/lib/provider/'+ temp

                params.append(param1)
                params.append(param2)
                params.append(param3) 
                core_gen.render_replace_template(source_path_template, dest_path_template, params)


            return project_name 
        else:
            print('project havent class')    
    
    def gen_model_flutter(self, idproject):
        project_name = models.Project.objects.filter(id = idproject)[0].name

        # copy sample to output
        core_gen.copy_folder('flutter/', project_name + '/flutter')

        tables = models.Class.objects.filter(idproject = idproject).order_by('id')
        params = []
        
        if len(tables) > 0: 
            content = ''
            import_services = ''
            blocs_import = ''
            model_export = ''
            provide_import = ''

            repository_param2 = ''
            repository_param3 = ''

            ans = 400
            print(serializers.serialize("json",  tables)) 
            for table in tables:  
                classname_origin = table.classname
                properties = table.properties.split(',')
                attributes = table.attributes.split(',')
                lengths = table.lengths.split(',')
                print('classname_origin', classname_origin) 
                print('properties', properties) #id,name,address
                print('attributes', attributes) #INT,VARCHAR,VARCHAR
                print('lengths', lengths)    #0,255,0 

                # Gen Blocs
                # b100_sinhvien_bloc.dart 
                blocs_file_name = 'b'+ str(ans) +'_'+ classname_origin.lower() +'_bloc.dart'
                blocs_class_name = 'B' + str(ans) + classname_origin + 'Bloc'
                blocs_class_name_camel = classname_origin[0].lower() + classname_origin[1:]
                blocs_import = blocs_import + 'export \''+ blocs_file_name +'\';\n'
                content = ''
                
                content = content + 'import \'../models/models.dart\';                                                                                                                                                    \n'
                content = content + 'import \'../repositories/repositories.dart\';                                                                                                                                        \n'
                content = content + 'import \'package:rxdart/rxdart.dart\';                                                                                                                                                          \n'
                content = content + '																																																\n'
                content = content + 'class '+ classname_origin +'Bloc {                                                                                                                                                            \n'
                content = content + '  final _repository = Repository();                                                                                                                                                             \n'
                content = content + '																																																\n'
                content = content + '  // for what '+ str(ans) +'                                                                                                                                                                  \n'
                content = content + '  List<M'+ str(ans) +''+ classname_origin +'Model> list'+ classname_origin +''+ str(ans) +' = [];                                                                                      \n'
                content = content + '  final _'+ blocs_class_name_camel +'Subject'+ str(ans) +' = PublishSubject<List<M'+ str(ans) +''+ classname_origin +'Model>>();                                                        \n'
                content = content + '  Stream<List<M'+ str(ans) +''+ classname_origin +'Model>> get '+ blocs_class_name_camel +'Stream'+ str(ans) +' => _'+ blocs_class_name_camel +'Subject'+ str(ans) +'.stream;       \n' 
                content = content + '																																																\n'
                content = content + '  // for what '+ str(ans+5) +'                                                                                                                                                                \n'
                content = content + '  List<M'+ str(ans) +''+ classname_origin +'Model> list'+ classname_origin +''+ str(ans+5) +' = [];                                                                                    \n'
                content = content + '  final _'+ blocs_class_name_camel +'Subject'+ str(ans+5) +' = PublishSubject<List<M'+ str(ans) +''+ classname_origin +'Model>>();                                                      \n'
                content = content + '  Stream<List<M'+ str(ans) +''+ classname_origin +'Model>> get '+ blocs_class_name_camel +'Stream'+ str(ans+5) +' => _'+ blocs_class_name_camel +'Subject'+ str(ans+5) +'.stream;   \n' 
                content = content + '																																																\n'
                content = content + '  /**                                                                                                                                                                                           \n'
                content = content + '   * dispose subject                                                                                                                                                                            \n'
                content = content + '   */                                                                                                                                                                                           \n'
                content = content + '  void dispose() {                                                                                                                                                                              \n'
                content = content + '    _'+ blocs_class_name_camel +'Subject'+ str(ans) +'.close();                                                                                                                             \n'
                content = content + '    _'+ blocs_class_name_camel +'Subject'+ str(ans+5) +'.close();                                                                                                                           \n'
                content = content + '  }                                                                                                                                                                                             \n'
                content = content + '																																																\n'
                content = content + '  /**                                                                                                                                                                                           \n'
                content = content + '   * callWhat'+ str(ans) +' get all data '+ classname_origin +'                                                                                                                             \n'
                content = content + '   */                                                                                                                                                                                           \n'
                content = content + '  callWhat'+ str(ans) +'() async {                                                                                                                                                            \n'
                content = content + '    try {                                                                                                                                                                                       \n'
                content = content + '      var what = '+ str(ans) +';                                                                                                                                                              \n'
                content = content + '																																																\n'
                content = content + '      await _repository.executeService(what, {}).then((value) {                                                                                                                                 \n'
                content = content + '        if (value.length != 0) {                                                                                                                                                                \n'
                content = content + '          list'+ classname_origin +''+ str(ans) +'.addAll(value);                                                                                                                          \n'
                content = content + '        }else{                                                                                                                                                                                  \n'
                content = content + '          list'+ classname_origin +''+ str(ans) +' = [];                                                                                                                                   \n'
                content = content + '        }                                                                                                                                                                                       \n'
                content = content + '      }).whenComplete(() {                                                                                                                                                                      \n'
                content = content + '        _'+ blocs_class_name_camel +'Subject'+ str(ans) +'.sink.add(list'+ classname_origin +''+ str(ans) +');                                                                         \n'
                content = content + '      });                                                                                                                                                                                       \n'
                content = content + '    } catch (e) {                                                                                                                                                                               \n'
                content = content + '      print(e);                                                                                                                                                                                 \n'
                content = content + '      throw e;                                                                                                                                                                                  \n'
                content = content + '    }                                                                                                                                                                                           \n'
                content = content + '  }                                                                                                                                                                                             \n'
                content = content + '																																																\n'
                content = content + '  /**                                                                                                                                                                                           \n'
                content = content + '   * callWhat'+ str(ans+5) +' get data limit                                                                                                                                                  \n'
                content = content + '   * @page : number pagination                                                                                                                                                                  \n'
                content = content + '   * @limit : limit of pagination                                                                                                                                                               \n'
                content = content + '   * @isPullRefresh: default is false                                                                                                                                                           \n'
                content = content + '   */                                                                                                                                                                                           \n'
                content = content + '  callWhat'+ str(ans+5) +'(int page, int limit, {bool isPullRefresh : false}) async {                                                                                                         \n'
                content = content + '    try {                                                                                                                                                                                       \n'
                content = content + '      var what = '+ str(ans+5) +';                                                                                                                                                            \n'
                content = content + '      var param = {"offset": page * limit, "limit": limit};                                                                                                                                     \n'
                content = content + '																																																\n'
                content = content + '      await _repository.executeService(what, param).then((value) {                                                                                                                              \n'
                content = content + '        if (value.length != 0) {                                                                                                                                                                \n'
                content = content + '          // clear data when pull refresh                                                                                                                                                       \n'
                content = content + '          if (isPullRefresh == true) {                                                                                                                                                          \n'
                content = content + '            list'+ classname_origin +''+ str(ans+5) +' = [];                                                                                                                               \n'
                content = content + '            list'+ classname_origin +''+ str(ans+5) +'.addAll(value);                                                                                                                      \n'
                content = content + '          } else {                                                                                                                                                                              \n'
                content = content + '            // add more data                                                                                                                                                                    \n'
                content = content + '            list'+ classname_origin +''+ str(ans+5) +'.addAll(value);                                                                                                                      \n'
                content = content + '          }                                                                                                                                                                                     \n'
                content = content + '        }                                                                                                                                                                                       \n'
                content = content + '      }).whenComplete(() {                                                                                                                                                                      \n'
                content = content + '        _'+ blocs_class_name_camel +'Subject'+ str(ans+5) +'.sink.add(list'+ classname_origin +''+ str(ans+5) +');                                                                     \n'
                content = content + '      });                                                                                                                                                                                       \n'
                content = content + '    } catch (e) {                                                                                                                                                                               \n'
                content = content + '      print(e);                                                                                                                                                                                 \n'
                content = content + '      throw e;                                                                                                                                                                                  \n'
                content = content + '    }                                                                                                                                                                                           \n'
                content = content + '  }                                                                                                                                                                                             \n'
                content = content + '}                                                                                                                                                                                               \n'
                
                # create file blocs 
                core_gen.create_file(project_name + '/flutter/blocs/' + blocs_file_name, content)


                # Gen Models
                model_file_name = 'm'+ str(ans) +'_'+ classname_origin.lower() +'_model.dart'
                model_class_name = 'M' + str(ans) + classname_origin + 'Model' 
                model_export = model_export + 'export \''+ model_file_name +'\';\n'
                content = ''
                param1 = ''
                param2 = ''
                param3 = ''
                param4 = ''

                for propertie in properties:
                    param1 = param1 + '  String? '+ propertie +';\n'
                    param2 = param2 + 'this.'+ propertie +','
                    param3 = param3 + '    '+ propertie +' = json[\''+ propertie +'\'];\n'
                    param4 = param4 + '    data[\''+ propertie +'\'] = this.'+ propertie +';\n'


                param2 = param2[:-1]
                param3 = param3[:-1]
                param4 = param4[:-1]

                content = content + 'class '+ model_class_name +' {                                           \n'
                content = content + ''+ param1 +'                                                                 \n'
                content = content + '																	\n'
                content = content + '  '+ model_class_name +'({'+ param2 +'});                                         \n'
                content = content + '																	\n'
                content = content + '  '+ model_class_name +'.fromJson(Map<String, dynamic> json) {           \n'
                content = content + ''+ param3 +'                                                                 \n'
                content = content + '  }                                                                 \n'
                content = content + '																	\n'
                content = content + '  Map<String, dynamic> toJson() {                                   \n'
                content = content + '    final Map<String, dynamic> data = new Map<String, dynamic>();   \n'
                content = content + ''+ param4 +'                                                                 \n'
                content = content + '    return data;                                                    \n'
                content = content + '  }                                                                 \n'
                content = content + '}                                                                   \n'

                # create file models 
                core_gen.create_file(project_name + '/flutter/models/' + model_file_name, content)


                # Gen repositorys
                provide_file_name = 'r'+ str(ans) +'_'+ classname_origin.lower() +'_provider.dart'
                provide_class_name = 'R' + str(ans) + classname_origin + 'Provider' 
                provide_class_name_first_lower = provide_class_name[0].lower() + provide_class_name[1:] 
                provide_import = provide_import + 'import \''+ provide_file_name +'\';\n'
                content = ''
                
                repository_param2 = repository_param2 + '  final '+ provide_class_name_first_lower +' = '+ provide_class_name +'();\n'
                repository_param3 = repository_param3 + '      if (what >= '+ str(ans) +' && what < '+ str(ans+100) +') {                           \n'
                repository_param3 = repository_param3 + '        // call provider '+ classname_origin +'                               \n'
                repository_param3 = repository_param3 + '        return r'+ str(ans) +''+ classname_origin +'Provider.p'+ str(ans) +''+ classname_origin +'(what, param);  \n'
                repository_param3 = repository_param3 + '      }                                                         \n\n'
                
                content = content + 'import \'dart:convert\';                                                                                               \n'
                content = content + '																													   \n'
                content = content + 'import \'../models/models.dart\';                                                                                      \n'
                content = content + 'import \'../helper/dioulti.dart\';                                                                                     \n'
                content = content + '																													   \n'
                content = content + 'class R'+ str(ans) +''+ classname_origin +'Provider {                                                                  \n'
                content = content + '  /**                                                                                                                  \n'
                content = content + '   * R'+ str(ans) +''+ classname_origin +'Provider                                                                     \n'
                content = content + '   */                                                                                                                  \n'
                content = content + '  R'+ str(ans) +''+ classname_origin +'Provider() {                                                                    \n'
                content = content + '    DioUtil.tokenInter();                                                                                              \n'
                content = content + '  }                                                                                                                    \n'
                content = content + '																													   \n'
                content = content + '  /**                                                                                                                  \n'
                content = content + '   *                                                                                                                   \n'
                content = content + '   */                                                                                                                  \n'
                content = content + '  Future<dynamic> p'+ str(ans) +''+ classname_origin +'(int what, Map<String, dynamic> param) async {                  \n'
                content = content + '																													   \n'
                content = content + '    param[\'what\'] = what;                                                                                            \n'
                content = content + '																													   \n'
                content = content + '    switch (what) {                                                                                                    \n'
                content = content + '    // Get all data from '+ classname_origin +'                                                                        \n'
                content = content + '      case '+ str(ans) +':                                                                                             \n'
                content = content + '        try {                                                                                                          \n'
                content = content + '          final response = await DioUtil.post(param);                                                             \n'
                content = content + '          Map<String, dynamic> result = jsonDecode(response.toString());                                               \n'
                content = content + '          if (result[\'status\'] == true) {                                                                            \n'
                content = content + '            return List<M'+ str(ans) +''+ classname_origin +'Model>.from(                                              \n'
                content = content + '                result[\'data\'].map((model) => M'+ str(ans) +''+ classname_origin +'Model.fromJson(model)));          \n'
                content = content + '          } else {                                                                                                     \n'
                content = content + '            throw Exception(result[\'error\']);                                                                        \n'
                content = content + '          }                                                                                                            \n'
                content = content + '        } catch (e) {                                                                                                  \n'
                content = content + '          throw e;                                                                                                     \n'
                content = content + '        }                                                                                                              \n'
                content = content + '        break;                                                                                                         \n'
                content = content + '																													   \n'
                content = content + '    // Insert data to '+ classname_origin +'                                                                           \n'
                content = content + '      case '+ str(ans+1) +':                                                                                           \n'
                content = content + '        try {                                                                                                          \n'
                content = content + '          final response = await DioUtil.post(param);                                                                  \n'
                content = content + '          Map<String, dynamic> result = jsonDecode(response.toString());                                               \n'
                content = content + '          if (result[\'status\'] == true) {                                                                            \n'
                content = content + '            return List<M'+ str(ans) +''+ classname_origin +'Model>.from(                                              \n'
                content = content + '                result[\'data\'].map((model) => M'+ str(ans) +''+ classname_origin +'Model.fromJson(model)));          \n'
                content = content + '          } else {                                                                                                     \n'
                content = content + '            throw Exception(result[\'error\']);                                                                        \n'
                content = content + '          }                                                                                                            \n'
                content = content + '        } catch (e) {                                                                                                  \n'
                content = content + '          print("AuthenticationService authWithToken $e");                                                             \n'
                content = content + '          throw e;                                                                                                     \n'
                content = content + '        }                                                                                                              \n'
                content = content + '        break;                                                                                                         \n'
                content = content + '																													   \n'
                content = content + '    // Update data '+ classname_origin +'                                                                              \n'
                content = content + '      case '+ str(ans+2) +':                                                                                           \n'
                content = content + '        try {                                                                                                          \n'
                content = content + '          final response = await DioUtil.post(param);                                                                  \n'
                content = content + '          Map<String, dynamic> result = jsonDecode(response.toString());                                               \n'
                content = content + '          if (result[\'status\'] == true) {                                                                            \n'
                content = content + '            return List<M'+ str(ans) +''+ classname_origin +'Model>.from(                                              \n'
                content = content + '                result[\'data\'].map((model) => M'+ str(ans) +''+ classname_origin +'Model.fromJson(model)));          \n'
                content = content + '          } else {                                                                                                     \n'
                content = content + '            throw Exception(result[\'error\']);                                                                        \n'
                content = content + '          }                                                                                                            \n'
                content = content + '        } catch (e) {                                                                                                  \n'
                content = content + '          throw e;                                                                                                     \n'
                content = content + '        }                                                                                                              \n'
                content = content + '        break;                                                                                                         \n'
                content = content + '																													   \n'
                content = content + '    // Delete data of '+ classname_origin +'                                                                           \n'
                content = content + '      case '+ str(ans+3) +':                                                                                           \n'
                content = content + '        try {                                                                                                          \n'
                content = content + '          final response = await DioUtil.post(param);                                                                  \n'
                content = content + '          Map<String, dynamic> result = jsonDecode(response.toString());                                               \n'
                content = content + '          if (result[\'status\'] == true) {                                                                            \n'
                content = content + '            return List<M'+ str(ans) +''+ classname_origin +'Model>.from(                                              \n'
                content = content + '                result[\'data\'].map((model) => M'+ str(ans) +''+ classname_origin +'Model.fromJson(model)));          \n'
                content = content + '          } else {                                                                                                     \n'
                content = content + '            throw Exception(result[\'error\']);                                                                        \n'
                content = content + '          }                                                                                                            \n'
                content = content + '        } catch (e) {                                                                                                  \n'
                content = content + '          throw e;                                                                                                     \n'
                content = content + '        }                                                                                                              \n'
                content = content + '        break;                                                                                                         \n'
                content = content + '																													   \n'
                content = content + '    // Find data with id '+ classname_origin +'                                                                        \n'
                content = content + '      case '+ str(ans+4) +':                                                                                           \n'
                content = content + '        try {                                                                                                          \n'
                content = content + '          final response = await DioUtil.post(param);                                                                  \n'
                content = content + '          Map<String, dynamic> result = jsonDecode(response.toString());                                               \n'
                content = content + '          if (result[\'status\'] == true) {                                                                            \n'
                content = content + '            return List<M'+ str(ans) +''+ classname_origin +'Model>.from(                                              \n'
                content = content + '                result[\'data\'].map((model) => M'+ str(ans) +''+ classname_origin +'Model.fromJson(model)));          \n'
                content = content + '          } else {                                                                                                     \n'
                content = content + '            throw Exception(result[\'error\']);                                                                        \n'
                content = content + '          }                                                                                                            \n'
                content = content + '        } catch (e) {                                                                                                  \n'
                content = content + '          throw e;                                                                                                     \n'
                content = content + '        }                                                                                                              \n'
                content = content + '        break;                                                                                                         \n'
                content = content + '																													   \n'
                content = content + '    // Select with pagination(offset, number-item-in-page) '+ classname_origin +'                                      \n'
                content = content + '      case '+ str(ans+5) +':                                                                                           \n'
                content = content + '        try {                                                                                                          \n'
                content = content + '          final response = await DioUtil.post(param);                                                                  \n'
                content = content + '          Map<String, dynamic> result = jsonDecode(response.toString());                                               \n'
                content = content + '          if (result[\'status\'] == true) {                                                                            \n'
                content = content + '            return List<M'+ str(ans) +''+ classname_origin +'Model>.from(                                              \n'
                content = content + '                result[\'data\'].map((model) => M'+ str(ans) +''+ classname_origin +'Model.fromJson(model)));          \n'
                content = content + '          } else {                                                                                                     \n'
                content = content + '            throw Exception(result[\'error\']);                                                                        \n'
                content = content + '          }                                                                                                            \n'
                content = content + '        } catch (e) {                                                                                                  \n'
                content = content + '          throw e;                                                                                                     \n'
                content = content + '        }                                                                                                              \n'
                content = content + '        break;                                                                                                         \n'
                content = content + '																													   \n'
                content = content + '    // Count number item of '+ classname_origin +'                                                                     \n'
                content = content + '      case '+ str(ans+6) +':                                                                                           \n'
                content = content + '        try {                                                                                                          \n'
                content = content + '          final response = await DioUtil.post(param);                                                                  \n'
                content = content + '          Map<String, dynamic> result = jsonDecode(response.toString());                                               \n'
                content = content + '          if (result[\'status\'] == true) {                                                                            \n'
                content = content + '            return List<M'+ str(ans) +''+ classname_origin +'Model>.from(                                              \n'
                content = content + '                result[\'data\'].map((model) => M'+ str(ans) +''+ classname_origin +'Model.fromJson(model)));          \n'
                content = content + '          } else {                                                                                                     \n'
                content = content + '            throw Exception(result[\'error\']);                                                                        \n'
                content = content + '          }                                                                                                            \n'
                content = content + '        } catch (e) {                                                                                                  \n'
                content = content + '          throw e;                                                                                                     \n'
                content = content + '        }                                                                                                              \n'
                content = content + '        break;                                                                                                         \n'
                content = content + '																													   \n'
                content = content + '      default:                                                                                                         \n'
                content = content + '        {                                                                                                              \n'
                content = content + '          //statements;                                                                                                \n'
                content = content + '        }                                                                                                              \n'
                content = content + '        break;                                                                                                         \n'
                content = content + '    }                                                                                                                  \n'
                content = content + '  }                                                                                                                    \n'
                content = content + '}                                                                                                                      \n'

                # create file provider 
                core_gen.create_file(project_name + '/flutter/repositories/' + provide_file_name, content)        

                  
                content = ''
                
                ans = ans + 100 

            params.append(import_services)


            # Blobs import 
            source_path_template = 'flutter/blocs/blocs.dart'
            dest_path_template = project_name + '/flutter/blocs/blocs.dart'
            core_gen.render_replace(source_path_template, dest_path_template, [blocs_import])

            # Models import 
            source_path_template = 'flutter/models/models.dart'
            dest_path_template = project_name + '/flutter/models/models.dart'
            core_gen.render_replace(source_path_template, dest_path_template, [model_export])

            # create repositories
            content = ''
            content = content + 'import \'dart:async\';                                                       \n'
            content = content + '																			 \n'
            content = content + ''+ provide_import +'                                                                          \n'
            content = content + '																			 \n'
            content = content + 'class Repository {                                                           \n'
            content = content + ''+ repository_param2 +'                                                                          \n'
            content = content + '																			 \n'
            content = content + '  /**                                                                        \n'
            content = content + '   * execute Service                                                         \n'
            content = content + '   */                                                                        \n'
            content = content + '  Future<dynamic> executeService(int what, Map<String, dynamic> param) async{     \n'
            content = content + '    try {                                                                    \n'
            content = content + '      param[\'what\'] = what;                                                \n'
            content = content + ''+ repository_param3 +'                                                                          \n'
            content = content + '																			 \n'
            content = content + '    } catch (e) {                                                            \n'
            content = content + '      throw e;                                                               \n'
            content = content + '    }                                                                        \n'
            content = content + '  }                                                                          \n'
            content = content + '}                                                                            \n'

            # create file            
            core_gen.create_file(project_name + '/flutter/repositories/repositories.dart', content)

            print('finish gen flutter')
            return project_name

        else:
            print('project havent class')
    
