export class CRUD {
    id?: Number;
    idproject?: string;
    idclass?: string;
    createdate?: string;
    actiontype?: string; // (0,1,2,3(list,create,update,delete))
    titles?: string;
    properties?: string;
    alias?: string;
    refers?: string; // (class:value:display)
    titlecreate?: string;
    titleupdate?: string;
    inputtypes?: string;
    validates?: string;
    formats?: string;
}
