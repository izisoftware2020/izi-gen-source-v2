export class Class {
    id?: Number;
    idproject?: string;
    classname?: string;
    properties?: string;
    attributes?: string;
    lengths?: string;
}
