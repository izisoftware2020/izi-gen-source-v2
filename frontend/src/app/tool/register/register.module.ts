import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransferHttpCacheModule } from '@nguniversal/common';
// import { RegisterComponent } from './register.component';

@NgModule({
  declarations: [
    // RegisterComponent
  ],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      // {
      //   path: '', component: RegisterComponent
      // }
    ]),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class RegisterModule { }
