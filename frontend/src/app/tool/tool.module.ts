import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiService } from './common/api-service/api.service'; 

@NgModule({
  declarations: [],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        loadChildren: () =>
          import('./home-page/home-page.module').then((m) => m.HomePageModule),
      },
      {
        path: 'login',
        loadChildren: () =>
          import('./login/login.module').then((m) => m.LoginModule),
      },
    ]),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [ 
    ApiService,
  ],
})
export class ToolModule {}
