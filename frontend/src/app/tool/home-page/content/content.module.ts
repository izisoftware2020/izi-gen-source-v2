import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common'; 

@NgModule({
    declarations: [ContentComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                // ng g module home-page/content/sellTicket --module content
                // ng g c home-page/content/sellTicket

                path: '',
                component: ContentComponent,
                children: [

                    {
                        path: '',
                        loadChildren: () =>
                            import('./a1-project/project.module').then(
                                m => m.ProjectModule
                            )
                    },
                    {
                        path: 'class',
                        loadChildren: () =>
                            import('./a2-class/class.module').then(
                                m => m.ClassModule
                            )
                    },
                    {
                        path: 'crud',
                        loadChildren: () =>
                            import('./a3-crud/crud.module').then(
                                m => m.CrudModule
                            )
                    },

                ]
            }
        ]),

    ],
    providers: [],
    entryComponents: []
})
export class ContentModule { }
