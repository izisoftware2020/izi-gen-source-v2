import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrudComponent } from './crud.component';
import { RouterModule } from '@angular/router';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog'; 
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ListDialogComponent } from './a1-list-dialog/list-dialog.component';
import { CreateDialogComponent } from './a2-create-dialog/create-dialog.component';
import { UpdateDialogComponent } from './a3-update-dialog/update-dialog.component';
import { DeleteDialogComponent } from './a4-delete-dialog/delete-dialog.component';

@NgModule({
  declarations: [
    CrudComponent,
    ListDialogComponent,
    CreateDialogComponent,
    UpdateDialogComponent,
    DeleteDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: CrudComponent, children: [
          
        ],
      }
    ]),
    MatCardModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    MatSnackBarModule,
  ]
})
export class CrudModule { }
