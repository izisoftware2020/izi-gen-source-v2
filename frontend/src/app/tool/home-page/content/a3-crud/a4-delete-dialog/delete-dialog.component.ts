import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs/internal/Subscription';
import { ApiService } from 'src/app/tool/common/api-service/api.service';
import { Project } from 'src/app/tool/common/models/100project.models';
import { Class } from 'src/app/tool/common/models/200class.models';
import { CRUD } from 'src/app/tool/common/models/300crud.models';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {
  // subscription
  subscription: Subscription[] = [];

  // list class
  projects: Project;
  classes: Class;

  idProject: string = '';
  idClass: string = '';

  crud: CRUD = {};

  type: number = 0;

  /**
   * constructor
   * @param dialogRef 
   * @param data 
   * @param dialog 
   */
  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private api: ApiService
  ) {
    console.log('data nhan duoc', data);
    this.type = data.type

    // if update
    if (this.type == 1) {
      this.crud = data.input;

      // load data project and class
      this.idProject = this.crud.idproject;
      this.idClass = this.crud.idclass;
    } else {
      // load data project and class
      this.idProject = data.idProject;
      this.idClass = data.idClass;
    }
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    // get data projects
    this.getDataProjects();

    // get data classes
    this.getDataClass();
  }

  /**
  * Get data project
  */
  getDataProjects() {
    const param = {};

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        if (data.length > 0) {
          this.projects = data;
        }
      }));
  }

  /**
   * Get data class
   */
  getDataClass() {
    const param = { idproject: this.idProject };

    //select data classes
    this.subscription.push(this.api.excuteAllByWhat(param, '207').subscribe((data) => {
      if (data.length > 0) {
        this.classes = data;
      }
    }));
  }

  /**
   * on Project Selection Change
   */
  onProjectSelectionChange() {
    // get class by project
    this.getDataClass();
  }

  /**
   * onBtnProcessClick
   */
  onBtnProcessClick() {
    // init data prepare for insert
    this.crud.idproject = this.idProject;
    this.crud.idclass = this.idClass;
    this.crud.createdate = this.api.formatDate(new Date());
    this.crud.actiontype = '3';

    this.crud.titles = '';
    this.crud.properties = '';
    this.crud.alias = '';
    this.crud.formats = '';
    this.crud.refers = '';

    this.crud.titlecreate = '';
    this.crud.titleupdate = '';
    this.crud.inputtypes = '';
    this.crud.validates = '';

    //if type = 0 => Insert data else update data
    this.subscription.push(this.api.excuteAllByWhat(this.crud, '' + Number(301 + this.type) + '')
      .subscribe((data) => {
        this.dialogRef.close({ status: true });
        this.api.showSuccess('Xử Lý Thành Công ');
      })
    );
  }

  /**
   * cancel click
   */
  onBtnCancelClick(): void {
    this.dialogRef.close({ status: false });
  }

}
