import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/tool/common/api-service/api.service';

import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

import { ListDialogComponent } from './a1-list-dialog/list-dialog.component';
import { CreateDialogComponent } from './a2-create-dialog/create-dialog.component';
import { UpdateDialogComponent } from './a3-update-dialog/update-dialog.component';
import { DeleteDialogComponent } from './a4-delete-dialog/delete-dialog.component';
import { Project } from 'src/app/tool/common/models/100project.models';
import { Class } from 'src/app/tool/common/models/200class.models';
import { CRUD } from 'src/app/tool/common/models/300crud.models';

export interface PeriodicElement {
  id: number;
  actionType: string;
  createDate: string;
}

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})

export class CrudComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  /** for table */
  displayedColumns: string[] = ['id', 'actionType', 'createDate', 'action'];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1
      }`;
  }
  // end for table;

  //subscription
  subscription: Subscription[] = [];

  // list class
  crud: CRUD;
  projects: Project;
  idProject: string;

  classes: Class;
  idClass: string;

  /**
    * constructor
    * @param dialog 
    * @param _snackBar
    * @param api 
    */
  constructor(private api: ApiService, public dialog: MatDialog, private _snackBar: MatSnackBar) { }


  /**
   * ngOnInit
   */
  ngOnInit() {
    this.getDataProjects();
  }

  /**
   * onProjectSelectionChange
   */
  onProjectSelectionChange() {
    // get class by project
    this.getDataClass();
  }

  /**
   * onClassSelectionChange
   */
  onClassSelectionChange() {
    this.getDataCRUD();
  }

  /**
   * Get List Project
   */
  getDataProjects() {
    const param = {};

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        if (data.length > 0) {
          this.projects = data;
          this.idProject = this.projects[0].id;

          // get class by project
          this.getDataClass();
        }
      }));
  }

  /**
   * Get List Class with Project selected
   */
  getDataClass() {
    const param = { idproject: this.idProject };
    //select data classes
    this.subscription.push(
      this.api.excuteAllByWhat(param, '207').subscribe((data) => {
        if (data.length > 0) {
          this.classes = data;
          this.idClass = this.classes[0].id;

          // get crud by class
          this.getDataCRUD();
        }
      }));
  }

  /**
   * get data CRUD
   */
  getDataCRUD() {
    const param = {
      idproject: this.idProject,
      idclass: this.idClass
    };

    this.subscription.push(
      this.api.excuteAllByWhat(param, '307').subscribe((data) => {
        console.log('data crud', data);

        if (data.length > 0) {
          // set data for table
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.dataSource = new MatTableDataSource([]);
        }

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);
      })
    );
  }

  /**
   * get Action Type By Id
   * @param actionType 
   */
  getActionTypeById(actionType) {
    switch (Number(actionType)) {
      case 0:
        return 'LIST';
      case 1:
        return 'INSERT';
      case 2:
        return 'UPDATE';
      case 3:
        return 'DELETE';
    }
  }

  /**
   * on Btn List Click
   * @param element 
   */
  onBtnListClick(): void {
    const dialogRef = this.dialog.open(ListDialogComponent, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',
      // data: element
      data: { type: 0, idProject: this.idProject, idClass: this.idClass }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.status == true) {
        // filter data
        this.getDataCRUD();
        return;
      }
    });
  }

  /**
   * on Btn Insert Click
   * @param element 
   */
  onBtnInsertClick(): void {
    const dialogRef = this.dialog.open(CreateDialogComponent, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',
      // data: element
      data: { type: 0, idProject: this.idProject, idClass: this.idClass }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.status == true) {
        // filter data
        this.getDataCRUD();
        return;
      }
    });
  }

  /**
   * on Btn Update Click
   * @param element 
   */
  onBtnUpdateClick(): void {
    const dialogRef = this.dialog.open(UpdateDialogComponent, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',
      // data: element
      data: { type: 0, idProject: this.idProject, idClass: this.idClass }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.status == true) {
        // filter data
        this.getDataCRUD();
        return;
      }
    });
  }

  /**
   * on Btn Delete Click
   * @param element 
   */
  onBtnDeleteClick(): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',
      // data: element
      data: { type: 0, idProject: this.idProject, idClass: this.idClass }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.status == true) {
        // filter data
        this.getDataCRUD();
        return;
      }
    });
  }

  /**
   * openDialogUpdate
   * @param element 
   */
  openDialogUpdate(element): void {
    let id = element.actionType;
    console.log(element);
    switch (element.actiontype) {
      // open ListDialogComponent
      case '0': {
        const dialogRef = this.dialog.open(ListDialogComponent, {
          width: '100%',
          height: '100%',
          maxWidth: '100%',
          maxHeight: '100%',
          data: { type: 1, input: element }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.status == true) {
            // filter data
            this.getDataCRUD();
            return;
          }
        });
        break;
      }

      // open CreateDialogComponent
      case '1': {
        const dialogRef = this.dialog.open(CreateDialogComponent, {
          width: '100%',
          height: '100%',
          maxWidth: '100%',
          maxHeight: '100%',
          data: { type: 1, input: element }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.status == true) {
            // filter data
            this.getDataCRUD();
            return;
          }
        });
        break;
      }

      // open UpdateDialogComponent
      case '2': {
        const dialogRef = this.dialog.open(UpdateDialogComponent, {
          width: '100%',
          height: '100%',
          maxWidth: '100%',
          maxHeight: '100%',
          data: { type: 1, input: element }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.status == true) {
            // filter data
            this.getDataCRUD();
            return;
          }
        });
        break;
      }

      // open DeleteDialogComponent
      case '3': {
        const dialogRef = this.dialog.open(DeleteDialogComponent, {
          width: '100%',
          height: '100%',
          maxWidth: '100%',
          maxHeight: '100%',
          data: { type: 1, input: element }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.status == true) {
            // filter data
            this.getDataCRUD();
            return;
          }
        });
        break;
      }
    }
  }

  /**
   * openSnackBar confirm delete project
   * @param element
   */
  openSnackBar(element) {
    let snackBarRef = this._snackBar.open('Bạn có chắc chắn xóa record này không?', 'Đồng ý', {
      duration: 4000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });

    snackBarRef.onAction().subscribe(() => {
      // delete project
      this.subscription.push(
        this.api.excuteAllByWhat(element, '303').subscribe((data) => {
          // load data grid
          this.getDataCRUD();

          // scroll top
          window.scroll({ left: 0, top: 0, behavior: 'smooth' });

          // show toast success
          this.api.showSuccess('Xóa thành công ');
        })
      );
    });
  }
}
