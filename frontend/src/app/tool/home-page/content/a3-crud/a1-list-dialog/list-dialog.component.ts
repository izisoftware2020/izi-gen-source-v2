import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog'; 
import { Subscription } from 'rxjs';
import { Project } from 'src/app/tool/common/models/100project.models';
import { Class } from 'src/app/tool/common/models/200class.models';
import { CRUD } from 'src/app/tool/common/models/300crud.models';
import { ApiService } from 'src/app/tool/common/api-service/api.service';

@Component({
  selector: 'app-list-dialog',
  templateUrl: './list-dialog.component.html',
  styleUrls: ['./list-dialog.component.scss']
})
export class ListDialogComponent implements OnInit {

  // subscription
  subscription: Subscription[] = [];

  // list class
  projects: Project;
  classes: Class;
  properties: any[] = [];
  allClass: any[] = [];

  dataClass: any[] = [];

  idProject: string = '';
  idClass: string = '';

  // list actions
  actions = [
    { id: 0, value: 'LIST' },
    { id: 1, value: 'CREATE' },
    { id: 2, value: 'UPDATE' },
    { id: 3, value: 'DELETE' },
  ];

  crud: CRUD = {};

  // type
  type: number = 0;

  // data list Properties And Attributes
  listItems: any[] = [
    {
      title: '',
      property: '',
      alias: '',
      format: '',
      class: '',
      value: '',
      display: '',
    },
  ];

  /**
   * constructor
   * @param api 
   * @param dialogRef 
   * @param data 
   * @param dialog 
   */
  constructor(private api: ApiService, public dialogRef: MatDialogRef<ListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog) {
    this.type = data.type

    console.log('data nhan duoc', data);

    // if update
    if (this.type == 1) {
      this.crud = data.input;

      this.decodeDataCRUD();
    } else {
      // load data project and class
      this.idProject = data.idProject;
      this.idClass = data.idClass;
    }
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    // get data project  
    this.getDataProjects();

    // get data class
    this.getDataClass();

    // load data properties of class
    this.loadDataProperties();
  }

  /**
   * decode Data CRUD
   */
  decodeDataCRUD() {
    // load data project and class
    this.idProject = this.crud.idproject;
    this.idClass = this.crud.idclass;

    let titles = this.crud.titles.split(',');
    let properties = this.crud.properties.split(',');
    let alias = this.crud.alias.split(',');
    let formats = this.crud.formats.split(',');
    let refers = this.crud.refers.split(',');

    for (var i = 0; i < properties.length; i++) {
      let refersArray = refers[i].split(':');
      this.listItems.push(
        {
          title: titles[i],
          property: properties[i],
          alias: alias[i],
          format: formats[i],
          class: refersArray[0],
          value: refersArray[1],
          display: refersArray[2],
        }
      );
    }
    this.listItems.splice(0, 1);
  }

  /**
   * add Item
   */
  addItems() {
    this.listItems.push(
      {
        title: '',
        property: '',
        alias: '',
        format: '',
        class: '',
        value: '',
        display: '',
      }
    );
    console.log(this.listItems);
  }

  /**
   * remove Item
   * @param index 
   */
  removeItem(index) {
    this.listItems.splice(index, 1);
  }


  /**
  * Get data project
  */
  getDataProjects() {
    const param = {};

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        if (data.length > 0) {
          this.projects = data;
        }
      }));
  }

  /**
   * Get data class
   */
  getDataClass() {
    const param = { idproject: this.idProject };

    //select data classes
    this.subscription.push(this.api.excuteAllByWhat(param, '207').subscribe((data) => {
      if (data.length > 0) {
        this.classes = data;

        // get all class by project
        this.getAllClassByProject();
      }
    }));
  }

  /**
   * getAllClassByProject
   */
  getAllClassByProject() {
    const param = { idproject: this.idProject };

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '207').subscribe((data) => {
        if (data.length > 0) {
          this.allClass = data;
        }
      }));
  }

  /**
   * load data properties
   */
  loadDataProperties() {
    const param = { id: this.idClass };
    this.dataClass = [];

    //select data classes
    this.subscription.push(this.api.excuteAllByWhat(param, '204').subscribe((data) => {
      if (data.length > 0) {

        // process propertites, attributes and lengths
        let properties = data[0].properties.split(',');
        let attributes = data[0].attributes.split(',');
        let lengths = data[0].lengths.split(',');

        properties.forEach((item, index) => {
          this.dataClass.push({
            'index': index,
            'property': properties[index],
            'attribute': attributes[index],
            'length': lengths[index],
          });
        });
      }
    }));
  }

  /**
   * on Project Selection Change
   */
  onProjectSelectionChange() {
    // get class by project
    this.getDataClass();
  }

  /**
   * on Class Selection Change
   */
  onClassSelectionChange() {
    this.loadDataProperties();
  }

  /**
   * on Property Selection Change
   * @param item 
   */
  onPropertySelectionChange(item) {
    item.title = item.property;
    item.alias = item.property;
  }

  /**
   * getPropertiesByIdClass
   * @param id 
   */
  getPropertiesByIdClass(idClass) {
    // select data classes
    return this.allClass.filter(x => x.id == idClass)[0]?.properties.split(',');
  }


  /**
   * onBtnProcessClick
   */
  onBtnProcessClick() {
    // init data prepare for insert
    this.crud.idproject = this.idProject;
    this.crud.idclass = this.idClass;
    this.crud.createdate = this.api.formatDate(new Date());
    this.crud.actiontype = '0';

    this.crud.titles = '';
    this.crud.properties = '';
    this.crud.alias = '';
    this.crud.formats = '';
    this.crud.refers = '';

    this.crud.titlecreate = '';
    this.crud.titleupdate = '';
    this.crud.inputtypes = '';
    this.crud.validates = '';

    // update value titles, properties, alias, formats, refers
    this.listItems.forEach((element, index) => {
      if (index == 0) {
        this.crud.titles += element.title;
        this.crud.properties += element.property;
        this.crud.alias += element.alias;
        this.crud.formats += element.format;
        this.crud.refers += element.class + ':' + element.value + ':' + element.display;
      } else {
        this.crud.titles += ',' + element.title;
        this.crud.properties += ',' + element.property;
        this.crud.alias += ',' + element.alias;
        this.crud.formats += ',' + element.format;
        this.crud.refers += ',' + element.class + ':' + element.value + ':' + element.display;
      }
    });

    //if type = 0 => Insert data else update data
    this.subscription.push(this.api.excuteAllByWhat(this.crud, '' + Number(301 + this.type) + '')
      .subscribe((data) => {
        this.dialogRef.close({ status: true });
        this.api.showSuccess('Xử Lý Thành Công ');
      })
    );
  }

  /**
   * on Btn Cancel Click
   */
  onBtnCancelClick(): void {
    this.dialogRef.close({ status: false });
  }

}
