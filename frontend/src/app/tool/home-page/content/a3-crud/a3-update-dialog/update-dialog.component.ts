import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs'; 
import { CreateDialogComponent } from '../a2-create-dialog/create-dialog.component';
import { ApiService } from 'src/app/tool/common/api-service/api.service';
import { Project } from 'src/app/tool/common/models/100project.models';
import { Class } from 'src/app/tool/common/models/200class.models';
import { CRUD } from 'src/app/tool/common/models/300crud.models';

@Component({
  selector: 'app-update-dialog',
  templateUrl: './update-dialog.component.html',
  styleUrls: ['./update-dialog.component.scss']
})
export class UpdateDialogComponent implements OnInit {
  // subscription
  subscription: Subscription[] = [];

  // list class
  projects: Project;
  classes: Class;
  properties: any[] = [];
  allClass: any[] = [];

  dataClass: any[] = [];

  idProject: string = '';
  idClass: string = '';

  crud: CRUD = {};

  // list actions
  actions = [
    { id: 1, value: 'List' },
    { id: 2, value: 'Create' },
    { id: 3, value: 'Update' },
    { id: 4, value: 'Delete' },
  ];

  // list input type
  inputTypes = [
    { id: 0, value: 'varchar' },
    { id: 1, value: 'number' },
    { id: 2, value: 'email' },
    { id: 3, value: 'password' },
    { id: 4, value: 'date' },
    { id: 5, value: 'ckeditor' },
    { id: 6, value: 'datetime' },
    { id: 7, value: 'checkbox' },
    { id: 8, value: 'radio' },
    { id: 9, value: 'select' },
    { id: 10, value: 'hidden' },
    { id: 11, value: 'month' },
    { id: 12, value: 'tel' },
    { id: 13, value: 'file' },
    { id: 14, value: 'image' },
    { id: 15, value: 'url' },
    { id: 16, value: 'week' },
  ];

  // list validators
  validators = [
    { id: 0, value: 'min' },
    { id: 1, value: 'max' },
    { id: 2, value: 'required' },
    { id: 3, value: 'requiredTrue' },
    { id: 4, value: 'email' },
    { id: 5, value: 'minLength' },
    { id: 6, value: 'maxLength' },
    { id: 7, value: 'pattern' },
    { id: 8, value: 'nullValidator' },
    { id: 9, value: 'compose' },
    { id: 10, value: 'composeAsync' },
  ];

  //data list Properties And Attributes
  listItems: any[] = [
    {
      title: '',
      property: '',
      inputtype: '',
      validate: '',
      class: '',
      value: '',
      display: '',
    },
  ];

  // type
  type: number = 0;

  constructor(public dialogRef: MatDialogRef<UpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private api: ApiService
  ) {
    console.log('data nhan duoc', data);
    this.type = data.type

    // if update
    if (this.type == 1) {
      this.crud = data.input;

      this.decodeDataCRUD();
    } else {
      // load data project and class
      this.idProject = data.idProject;
      this.idClass = data.idClass;
    }
  }

  ngOnInit(): void {
    // get data project  
    this.getDataProjects();

    // get data class
    this.getDataClass();

    // load data properties of class
    this.loadDataProperties();
  }

  /**
   * decode Data CRUD
   */
  decodeDataCRUD() {
    // load data project and class
    this.idProject = this.crud.idproject;
    this.idClass = this.crud.idclass;

    let titles = this.crud.titles.split(',');
    let properties = this.crud.properties.split(',');
    let inputtypes = this.crud.inputtypes.split(',');
    let validates = this.crud.validates.split(',');
    let refers = this.crud.refers.split(',');

    for (var i = 0; i < properties.length; i++) {
      let refersArray = refers[i].split(':');
      this.listItems.push(
        {
          title: titles[i],
          property: properties[i],
          inputtype: inputtypes[i],
          validate: validates[i].split(':'),
          class: refersArray[0],
          value: refersArray[1],
          display: refersArray[2],
        }
      );
    }
    this.listItems.splice(0, 1);
    console.log('data phuong ', this.listItems);

  }

  /**
   * add Language
   */
  addItems() {
    this.listItems.push(
      {
        title: '',
        property: '',
        inputtype: '',
        validate: '',
        class: '',
        value: '',
        display: '',
      }
    );
  }

  /**
   * removeItem
   * @param index 
   */
  removeItem(index) {
    this.listItems.splice(index, 1);
  }


  /**
  * Get data project
  */
  getDataProjects() {
    const param = {};

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        if (data.length > 0) {
          this.projects = data;
        }
      }));
  }

  /**
   * Get data class
   */
  getDataClass() {
    const param = { idproject: this.idProject };

    //select data classes
    this.subscription.push(this.api.excuteAllByWhat(param, '207').subscribe((data) => {
      if (data.length > 0) {
        this.classes = data;

        // get all class by project
        this.getAllClassByProject();
      }
    }));
  }

  /**
   * getAllClassByProject
   */
  getAllClassByProject() {
    const param = { idproject: this.idProject };

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '207').subscribe((data) => {
        if (data.length > 0) {
          this.allClass = data;
        }
      }));
  }

  /**
   * load data properties
   */
  loadDataProperties() {
    const param = { id: this.idClass };
    this.dataClass = [];

    //select data classes
    this.subscription.push(this.api.excuteAllByWhat(param, '204').subscribe((data) => {
      if (data.length > 0) {

        // process propertites, attributes and lengths
        let properties = data[0].properties.split(',');
        let attributes = data[0].attributes.split(',');
        let lengths = data[0].lengths.split(',');

        properties.forEach((item, index) => {
          this.dataClass.push({
            'index': index,
            'property': properties[index],
            'attribute': attributes[index],
            'length': lengths[index],
          });
        });
      }
    }));
  }

  /**
   * on Project Selection Change
   */
  onProjectSelectionChange() {
    // get class by project
    this.getDataClass();
  }

  /**
   * on Class Selection Change
   */
  onClassSelectionChange() {
    this.loadDataProperties();
  }

  /**
   * on Property Selection Change
   * @param item 
   */
  onPropertySelectionChange(item) {
    item.title = item.property;
  }

  /**
   * getPropertiesByIdClass
   * @param id 
   */
  getPropertiesByIdClass(idClass) {
    // select data classes
    return this.allClass.filter(x => x.id == idClass)[0]?.properties.split(',');
  }

  /**
   * onBtnProcessClick
   */
  onBtnProcessClick() {
    // init data prepare for insert
    this.crud.idproject = this.idProject;
    this.crud.idclass = this.idClass;
    this.crud.createdate = this.api.formatDate(new Date());
    this.crud.actiontype = '2';

    this.crud.titles = '';
    this.crud.properties = '';
    this.crud.alias = '';
    this.crud.formats = '';
    this.crud.refers = '';

    this.crud.titlecreate = '';
    // this.crud.titleupdate = '';
    this.crud.inputtypes = '';
    this.crud.validates = '';

    // update value titles, properties, alias, formats, refers
    this.listItems.forEach((element, index) => {
      if (index == 0) {
        this.crud.titles += element.title;
        this.crud.properties += element.property;
        this.crud.inputtypes += element.inputtype;
        this.crud.validates += element.validate.length > 0 ? element.validate.join(':') : '';
        this.crud.refers += element.class + ':' + element.value + ':' + element.display;
      } else {
        this.crud.titles += ',' + element.title;
        this.crud.properties += ',' + element.property;
        this.crud.inputtypes += ',' + element.inputtype;
        this.crud.validates += ',' + (element.validate.length > 0 ? element.validate.join(':') : '');
        this.crud.refers += ',' + element.class + ':' + element.value + ':' + element.display;
      }
    });

    //if type = 0 => Insert data else update data
    this.subscription.push(this.api.excuteAllByWhat(this.crud, '' + Number(301 + this.type) + '')
      .subscribe((data) => {
        this.dialogRef.close({ status: true });
        this.api.showSuccess('Xử Lý Thành Công ');
      })
    );
  }

  /**
   * onBtnCancelClick
   */
  onBtnCancelClick(): void {
    this.dialogRef.close({ status: false });
  }

}
