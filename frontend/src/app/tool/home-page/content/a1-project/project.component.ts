import { Project } from './../../../common/models/100project.models';
import { Component, OnInit, ViewChild, Inject, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ApiService } from 'src/app/tool/common/api-service/api.service';

import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { A1GenerationDialogComponent } from './a1-generation/a1-generation-dialog.component';

export interface DialogData { }

export interface PeriodicElement {
  id: number;
  projectName: string;
  createDate: string;
}

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
})
export class ProjectComponent implements OnInit, OnDestroy {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  /** for table */
  displayedColumns: string[] = ['id', 'projectName', 'createDate', 'action'];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1
      }`;
  }
  // end for table;

  // list project
  listProject: any[];
  projects: any;
  projectId: string = '0';

  //subscription
  subscription: Subscription[] = [];

  /**
   * constructor
   * @param api
   * @param dialog
   * @param _snackBar
   */
  constructor(
    private api: ApiService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.getListProjectSearch();

    this.getListProject();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * Get List Project to Search
   */
  getListProjectSearch() {
    const param = {};

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        if (data.length > 0) {
          let temp = [
            {
              id: '0',
              name: 'Tất cả',
            },
          ];
          data.forEach((element) => {
            temp.push(element);
          });
          this.projects = temp;
        }
      })
    );
  }

  /**
   * getListProject
   */
  getListProject() {
    const param = { id: this.projectId };

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '107').subscribe((data) => {
        if (data) {
          // set data for table
          this.listProject = data;
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.dataSource = new MatTableDataSource([]);
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);
      })
    );
  }

  /**
   * 
   * @param row 
   */
  onBtnGenFlutterClick(row) {
    console.log(row);

    // call core generation code
    const idproject = row.id;
    const param = { idproject: idproject };
    this.api
      .excuteAllGenCode(param, this.api.BACKEND_CORE_URL + '/api/genflutter')
      .subscribe((data) => {
        window.open(this.api.BACKEND_CORE_URL + data.result, '_blank');
      });
  }

  /**
   * open Dialog Update
   * @param element
   */
  openDialogAdd(): void {
    const dialogRef = this.dialog.open(ProjectDialog, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 0, id: 0 },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getListProject();
      }
    });
  }

  /**
   * openDialogUpdate
   * @param element
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ProjectDialog, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',
      // data: element
      data: { type: 1, input: element },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getListProject();
      }
    });
  }

  /**
   * openSnackBar confirm delete project
   */
  openSnackBar(element) {
    let snackBarRef = this._snackBar.open(
      'Do you want delete this project?',
      'YES',
      {
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      }
    );

    snackBarRef.onAction().subscribe(() => {
      // delete project
      this.subscription.push(
        this.api.excuteAllByWhat(element, '103').subscribe((data) => {
          // load data grid
          this.getListProject();

          //scroll top
          window.scroll({ left: 0, top: 0, behavior: 'smooth' });

          // show toast success
          this.api.showSuccess('Xóa thành công');
        })
      );
    });
  }

  /**
   * openGenerationDialog
   * @param element
   */
  openGenerationDialog(element): void {
    const dialogRef = this.dialog.open(A1GenerationDialogComponent, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 1, input: element },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getListProject();
      }
    });
  }

  /**
   * on Btn CreateCRUD click
   */
  onBtnCreateCRUDClick(row) {
    let projectId = row.id;
    let projectName = row.name;

    // get all table of project
    this.subscription.push(
      this.api
        .excuteAllByWhat({ idproject: projectId }, '207')
        .subscribe((dataClass) => {
          console.log(dataClass);
          if (dataClass.length > 0) {
            // loop in table and create crud
            dataClass.forEach((itemClass) => {
              // check list of table exists
              this.subscription.push(
                this.api
                  .excuteAllByWhat(
                    { actiontype: 0, idclass: itemClass.id },
                    '310'
                  )
                  .subscribe((count0) => {
                    if (count0[0].ans == 0) {
                      let refer = this.createReferList(
                        itemClass.properties,
                        dataClass,
                        itemClass.classname
                      );

                      // error
                      if (refer == null) {
                        return;
                      }

                      // create list
                      let param = {
                        createdate: this.api.formatDate(new Date()),
                        idproject: projectId,
                        idclass: itemClass.id,
                        actiontype: '0',
                        titles: itemClass.properties,
                        properties: itemClass.properties,
                        alias: itemClass.properties,
                        // create refer list
                        refers: refer,
                        titlecreate: '',
                        titleupdate: '',
                        inputtypes: '',
                        validates: '',
                        formats: this.createCopet(itemClass.properties),
                      };

                      // create list
                      this.subscription.push(
                        this.api
                          .excuteAllByWhat(param, '301')
                          .subscribe((data) => { })
                      );
                    }
                  })
              );

              // check insert of table exists
              this.subscription.push(
                this.api
                  .excuteAllByWhat(
                    { actiontype: 1, idclass: itemClass.id },
                    '310'
                  )
                  .subscribe((count0) => {
                    console.log('count0', count0[0].ans);
                    if (count0[0].ans == 0) {
                      let refer = this.createReferInsert(
                        itemClass.properties,
                        dataClass,
                        itemClass.classname
                      ); 

                      // error
                      if (refer == null) {
                        return;
                      }

                      // create list
                      let param = {
                        createdate: this.api.formatDate(new Date()),
                        idproject: projectId,
                        idclass: itemClass.id,
                        actiontype: '1',
                        titles: itemClass.properties.substr(3),
                        properties: itemClass.properties.substr(3),
                        alias: '',
                        // create refer list
                        refers: refer,
                        titlecreate:
                          'INFOMATION ' + itemClass.classname.toUpperCase(),
                        titleupdate:
                          'INFOMATION ' + itemClass.classname.toUpperCase(),
                        inputtypes: this.createInputType(
                          itemClass.properties,
                          itemClass.attributes
                        ),
                        validates: this.createRequire(itemClass.properties),
                        formats: '',
                      };

                      // create insert
                      this.subscription.push(
                        this.api
                          .excuteAllByWhat(param, '301')
                          .subscribe((data) => { })
                      );
                    }
                  })
              );

              // check delete of table exists
              this.subscription.push(
                this.api
                  .excuteAllByWhat(
                    { actiontype: 3, idclass: itemClass.id },
                    '310'
                  )
                  .subscribe((count0) => {
                    console.log('count0', count0[0].ans);
                    if (count0[0].ans == 0) {
                      // create list
                      let param = {
                        createdate: this.api.formatDate(new Date()),
                        idproject: projectId,
                        idclass: itemClass.id,
                        actiontype: '3',
                        titles: '',
                        properties: '',
                        alias: '',
                        refers: '',
                        titlecreate: '',
                        titleupdate: '',
                        inputtypes: '',
                        validates: '',
                        formats: '',
                      };

                      // create delete
                      this.subscription.push(
                        this.api
                          .excuteAllByWhat(param, '301')
                          .subscribe((data) => { })
                      );
                    }
                  })
              );
            });

            this.api.showSuccess('Create CRUD basic Success!');
          }
        })
    );
  }

  /**
   * create refer for list
   * id,Name => ::,::
   * id,IdLop,Fullname,Address,Born => ::,103:id:Name,::,::,::
   * table = HocVien
   * @param properties
   */
  private createReferList(properties, dataClass, table) {
    if (properties == 'id,IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient') {
      console.log('phuong1', dataClass);
    }
    let result = '::';
    let arrProperties = properties.split(',');
    for (let i = 1; i < arrProperties.length; i++) {
      // check have IdXxxx
      const searchId = arrProperties[i].search('Id');
      const searchid = arrProperties[i].search('id');
      if ((searchId >= 0 && searchId < 3) || (searchid >= 0 && searchid < 3)) {
        let classId = '';
        let value = '';
        let display = '';
        let className = arrProperties[i].substr(2);

        if (properties == 'id,IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient') {
          console.log('phuong1 className', className);
        }

        let flagExistsClass = false;

        // find className in list
        dataClass.forEach((item) => {
          if (item.classname == className) {
            flagExistsClass = true;
            classId = item.id;
            value = item.properties.split(',')[0];
            display = item.properties.split(',')[1];

            result += ',' + classId + ':' + value + ':' + display;
          }
        });

        if (!flagExistsClass) {
          result += ',::';
          alert('Table ' + table + '  câu lệnh [LIST] đang liên kết đến table [' + className + '] không tồn tại');
          return null;
        }

      } else {
        result += ',::';
      }
    }

    if (properties == 'id,IdCompany,IdShift,Title,OnHolidayStart,OnHolidayEnd,WorkingWageCoeficient') {
      console.log('phuong1 result', result);
    }

    return result;
  }

  /**
   * create copet for list
   * id,Name => ,
   * @param properties
   */
  private createCopet(properties) {
    let result = '';
    let arrProperties = properties.split(',');
    for (let i = 1; i < arrProperties.length; i++) {
      result += ',';
    }

    return result;
  }

  /**
   * create refer for insert
   * id,Name => ::
   * id,IdLop,Fullname,Address,Born => 103:id:Name,::,::,::
   * table = HocVien
   * @param properties
   */
  private createReferInsert(properties, dataClass, table) {
    let result = '';
    let arrProperties = properties.split(',');
    for (let i = 1; i < arrProperties.length; i++) {
      // check have IdXxxx
      const searchId = arrProperties[i].search('Id');
      const searchid = arrProperties[i].search('id');
      if ((searchId >= 0 && searchId < 3) || (searchid >= 0 && searchid < 3)) {
        let classId = '';
        let value = '';
        let display = '';
        let className = arrProperties[i].substr(2);
 
        let flagExistsClass = false; 

        // find className in list
        dataClass.forEach((item) => { 
          if (item.classname == className) {
            flagExistsClass = true;
            classId = item.id;
            value = item.properties.split(',')[0];
            display = item.properties.split(',')[1];

            result += ',' + classId + ':' + value + ':' + display;
          }
        });

        // return error and exit
        if (!flagExistsClass) {
          result += ',::';
          alert('Table ' + table + '  câu lệnh [UPDATE] đang liên kết đến table [' + className + '] không tồn tại');
          return null;
        }

      } else {
        result += ',::';
      }
    }

    // ,:: => ::
    result = result.substr(1);

    return result;
  }

  /**
   * create required for insert
   * id,Name => required
   * id,IdLop,Fullname,Address,Born => required,,required,required
   * @param properties
   */
  private createRequire(properties) {
    let result = '';
    let arrProperties = properties.split(',');
    for (let i = 1; i < arrProperties.length; i++) {
      result += ',required';
    }

    // ,required => required
    result = result.substr(1);

    return result;
  }

  /**
   * create input type for insert
   * id,Name => required
   * id,IdLop,Fullname,Address,Born => required,,required,required
   * @param properties
   * @param dataClass
   */
  private createInputType(properties, attributes) {
    let result = '';
    let arrProperties = properties.split(',');
    let arrAttributes = attributes.split(',');
    for (let i = 1; i < arrProperties.length; i++) {
      // check have IdXxxx
      const searchId = arrProperties[i].search('Id');
      const searchid = arrProperties[i].search('id');

      // select
      if ((searchId >= 0 && searchId < 3) || (searchid >= 0 && searchid < 3)) {
        result += ',select';
      } else {
        // varchar
        if (arrAttributes[i] == 'VARCHAR') {
          result += ',varchar';
        } else {
          // int
          if (arrAttributes[i] == 'INT') {
            result += ',number';
          } else {
            // datetime or date
            if (arrAttributes[i] == 'DATETIME' || arrAttributes[i] == 'DATE') {
              result += ',date';
            } else {
              // ckeditor
              if (arrAttributes[i] == 'TEXT') {
                result += ',ckeditor';
              } else {
                // number
                if (arrAttributes[i] == 'TEXT') {
                  result += ',ckeditor';
                } else {
                  // else all => varchar
                  result += ',varchar';
                }
              }
            }
          }
        }
      }
    }

    // ,required => required
    result = result.substr(1);

    return result;
  }
}

@Component({
  selector: 'app-project-dialog',
  templateUrl: './project-dialog.component.html',
  styleUrls: ['./project.component.scss'],
})
export class ProjectDialog implements OnInit, OnDestroy {
  //subscription
  subscription: Subscription[] = [];
  project: Project = {
    name: '',
    createdate: this.api.formatDate(new Date()),
  };

  //type
  type: number;
  form: FormGroup;

  /**
   * constructor
   * @param dialogRef
   * @param api
   * @param formBuilder
   */
  constructor(
    public dialogRef: MatDialogRef<ProjectDialog>,
    private api: ApiService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.type = data.type;

    // if update
    if (this.type == 1) {
      this.project = data.input;
    }

    // add validate for controls
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(100),
        ],
      ],
      createdate: [null],
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    //scroll top
    window.scroll({ left: 0, top: 0, behavior: 'smooth' });
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Button click
   */
  onBtnClick(): void {
    //check validation
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    } else {
      //if type = 0 => Insert data else update data
      this.subscription.push(
        this.api
          .excuteAllByWhat(this.project, '' + Number(101 + this.type) + '')
          .subscribe((data) => {
            this.dialogRef.close(true);
            this.api.showSuccess('Xử Lý Thành Công ');
          })
      );
    }
  }

  /**
   * on Button No Click
   */
  onBtnNoClick(): void {
    this.dialogRef.close();
  }
}
