import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from '@angular/material/dialog';
import { Subscription } from 'rxjs/internal/Subscription';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { ApiService } from 'src/app/tool/common/api-service/api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-a1-generation-dialog',
  templateUrl: './a1-generation-dialog.component.html',
  styleUrls: ['./a1-generation-dialog.component.scss'],
})
export class A1GenerationDialogComponent implements OnInit {
  // subscription
  subscription: Subscription[] = [];

  // list class
  databases: any[] = [
    { id: '0', value: 'My Sql' },
    { id: '1', value: 'SQL Server' },
    { id: '2', value: 'Oracle' },
    { id: '3', value: 'Postgres' },
    { id: '4', value: 'MongoDB' },
    { id: '5', value: 'Firebase' },
  ];

  backends: any[] = [
    { id: '0', value: 'PHP' },
    { id: '1', value: 'Java Spring' },
    { id: '2', value: 'Python Django' },
    { id: '3', value: 'Python Flask' },
    { id: '4', value: '.Net MVC' },
    { id: '5', value: '.Net Core' },
    { id: '6', value: 'NodeJS' },
    { id: '7', value: 'R' },
    { id: '8', value: 'Ruby On Rail' },
    { id: '9', value: 'Golang' },
  ];

  frontends: any[] = [
    { id: '0', value: 'Angular Framework' },
    { id: '1', value: 'React JS' },
    { id: '2', value: 'Vue JS' },
  ];

  devops: any[] = [{ id: '0', value: 'Docker' }];

  modelDatabase: string = '0';
  modelBackend: string = '0';
  modelFrontEnd: string = '0';
  modelDevops: string = '0';

  type: number = 0;
  input: any;

  isProcessing: boolean = false;

  color: ThemePalette = 'primary';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 50;
  spinnerInterval: any;

  /**
   * constructor
   * @param dialogRef
   * @param data
   * @param dialog
   */
  constructor(
    public dialogRef: MatDialogRef<A1GenerationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private api: ApiService,
    private spinner: NgxSpinnerService
  ) {
    console.log('data nhan duoc', data);
    this.type = data.type;
    this.input = data.input;
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void { }

  /**
   * onBtnProcessClick
   */
  onBtnProcessClick() {
    this.spinner.show();
    this.startSpinner();
    // call core generation code
    const idproject = this.input.id;
    const param = { idproject: idproject, backend: this.modelBackend, datatabase: this.modelDatabase };
    this.api
      .excuteAllGenCode(param, this.api.BACKEND_CORE_URL + '/api/gencode')
      .subscribe((data) => {
        window.open(this.api.BACKEND_CORE_URL + data.result, '_blank');
        this.stopSpnner();
        this.spinner.hide();
      });
  }

  /**
   * onBtnPreviewClick
   */
  onBtnPreviewClick() {
    this.spinner.show();
    this.startSpinner();
    // call core generation code
    const idproject = this.input.id;
    const param = { idproject: idproject };
    this.api
      .excuteAllGenCode(param, this.api.BACKEND_CORE_URL + '/api/copy')
      .subscribe((data) => {
        // window.open(data.result, '_blank');
        this.api.showSuccess('Copy code Success');
        this.stopSpnner();
      });
  }

  private startSpinner() {
    this.isProcessing = true;
    const arrColor = ['primary', 'accent', 'warn'];
    let ans = 0;
    this.spinnerInterval = setInterval(() => {
      this.color = arrColor[ans % 3] as ThemePalette;
      ans += 1;
    }, 300);
  }

  private stopSpnner() {
    this.isProcessing = false;
    clearInterval(this.spinnerInterval);
  }

  /**
   * cancel click
   */
  onBtnCancelClick(): void {
    this.dialogRef.close({ status: false });
  }
}
