import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassComponent, ClassDialog } from './class.component';
import { RouterModule } from '@angular/router';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ClassComponent, ClassDialog],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: ClassComponent, children: [
        ],
      }
    ]),
    
    TransferHttpCacheModule,
    MatCardModule,
    MatPaginatorModule,  
    MatSelectModule,    
    MatSortModule,
    MatTableModule,   
    MatInputModule,
    MatButtonModule, 
    MatDialogModule,
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ClassModule { }
