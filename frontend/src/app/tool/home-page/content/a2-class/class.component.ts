import { Component, OnInit, ViewChild, Inject, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs'; 
import { ApiService } from 'src/app/tool/common/api-service/api.service';

import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import {
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms'; 
import { Project } from 'src/app/tool/common/models/100project.models';
import { Class } from 'src/app/tool/common/models/200class.models';

export interface DialogData {
}

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.scss']
})
export class ClassComponent implements OnInit, OnDestroy {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  /** for table */
  displayedColumns: string[] = ['id', 'className', 'properties', 'attributes','lengths', 'action'];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
      }`;
  }
  // end for table;

  //subscription
  subscription: Subscription[] = [];

  // list class
  listClass: any[];
  class: Class;
  projects: Project;
  projectId: string = '1';

  /**
   * constructor
   * @param dialog 
   * @param _snackBar
   * @param api 
   */
  constructor(public dialog: MatDialog, private _snackBar: MatSnackBar, private api: ApiService,) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.getListProject();
  }

  /**
  * ngOnDestroy
  */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * Get List Project
   */
  getListProject() {
    const param = {};
    // select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        if (data.length > 0) {
          this.projects = data;
          this.projectId = this.projects[0].id;

          this.getListClass();
        }
      }));
  }


  /**
   * get List Class
   */
  getListClass() {
    console.log('getListClass');

    const param = { idproject: this.projectId };

    //select data class
    this.subscription.push(
      this.api.excuteAllByWhat(param, '207').subscribe((data) => {
        if (data.length > 0) {
          // set data for table
          this.listClass = data;
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.dataSource = new MatTableDataSource([]);
        }

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);
      })
    );
  }


  /**
   * open Dialog class
   * @param element 
   */
  openDialogClass(): void {
    const dialogRef = this.dialog.open(ClassDialog, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 0, idproject: this.projectId }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getListClass();
      }
    });
  }

  /**
   * open Dialog Update
   * @param element 
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ClassDialog, {
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      maxHeight: '100%',

      // data: element
      data: { type: 1, input: element }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getListClass();
      }
    });
  }

  /**
   * openSnackBar confirm delete project
   */
  openSnackBar(element) {
    let snackBarRef = this._snackBar.open('Do you want delete this class?', 'YES', {
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });

    snackBarRef.onAction().subscribe(() => {
      // delete project
      this.subscription.push(
        this.api.excuteAllByWhat(element, '203').subscribe((data) => {
          // load data grid
          this.getListClass();

          //scroll top
          window.scroll({ left: 0, top: 0, behavior: 'smooth' });

          // show toast success
          this.api.showSuccess('Xóa thành công ');
        })
      );
    });
  }
}

@Component({
  selector: 'app-class-dialog',
  templateUrl: './class-dialog.component.html',
  styleUrls: ['./class.component.scss']
})
export class ClassDialog implements OnInit, OnDestroy {

  // subscription
  subscription: Subscription[] = [];

  // class
  class: Class = {};

  // project
  projects: Project;

  // type
  type: number;
  form: FormGroup;

  /**
   * constructor
   * @param dialogRef 
   * @param api 
   * @param formBuilder 
   */
  constructor(
    public dialogRef: MatDialogRef<ClassDialog>,
    private api: ApiService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    //add
    this.type = data.type;

    // if update
    if (this.type == 1) {
      this.class = data.input;
    } else {
      this.class.idproject = data.idproject;
    }

    // add validate for controls
    this.form = this.formBuilder.group({
      classname: [
        null,
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(100),
        ],
      ],
      properties: [null],
      attributes: [null],
      lengths: [null],
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    //scroll top
    window.scroll({ left: 0, top: 0, behavior: 'smooth' });

    this.getListProject();

    if (this.type == 1) {
      this.processingListPropAndAttr();
    }
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  //attributes
  attributes: any = [
    { id: '0', value: 'INT' },
    { id: '1', value: 'VARCHAR' },
    { id: '2', value: 'TEXT' },
    { id: '3', value: 'DATE' },
    { id: '4', value: 'FLOAT' },
    { id: '5', value: 'DOUBLE' },
    { id: '6', value: 'DATETIME' },
    { id: '7', value: 'BOOLEAN' },
    { id: '8', value: 'BINARY' }
  ]

  //data list Properties And Attributes
  listPropAndAttr: any[] = [
    {
      property: 'id',
      attribute: 'INT',
      length: '0',
    },
  ];

  /**
   * Get List Project
   */
  getListProject() {
    const param = {};

    //select data project
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        if (data.length > 0) {
          this.projects = data;
        }
      }));
  }

  /**
   * on Button process click
   */
  onBtnProcessClick(): void {
    this.class.properties = '';
    this.class.attributes = '';
    this.class.lengths = '';

    // merger attributes for insert to database
    this.listPropAndAttr.forEach(item => {
      if (this.class.properties == '' || this.class.properties == null) {
        this.class.properties += item.property;
        this.class.attributes += item.attribute;
        this.class.lengths += item.length;
      }
      else {
        this.class.properties += ',' + item.property;
        this.class.attributes += ',' + item.attribute;
        this.class.lengths += ',' + item.length;
      }
    });

    //check validation
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    } else {
      this.subscription.push(this.api.excuteAllByWhat(this.class, '' + Number(201 + this.type) + '')
        .subscribe((data) => {
          this.dialogRef.close(true);
          this.api.showSuccess('Xử Lý Thành Công ');
        }));
    }
  }

  /**
   * add Language
   */
  addPropAndAttr() {
    this.listPropAndAttr.push(
      {
        property: '',
        attribute: '',
        length: '0',
      }
    );
  }

  /**
   * removePropAndAttr
   * @param index 
   */
  removePropAndAttr(index) {
    this.listPropAndAttr.splice(index, 1);
  }

  /**
   * processing List Language
   */
  processingListPropAndAttr() {
    let propertiesAr = this.class.properties.split(',');
    let attributesAr = this.class.attributes.split(',');
    let lengthsAr = this.class.lengths.split(',');
    for (var i = 0; i < propertiesAr.length; i++) {
      this.listPropAndAttr.push(
        {
          property: propertiesAr[i],
          attribute: attributesAr[i],
          length: lengthsAr[i],
        }
      );
    }
    this.listPropAndAttr.splice(0, 1);
  }

  /**
   * onNoClick
   */
  onBtnCancelClick(): void {
    this.dialogRef.close();
  }
}
