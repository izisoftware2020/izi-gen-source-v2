import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { LoginCookie } from '../../common/core/login-cookie';

// import $ from 'jquery';
import * as $ from 'jquery';
import { ApiService } from '../../common/api-service/api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, AfterViewInit, OnDestroy {
  // flag for menu
  navLeft: boolean = false;
  navLeftMobile: boolean = false;
  settingFlag: boolean = false;

  numberProject: number = 0;
  numberClass: number = 0;

  /**
   * constructor
   * @param login
   */
  constructor(
    private login: LoginCookie,
    private api: ApiService,
    private spinner: NgxSpinnerService
  ) {}

  /**
   * ngOnInit
   */
  ngOnInit() {
    // get info project and table
    this.loadDataProjectAndClass();
  }

  /**
   * loadDataProjectAndClass
   */
  loadDataProjectAndClass() {
    // count number projects
    this.api.excuteAllByWhat({}, '106').subscribe((data) => {
      this.numberProject = data[0].count;
    });

    // count number class
    this.api.excuteAllByWhat({}, '206').subscribe((data) => {
      this.numberClass = data[0].count;
    });
  }

  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() {
    // process click of menu response
    $(document).ready(function ($) {
      'use strict';
      //Open submenu on hover in compact sidebar mode and horizontal menu mode
      $(document).on(
        'mouseenter mouseleave',
        '.sidebar .nav-item',
        function (ev) {
          var body = $('#body');
          var sidebarIconOnly = body.hasClass('sidebar-icon-only');
          var horizontalMenu = body.hasClass('horizontal-menu');
          var sidebarFixed = body.hasClass('sidebar-fixed');
          if (!('ontouchstart' in document.documentElement)) {
            if (sidebarIconOnly || horizontalMenu) {
              if (sidebarFixed) {
                if (ev.type === 'mouseenter') {
                  body.removeClass('sidebar-icon-only');
                }
              } else {
                var $menuItem = $(this);
                if (ev.type === 'mouseenter') {
                  $menuItem.addClass('hover-open');
                } else {
                  $menuItem.removeClass('hover-open');
                }
              }
            }
          }
        }
      );
    });
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }

  /**
   * onToggleButtonDesktopClick
   */
  onToggleButtonDesktopClick() {
    this.navLeft = !this.navLeft;
  }

  /**
   * onToggleButtonMobileClick
   */
  onToggleButtonMobileClick() {
    this.navLeftMobile = !this.navLeftMobile;
  }

  onSettingButtonClick() {
    this.settingFlag = !this.settingFlag;
  }

  /**
   * on Click Update Generation
   */
  onClickUpdateGeneration() {
    // show spiner
    this.spinner.show();

    // call core generation code
    const param = { idproject: '1' };
    this.api
      .excuteAllGenCode(param, 'http://localhost:8000/api/preview')
      .subscribe((data) => {
        // window.open(data.result, '_blank');
        this.api.showSuccess('Update code Success');
      });
  }
}
